static char const PKGVERSION[]="(tzcode) ";
static char const TZVERSION[]="2023d";
static char const REPORT_BUGS_TO[]="https://bitbucket.org/tnozaki/netbsd/issues";
