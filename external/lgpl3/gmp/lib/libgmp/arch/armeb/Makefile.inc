GMP_LIMB_BITS=32

M4FLAGS= -DPIC
COPTS+= -fPIC

.if ${MACHINE_ARCH} == "armeb" || !empty(MACHINE_ARCH:Mearmv4*)
M4FLAGS+=	-DNOTHUMB
.endif

.include "srcs.mk"
