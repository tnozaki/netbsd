/*	$NetBSD: rc_cmdlength.h,v 1.1.1.1.2.2 2015/11/07 22:26:34 snj Exp $	*/


extern size_t remoteconfig_cmdlength( const char *src_buf, const char *src_end );
