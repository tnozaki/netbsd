/*	$NetBSD: refidsmear.h,v 1.1.1.2.2.2 2015/11/07 22:26:34 snj Exp $	*/


extern l_fp	convertRefIDToLFP(uint32_t r);
extern uint32_t	convertLFPToRefID(l_fp num);
