/*	$NetBSD: unity_config.h,v 1.1.1.1.2.2 2015/11/07 22:26:45 snj Exp $	*/

/* unity_config.h */

#ifndef UNITY_CONFIG_H
#define UNITY_CONFIG_H

#define UNITY_INCLUDE_DOUBLE

#ifndef HAVE_STDINT_H
# define UNITY_EXCLUDE_STDINT_H
#endif

#endif /* UNITY_CONFIG_H */
