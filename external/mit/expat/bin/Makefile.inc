BINDIR?=	/usr/bin

CPPFLAGS+=	-I${EXPATSRCDIR}/lib -I${EXPATDIR}/lib/libexpat

LDADD+=	-L${EXPATOBJDIR.expat} -lexpat
DPADD+=	${EXPATLIB.expat}

.include "../Makefile.inc"
