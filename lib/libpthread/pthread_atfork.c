/*-
 * Copyright (c) 2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include "namespace.h"
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/sysctl.h>
#include <sys/lwpctl.h>

#include <sys/queue.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "pthread.h"
#include "pthread_int.h"
#include "reentrant.h"

pid_t	__fork(void);	/* XXX */

struct atfork_callback {
	TAILQ_ENTRY(atfork_callback) entry;
	void (*preparefn)(void);
	void (*parentfn)(void);
	void (*childfn)(void);
};

/*
 * Hypothetically, we could protect the queues with a rwlock which is
 * write-locked by pthread_atfork() and read-locked by fork(), but
 * since the intended use of the functions is obtaining locks to hold
 * across the fork, forking is going to be serialized anyway.
 */
static pthread_mutex_t atfork_lock = PTHREAD_MUTEX_INITIALIZER;

TAILQ_HEAD(atfork_callback_q, atfork_callback);
static struct atfork_callback_q atforkq = TAILQ_HEAD_INITIALIZER(atforkq);

int
pthread_atfork(void (*prepare)(void), void (*parent)(void),
    void (*child)(void))
{
	struct atfork_callback *newatfork;

	if (prepare != NULL || parent != NULL || child != NULL) {
		newatfork = malloc(sizeof(struct atfork_callback));
		if (newatfork == NULL)
			return ENOMEM;
		newatfork->preparefn = prepare;
		newatfork->parentfn  = parent;
		newatfork->childfn   = child;
		pthread_mutex_lock(&atfork_lock);
		TAILQ_INSERT_TAIL(&atforkq, newatfork, entry);
		pthread_mutex_unlock(&atfork_lock);
	}

	return 0;
}

/* XXX: FIXME */
extern int pthread__started;
extern pthread_t pthread__first;

pid_t
__libc_fork(void)
{
	struct atfork_callback *head, *tail, *iter;
	pid_t ret;
	struct __pthread_st *self;

	pthread_mutex_lock(&atfork_lock);
	head = TAILQ_FIRST(&atforkq);
	tail = TAILQ_LAST(&atforkq, atfork_callback_q);
	pthread_mutex_unlock(&atfork_lock);

	/*
	 * The order in which the functions are called is specified as
	 * LIFO for the prepare handler and FIFO for the others.
	 */
	if (tail != NULL) {
		for (iter = tail; /**/;
		     iter = TAILQ_PREV(iter, atfork_callback_q, entry)) {
			if (iter->preparefn)
				(*iter->preparefn)();
			if (iter == head)
				break;
		}
	}

	ret = __fork();

	if (ret != 0) {
		/*
		 * We are the parent. It doesn't matter here whether
		 * the fork call succeeded or failed.
		 */
		if (head != NULL) {
			for (iter = head; /**/;
			     iter = TAILQ_NEXT(iter, entry)) {
				if (iter->parentfn)
					(*iter->parentfn)();
				if (iter == tail)
					break;
			}
		}
	} else {
		/* lwpctl state is not copied across fork. */
		self = pthread__self();
		if (_lwp_ctl(LWPCTL_FEATURE_CURCPU, &self->pt_lwpctl))
			err(1, "_lwp_ctl");
		self->pt_lid = _lwp_self();

		/*
		 * Clean up data structures that a forked child process might
		 * trip over. Note that if threads have been created (causing
		 * this handler to be registered) the standards say that the
		 * child will trigger undefined behavior if it makes any
		 * pthread_* calls (or any other calls that aren't
		 * async-signal-safe), so we don't really have to clean up
		 * much. Anything that permits some pthread_* calls to work is
		 * merely being polite.
		 */
		pthread__started = 0;

		/* We are the child */
		if (head != NULL) {
			for (iter = head; /**/;
			     iter = TAILQ_NEXT(iter, entry)) {
				if (iter->childfn)
					(*iter->childfn)();
				if (iter == tail)
					break;
			}
		}
	}

	return ret;
}
