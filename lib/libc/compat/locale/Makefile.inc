# $NetBSD: Makefile.inc,v 1.1 2010/06/07 13:52:30 tnozaki Exp $

.PATH: ${COMPATDIR}/arch/${MACHINE_ARCH}/locale ${COMPATDIR}/locale

CPPFLAGS+=	-I${COMPATDIR}/../locale -D_COMPAT_BSDCTYPE
SRCS+=		compat__def_messages.c \
		compat__def_monetary.c \
		compat__def_numeric.c \
		compat__def_time.c \
		compat_setlocale1.c \
		compat_setlocale32.c
