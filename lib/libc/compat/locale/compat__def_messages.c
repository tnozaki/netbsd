/*
 * Written by J.T. Conklin <jtc@NetBSD.org>.
 * Public domain.
 */

#include <sys/cdefs.h>

#include "localedef.h"

const _MessagesLocale *_CurrentMessagesLocale = &_DefaultMessagesLocale;
