/*	$NetBSD: _def_messages.c,v 1.6 2005/06/12 05:21:27 lukem Exp $	*/

/*
 * Written by J.T. Conklin <jtc@NetBSD.org>.
 * Public domain.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: _def_messages.c,v 1.6 2005/06/12 05:21:27 lukem Exp $");
#endif /* LIBC_SCCS and not lint */

#include <locale.h>
#include "localedef.h"

const _MessagesLocale _DefaultMessagesLocale = 
{
	_DEFAULT_YESEXPR,
	_DEFAULT_NOEXPR,
	_DEFAULT_YESSTR,
	_DEFAULT_NOSTR
};
