/* $NetBSD: global_locale.c,v 1.12.2.1 2013/09/07 16:11:41 bouyer Exp $ */

/*-
 * Copyright (c)2008 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: global_locale.c,v 1.12.2.1 2013/09/07 16:11:41 bouyer Exp $");
#endif /* LIBC_SCCS and not lint */

#include <sys/types.h>
#include <sys/ctype_bits.h>
#include <langinfo.h>
#include <limits.h>
#define __SETLOCALE_SOURCE__
#include <locale.h>
#include <stdlib.h>

#include "localedef.h"
#include "runetype_local.h"
#include "setlocale_local.h"

static struct lconv _global_ldata = {
	.decimal_point		= __UNCONST(_DEFAULT_RADIXCHAR),
	.thousands_sep		= __UNCONST(_DEFAULT_THOUSEP),
	.grouping		= __UNCONST(""),
	.int_curr_symbol	= __UNCONST(""),
	.currency_symbol	= __UNCONST(""),
	.mon_decimal_point	= __UNCONST(""),
	.mon_thousands_sep	= __UNCONST(""),
	.mon_grouping		= __UNCONST(""),
	.positive_sign		= __UNCONST(""),
	.negative_sign		= __UNCONST(""),
	.int_frac_digits	= CHAR_MAX,
	.frac_digits		= CHAR_MAX,
	.p_cs_precedes		= CHAR_MAX,
	.p_sep_by_space		= CHAR_MAX,
	.n_cs_precedes		= CHAR_MAX,
	.n_sep_by_space		= CHAR_MAX,
	.p_sign_posn		= CHAR_MAX,
	.n_sign_posn		= CHAR_MAX,
	.int_p_cs_precedes	= CHAR_MAX,
	.int_n_cs_precedes	= CHAR_MAX,
	.int_p_sep_by_space	= CHAR_MAX,
	.int_n_sep_by_space	= CHAR_MAX,
	.int_p_sign_posn	= CHAR_MAX,
	.int_n_sign_posn	= CHAR_MAX,
};

static const char *_global_items[(size_t)_DATE_FMT + 1] = {
	[(size_t)D_T_FMT    ] = _DEFAULT_D_T_FMT,
	[(size_t)D_FMT      ] = _DEFAULT_D_FMT,
	[(size_t)T_FMT      ] = _DEFAULT_T_FMT,
	[(size_t)T_FMT_AMPM ] = _DEFAULT_T_FMT_AMPM,
	[(size_t)AM_STR     ] = _DEFAULT_AM_STR,
	[(size_t)PM_STR     ] = _DEFAULT_PM_STR,
	[(size_t)DAY_1      ] = _DEFAULT_DAY_1,
	[(size_t)DAY_2      ] = _DEFAULT_DAY_2,
	[(size_t)DAY_3      ] = _DEFAULT_DAY_3,
	[(size_t)DAY_4      ] = _DEFAULT_DAY_4,
	[(size_t)DAY_5      ] = _DEFAULT_DAY_5,
	[(size_t)DAY_6      ] = _DEFAULT_DAY_6,
	[(size_t)DAY_7      ] = _DEFAULT_DAY_7,
	[(size_t)ABDAY_1    ] = _DEFAULT_ABDAY_1,
	[(size_t)ABDAY_2    ] = _DEFAULT_ABDAY_2,
	[(size_t)ABDAY_3    ] = _DEFAULT_ABDAY_3,
	[(size_t)ABDAY_4    ] = _DEFAULT_ABDAY_4,
	[(size_t)ABDAY_5    ] = _DEFAULT_ABDAY_5,
	[(size_t)ABDAY_6    ] = _DEFAULT_ABDAY_6,
	[(size_t)ABDAY_7    ] = _DEFAULT_ABDAY_7,
	[(size_t)MON_1      ] = _DEFAULT_MON_1,
	[(size_t)MON_2      ] = _DEFAULT_MON_2,
	[(size_t)MON_3      ] = _DEFAULT_MON_3,
	[(size_t)MON_4      ] = _DEFAULT_MON_4,
	[(size_t)MON_5      ] = _DEFAULT_MON_5,
	[(size_t)MON_6      ] = _DEFAULT_MON_6,
	[(size_t)MON_7      ] = _DEFAULT_MON_7,
	[(size_t)MON_8      ] = _DEFAULT_MON_8,
	[(size_t)MON_9      ] = _DEFAULT_MON_9,
	[(size_t)MON_10     ] = _DEFAULT_MON_10,
	[(size_t)MON_11     ] = _DEFAULT_MON_11,
	[(size_t)MON_12     ] = _DEFAULT_MON_12,
	[(size_t)ABMON_1    ] = _DEFAULT_ABMON_1,
	[(size_t)ABMON_2    ] = _DEFAULT_ABMON_2,
	[(size_t)ABMON_3    ] = _DEFAULT_ABMON_3,
	[(size_t)ABMON_4    ] = _DEFAULT_ABMON_4,
	[(size_t)ABMON_5    ] = _DEFAULT_ABMON_5,
	[(size_t)ABMON_6    ] = _DEFAULT_ABMON_6,
	[(size_t)ABMON_7    ] = _DEFAULT_ABMON_7,
	[(size_t)ABMON_8    ] = _DEFAULT_ABMON_8,
	[(size_t)ABMON_9    ] = _DEFAULT_ABMON_9,
	[(size_t)ABMON_10   ] = _DEFAULT_ABMON_10,
	[(size_t)ABMON_11   ] = _DEFAULT_ABMON_11,
	[(size_t)ABMON_12   ] = _DEFAULT_ABMON_12,
	[(size_t)RADIXCHAR  ] = _DEFAULT_RADIXCHAR,
	[(size_t)THOUSEP    ] = _DEFAULT_THOUSEP,
	[(size_t)YESSTR     ] = _DEFAULT_YESSTR,
	[(size_t)YESEXPR    ] = _DEFAULT_YESEXPR,
	[(size_t)NOSTR      ] = _DEFAULT_NOSTR,
	[(size_t)NOEXPR     ] = _DEFAULT_NOEXPR,
	[(size_t)CRNCYSTR   ] = _DEFAULT_CRNCYSTR,
	[(size_t)CODESET    ] = _DEFAULT_CODESET,
	[(size_t)ERA        ] = _DEFAULT_ERA,
	[(size_t)ERA_D_FMT  ] = _DEFAULT_ERA_D_FMT,
	[(size_t)ERA_D_T_FMT] = _DEFAULT_ERA_D_T_FMT,
	[(size_t)ERA_T_FMT  ] = _DEFAULT_ERA_T_FMT,
	[(size_t)ALT_DIGITS ] = _DEFAULT_ALT_DIGITS,
	[(size_t)_DATE_FMT  ] = _DEFAULT__DATE_FMT,
};

static struct _locale_cache_t _global_cache = {
    .ctype_tab = (const unsigned short *)&_C_ctype_tab_[0],
    .tolower_tab = (const short *)&_C_tolower_tab_[0],
    .toupper_tab = (const short *)&_C_toupper_tab_[0],
    .mb_cur_max = (size_t)1,
    .ldata = &_global_ldata,
    .items = &_global_items[0],

#ifdef _COMPAT_BSDCTYPE
    .compat_bsdctype = (const unsigned char *)&_C_compat_bsdctype[0],
#endif
};

struct _locale_impl_t _global_locale = {
    .cache = &_global_cache,
    .query = { _C_LOCALE },
    .part_name = {
	[(size_t)LC_ALL     ] = _C_LOCALE,
	[(size_t)LC_COLLATE ] = _C_LOCALE,
	[(size_t)LC_CTYPE   ] = _C_LOCALE,
	[(size_t)LC_MONETARY] = _C_LOCALE,
	[(size_t)LC_NUMERIC ] = _C_LOCALE,
	[(size_t)LC_TIME    ] = _C_LOCALE,
	[(size_t)LC_MESSAGES] = _C_LOCALE,
    },
    .part_impl = {
	[(size_t)LC_ALL     ] = (_locale_part_t)NULL,
	[(size_t)LC_COLLATE ] = (_locale_part_t)NULL,
	[(size_t)LC_CTYPE   ] = (_locale_part_t)
	    __UNCONST(&_DefaultRuneLocale),
	[(size_t)LC_MONETARY] = (_locale_part_t)
	    __UNCONST(&_DefaultMonetaryLocale),
	[(size_t)LC_NUMERIC ] = (_locale_part_t)
	    __UNCONST(&_DefaultNumericLocale),
	[(size_t)LC_TIME    ] = (_locale_part_t)
	    __UNCONST(&_DefaultTimeLocale),
	[(size_t)LC_MESSAGES] = (_locale_part_t)
	    __UNCONST(&_DefaultMessagesLocale),
    },
};
