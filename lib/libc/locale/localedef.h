/*
 * Copyright (c) 1994 Winning Strategies, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *      This product includes software developed by Winning Strategies, Inc.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _LOCALEDEF_H_
#define _LOCALEDEF_H_

#define _DEFAULT_YESSTR		"yes"
#define _DEFAULT_YESEXPR	"^[Yy]"
#define _DEFAULT_NOSTR		"no"
#define _DEFAULT_NOEXPR		"^[Nn]"

typedef struct {
	const char *yesexpr;
	const char *noexpr;
	const char *yesstr;
	const char *nostr;
} _MessagesLocale;

extern const _MessagesLocale  _DefaultMessagesLocale;

#define _DEFAULT_CRNCYSTR	NULL

typedef struct {
	const char *int_curr_symbol;
	const char *currency_symbol;
	const char *mon_decimal_point;
	const char *mon_thousands_sep;
	const char *mon_grouping;
	const char *positive_sign;
	const char *negative_sign;
	char int_frac_digits;
	char frac_digits;
	char p_cs_precedes;
	char p_sep_by_space;
	char n_cs_precedes;
	char n_sep_by_space;
	char p_sign_posn;
	char n_sign_posn;
	char int_p_cs_precedes;
	char int_n_cs_precedes;
	char int_p_sep_by_space;
	char int_n_sep_by_space;
	char int_p_sign_posn;
	char int_n_sign_posn;
} _MonetaryLocale;

extern const _MonetaryLocale  _DefaultMonetaryLocale;

#define _DEFAULT_RADIXCHAR	"."
#define _DEFAULT_THOUSEP	""

typedef struct {
	const char *decimal_point;
	const char *thousands_sep;
	const char *grouping;
} _NumericLocale;

extern const _NumericLocale  _DefaultNumericLocale;

#define _DEFAULT_D_T_FMT	"%a %b %e %H:%M:%S %Y"
#define _DEFAULT_D_FMT		"%m/%d/%y"
#define _DEFAULT_T_FMT		"%H:%M:%S"
#define _DEFAULT_T_FMT_AMPM	"%I:%M:%S %p"
#define _DEFAULT_AM_STR		"AM"
#define _DEFAULT_PM_STR		"PM"
#define _DEFAULT_DAY_1		"Sunday"
#define _DEFAULT_DAY_2		"Monday"
#define _DEFAULT_DAY_3		"Tuesday"
#define _DEFAULT_DAY_4		"Wednesday"
#define _DEFAULT_DAY_5		"Thursday"
#define _DEFAULT_DAY_6		"Friday"
#define _DEFAULT_DAY_7		"Saturday"
#define _DEFAULT_ABDAY_1	"Sun"
#define _DEFAULT_ABDAY_2	"Mon"
#define _DEFAULT_ABDAY_3	"Tue"
#define _DEFAULT_ABDAY_4	"Wed"
#define _DEFAULT_ABDAY_5	"Thu"
#define _DEFAULT_ABDAY_6	"Fri"
#define _DEFAULT_ABDAY_7	"Sat"
#define _DEFAULT_MON_1		"January"
#define _DEFAULT_MON_2		"February"
#define _DEFAULT_MON_3		"March"
#define _DEFAULT_MON_4		"April"
#define _DEFAULT_MON_5		"May"
#define _DEFAULT_MON_6		"June"
#define _DEFAULT_MON_7		"July"
#define _DEFAULT_MON_8		"August"
#define _DEFAULT_MON_9		"September"
#define _DEFAULT_MON_10		"October"
#define _DEFAULT_MON_11		"November"
#define _DEFAULT_MON_12		"December"
#define _DEFAULT_ABMON_1	"Jan"
#define _DEFAULT_ABMON_2	"Feb"
#define _DEFAULT_ABMON_3	"Mar"
#define _DEFAULT_ABMON_4	"Apr"
#define _DEFAULT_ABMON_5	"May"
#define _DEFAULT_ABMON_6	"Jun"
#define _DEFAULT_ABMON_7	"Jul"
#define _DEFAULT_ABMON_8	"Aug"
#define _DEFAULT_ABMON_9	"Sep"
#define _DEFAULT_ABMON_10	"Oct"
#define _DEFAULT_ABMON_11	"Nov"
#define _DEFAULT_ABMON_12	"Dec"
#define _DEFAULT_ERA		NULL
#define _DEFAULT_ERA_D_FMT	NULL
#define _DEFAULT_ERA_D_T_FMT	NULL
#define _DEFAULT_ERA_T_FMT	NULL
#define _DEFAULT_ALT_DIGITS	NULL
#define _DEFAULT__DATE_FMT	"%a %b %e %H:%M:%S %Z %Y"

typedef struct {
	const char *abday[7];
	const char *day[7];
	const char *abmon[12];
	const char *mon[12];
	const char *am_pm[2];
	const char *d_t_fmt;
	const char *d_fmt;
	const char *t_fmt;
	const char *t_fmt_ampm;
	const char *era;
	const char *era_d_fmt;
	const char *era_d_t_fmt;
	const char *era_t_fmt;
	const char *alt_digits;
	const char *_date_fmt;
} _TimeLocale;

extern const _TimeLocale  _DefaultTimeLocale;

#endif /* !_LOCALEDEF_H_ */
