#	from: @(#)Makefile.inc	5.1 (Berkeley) 2/18/91
#	$NetBSD: Makefile.inc,v 1.60 2012/01/20 16:31:29 joerg Exp $

# locale sources
.PATH: ${ARCHDIR}/locale ${.CURDIR}/locale

SRCS+=	__mb_cur_max.c _def_messages.c _def_monetary.c _def_numeric.c \
	_def_time.c _wctrans.c _wctype.c current_locale.c \
	dummy_lc_collate.c fix_grouping.c generic_lc_all.c \
	global_locale.c iswctype_mb.c localeconv.c multibyte_c90.c \
	nl_langinfo.c rune.c runetable.c setlocale.c

SRCS+=	multibyte_amd1.c

SRCS+=	wcstol.c wcstoll.c wcstoimax.c wcstoul.c wcstoull.c wcstoumax.c \
	wcstod.c wcstof.c wcstold.c wcscoll.c wcsxfrm.c wcsftime.c

SRCS+=	multibyte_posix2008.c

CPPFLAGS+=			-I${.CURDIR}
CPPFLAGS.rune.c+=		-I${LIBCDIR}/citrus
CPPFLAGS.runetable.c+=		-I${LIBCDIR}/citrus
CPPFLAGS.multibyte_c90.c+=	-I${LIBCDIR}/citrus
CPPFLAGS.multibyte_amd1.c+=	-I${LIBCDIR}/citrus
CPPFLAGS.multibyte_posix2008.c+=-I${LIBCDIR}/citrus

CPPFLAGS.wcstol.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib
CPPFLAGS.wcstoll.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib
CPPFLAGS.wcstoimax.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib
CPPFLAGS.wcstoul.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib
CPPFLAGS.wcstoull.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib
CPPFLAGS.wcstoumax.c+=		-I${LIBCDIR}/../../common/lib/libc/stdlib

COPTS.wcsftime.c=	-Wno-format-nonliteral

MAN+=	iswalnum.3 iswctype.3 mblen.3 mbstowcs.3 mbtowc.3 nl_langinfo.3 \
	setlocale.3 towctrans.3 towlower.3 wcstombs.3 wctomb.3 wctrans.3 \
	wctype.3 wcwidth.3

MAN+=	btowc.3 mbrlen.3 mbrtowc.3 mbsinit.3 mbsrtowcs.3 wcrtomb.3 \
	wcsrtombs.3 wctob.3

MAN+=	wcstol.3 wcstod.3 wcscoll.3 wcsxfrm.3 wcsftime.3

MLINKS+=setlocale.3 localeconv.3

MLINKS+=iswalnum.3 iswalpha.3 iswalnum.3 iswblank.3 \
	iswalnum.3 iswcntrl.3 iswalnum.3 iswdigit.3 \
	iswalnum.3 iswgraph.3 iswalnum.3 iswlower.3 \
	iswalnum.3 iswprint.3 iswalnum.3 iswpunct.3 \
	iswalnum.3 iswspace.3 iswalnum.3 iswupper.3 \
	iswalnum.3 iswxdigit.3

MLINKS+=towlower.3 towupper.3

MLINKS+=wcstod.3 wcstof.3 wcstod.3 wcstold.3
MLINKS+=wcstol.3 wcstoll.3 wcstol.3 wcstoimax.3 \
	wcstol.3 wcstoul.3 wcstol.3 wcstoull.3 wcstol.3 wcstoumax.3
