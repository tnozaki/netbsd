/*-
 * Copyright (c)2010 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <langinfo.h>
#define __SETLOCALE_SOURCE__
#include <locale.h>
#include <stddef.h>
#include <string.h>
#include <wchar.h>

#include "setlocale_local.h"

#include "citrus_module.h"
#include "citrus_ctype.h"
#include "runetype_local.h"
#include "multibyte.h"

#define _RUNE_LOCALE() \
    ((_RuneLocale *)(*_current_locale())->part_impl[(size_t)LC_CTYPE])

#define _CITRUS_CTYPE() \
    (_RUNE_LOCALE()->rl_citrus_ctype)

size_t
mbsnrtowcs(wchar_t * __restrict dst, const char ** __restrict src,
    size_t slen, size_t dlen, mbstate_t * __restrict ps)
{
	size_t ret;
	int err0;

	_fixup_ps(_RUNE_LOCALE(), ps);
	err0 = _citrus_ctype_mbsnrtowcs(_ps_to_ctype(ps),
	    dst, src, slen, dlen, _ps_to_private(ps), &ret);
	if (err0)
		errno = err0;

	return ret;
}

size_t
wcsnrtombs(char * __restrict dst, const wchar_t ** __restrict src,
    size_t slen, size_t dlen, mbstate_t * __restrict ps)
{
	size_t ret;
	int err0;

	_fixup_ps(_RUNE_LOCALE(), ps);
	err0 = _citrus_ctype_wcsnrtombs(_ps_to_ctype(ps),
	    dst, src, slen, dlen, _ps_to_private(ps), &ret);
	if (err0)
		errno = err0;

	return ret;
}
