/*	$NetBSD: _def_time.c,v 1.10 2008/05/17 03:49:54 ginsbach Exp $	*/

/*
 * Written by J.T. Conklin <jtc@NetBSD.org>.
 * Public domain.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: _def_time.c,v 1.10 2008/05/17 03:49:54 ginsbach Exp $");
#endif /* LIBC_SCCS and not lint */

#include <locale.h>
#include "localedef.h"

const _TimeLocale _DefaultTimeLocale = 
{
	{
		_DEFAULT_ABDAY_1,
		_DEFAULT_ABDAY_2,
		_DEFAULT_ABDAY_3,
		_DEFAULT_ABDAY_4,
		_DEFAULT_ABDAY_5,
		_DEFAULT_ABDAY_6,
		_DEFAULT_ABDAY_7,
	},
	{
		_DEFAULT_DAY_1,
		_DEFAULT_DAY_2,
		_DEFAULT_DAY_3,
		_DEFAULT_DAY_4,
		_DEFAULT_DAY_5,
		_DEFAULT_DAY_6,
		_DEFAULT_DAY_7
	},
	{
		_DEFAULT_ABMON_1,
		_DEFAULT_ABMON_2,
		_DEFAULT_ABMON_3,
		_DEFAULT_ABMON_4,
		_DEFAULT_ABMON_5,
		_DEFAULT_ABMON_6,
		_DEFAULT_ABMON_7,
		_DEFAULT_ABMON_8,
		_DEFAULT_ABMON_9,
		_DEFAULT_ABMON_10,
		_DEFAULT_ABMON_11,
		_DEFAULT_ABMON_12
	},
	{
		_DEFAULT_MON_1,
		_DEFAULT_MON_2,
		_DEFAULT_MON_3,
		_DEFAULT_MON_4,
		_DEFAULT_MON_5,
		_DEFAULT_MON_6,
		_DEFAULT_MON_7,
		_DEFAULT_MON_8,
		_DEFAULT_MON_9,
		_DEFAULT_MON_10,
		_DEFAULT_MON_11,
		_DEFAULT_MON_12
	},
	{
		_DEFAULT_AM_STR,
		_DEFAULT_PM_STR
	},
	_DEFAULT_D_T_FMT,
	_DEFAULT_D_FMT,
	_DEFAULT_T_FMT,
	_DEFAULT_T_FMT_AMPM,
	_DEFAULT_ERA,
	_DEFAULT_ERA_D_FMT,
	_DEFAULT_ERA_D_T_FMT,
	_DEFAULT_ERA_T_FMT,
	_DEFAULT_ALT_DIGITS,
	_DEFAULT__DATE_FMT
};
