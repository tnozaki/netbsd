/*	$NetBSD: citrus_ctype_fallback.c,v 1.2 2003/06/27 14:52:25 yamt Exp $	*/

/*-
 * Copyright (c)2003 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: citrus_ctype_fallback.c,v 1.2 2003/06/27 14:52:25 yamt Exp $");
#endif /* LIBC_SCCS and not lint */

#include "namespace.h"

#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <wchar.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_ctype_fallback.h"

#include "runetype_local.h"
#include "multibyte.h"

/*
 * for ABI version >= 0x00000002
 */ 

int
_citrus_ctype_btowc_fallback(_citrus_ctype_rec_t * __restrict cc,
			     int c, wint_t * __restrict wcresult)
{
	char mb;
	char pspriv[_PRIVSIZE];
	wchar_t wc;
	size_t nr;
	int err;

	_DIAGASSERT(cc != NULL && cc->cc_closure != NULL);

	if (c == EOF) {
		*wcresult = WEOF;
		return 0;
	}

	memset(&pspriv, 0, sizeof(pspriv));
	mb = (char)(unsigned)c;
	err = _citrus_ctype_mbrtowc(cc, &wc, &mb, 1, (void *)&pspriv, &nr);
	if (!err && (nr == 0 || nr == 1))
		*wcresult = wc;
	else
		*wcresult = WEOF;

	return 0;
}

int
_citrus_ctype_wctob_fallback(_citrus_ctype_rec_t * __restrict cc,
			     wint_t wc, int * __restrict cresult)
{
	char pspriv[_PRIVSIZE];
	char buf[MB_LEN_MAX];
	size_t nr;
	int err;

	_DIAGASSERT(cc != NULL && cc->cc_closure != NULL);

	if (wc == WEOF) {
		*cresult = EOF;
		return 0;
	}
	memset(&pspriv, 0, sizeof(pspriv));
	err = _citrus_ctype_wcrtomb(cc, buf, (wchar_t)wc, (void *)&pspriv, &nr);
	if (!err && nr == 1)
		*cresult = buf[0];
	else
		*cresult = EOF;

	return 0;
}

/*
 * for ABI version >= 0x00000003
 */

int
/*ARGSUSED*/
_citrus_ctype_mbsnrtowcs_fallback(_citrus_ctype_rec_t * __restrict cc,
    wchar_t * __restrict dst, const char ** __restrict src,
    size_t slen, size_t dlen, void * __restrict ps,
    size_t * __restrict nresult)
{
	int err = 0;
	const char *s;
	size_t n = 0, len;
	wchar_t dummy;

	_DIAGASSERT(cc != NULL && cc->cc_closure != NULL);
	_DIAGASSERT(src != NULL);
	_DIAGASSERT(*src != NULL || slen == 0);
	_DIAGASSERT(ps != NULL);
	_DIAGASSERT(nresult != NULL);

	s = *src;
	if (dst == NULL) {
		while (slen > 0) {
			err = _citrus_ctype_mbrtowc(cc, &dummy, s, slen,
			    ps, &len);
			if (err) {
				n = (size_t)-1;
				break;
			}
			if (len == (size_t)-2 || dummy == L'\0')
				break;
			++n;
			s += len, slen -= len;
		}
	} else {
		while (n < dlen  && slen > 0) {
			err = _citrus_ctype_mbrtowc(cc, dst, s, slen,
			    ps, &len);
			if (err) {
				n = (size_t)-1;
				break;
			}
			if (len == (size_t)-2) {
				s += slen;
				break;
			}
			if (*dst == L'\0') {
				s = NULL;
				break;
			}
			++dst, ++n;
			s += len, slen -= len;
		}
		*src = s;
	}
	*nresult = n;
	return err;
}

int
/*ARGSUSED*/
_citrus_ctype_wcsnrtombs_fallback(_citrus_ctype_rec_t * __restrict cc,
    char * __restrict dst, const wchar_t ** __restrict src,
    size_t slen, size_t dlen, void * __restrict ps,
    size_t * __restrict nresult)
{
	int err = 0;
	size_t n = 0, len;
	const wchar_t *s;
	char dummy[MB_LEN_MAX], sv[_PRIVSIZE];

	s = *src;
	if (dst == NULL) {
		while (slen-- > 0) {
			err = _citrus_ctype_wcrtomb(cc, &dummy[0], *s,
			    ps, &len);
			if (err) {
				n = (size_t)-1;
				break;
			}
			if (*s == L'\0') {
				n += len - 1;
				break;
			}
			n += len;
			++s;
		}
	} else {
		while (n < dlen && slen-- > 0) {
			memcpy((void *)&sv[0], (const void *)ps, sizeof(sv));
			err = _citrus_ctype_wcrtomb(cc, &dummy[0], *s,
			    ps, &len);
			if (err) {
				n = (size_t)-1;
				break;
			}
			if (n + len > dlen) {
				memcpy(ps, (const void *)&sv[0], sizeof(sv));
				break;
			}
			memcpy(dst, &dummy[0], len);
			if (*s == L'\0') {
				s = NULL;
				n += len - 1;
				break;
			}
			dst += len, n += len;
			++s;
		}
		*src = s;
	}
	*nresult = n;
	return err;
}
