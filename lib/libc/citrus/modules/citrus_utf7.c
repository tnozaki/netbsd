/*-
 * Copyright (c)2019, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
 
#include <sys/cdefs.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_utf7.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

#define	BASE64_BIT	6
#define	UTF16_BIT	16

#define	BASE64_MAX	UINT32_C(0x3f)

#define	BASE64_IN	'+'
#define	BASE64_OUT	'-'

#define UTF7_MB_CUR_MAX	5 /* BASE64_IN, 4 * 6 = 24, most closed to 21bit */

static const char base64[BASE64_MAX+1] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

#define D	0x1	/* Direct */
#define P	0x2	/* oPtion */
#define S	0x4	/* Spaces */
#define L	0x8	/* speciaL */

static const int utf7[0x80] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ L, 0, 0, 0, 0, 0, 0, 0, 0, S, S, 0, 0, S, 0, 0,
/* 1 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 2 */ S, P, P, P, P, P, P, D, D, D, P, L, D, D, D, D,
/* 3 */ D, D, D, D, D, D, D, D, D, D, D, P, P, P, P, D,
/* 4 */ P, D, D, D, D, D, D, D, D, D, D, D, D, D, D, D,
/* 5 */ D, D, D, D, D, D, D, D, D, D, D, P, 0, P, P, P,
/* 6 */ P, D, D, D, D, D, D, D, D, D, D, D, D, D, D, D,
/* 7 */ D, D, D, D, D, D, D, D, D, D, D, P, P, P, 0, 0,
};

static const int utf7len[0x80] = {
/*      0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f */
/* 0 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 1 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 2 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
/* 3 */ 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
/* 4 */ -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
/* 5 */ 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
/* 6 */ -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
/* 7 */ 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1,
};

typedef struct {
	int dummy;
} _UTF7EncodingInfo;

typedef struct {
	size_t chlen;
	char ch[UTF7_MB_CUR_MAX];
	_Bool mode;
	int bits;
	uint32_t cache;
} _UTF7State;

#define	_FUNCNAME(m)			_citrus_UTF7_##m
#define	_ENCODING_INFO			_UTF7EncodingInfo
#define	_CTYPE_INFO			_UTF7CTypeInfo
#define	_ENCODING_STATE			_UTF7State
#define	_ENCODING_IS_STATE_DEPENDENT		1
#define	_STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static __inline void
/*ARGSUSED*/
_citrus_UTF7_init_state(_UTF7EncodingInfo * __restrict ei,
    _UTF7State * __restrict s)
{
	_DIAGASSERT(s != NULL);

	memset((void *)s, 0, sizeof(*s));
}

static __inline void
/*ARGSUSED*/
_citrus_UTF7_pack_state(_UTF7EncodingInfo * __restrict ei,
    void *__restrict pspriv, const _UTF7State * __restrict s)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(s != NULL);

	memcpy(pspriv, (const void *)s, sizeof(*s));
}

static __inline void
/*ARGSUSED*/
_citrus_UTF7_unpack_state(_UTF7EncodingInfo * __restrict ei,
    _UTF7State * __restrict s, const void * __restrict pspriv)
{
	_DIAGASSERT(s != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy((void *)s, pspriv, sizeof(*s));
}

static __inline int
_citrus_UTF7_decode16(_UTF7EncodingInfo * __restrict ei,
    uint16_t * __restrict pc16, const char * __restrict s, size_t n,
    _UTF7State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	const char *t;
	int c, len;
	uint16_t c16;

	_DIAGASSERT(pc16 != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	t = s;
	if (psenc->bits % 2)
		return EINVAL;
	for (;;) {
		if (n-- < 1) {
			*rstate = (t == s)
			    ? _STDENC_SDGEN_STABLE
			    : _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c = (unsigned char)*t++;
		if (c & ~0x7f)
			return EILSEQ;
		if (psenc->mode) {
			if (c == BASE64_OUT && psenc->cache == 0) {
				psenc->mode = false;
				c16 = BASE64_IN;
				break;
			}
			len = utf7len[c];
			if (len < 0) {
				if (psenc->bits >= BASE64_BIT)
					return EINVAL;
				psenc->mode = false;
				psenc->bits = 0;
				psenc->cache = 0;
				if (c != BASE64_OUT) {
					if ((utf7[c] & (D|P|S|L)) == 0)
						return EILSEQ;
					c16 = c;
					break;
				}
			} else {
				psenc->cache = (psenc->cache << BASE64_BIT) | len;
				if (psenc->bits < UTF16_BIT - BASE64_BIT) {
					psenc->bits += BASE64_BIT;
				} else if (psenc->bits < UTF16_BIT) {
					psenc->bits -= (UTF16_BIT - BASE64_BIT);
					c16 = (psenc->cache >> psenc->bits) & UTF16_MAX;
					break;
				} else {
					return EINVAL;
				}
			}
		} else {
			if (psenc->bits != 0 || psenc->cache != 0)
				return EINVAL;
			if (c == BASE64_IN) {
				psenc->mode = true;
			} else {
				if ((utf7[c] & (D|P|S|L)) == 0)
					return EILSEQ;
				psenc->mode = true;
				c16 = c;
				break;
			}
		}
	}
	*pc16 = c16;
	*nresult = t - s;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF7_decode(_UTF7EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _UTF7State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	uint16_t hi, lo;
	size_t hinr, lonr;
	int ret;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(nresult != NULL);

	ret = _citrus_UTF7_decode16(ei, &hi, s, n, psenc, &hinr, rstate);
	if (ret)
		return ret;
	if (!is_hi_surrogate((uint32_t)hi)) {
		*pwc = hi;
		*nresult = hinr;
		return 0;
	}
	ret = _citrus_UTF7_decode16(ei, &lo, s + hinr, n - hinr, psenc, &lonr, rstate);
	if (ret)
		return ret;
	if (!is_lo_surrogate((uint32_t)lo))
		return EILSEQ;
	*pwc = utf16to32(hi, lo);
	*nresult = hinr + lonr;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF7_encode16(_UTF7EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, uint16_t c16,
    _UTF7State * __restrict psenc, size_t * __restrict nresult)
{
	int bits;
	uint32_t pos;
	char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	if (psenc->bits > BASE64_BIT)
		return EINVAL;
	if ((c16 & ~0x7f) == 0 && utf7[c16] & (D|S|L)) {
		if (psenc->mode) {
			if (psenc->bits > 0) {
				bits = BASE64_BIT - psenc->bits;
				pos = (psenc->cache << bits) & BASE64_MAX;
				if (n-- < 1)
					return E2BIG;
				*t++ = base64[pos];
				psenc->bits = 0;
				psenc->cache = 0;
			}
			if (c16 == BASE64_OUT || utf7len[c16] >= 0) {
				if (n-- < 1)
					return E2BIG;
				*t++ = BASE64_OUT;
			}
			psenc->mode = false;
		}
		if (psenc->bits != 0)
			return EINVAL;
		if (n-- < 1)
			return E2BIG;
		*t++ = c16;
		if (c16 == BASE64_IN) {
			if (n-- < 1)
				return E2BIG;
			*t++ = BASE64_OUT;
		}
	} else {
		if (!psenc->mode) {
			if (psenc->bits > 0)
				return EINVAL;
			if (n-- < 1)
				return E2BIG;
			*t++ = BASE64_IN;
			psenc->mode = true;
		}
		psenc->cache = (psenc->cache << UTF16_BIT) | c16;
		bits = UTF16_BIT + psenc->bits;
		psenc->bits = bits % BASE64_BIT;
		while ((bits -= BASE64_BIT) >= 0) {
			if (n-- < 1)
				return E2BIG;
			pos = (psenc->cache >> bits) & BASE64_MAX;
			*t++ = base64[pos];
		}
	}
	*nresult = t - s;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF7_encode(_UTF7EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _UTF7State * __restrict psenc, size_t * __restrict nresult)
{
	uint32_t c32;
	uint16_t c16[2];
	int ret, len, i;
	size_t siz, nr;

	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	if (c32 <= UTF16_MAX) {
		c16[0] = (uint16_t)c32;
		len = 1;
	} else if (c32 <= UTF32_MAX) {
		utf32to16(c32, &c16[0], &c16[1]);
		len = 2;
	} else {
		return EILSEQ;
	}
	siz = 0;
	for (i = 0; i < len; ++i) {
		ret = _citrus_UTF7_encode16(ei, s, n, c16[i], psenc, &nr);
		if (ret != 0)
			return ret;
		s += nr;
		n -= nr;
		siz += nr;
	}
	*nresult = siz;
	return 0;
}

static int
/* ARGSUSED */
_citrus_UTF7_put_state_reset(_UTF7EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, _UTF7State * __restrict psenc,
    size_t * __restrict nresult)
{
	char *t;
	int bits;
	uint32_t pos;

	_DIAGASSERT(s != NULL);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	if (psenc->bits > BASE64_BIT)
		return EINVAL;
	if (psenc->mode) {
		if (psenc->bits > 0) {
			if (n < 2)
				return E2BIG;
			bits = BASE64_BIT - psenc->bits;
			pos = (psenc->cache << bits) & BASE64_MAX;
			t[0] = base64[pos];
			t[1] = BASE64_OUT;
			psenc->bits = 0;
			psenc->cache = 0;
			t += 2;
			n -= 2;
		}
		psenc->mode = false;
	}
	if (psenc->bits != 0)
		return EINVAL;
	*nresult = t - s;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF7_stdenc_wctocs(_UTF7EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wc;

	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF7_stdenc_cstowc(_UTF7EncodingInfo * __restrict ei,
    wchar_t * __restrict wc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(wc != NULL);

	if (csid != 0)
		return EILSEQ;
	*wc = (wchar_t)idx;

	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF7_encoding_module_init(_UTF7EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_UTF7_encoding_module_uninit(_UTF7EncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(UTF7);
_CITRUS_STDENC_DEF_OPS(UTF7);

#include "citrus_stdenc_template.h"
