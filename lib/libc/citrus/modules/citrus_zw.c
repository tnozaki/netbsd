/*-
 * Copyright (c)2019, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
 
#include <sys/cdefs.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_zw.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef enum { LINESTART, ASCII, GB2312 } _ZWCharset;

typedef struct {
	int dummy;
} _ZWEncodingInfo;

typedef struct {
	_ZWCharset	charset;
	size_t		chlen;
	char		ch[MB_LEN_MAX];
} _ZWState;

#define _FUNCNAME(m)			_citrus_ZW_##m
#define _ENCODING_INFO			_ZWEncodingInfo
#define _CTYPE_INFO			_ZWCTypeInfo
#define _ENCODING_STATE			_ZWState
#define _ENCODING_IS_STATE_DEPENDENT		1
#define _MBTOCS_NEEDS_STATE_RESET		1
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	((_ps_)->charset != LINESTART)

static __inline void
/*ARGSUSED*/
_citrus_ZW_init_state(_ZWEncodingInfo * __restrict ei,
    _ZWState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->charset = LINESTART;
	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_ZW_pack_state(_ZWEncodingInfo * __restrict ei,
    void *__restrict pspriv, const _ZWState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, (const void *)psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_ZW_unpack_state(_ZWEncodingInfo * __restrict ei,
    _ZWState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy((void *)psenc, pspriv, sizeof(*psenc));
}

static __inline int
is94(int c)
{
	return c >= 0x21 && c <= 0x7e;
}

static int
/*ARGSUSED*/
_citrus_ZW_decode(_ZWEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _ZWState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	_ZWCharset charset;
	const char *t;
	int c;
	wchar_t wc;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	charset = psenc->charset;
	t = s;
restart:
	if (n-- < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = (unsigned char)*t++;
	if (c & ~0x7f)
		return EILSEQ;
	switch (charset) {
	case LINESTART:
		switch (c) {
		case '\n':
		case '\0':
			break;
		case 'z':
			if (n < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_SHIFT;
				return E2BIG;
			}
			if (*t == 'W') {
				++t;
				--n;
				charset = GB2312;
				goto restart;
			}
		/*FALLTHROUGH*/
		default:
			charset = ASCII;
		}
		wc = (wchar_t)c;
		break;
	case ASCII:
		switch (c) {
		case '\n':
		case '\0':
			charset = LINESTART;
		}
		wc = (wchar_t)c;
		break;
	case GB2312:
		switch (c) {
		case '\n':
			charset = LINESTART;
			goto restart;
		case '\0':
			charset = LINESTART;
			wc = c;
			break;
		case '#':
			if (n-- < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
				return E2BIG;
			}
			c = (unsigned char)*t++;
			switch (c) {
			case '\n':
				charset = LINESTART;
			/*FALLTHROUGH*/
			case ' ':
				break;
			default:
				return EILSEQ;
			}
			wc = c;
			break;
		case ' ':
			if (n-- < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
				return E2BIG;
			}
			c = (unsigned char)*t++;
			if (c & ~0x7f)
				return EILSEQ;
			wc = c;
			break;
		default:
			if (!is94(c))
				return EILSEQ;
			wc = c << 8;
			if (n-- < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
				return E2BIG;
			}
			c = (unsigned char)*t++;
			if (!is94(c))
				return EILSEQ;
			wc |= c;
		}
		break;
	default:
		return EINVAL;
	}
	*pwc = wc;
	psenc->charset = charset;
	*nresult = t - s;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_ZW_encode(_ZWEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _ZWState * __restrict psenc, size_t * __restrict nresult)
{
	_ZWCharset charset;
	char *t;

	charset = psenc->charset;
	t = s;
	if ((wc & ~0x7f) == 0) {
		switch (charset) {
		case LINESTART:
			switch (wc) {
			case L'\0':
			case L'\n':
				*t++ = (unsigned char)wc;
				break;
			default:
				if (n < 4)
					return E2BIG;
				t[0] = 'z';
				t[1] = 'W';
				t[2] = ' ';
				t[3] = (unsigned char)wc;
				charset = GB2312;
				t += 4;
				n -= 4;
			}
			break;
		case GB2312:
			if (n < 2)
				return E2BIG;
			switch (wc) {
			case L'\0':
				charset = LINESTART;
				t[0] = '\n';
				break;
			case L'\n':
				charset = LINESTART;
				t[0] = '#';
				break;
			default:
				t[0] = ' ';
			}
			t[1] = (unsigned char)wc;
			t += 2;
			n -= 2;
			break;
		default:
			return EINVAL;
		}
	} else if ((wc & ~0x7f7f) == 0) {
		switch (charset) {
		case LINESTART:
			if (n < 2)
				return E2BIG;
			charset = GB2312;
			t[0] = 'z';
			t[1] = 'W';
			t += 2;
			n -= 2;
		/* FALLTHROUGH*/
		case GB2312:
			if (n < 2)
				return E2BIG;
			t[0] = (unsigned char)((wc >> 8) & 0xff);
			if (!is94((unsigned char)t[0]))
				return EILSEQ;
			t[1] = (unsigned char)(wc & 0xff);
			if (!is94((unsigned char)t[1]))
				return EILSEQ;
			t += 2;
			n -= 2;
			break;
		default:
			return EINVAL;
		}
	} else {
		return EILSEQ;
	}
	*nresult = t - s;
	psenc->charset = charset;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_ZW_stdenc_wctocs(_ZWEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = (_csid_t)(wc & ~0x7f) ? 1 : 0;
	*idx = (_index_t)wc;

	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_ZW_stdenc_cstowc(_ZWEncodingInfo * __restrict ei,
    wchar_t * __restrict wc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(wc != NULL);

	switch (csid) {
	case 0: case 1:
		break;
	default:
		return EINVAL;
	}
	*wc = (wchar_t)idx;

	return 0;
}

static int
/*ARGSUSED*/
_citrus_ZW_put_state_reset(_ZWEncodingInfo * __restrict ei,
    char * __restrict s, size_t n,
    _ZWState * __restrict psenc, size_t * __restrict nresult)
{
	char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	switch (psenc->charset) {
	case LINESTART:
		break;
	case GB2312:
		if (n < 1)
			return E2BIG;
		t[0] = '\n';
		psenc->charset = LINESTART;
		break;
	default:
		return EINVAL;
	}
	*nresult = t - s;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_ZW_mbrtowc_state_reset(_ZWEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _ZWState * __restrict psenc,
    size_t * __restrict nresult)
{
	int ret = 0;
	size_t nr = 0;
	int rstate;

	if (psenc->chlen > 0) {
		psenc->ch[psenc->chlen++] = '\0';
		ret = _citrus_ZW_decode(ei, pwc,
		    psenc->ch, psenc->chlen, psenc, &nr, &rstate);
		psenc->chlen = 0;
	}
	*nresult = nr;
	return ret;
}

static int
/*ARGSUSED*/
_citrus_ZW_encoding_module_init(_ZWEncodingInfo * __restrict ei,
    const void *__restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_ZW_encoding_module_uninit(_ZWEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(ZW);
_CITRUS_STDENC_DEF_OPS(ZW);

#include "citrus_stdenc_template.h"
