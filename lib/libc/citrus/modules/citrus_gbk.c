/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_stdenc.h"

#include "citrus_gbk.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[2];
} _GBKState;

typedef struct {
	int dummy;
} _GBKEncodingInfo;

#define _FUNCNAME(m)			_citrus_GBK_##m
#define _ENCODING_INFO			_GBKEncodingInfo
#define _CTYPE_INFO			_GBKCTypeInfo
#define _ENCODING_STATE			_GBKState
#define _ENCODING_MB_CUR_MAX(_ei_)		2
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

#define S	0x1	/* Single byte */
#define G	0x2	/* GB2312 compatible lead byte */
#define B	0x4	/* GBK compatible lead byte */
#define K	0x8	/* GBK compatible trail byte */
#define L	(G|B)	/* Lead byte */
#define T	(G|K)	/* Trail byte */

static const char gbk[] = {
/*      0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f */
/* 0 */ S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,
/* 1 */ S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,
/* 2 */ S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,
/* 3 */ S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,  S,
/* 4 */ S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,
/* 5 */ S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,
/* 6 */ S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,
/* 7 */ S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,S|K,
/* 8 */ S|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,
/* 9 */ B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,B|K,
/* a */ B|K,G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,
/* b */ G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,
/* c */ G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,
/* d */ G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,
/* e */ G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,
/* f */ G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  G,  0,
};

static __inline void
/*ARGSUSED*/
_citrus_GBK_init_state(_GBKEncodingInfo * __restrict ei,
    _GBKState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_GBK_pack_state(_GBKEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _GBKState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_GBK_unpack_state(_GBKEncodingInfo * __restrict ei,
    _GBKState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_GBK_decode(_GBKEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _GBKState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int c;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = gbk[s[0] & 0xff];
	if (c & S) {
		*pwc = s[0] & 0xff;
		*nresult = 1;
	} else if (c & L) {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c = gbk[s[1] & 0xff];
		if (c & T) {
			*pwc = (s[0] & 0xff) << 8 |
			        s[1] & 0xff;
			*nresult = 2;
		} else {
			return EILSEQ;
		}
	} else {
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_GBK_encode(_GBKEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _GBKState * __restrict psenc, size_t * __restrict nresult)
{
	unsigned char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	t = (unsigned char *)s;
	if ((wc & ~0xff) == 0) {
		if (n < 1)
			return E2BIG;
		if ((gbk[t[0] =  wc        & 0xff] & S) == 0)
			return EILSEQ;
		*nresult = 1;
	} else if ((wc & ~0xffff) == 0) {
		if (n < 2)
			return E2BIG;
		if ((gbk[t[0] = (wc >>  8) & 0xff] & L) == 0 ||
		    (gbk[t[1] =  wc        & 0xff] & T) == 0)
			return EILSEQ;
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_GBK_stdenc_wctocs(_GBKEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx,
    wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	if ((wc & ~0xff) == 0) {
		*csid = 0;
	} else if ((wc & ~0xffff) == 0) {
		if ((gbk[(wc >> 8) & 0xff] & G) &&
		    (gbk[ wc       & 0xff] & G)) {
			*csid = 1;
			wc &= ~0x8080;
		} else {
			*csid = 2;
		}
	} else {
		return EILSEQ;
	}
	*idx = (_index_t)wc;

	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_GBK_stdenc_cstowc(_GBKEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	switch (csid) {
	case 1:
		idx |= 0x8080;
	/*FALLTHROUGH*/
	case 0:
	case 2:
		break;
	default:
		return EILSEQ;
	}
	*pwc = (wchar_t)idx;

	return 0;
}

static int
/*ARGSUSED*/
_citrus_GBK_encoding_module_init(_GBKEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_GBK_encoding_module_uninit(_GBKEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for ctype
 */

_CITRUS_CTYPE_DECLS(GBK);
_CITRUS_CTYPE_DEF_OPS(GBK);

#include "citrus_ctype_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(GBK);
_CITRUS_STDENC_DEF_OPS(GBK);

#include "citrus_stdenc_template.h"
