/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_dechanyu.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[4];
} _DECHanyuState;

typedef struct {
	int dummy;
} _DECHanyuEncodingInfo;

#define _FUNCNAME(m)			__CONCAT(_citrus_DECHanyu_,m)
#define _ENCODING_INFO			_DECHanyuEncodingInfo
#define _CTYPE_INFO			_DECHanyuCTypeInfo
#define _ENCODING_STATE			_DECHanyuState
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

#define S	0x1	/* single byte */
#define D	0x2	/* double byte */
#define Q	0x4	/* quad byte(1st) */
#define R	0x8	/* quad byte(2st) */
#define L	(D|Q|R)	/* lead byte */
#define T	0x10	/* trail byte */

static const char dechanyu[] = {
/*      0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f */
/* 0 */ S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,
/* 1 */ S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,
/* 2 */ S  ,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 3 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 4 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 5 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 6 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 7 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S  ,
/* 8 */   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
/* 9 */   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
/* a */   0,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,
/* b */ D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,
/* c */ D|T,D|T,Q|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,R|T,D|T,D|T,D|T,D|T,
/* d */ D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,
/* e */ D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,
/* f */ D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,D|T,  0,
};

static __inline void
/*ARGSUSED*/
_citrus_DECHanyu_init_state(_DECHanyuEncodingInfo * __restrict ei,
    _DECHanyuState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_DECHanyu_pack_state(_DECHanyuEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _DECHanyuState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_DECHanyu_unpack_state(_DECHanyuEncodingInfo * __restrict ei,
    _DECHanyuState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_DECHanyu_decode(_DECHanyuEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _DECHanyuState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int c;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = dechanyu[(unsigned char)s[0]];
	if (c & S) {
		*pwc = (unsigned char)s[0];
		*nresult = 1;
	} else if (c & D) {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		if ((dechanyu[(unsigned char)s[1]] & T) == 0)
			return EILSEQ;
		*pwc = (unsigned char)s[0] << 8 |
		       (unsigned char)s[1];
		*nresult = 2;
	} else if (c & Q) {
		if (n < 4) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		if ((dechanyu[(unsigned char)s[1]] & R) == 0 ||
		    (dechanyu[(unsigned char)s[2]] & L) == 0 ||
		    (dechanyu[(unsigned char)s[3]] & T) == 0)
			return EILSEQ;
		*pwc = (unsigned char)s[0] << 24 |
		       (unsigned char)s[1] << 16 |
		       (unsigned char)s[2] <<  8 |
		       (unsigned char)s[3];
		*nresult = 4;
	} else {
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_DECHanyu_encode(_DECHanyuEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _DECHanyuState * __restrict psenc, size_t * __restrict nresult)
{
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0xff) == 0) {
		if (n < 1)
			return E2BIG;
		s[0] = wc & 0xff;
		if ((dechanyu[(unsigned char)s[0]] & S) == 0)
			return EILSEQ;
		*nresult = 1;
	} else if ((wc & ~0xffff) == 0) {
		if (n < 2)
			return E2BIG;
		s[0] = (wc >>  8) & 0xff;
		s[1] =  wc       & 0xff;
		if ((dechanyu[(unsigned char)s[0]] & D) == 0 ||
		    (dechanyu[(unsigned char)s[1]] & T) == 0)
			return EILSEQ;
		*nresult = 2;
	} else {
		if (n < 4)
			return E2BIG;
		s[0] = (wc >> 24) & 0xff;
		s[1] = (wc >> 16) & 0xff;
		s[2] = (wc >>  8) & 0xff;
		s[3] =  wc        & 0xff;
		if ((dechanyu[(unsigned char)s[0]] & Q) == 0 ||
		    (dechanyu[(unsigned char)s[1]] & R) == 0 ||
		    (dechanyu[(unsigned char)s[2]] & L) == 0 ||
		    (dechanyu[(unsigned char)s[2]] & T) == 0)
			return EILSEQ;
		*nresult = 4;
	}
	return 0;

ilseq:
	*nresult = (size_t)-1;
	return EILSEQ;
}

static __inline int
/*ARGSUSED*/
_citrus_DECHanyu_stdenc_wctocs(_DECHanyuEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = wc & ~0x7f7f;
	*idx = wc & 0x7f7f;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_DECHanyu_stdenc_cstowc(_DECHanyuEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	*pwc = (wchar_t)(csid | idx);
	return 0;
}

static int
/*ARGSUSED*/
_citrus_DECHanyu_encoding_module_init(_DECHanyuEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_DECHanyu_encoding_module_uninit(_DECHanyuEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(DECHanyu);
_CITRUS_STDENC_DEF_OPS(DECHanyu);

#include "citrus_stdenc_template.h"
