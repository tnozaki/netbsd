/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_stdenc.h"

#include "citrus_euc.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[3];
} _EUCState;

typedef struct {
	size_t len;
	wchar_t bit;
} _EUCCharset;

typedef struct {
	_EUCCharset g[4];
	wchar_t mask;
	size_t mb_cur_max;
} _EUCEncodingInfo;

#define _FUNCNAME(m)			_citrus_EUC_##m
#define _ENCODING_INFO			_EUCEncodingInfo
#define _CTYPE_INFO			_EUCCTypeInfo
#define _ENCODING_STATE			_EUCState
#define _ENCODING_MB_CUR_MAX(_ei_)	(_ei_)->mb_cur_max
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

#define GL	0
#define GR	1
#define SS2R	2
#define SS3R	3

/* leadbyte -> graphic */
static const char lead2g[] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 1 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 2 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 3 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 4 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 5 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 6 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 7 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 8 */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,SS2R,SS3R,
/* 9 */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* a */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* b */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* c */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* d */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* e */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* f */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
};

/* graphic -> single shift */
static const char g2ss[] = {
/* GL   */ 0,
/* GR   */ 0,
/* SS2R */ 0x8e,
/* SS3R */ 0x8f
};

/* graphic -> max length */
static const long g2len[] = {
/* GL   */ 1,
/* GR   */ 4,
/* SS2R */ 5,
/* SS3R */ 5
};

#define MASK_FULL	0x80808080

/* graphic -> max mask */
static const long g2mask[] = {
/* GL   */ 0x0,
/* GR   */ MASK_FULL,
/* SS2R */ MASK_FULL,
/* SS3R */ MASK_FULL
};

static __inline void
/*ARGSUSED*/
_citrus_EUC_init_state(_EUCEncodingInfo * __restrict ei,
    _EUCState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_EUC_pack_state(_EUCEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _EUCState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_EUC_unpack_state(_EUCEncodingInfo * __restrict ei,
    _EUCState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_EUC_decode(_EUCEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _EUCState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int g;
	const _EUCCharset *gp;
	wchar_t wc;
	size_t len, i;
	const char *t;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	g = lead2g[s[0] & 0xff];
	if (g == GL) {
		*pwc = s[0];
		*nresult = 1;
		return 0;
	}
	gp = &ei->g[g];
	if (gp->len == 0)
		return EILSEQ;
	if (n < gp->len) {
		*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
		return E2BIG;
	}
	t = &s[gp->len];
	if (g == SS2R || g == SS3R) {
		if ((*++s & 0x80) == 0)
			return EILSEQ;
	}
	wc = *s++ & 0x7f;
	while (s < t) {
		if ((*s & 0x80) == 0)
			return EILSEQ;
		wc <<= 8;
		wc |= *s++ & 0x7f;
	}
	wc |= gp->bit;
	*pwc = wc;
	*nresult = gp->len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_EUC_encode(_EUCEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _EUCState * __restrict psenc, size_t * __restrict nresult)
{
	int g;
	const _EUCCharset *gp;
	wchar_t bit;
	size_t len;
	char *t;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0x7f) == 0) {
		if (n < 1)
			return E2BIG;
		*s = wc;
		*nresult = 1;
		return 0;
	}
	bit = wc & ei->mask;
	for (g = GR; g <= SS3R; ++g) {
		gp = &ei->g[g];
		if (gp->bit == bit)
			goto found;
	}
	return EILSEQ;
found:
	if (n < gp->len)
		return E2BIG;
	t = &s[gp->len];
	if (g2ss[g])
		*s++ = g2ss[g];
	while (t-- > s) {
		*t = (wc & 0x7f) | 0x80;
		wc >>= 8;
	}
	*nresult = gp->len;
	return 0;
}

static __inline int
_citrus_EUC_stdenc_wctocs(_EUCEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	wchar_t bit;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	bit = wc & ei->mask;

	*csid = (_csid_t)bit;
	*idx = (_index_t)(wc & ~bit);

	return 0;
}

static __inline int
_citrus_EUC_stdenc_cstowc(_EUCEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(pwc != NULL);

	if (csid & ~ei->mask || idx & ei->mask)
		return EINVAL;

	*pwc = (wchar_t)csid | (wchar_t)idx;

	return 0;
}

static __inline int
parse_value(const char **s, size_t * n, unsigned long *l)
{
	int ret;
	const char *p;
	char *token, *endptr;

	_DIAGASSERT(s != NULL);
	_DIAGASSERT(n != NULL);
	_DIAGASSERT(l != NULL);
	_DIAGASSERT(*s != NULL || *n < 1);

	if (*n < 1)
		return EINVAL;
	p = _bcs_skip_ws_len(*s, n);
	if (*n < 1)
		return EINVAL;
	*s = _bcs_skip_nonws_len(p, n);
	token = strndup(p, *s - p);
	if (token == NULL)
		return errno;
	*l = _bcs_strtoul((const char *)token, &endptr, 0);
	ret = (token == endptr || *endptr) ? EINVAL : 0;
	free(token);
	return ret;
}

static int
_citrus_EUC_encoding_module_init(_EUCEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	const char *s;
	int g;
	_EUCCharset *gp;
	int ret;
	unsigned long l;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(var != NULL || lenvar < 1);

	s = (const char *)var;
	ei->mb_cur_max = 1;
	for (g = GL; g <= SS3R; ++g) {
		gp = &ei->g[g];
		ret = parse_value(&s, &lenvar, &l);
		if (ret)
			return ret;
		if (l < 0 || (g2ss[g] && l == 1) ||
		    l > g2len[g])
			return EINVAL;
		gp->len = (size_t)l;
		ret = parse_value(&s, &lenvar, &l);
		if (ret)
			return ret;
		if (l & ~g2mask[g])
			return EINVAL;
		gp->bit = (wchar_t)l;
		if (ei->mb_cur_max < gp->len)
			ei->mb_cur_max = gp->len;
	}
	ret = parse_value(&s, &lenvar, &l);
	if (ret)
		return ret;
	if (l & ~MASK_FULL)
		return EINVAL;
	ei->mask = (wchar_t)l;
	return 0;
}

static void
/*ARGSUSED*/
_citrus_EUC_encoding_module_uninit(_EUCEncodingInfo * __restrict ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for ctype
 */

_CITRUS_CTYPE_DECLS(EUC);
_CITRUS_CTYPE_DEF_OPS(EUC);

#include "citrus_ctype_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(EUC);
_CITRUS_STDENC_DEF_OPS(EUC);

#include "citrus_stdenc_template.h"
