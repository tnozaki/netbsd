/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*-
 * Copyright (c)2002 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_utf8.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

#define UTF8_MB_LEN_MAX	6

typedef struct {
	size_t chlen;
	char ch[UTF8_MB_LEN_MAX];
} _UTF8State;

typedef struct {
	int dummy;
} _UTF8EncodingInfo;

#define _FUNCNAME(m)			_citrus_UTF8_##m
#define _ENCODING_INFO			_UTF8EncodingInfo
#define _CTYPE_INFO			_UTF8CTypeInfo
#define _ENCODING_STATE			_UTF8State
#define _ENCODING_MB_CUR_MAX(ei)	UTF8_MB_LEN_MAX
#define _ENCODING_IS_STATE_DEPENDENT	0
#define _STATE_NEEDS_EXPLICIT_INIT(ps)	0

static const char utf8mask[UTF8_MB_LEN_MAX + 1] = {
	0x00, /* dummy */
	0x7f, 0x1f, 0x0f, 0x07, 0x03, 0x01,
};

static const char utf8bit[UTF8_MB_LEN_MAX + 1] = {
	0x00, /* dummy */
	0x00, 0xc0, 0xe0, 0xf0, 0xf8, 0xfc,
};

static const char utf8len[UCHAR_MAX + 1] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 1 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 2 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 3 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 4 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 5 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 6 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 7 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 8 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 9 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* a */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* b */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* c */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* d */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* e */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
/* f */ 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 0, 0,
};

static __inline size_t
ucs4len(uint32_t c32)
{
	if ((c32 & ~0x7f) == 0) {
		return 1;
	} else if ((c32 & ~0x7ff) == 0) {
		return 2;
	} else if ((c32 & ~0xffff) == 0) {
		if (!is_surrogate(c32))
			return 3;
	} else if ((c32 & ~0x1fffff) == 0) {
		return 4;
	} else if ((c32 & ~0x3ffffff) == 0) {
		return 5;
	} else if ((c32 & ~0x7fffffff) == 0) {
		return 6;
	}
        return 0;
}

static __inline void
/*ARGSUSED*/
_citrus_UTF8_init_state(_UTF8EncodingInfo * __restrict ei,
    _UTF8State * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_UTF8_pack_state(_UTF8EncodingInfo * __restrict ei,
    void * __restrict pspriv, const _UTF8State * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_UTF8_unpack_state(_UTF8EncodingInfo * __restrict ei,
    _UTF8State * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_UTF8_decode(_UTF8EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _UTF8State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	size_t len, i;
	uint32_t c32;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	len = utf8len[(unsigned char)s[0]];
	switch (len) {
	case 0:
		return EILSEQ;
	case 1:
		c32 = (unsigned char)s[0];
		break;
	default:
		if (n < len) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c32 = s[0] & utf8mask[len];
		for (i = 1; i < len; ++i) {
			if ((s[i] & 0xc0) != 0x80)
				return EILSEQ;
			c32 <<= 6;
			c32 |= s[i] & 0x3f;
		}
		if (ucs4len(c32) != len)
			return EILSEQ;
	}
	*pwc = (wchar_t)c32;
	*nresult = len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF8_encode(_UTF8EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _UTF8State * __restrict psenc, size_t * __restrict nresult)
{
	size_t len;
	char *t;
	uint32_t c32;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	if (n < 1)
		return E2BIG;
	len = ucs4len(c32);
	switch (len) {
	case 0:
		return EILSEQ;
	case 1:
		*s = (unsigned char)c32;
		break;
	default:
		if (n < len)
			return E2BIG;
		for (t = &s[len - 1]; s < t; --t) {
			*t = (c32 & 0x3f) | 0x80;
			c32 >>= 6;
		}
		*t = c32 | utf8bit[len];
	}
	*nresult = len;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF8_stdenc_wctocs(_UTF8EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wc;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF8_stdenc_cstowc(_UTF8EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	if (csid != 0)
		return EILSEQ;
	*pwc = (wchar_t)idx;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF8_encoding_module_init(_UTF8EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_UTF8_encoding_module_uninit(_UTF8EncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for ctype
 */

_CITRUS_CTYPE_DECLS(UTF8);
_CITRUS_CTYPE_DEF_OPS(UTF8);

#include "citrus_ctype_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(UTF8);
_CITRUS_STDENC_DEF_OPS(UTF8);

#include "citrus_stdenc_template.h"
