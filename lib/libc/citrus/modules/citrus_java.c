/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_java.h"

typedef struct {
	int dummy;
} _JavaEncodingInfo;

typedef struct {
	size_t chlen;
	char ch[12];
} _JavaState;

#define _FUNCNAME(m)			_citrus_Java_##m
#define _ENCODING_INFO			_JavaEncodingInfo
#define _CTYPE_INFO			_JavaCTypeInfo
#define _ENCODING_STATE			_JavaState
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static const char xdigit[] = "0123456789abcdef";

static __inline void
/*ARGSUSED*/
_citrus_Java_init_state(_JavaEncodingInfo * __restrict ei,
    _JavaState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline int
to_int(int c)
{
	switch (c) {
	case '0': return 0;
	case '1': return 1;
	case '2': return 2;
	case '3': return 3;
	case '4': return 4;
	case '5': return 5;
	case '6': return 6;
	case '7': return 7;
	case '8': return 8;
	case '9': return 9;
	case 'A': case 'a': return 10;
	case 'B': case 'b': return 11;
	case 'C': case 'c': return 12;
	case 'D': case 'd': return 13;
	case 'E': case 'e': return 14;
	case 'F': case 'f': return 15;
	}
	return -1;
}

static int
/*ARGSUSED*/
_citrus_Java_decode(_JavaEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _JavaState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	size_t len, i;
	int val;
	uint16_t hi, lo;
	uint32_t c32;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	if (s[0] == '\\') {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		if (s[1] != 'u')
			goto noescape;
		if (n < 6) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		hi = (uint16_t)0;
		for (i = 2; i < 6; ++i) {
			val = to_int((unsigned char)s[i]);
			if (val == -1)
				return EILSEQ;
			hi = (hi << 4) | val;
		}
		if ((hi & ~0x7f) == 0 || is_lo_surrogate((uint32_t)hi))
			return EILSEQ;
		if (is_hi_surrogate((uint32_t)hi)) {
			if (n < 12) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
				return E2BIG;
			}
			if (s[6] != '\\' || s[7] != 'u')
				return EILSEQ;
			lo = 0;
			for (i = 8; i < 12; ++i) {
				val = to_int((unsigned char)s[i]);
				if (val == -1)
					return EILSEQ;
				lo = (lo << 4) | val;
			}
			if (!is_lo_surrogate((uint32_t)lo))
				return EILSEQ;
			c32 = utf16to32(hi, lo);
			len = 12;
		} else {
			c32 = (uint32_t)hi;
			len = 6;
		}
	} else {
noescape:
		c32 = (uint32_t)(unsigned char)s[0];
		len = 1;
	}
	*pwc = (wchar_t)c32;
	*nresult = len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_Java_encode(_JavaEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _JavaState * __restrict psenc, size_t * __restrict nresult)
{
	uint32_t c32;
	uint16_t hi, lo;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	if (c32 <= 0x7f) {
		if (n < 1)
			return E2BIG;
		s[0] = (unsigned char)c32;
		*nresult = 1;
	} else if (c32 <= UTF16_MAX) {
		if (n < 6)
			return E2BIG;
		s[ 0] = '\\';
		s[ 1] = 'u';
		s[ 2] = xdigit[(c32 >> 12) & 0xf];
		s[ 3] = xdigit[(c32 >>  8) & 0xf];
		s[ 4] = xdigit[(c32 >>  4) & 0xf];
		s[ 5] = xdigit[ c32        & 0xf];
		*nresult = 6;
	} else if (c32 <= UTF32_MAX) {
		if (n < 12)
			return E2BIG;
		utf32to16(c32, &hi, &lo);
		s[ 0] = '\\';
		s[ 1] = 'u';
		s[ 2] = xdigit[(hi >> 12) & 0xf];
		s[ 3] = xdigit[(hi >>  8) & 0xf];
		s[ 4] = xdigit[(hi >>  4) & 0xf];
		s[ 5] = xdigit[ hi        & 0xf];
		s[ 6] = '\\';
		s[ 7] = 'u';
		s[ 8] = xdigit[(lo >> 12) & 0xf];
		s[ 9] = xdigit[(lo >>  8) & 0xf];
		s[10] = xdigit[(lo >>  4) & 0xf];
		s[11] = xdigit[ lo        & 0xf];
		*nresult = 12;
	} else {
		return EILSEQ;
	}
	return 0;
}

/*ARGSUSED*/
static int
_citrus_Java_stdenc_wctocs(_JavaEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wc;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_Java_stdenc_cstowc(_JavaEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	if (csid != 0)
		return EILSEQ;
	*pwc = (wchar_t)idx;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_Java_encoding_module_init(_JavaEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_Java_encoding_module_uninit(_JavaEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(Java);
_CITRUS_STDENC_DEF_OPS(Java);

#include "citrus_stdenc_template.h"
