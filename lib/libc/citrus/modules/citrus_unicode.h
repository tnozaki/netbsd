/*-
 * Copyright (c)2019 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _CITRUS_UNICODE_H_
#define _CITRUS_UNICODE_H_

#define UCS2_MAX	UINT32_C(0xffff)
#define UCS4_MAX	UINT32_C(0x7fffffff)

#define UTF16_MAX	UINT32_C(0xffff)
#define UTF32_MAX	UINT32_C(0x10ffff)

/* surrogate pair */
#define SRG_BASE	UINT32_C(0x10000)
#define HISRG_MIN	UINT32_C(0xd800)
#define HISRG_MAX	UINT32_C(0xdbff)
#define LOSRG_MIN	UINT32_C(0xdc00)
#define LOSRG_MAX	UINT32_C(0xdfff)

static __inline int
is_surrogate(uint32_t c32)
{
	return c32 >= HISRG_MIN && c32 <= LOSRG_MAX;
}

static __inline int
is_hi_surrogate(uint32_t c32)
{
	return c32 >= HISRG_MIN && c32 <= HISRG_MAX;
}

static __inline int
is_lo_surrogate(uint32_t c32)
{
	return c32 >= LOSRG_MIN && c32 <= LOSRG_MAX;
}

static __inline uint32_t
utf16to32(uint16_t hi, uint16_t lo)
{
	hi -= HISRG_MIN;
	lo -= LOSRG_MIN;
	return ((uint32_t)hi << 10 | (uint32_t)lo) + SRG_BASE;
}

static __inline void
utf32to16(uint32_t c32, uint16_t *hi, uint16_t *lo)
{
	c32 -= SRG_BASE;
	*hi = (uint16_t)((c32 >> 10) + HISRG_MIN);
	*lo = (uint16_t)((c32 & UINT32_C(0x3ff)) + LOSRG_MIN);
}

#endif
