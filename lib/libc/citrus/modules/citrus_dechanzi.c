/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_dechanzi.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[2];
} _DECHanziState;

typedef struct {
	int dummy;
} _DECHanziEncodingInfo;

#define _FUNCNAME(m)			_citrus_DECHanzi_##m
#define _ENCODING_INFO			_DECHanziEncodingInfo
#define _CTYPE_INFO			_DECHanziCTypeInfo
#define _ENCODING_STATE			_DECHanziState
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static __inline void
/*ARGSUSED*/
_citrus_DECHanzi_init_state(_DECHanziEncodingInfo * __restrict ei,
    _DECHanziState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_DECHanzi_pack_state(_DECHanziEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _DECHanziState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_DECHanzi_unpack_state(_DECHanziEncodingInfo * __restrict ei,
    _DECHanziState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_DECHanzi_decode(_DECHanziEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _DECHanziState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	if ((s[0] & ~0x7f) == 0) {
		*pwc = s[0];
		*nresult = 1;
	} else {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		*pwc = (unsigned char)s[0] << 8 |
		       (unsigned char)s[1];
		*nresult = 2;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_DECHanzi_encode(_DECHanziEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _DECHanziState * __restrict psenc, size_t * __restrict nresult)
{
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0xff) == 0) {
		s[0] = wc;
		if (s[0] & 0x80)
			return EILSEQ;
		*nresult = 1;
	} else if ((wc & ~0xffff) == 0) {
		s[0] = (wc >> 8) & 0xff;
		s[1] =  wc       & 0xff;
		if ((s[0] & 0x80) == 0)
			return EILSEQ;
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_DECHanzi_stdenc_wctocs(_DECHanziEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = (_csid_t)(wc & ~0x7f7f);
	*idx = (_index_t)(wc & 0x7f7f);

	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_DECHanzi_stdenc_cstowc(_DECHanziEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	*pwc = (wchar_t)csid | (wchar_t)idx;

	return 0;
}

static int
/*ARGSUSED*/
_citrus_DECHanzi_encoding_module_init(_DECHanziEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_DECHanzi_encoding_module_uninit(_DECHanziEncodingInfo * __restrict ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(DECHanzi);
_CITRUS_STDENC_DEF_OPS(DECHanzi);

#include "citrus_stdenc_template.h"
