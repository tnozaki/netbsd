/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_stdenc.h"

#include "citrus_euctw.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[4];
} _EUCTWState;

typedef struct {
	int dummy;
} _EUCTWEncodingInfo;

#define _FUNCNAME(m)			_citrus_EUCTW_##m
#define _ENCODING_INFO			_EUCTWEncodingInfo
#define _CTYPE_INFO			_EUCTWCTypeInfo
#define _ENCODING_STATE			_EUCTWState
#define _ENCODING_MB_CUR_MAX(_ei_)		4
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

#define GL	0
#define GR	1
#define SS2R	2
#define SS3R	3

/* leadbyte -> graphic */
static const char lead2g[] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 1 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 2 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 3 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 4 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 5 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 6 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 7 */ GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,GL,
/* 8 */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,SS2R,SS3R,
/* 9 */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* a */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* b */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* c */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* d */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* e */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
/* f */ GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,GR,
};

static __inline void
/*ARGSUSED*/
_citrus_EUCTW_init_state(_EUCTWEncodingInfo * __restrict ei,
    _EUCTWState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_EUCTW_pack_state(_EUCTWEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _EUCTWState * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_EUCTW_unpack_state(_EUCTWEncodingInfo * __restrict ei,
    _EUCTWState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_EUCTW_decode(_EUCTWEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _EUCTWState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int plane;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	switch (lead2g[s[0] & 0xff]) {
	case GL:
		*pwc = s[0];
		*nresult = 1;
		break;
	case GR:
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		if ((s[1] & 0x80) == 0)
			return EILSEQ;
		*pwc = 'G' << 24 |
		    (s[0] & 0xff) << 8 | (s[1] & 0xff);
		*nresult = 2;
		break;
	case SS2R:
		if (n < 4) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		plane = s[1] & 0xff;
		if (plane < 0xa1 || plane > 0xb0 ||
		    (s[2] & 0x80) == 0 || (s[3] & 0x80) == 0)
			return EILSEQ;
		plane -= 0xa1;
		*pwc = ('G' + plane) << 24 |
		    (s[2] & 0xff) << 8 | (s[3] & 0xff);
		*nresult = 4;
		break;
	default:
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_EUCTW_encode(_EUCTWEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _EUCTWState * __restrict psenc, size_t * __restrict nresult)
{
	int plane;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0x7f) == 0) {
		if (n < 1)
			return E2BIG;
		*s = wc;
		*nresult = 1;
		return 0;
	} else {
		plane = (wc >> 24) & 0xff;
		if (plane == 'G') {
			if (n < 2)
				return E2BIG;
			s[0] = (wc >> 8) & 0xff;
			s[1] = wc & 0xff;
			*nresult = 2;
		} else if (plane >= 'H' && plane <= 'V') {
			if (n < 4)
				return E2BIG;
			plane -= 'G';
			s[0] = 0x8e;
			s[1] = 0xa1 + plane;
			s[2] = (wc >> 8) & 0xff;
			s[3] = wc & 0xff;
			*nresult = 4;
		} else {
			return EILSEQ;
		}
	}
	return 0;
}

static __inline int
_citrus_EUCTW_stdenc_wctocs(_EUCTWEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = (_csid_t)(wc >> 24) & 0xff;
	*idx  = (_index_t)(wc & 0x7f7f);

	return 0;
}

static __inline int
_citrus_EUCTW_stdenc_cstowc(_EUCTWEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	if (csid == 0) {
		if ((idx & ~0x7f) == 0) {
			*pwc = (wchar_t)idx;
			return 0;
		}
	} else if (csid >= 'G' && csid <= 'V') {
		if ((idx & ~0x7f7f) == 0) {
			*pwc = csid << 24 | idx | 0x8080;
			return 0;
		}
	}
	return EILSEQ;
}

static int
_citrus_EUCTW_encoding_module_init(_EUCTWEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_EUCTW_encoding_module_uninit(_EUCTWEncodingInfo * __restrict ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for ctype
 */

_CITRUS_CTYPE_DECLS(EUCTW);
_CITRUS_CTYPE_DEF_OPS(EUCTW);

#include "citrus_ctype_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(EUCTW);
_CITRUS_STDENC_DEF_OPS(EUCTW);

#include "citrus_stdenc_template.h"
