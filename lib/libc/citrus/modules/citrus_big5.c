/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <sys/queue.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_ctype.h"
#include "citrus_stdenc.h"
#include "citrus_prop.h"

#include "citrus_big5.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[2];
} _BIG5State;

typedef struct _BIG5Exclude {
	TAILQ_ENTRY(_BIG5Exclude) entry;
	wint_t start, end;
} _BIG5Exclude;

typedef TAILQ_HEAD(_BIG5ExcludeList, _BIG5Exclude) _BIG5ExcludeList;

typedef struct {
	unsigned char cells[0x100];
#define S	0x1	/* Single byte */
#define L	0x2	/* Lead byte   */
#define T	0x4	/* Trail byte  */
	_BIG5ExcludeList excludes;
} _BIG5EncodingInfo;

#define _FUNCNAME(m)			_citrus_BIG5_##m
#define _ENCODING_INFO			_BIG5EncodingInfo
#define _CTYPE_INFO			_BIG5CTypeInfo
#define _ENCODING_STATE			_BIG5State
#define _ENCODING_MB_CUR_MAX(_ei_)		2
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static __inline void
/*ARGSUSED*/
_citrus_BIG5_init_state(_BIG5EncodingInfo * __restrict ei,
    _BIG5State * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_BIG5_pack_state(_BIG5EncodingInfo * __restrict ei,
    void * __restrict pspriv, const _BIG5State * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_BIG5_unpack_state(_BIG5EncodingInfo * __restrict ei,
    _BIG5State * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static __inline int
excludes(_BIG5EncodingInfo *ei, wint_t c)
{
	_BIG5Exclude *exclude;

	_DIAGASSERT(ei != NULL);

	TAILQ_FOREACH(exclude, &ei->excludes, entry) {
		if (c >= exclude->start && c <= exclude->end)
			return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_BIG5_decode(_BIG5EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _BIG5State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int c;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = ei->cells[(unsigned char)s[0]];
	if (c & S) {
		*pwc = (unsigned char)s[0];
		*nresult = 1;
	} else if (c & L) {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		if ((ei->cells[(unsigned char)s[1]] & T) == 0)
			return EILSEQ;
		*pwc = (unsigned char)s[0] << 8 |
		       (unsigned char)s[1];
		if (excludes(ei, (wint_t)*pwc))
			return EILSEQ;
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_BIG5_encode(_BIG5EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _BIG5State * __restrict psenc, size_t * __restrict nresult)
{
	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0xff) == 0) {
		if (n < 1)
			return E2BIG;
		s[0] = wc & 0xff;
		if ((ei->cells[(unsigned char)s[0]] & S) == 0)
			return EILSEQ;
		*nresult = 1;
	} else if ((wc & ~0xffff) == 0 && !excludes(ei, (wint_t)wc)) {
		if (n < 2)
			return E2BIG;
		s[0] = (wc >> 8) & 0xff;
		if ((ei->cells[(unsigned char)s[0]] & L) == 0)
			return EILSEQ;
		s[1] = wc & 0xff;
		if ((ei->cells[(unsigned char)s[1]] & T) == 0)
			return EILSEQ;
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}


static __inline int
/*ARGSUSED*/
_citrus_BIG5_stdenc_wctocs(_BIG5EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = ((wc & ~0xff) == 0) ? 0 : 1;
	*idx = (_index_t)wc;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_BIG5_stdenc_cstowc(_BIG5EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	switch (csid) {
	case 0:
	case 1:
		*pwc = (wchar_t)idx;
		break;
	default:
		return EILSEQ;
	}
	return 0;
}

static int
_citrus_BIG5_parse_rowcol(void ** __restrict ctx,
    const char * __restrict s, uint64_t start, uint64_t end)
{
	_BIG5EncodingInfo *ei;
	int bit;
	uint64_t i;

	_DIAGASSERT(ctx != NULL);
	_DIAGASSERT(*ctx != NULL);
	_DIAGASSERT(s != NULL);

	if (start > 0xff || end > 0xff)
		return EINVAL;
	ei = (_BIG5EncodingInfo *)*ctx;
	bit = strcmp("row", s) ? T : L;
	for (i = start; i <= end; ++i)
		ei->cells[i] |= bit;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_BIG5_parse_excludes(void ** __restrict ctx,
    const char * __restrict s, uint64_t start, uint64_t end)
{
	_BIG5EncodingInfo *ei;
	_BIG5Exclude *exclude;

	_DIAGASSERT(ctx != NULL);
	_DIAGASSERT(*ctx != NULL);
	_DIAGASSERT(s != NULL);

	if (start > 0xffff || end > 0xffff)
		return EINVAL;
	ei = (_BIG5EncodingInfo *)*ctx;
	exclude = TAILQ_LAST(&ei->excludes, _BIG5ExcludeList);
	if (exclude != NULL && (wint_t)start <= exclude->end)
		return EINVAL;
	exclude = (void *)malloc(sizeof(*exclude));
	if (exclude == NULL)
		return ENOMEM;
	exclude->start = (wint_t)start;
	exclude->end = (wint_t)end;
	TAILQ_INSERT_TAIL(&ei->excludes, exclude, entry);

	return 0;
}

static const _citrus_prop_hint_t root_hints[] = {
    _CITRUS_PROP_HINT_NUM("row", &_citrus_BIG5_parse_rowcol),
    _CITRUS_PROP_HINT_NUM("col", &_citrus_BIG5_parse_rowcol),
    _CITRUS_PROP_HINT_NUM("excludes", &_citrus_BIG5_parse_excludes),
    _CITRUS_PROP_HINT_END
};

static int
/*ARGSUSED*/
_citrus_BIG5_encoding_module_init(_BIG5EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	int i;

	_DIAGASSERT(ei != NULL);

	memset((void *)ei, 0, sizeof(*ei));
	for (i = 0; i <= 0x7f; ++i)
		ei->cells[i] |= S;
	TAILQ_INIT(&ei->excludes);

	if (lenvar > 0 && var != NULL)
		return _citrus_prop_parse_variable(root_hints,
		    (void *)ei, (const char *)var, lenvar);

	/* fallback mapping */
	_citrus_BIG5_parse_rowcol((void **)&ei, "row", 0xa1, 0xfe);
	_citrus_BIG5_parse_rowcol((void **)&ei, "col", 0x40, 0x7e);
	_citrus_BIG5_parse_rowcol((void **)&ei, "col", 0xa1, 0xfe);

	return 0;
}

static void
/*ARGSUSED*/
_citrus_BIG5_encoding_module_uninit(_BIG5EncodingInfo *ei)
{
	_BIG5Exclude *exclude;

	_DIAGASSERT(ei != NULL);

	while ((exclude = TAILQ_FIRST(&ei->excludes)) != NULL) {
		TAILQ_REMOVE(&ei->excludes, exclude, entry);
		free(exclude);
	}
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for ctype
 */

_CITRUS_CTYPE_DECLS(BIG5);
_CITRUS_CTYPE_DEF_OPS(BIG5);

#include "citrus_ctype_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(BIG5);
_CITRUS_STDENC_DEF_OPS(BIG5);

#include "citrus_stdenc_template.h"
