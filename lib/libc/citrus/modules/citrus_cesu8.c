/*-
 * Copyright (c)2019, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_cesu8.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

#define CESU8_UTF32_LEN_MAX	6
#define CESU8_UTF16_LEN_MAX	3

typedef struct {
	size_t chlen;
	char ch[CESU8_UTF32_LEN_MAX];
} _CESU8State;

typedef struct {
	int flags;
#define USE_ALTERNATE_NUL	0x1
} _CESU8EncodingInfo;

#define _FUNCNAME(m)			_citrus_CESU8_##m
#define _ENCODING_INFO			_CESU8EncodingInfo
#define _CTYPE_INFO			_CESU8CTypeInfo
#define _ENCODING_STATE			_CESU8State
#define _ENCODING_IS_STATE_DEPENDENT	0
#define _STATE_NEEDS_EXPLICIT_INIT(ps)	0

static const char cesu8mask[CESU8_UTF16_LEN_MAX + 1] = {
	0x00, /* dummy */
	0x7f, 0x1f, 0x0f,
};

static const char cesu8bit[CESU8_UTF16_LEN_MAX + 1] = {
	0x00, /* dummy */
	0x00, 0xc0, 0xe0,
};

static const char cesu8len[UCHAR_MAX + 1] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 1 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 2 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 3 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 4 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 5 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 6 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 7 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 8 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 9 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* a */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* b */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* c */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* d */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* e */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
/* f */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};

static __inline size_t
utf16len(_CESU8EncodingInfo * ei, uint16_t c16)
{
	if ((ei->flags & USE_ALTERNATE_NUL) && c16 == 0)
		return 2;
	else if ((c16 & ~0x7f) == 0)
		return 1;
	else if ((c16 & ~0x7ff) == 0)
		return 2;
	return 3;
}

static __inline void
/*ARGSUSED*/
_citrus_CESU8_init_state(_CESU8EncodingInfo * __restrict ei,
    _CESU8State * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_CESU8_pack_state(_CESU8EncodingInfo * __restrict ei,
    void * __restrict pspriv, const _CESU8State * __restrict psenc)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_CESU8_unpack_state(_CESU8EncodingInfo * __restrict ei,
    _CESU8State * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

static __inline int
/*ARGSUSED*/
_citrus_CESU8_decode16(_CESU8EncodingInfo * __restrict ei,
    uint16_t * __restrict pc16, const char * __restrict s, size_t n,
    _CESU8State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	size_t len, i;
	uint16_t c16;

	_DIAGASSERT(pc16 != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	len = cesu8len[(unsigned char)s[0]];
	switch (len) {
	case 0:
		return EILSEQ;
	case 1:
		c16 = (unsigned char)s[0];
		break;
	default:
		if (n < len) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c16 = s[0] & cesu8mask[len];
		for (i = 1; i < len; ++i) {
			if ((s[i] & 0xc0) != 0x80)
				return EILSEQ;
			c16 <<= 6;
			c16 |= s[i] & 0x3f;
		}
		if (utf16len(ei, c16) != len)
			return EILSEQ;
	}
	*pc16 = c16;
	*nresult = len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_CESU8_decode(_CESU8EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _CESU8State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	uint16_t hi, lo;
	size_t hinr, lonr;
	int ret;

	ret = _citrus_CESU8_decode16(ei, &hi, s, n, psenc, &hinr, rstate);
	if (ret)
		return ret;
	if (!is_hi_surrogate((uint32_t)hi)) {
		*pwc = (wchar_t)(uint32_t)hi;
		*nresult = hinr;
		return 0;
	}
	ret = _citrus_CESU8_decode16(ei, &lo, s + hinr, n - hinr, psenc, &lonr, rstate);
	if (ret)
		return ret;
	if (!is_lo_surrogate((uint32_t)lo))
		return EILSEQ;
	*pwc = (wchar_t)utf16to32(hi, lo);
	*nresult = hinr + lonr;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_CESU8_encode16(_CESU8EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, uint16_t c16,
    _CESU8State * __restrict psenc, size_t * __restrict nresult)
{
	size_t len;
	char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if (n < 1)
		return E2BIG;
	len = utf16len(ei, c16);
	switch (len) {
	case 0:
		return EILSEQ;
	case 1:
		*s = (unsigned char)c16;
		break;
	default:
		if (n < len)
			return E2BIG;
		for (t = &s[len - 1]; s < t; --t) {
			*t = (c16 & 0x3f) | 0x80;
			c16 >>= 6;
		}
		*t = c16 | cesu8bit[len];
	}
	*nresult = len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_CESU8_encode(_CESU8EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _CESU8State * __restrict psenc, size_t * __restrict nresult)
{
	uint32_t c32;
	uint16_t c16[2];
	int ret, len, i;
	size_t siz, nr;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(s != NULL);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	if (c32 <= UTF16_MAX) {
		c16[0] = (uint16_t)c32;
		len = 1;
	} else if (c32 <= UTF32_MAX) {
		utf32to16(c32, &c16[0], &c16[1]);
		len = 2;
	} else {
		return EILSEQ;
	}
	siz = 0;
	for (i = 0; i < len; ++i) {
		ret = _citrus_CESU8_encode16(ei, s, n, c16[i], psenc, &nr);
		if (ret != 0)
			return ret;
		s += nr;
		n -= nr;
		siz += nr;
	}
	*nresult = siz;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_CESU8_stdenc_wctocs(_CESU8EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wc;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_CESU8_stdenc_cstowc(_CESU8EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	if (csid != 0)
		return EILSEQ;
	*pwc = (wchar_t)idx;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_CESU8_encoding_module_init(_CESU8EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	const char *p;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(var != NULL || lenvar < 1);

	memset(ei, 0, sizeof(*ei));
	if (lenvar > 0) {
		p = (const char *)var;
		if (!_bcs_strncasecmp(p, "MUTF-8", lenvar))
			 ei->flags = USE_ALTERNATE_NUL;
	}
	return 0;
}

static void
/*ARGSUSED*/
_citrus_CESU8_encoding_module_uninit(_CESU8EncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(CESU8);
_CITRUS_STDENC_DEF_OPS(CESU8);

#include "citrus_stdenc_template.h"
