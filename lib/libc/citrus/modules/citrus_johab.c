/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_johab.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct {
	size_t chlen;
	char ch[2];
} _JOHABState;

typedef struct {
	int dummy;
} _JOHABEncodingInfo;

#define _FUNCNAME(m)			_citrus_JOHAB_##m
#define _ENCODING_INFO			_JOHABEncodingInfo
#define _CTYPE_INFO			_JOHABCTypeInfo
#define _ENCODING_STATE			_JOHABState
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static __inline void
/*ARGSUSED*/
_citrus_JOHAB_init_state(_JOHABEncodingInfo * __restrict ei,
    _JOHABState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
}

static __inline void
/*ARGSUSED*/
_citrus_JOHAB_pack_state(_JOHABEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _JOHABState * __restrict psenc)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(pspriv, psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_JOHAB_unpack_state(_JOHABEncodingInfo * __restrict ei,
    _JOHABState * __restrict psenc, const void * __restrict pspriv)
{
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy(psenc, pspriv, sizeof(*psenc));
}

#define S	0x1		/* single byte			*/
#define H	0x2		/* lead byte of Hangle		*/
#define U	0x4		/* lead byte of UDA		*/
#define K	(H|U)		/* lead byte of Hangle or UDA	*/
#define J	0x8		/* lead byte of Hanji		*/
#define L	(K|J)		/* lead byte of JOHAB		*/
#define G	(H<<4)		/* trail byte of Hangle		*/
#define D	((U<<4)|(J<<4))	/* trail byte of UDA or Hanji	*/
#define T	(G|D)		/* trail byte of JOHAB		*/

static const unsigned char johab[] = {
/*      0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f */
/* 0 */ S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,
/* 1 */ S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,
/* 2 */ S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,S  ,
/* 3 */ S  ,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,S|D,
/* 4 */ S|D,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 5 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 6 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,
/* 7 */ S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S|T,S  ,
/* 8 */   0,  G,  G,  G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,H|G,
/* 9 */ H|G,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,
/* a */ H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,
/* b */ H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,
/* c */ H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,H|T,
/* d */ H|T,H|T,H|T,H|T,  T,  T,  T,  T,U|T,J|T,J|T,J|T,J|T,J|T,J|T,  T,
/* e */ J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,
/* f */ J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,J|T,  T,  T,  T,  T,  T,  0,
};

static int
/*ARGSUSED*/
_citrus_JOHAB_decode(_JOHABEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _JOHABState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	int c;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	if (n < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = johab[(unsigned char)s[0]];
	if (c & S) {
		*pwc = (unsigned char)s[0];
		*nresult = 1;
	} else if (c & L) {
		if (n < 2) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c = (c & L) << 4;
		if ((johab[(unsigned char)s[1]] & c) == 0)
			return EILSEQ;
		*pwc = (unsigned char)s[0] << 8 |
		       (unsigned char)s[1];
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_JOHAB_encode(_JOHABEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _JOHABState * __restrict psenc, size_t * __restrict nresult)
{
	int c;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(nresult != NULL);

	if ((wc & ~0xff) == 0) {
		if (n < 1)
			return E2BIG;
		s[0] = wc & 0xff;
		if ((johab[(unsigned char)s[0]] & S) == 0)
			return EILSEQ;
		*nresult = 1;
	} else if ((wc & ~0xffff) == 0) {
		if (n < 2)
			return E2BIG;
		s[0] = (wc >> 8) & 0xff;
		s[1] =  wc       & 0xff;
		c = johab[(unsigned char)s[0]];
		if ((c & L) == 0)
			return EILSEQ;
		c = (c & L) << 4;
		if ((johab[(unsigned char)s[1]] & c) == 0)
			return EILSEQ;
		*nresult = 2;
	} else {
		return EILSEQ;
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_JOHAB_stdenc_wctocs(_JOHABEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	int l, t, c, m, linear;

	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	if ((wc & ~0xff) == 0) {
		*csid = 0;
		*idx = (_index_t)wc;
	} else if ((wc & ~0xffff) == 0) {
		l = (wc >> 8) & 0xff;
		t =  wc       & 0xff;
		c = johab[l];
		if (c & K) {
			*csid = 1;
			*idx = (_index_t)wc;
		} else if (c & J) {
			if (l >= 0xd9 && l <= 0xde) {
				linear = l - 0xd9;
				m = 0x21;
			} else if (l >= 0xe0 && l <= 0xf9) {
				linear = l - 0xe0;
				m = 0x4a;
			} else {
				return EILSEQ;
			}
			linear *= 188;
			if (t >= 0x31 && t <= 0x7e)
				linear += t - 0x31;
			else if (t >= 0x91 && t <= 0xfe)
				linear += t - 0x43;
			else
				return EILSEQ;
			l = (linear / 94) + m;
			t = (linear % 94) + 0x21;
			*csid = 2;
			*idx = (_index_t)((l << 8) | t);
		} else {
			return EILSEQ;
		}
	} else {
		return EILSEQ;
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_JOHAB_stdenc_cstowc(_JOHABEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	int m, n, l, t, linear;

	_DIAGASSERT(pwc != NULL);

	switch (csid) {
	case 0:
	case 1:
		*pwc = (wchar_t)idx;
		break;
	case 2:
		l = (idx >> 8) & 0xff;
		t =  idx       & 0xff;
		if (idx >= 0x2121 && idx <= 0x2c7e) {
			m = 0xd9;
			n = 0x21;
		} else if (idx >= 0x4a21 && idx <= 0x7d7e) {
			m = 0xe0;
			n = 0x4a;
		} else {
			return EILSEQ;
		}
		l -= n;
		t -= 0x21;
		linear = (l * 94) + t;
		l = (linear / 188) + m;
		t = linear % 188;
		t += (t <= 0x4d) ? 0x31 : 0x43;
		*pwc = (wchar_t)((l << 8) | t);
		break;
	default:
		return EILSEQ;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_JOHAB_encoding_module_init(_JOHABEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_JOHAB_encoding_module_uninit(_JOHABEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(JOHAB);
_CITRUS_STDENC_DEF_OPS(JOHAB);

#include "citrus_stdenc_template.h"
