/*-
 * Copyright (c)2019, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_utf5.h"

/*
 * ----------------------------------------------------------------------
 * private stuffs used by templates
 */

#define UTF5_MB_CUR_MAX	8

static const char *base32 = "0123456789ABCDEFGHIJKLMNOPQRSTUV";

static const int utf5len[0x100] = {
/*      0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f */
/* 0 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 1 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 2 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 3 */  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
/* 4 */ -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
/* 5 */ 25, 26, 27, 28, 29, 30, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 6 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 7 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 8 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* 9 */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* a */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* b */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* c */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* d */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* e */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
/* f */ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
};

static __inline size_t
ucs4len(uint32_t c32)
{
	if ((c32 & ~0xf) == 0) {
		return 1;
	} else if ((c32 & ~0xff) == 0) {
		return 2;
	} else if ((c32 & ~0xfff) == 0) {
		return 3;
	} else if ((c32 & ~0xffff) == 0) {
		if (!is_surrogate(c32))
			return 4;
        } else if ((c32 & ~0xfffff) == 0) {
		return 5;
        } else if ((c32 & ~0xffffff) == 0) {
		return 6;
        } else if ((c32 & ~0xfffffff) == 0) {
		return 7;
        } else if ((c32 & ~0x7ffffff) == 0) {
		return 8;
	}
        return 0;
}

typedef struct {
	int dummy;
} _UTF5EncodingInfo;

typedef struct {
	int chlen;
	char ch[UTF5_MB_CUR_MAX];
} _UTF5State;

#define _FUNCNAME(m)			_citrus_UTF5_##m
#define _ENCODING_INFO			_UTF5EncodingInfo
#define _CTYPE_INFO			_UTF5CTypeInfo
#define _ENCODING_STATE			_UTF5State
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _MBTOCS_NEEDS_STATE_RESET		1
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	0

static __inline void
/*ARGSUSED*/
_citrus_UTF5_init_state(_UTF5EncodingInfo * __restrict ei,
    _UTF5State * __restrict s)
{
	_DIAGASSERT(s != NULL);

	memset((void *)s, 0, sizeof(*s));
}

static __inline void
/*ARGSUSED*/
_citrus_UTF5_pack_state(_UTF5EncodingInfo * __restrict ei,
    void *__restrict pspriv, const _UTF5State * __restrict s)
{
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(s != NULL);

	memcpy(pspriv, (const void *)s, sizeof(*s));
}

static __inline void
/*ARGSUSED*/
_citrus_UTF5_unpack_state(_UTF5EncodingInfo * __restrict ei,
    _UTF5State * __restrict s, const void * __restrict pspriv)
{
	_DIAGASSERT(s != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy((void *)s, pspriv, sizeof(*s));
}

static int
/*ARGSUSED*/
_citrus_UTF5_decode(_UTF5EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _UTF5State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	const char *t;
	int c, bit;
	uint32_t c32;
	size_t len, i;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	t = s;
	if (n-- < 1) {
		*rstate = _STDENC_SDGEN_INITIAL;
		return E2BIG;
	}
	c = (unsigned char)*t++;
	bit = utf5len[c];
	if (bit == -1 || (bit & 0x10) == 0)
		return EILSEQ;
	c32 = bit & 0xf;
	for (i = 1; i < UTF5_MB_CUR_MAX; ++i) {
		if (n < 1) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c = (unsigned char)*t;
		bit = utf5len[c];
		if (bit == -1)
			return EILSEQ;
		if (bit & 0x10)
			break;
		c32 <<= 4;
		c32 |= bit & 0xf;
		--n;
		++t;
	}
	len = t - s;
	if (ucs4len(c32) != len)
		return EILSEQ;
	*pwc = c32;
	*nresult = len;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF5_encode(_UTF5EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _UTF5State * __restrict psenc, size_t * __restrict nresult)
{
	uint32_t c32;
	size_t len;
	char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	len = ucs4len(c32);
	if (len < 1)
		return EILSEQ;
	if (n < len)
		return E2BIG;
	for (t = &s[len - 1]; s < t; --t) {
		*t = base32[c32 & 0xf];
		c32 >>= 4;
	}
	*t = base32[(c32 & 0xf) | 0x10];
	*nresult = len;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF5_stdenc_wctocs(_UTF5EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wchar)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wchar;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF5_stdenc_cstowc(_UTF5EncodingInfo * __restrict ei,
    wchar_t * __restrict wchar, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(wchar != NULL);

	if (csid != 0)
		return EILSEQ;
	*wchar = (wchar_t)idx;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF5_mbrtowc_state_reset(_UTF5EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _UTF5State * __restrict psenc,
    size_t * __restrict nresult)
{
	int ret = 0;
	size_t nr = 0;
	int rstate;

	if (psenc->chlen > 0) {
		psenc->ch[psenc->chlen++] = 'G';
		ret = _citrus_UTF5_decode(ei, pwc, psenc->ch, psenc->chlen,
		    psenc, &nr, &rstate);
		psenc->chlen = 0;
	}
	*nresult = nr;
	return ret;
}

static int
/*ARGSUSED*/
_citrus_UTF5_encoding_module_init(_UTF5EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_UTF5_encoding_module_uninit(_UTF5EncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/*
 * ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(UTF5);
_CITRUS_STDENC_DEF_OPS(UTF5);

#include "citrus_stdenc_template.h"
