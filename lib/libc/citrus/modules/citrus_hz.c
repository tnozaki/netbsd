/*-
 * Copyright (c)2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#include <sys/queue.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_hz.h"

#define L	0x1	/* lead byte */
#define T	0x2	/* trail byte */
#define B	(L|T)	/* both */

static const char hz[0x80] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 1 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 2 */ 0, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
/* 3 */ B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
/* 4 */ B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
/* 5 */ B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
/* 6 */ B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B,
/* 7 */ B, B, B, B, B, B, B, B, B, B, B, B, B, B, T, 0,
};

#define GB2312_IN	0x7b
#define GB2312_OUT	0x7d
#define ESCAPE_CHAR	0x7e

typedef struct {
	int msb;
} _HZEncodingInfo;

typedef struct {
	size_t chlen;
	char ch[MB_LEN_MAX];
	_Bool gb2312;
} _HZState;

#define _FUNCNAME(m)			_citrus_HZ_##m
#define _ENCODING_INFO			_HZEncodingInfo
#define _CTYPE_INFO			_HZCTypeInfo
#define _ENCODING_STATE			_HZState
#define _ENCODING_IS_STATE_DEPENDENT		1
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	((_ps_)->gb2312)

static __inline void
/*ARGSUSED*/
_citrus_HZ_init_state(_HZEncodingInfo * __restrict ei,
    _HZState * __restrict psenc)
{
	/* ei may be unused */
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
	psenc->gb2312 = false;
}

static __inline void
/*ARGSUSED*/
_citrus_HZ_pack_state(_HZEncodingInfo * __restrict ei,
    void *__restrict pspriv, const _HZState * __restrict psenc)
{
	/* ei may be unused */
	_DIAGASSERT(pspriv != NULL);
	_DIAGASSERT(psenc != NULL);

	memcpy(pspriv, (const void *)psenc, sizeof(*psenc));
}

static __inline void
/*ARGSUSED*/
_citrus_HZ_unpack_state(_HZEncodingInfo * __restrict ei,
    _HZState * __restrict psenc, const void * __restrict pspriv)
{
	/* ei may be unused */
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(pspriv != NULL);

	memcpy((void *)psenc, pspriv, sizeof(*psenc));
}

static int
/*ARGSUSED*/
_citrus_HZ_decode(_HZEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _HZState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	const char *t;
	_Bool gb2312;
	int c;
	wchar_t wc;

	/* ei may be unused */
	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	t = s;
	gb2312 = psenc->gb2312;
restart:
	if (!gb2312) {
		if (n-- < 1) {
			*rstate = _STDENC_SDGEN_INITIAL;
			return E2BIG;
		}
		c = (unsigned char)*t++;
		if (c == ESCAPE_CHAR) {
			if (n-- < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_SHIFT;
				return E2BIG;
			}
			c = (unsigned char)*t++;
			switch (c) {
			case ESCAPE_CHAR:
				break;
			case GB2312_IN:
				gb2312 = true;
			/*FALLTHROUGH*/
			case '\n':
				goto restart;
			default:
				return EILSEQ;
			}
		}
		if (c & 0x80)
			return EILSEQ;
		wc = c;
	} else {
		if (n-- < 1) {
			*rstate = _STDENC_SDGEN_STABLE;
			return E2BIG;
		}
		c = (unsigned char)*t++;
		if (c == ESCAPE_CHAR) {
			if (n-- < 1) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_SHIFT;
				return E2BIG;
			}
			c = (unsigned char)*t++;
			if (c != GB2312_OUT)
				return EILSEQ;
			gb2312 = false;
			goto restart;
		}
		if ((c & 0x80) != ei->msb)
			return EILSEQ;
		c &= ~ei->msb;
		if ((hz[c] & L) == 0)
			return EILSEQ;
		wc = c << 8;
		if (n-- < 1) {
			*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		c = (unsigned char)*t++;
		if ((c & 0x80) != ei->msb)
			return EILSEQ;
		c &= ~ei->msb;
		if ((hz[c] & T) == 0)
			return EILSEQ;
		wc |= c;
	}
	*pwc = wc;
	psenc->gb2312 = gb2312;
	*nresult = (size_t)(t - s);
	return 0;
}

static int
/*ARGSUSED*/
_citrus_HZ_encode(_HZEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _HZState * __restrict psenc, size_t * __restrict nresult)
{
	char *t;
	_Bool gb2312;
	int c;

	/* ei may be unused */
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	gb2312 = psenc->gb2312;
	if ((wc & ~0x7f) == 0) {
		if (gb2312) {
			if (n < 2)
				return E2BIG;
			*t++ = ESCAPE_CHAR;
			*t++ = GB2312_OUT;
			n -= 2;
			gb2312 = false;
		}
		if (wc == ESCAPE_CHAR) {
			if (n-- < 1)
				return E2BIG;
			*t++ = ESCAPE_CHAR;
		}
		if (n < 1)
			return E2BIG;
		*t++ = wc;
	} else if ((wc & ~0x7f7f) == 0) {
		if (!gb2312) {
			if (n < 2)
				return E2BIG;
			*t++ = ESCAPE_CHAR;
			*t++ = GB2312_IN;
			n -= 2;
			gb2312 = true;
		}
		if (n < 2)
			return E2BIG;
		c = wc >> 8 & 0xff;
		if ((hz[c] & L) == 0)
			return EILSEQ;
		*t++ = c | ei->msb;
		c = wc & 0xff;
		if ((hz[c] & T) == 0)
			return EILSEQ;
		*t++ = c | ei->msb;
	} else {
		return EILSEQ;
	}
	*nresult = (size_t)(t - s);
	psenc->gb2312 = gb2312;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_HZ_put_state_reset(_HZEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, _HZState * __restrict psenc,
    size_t * __restrict nresult)
{
	char *t;
	_Bool gb2312;

	/* ei may be unused */
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	gb2312 = psenc->gb2312;
	if (gb2312) {
		if (n < 2)
			return E2BIG;
		*t++ = ESCAPE_CHAR;
		*t++ = GB2312_OUT;
		gb2312 = false;
		n -= 2;
	}
	*nresult = (size_t)(t - s);
	psenc->gb2312 = gb2312;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_HZ_stdenc_wctocs(_HZEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	if ((wc & ~0x7f) == 0) {
		*csid = (_csid_t)0;
	} else if ((wc & ~0x7f7f) == 0) {
		*csid = (_csid_t)1;
	} else {
		return EILSEQ;
	}
	*idx = (_index_t)wc;

	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_HZ_stdenc_cstowc(_HZEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	switch (csid) {
	case 0:
	case 1:
		*pwc = (wchar_t)idx;
		break;
	default:
		return EINVAL;
	}

	return 0;
}

static int
/*ARGSUSED*/
_citrus_HZ_encoding_module_init(_HZEncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	const char *p;

	_DIAGASSERT(ei != NULL);

	p = var;
#define MATCH(x, act)						\
do {								\
        if (lenvar >= (sizeof(#x)-1) &&				\
            _bcs_strncasecmp(p, #x, sizeof(#x)-1) == 0) {	\
                act;						\
                lenvar -= sizeof(#x)-1;				\
                p += sizeof(#x)-1;				\
        }							\
} while (/*CONSTCOND*/0)
	memset((void *)ei, 0, sizeof(*ei));
	while (lenvar > 0) {
		switch (_bcs_toupper(*p)) {
		case '8':
			MATCH(8BIT, ei->msb = 0x80);
			break;
		}
		++p;
		--lenvar;
	}

	return 0;
}

static void
/*ARGSUSED*/
_citrus_HZ_encoding_module_uninit(_HZEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(HZ);
_CITRUS_STDENC_DEF_OPS(HZ);

#include "citrus_stdenc_template.h"
