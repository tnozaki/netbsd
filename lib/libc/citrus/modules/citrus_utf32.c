/*-
 * Copyright (c)2015, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>

#include <sys/endian.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_unicode.h"
#include "citrus_utf32.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef enum {
        _ENDIAN_UNKNOWN = 0,
        _ENDIAN_BIG     = 1,
        _ENDIAN_LITTLE  = 2,
} _UTF32Endian;

typedef struct {
	_UTF32Endian byteorder;
	char ch[4];
	int order[4];
} _UTF32Bom;

static const _UTF32Bom big = {
	_ENDIAN_BIG,
	{ 0x00, 0x00, 0xfe, 0xff },
	{ 3, 2, 1, 0 }
};
static const _UTF32Bom little = {
	_ENDIAN_LITTLE,
	{ 0xff, 0xfe, 0x00, 0x00 },
	{ 0, 1, 2, 3 }
};
static const _UTF32Bom *bom[] = {
#if _BYTE_ORDER == _BIG_ENDIAN
	&big,
#else
	&little,
#endif
	&big, &little
};

typedef struct {
	_UTF32Endian byteorder;
	size_t mb_cur_max;
} _UTF32EncodingInfo;

typedef struct {
	size_t chlen;
	char ch[8];
	_UTF32Endian byteorder;
} _UTF32State;

#define _FUNCNAME(m)			_citrus_UTF32_##m
#define _ENCODING_INFO			_UTF32EncodingInfo
#define _ENCODING_STATE			_UTF32State
#define _ENCODING_IS_STATE_DEPENDENT		0
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	1

static __inline void
_citrus_UTF32_init_state(_UTF32EncodingInfo * __restrict ei,
    _UTF32State * __restrict psenc)
{
	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(psenc != NULL);

	psenc->chlen = 0;
	psenc->byteorder = ei->byteorder;
}

static int
/*ARGSUSED*/
_citrus_UTF32_decode(_UTF32EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _UTF32State * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	const _UTF32Bom *bp;
	size_t i;
	uint32_t c32;

	_DIAGASSERT(pwc != NULL);
	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);
	_DIAGASSERT(rstate != NULL);

	switch (psenc->byteorder) {
	case _ENDIAN_UNKNOWN:
		if (n < 8) {
			*rstate = (n == 0)
			    ? _STDENC_SDGEN_INITIAL
			    : _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		for (i = 1; i < __arraycount(bom); ++i) {
			bp = bom[i];
			if (!memcmp(s, &bp->ch[0], 4))
				goto found;
		}
		return EILSEQ;
found:
		psenc->byteorder = bp->byteorder;
		s += 4;
		*nresult = 8;
		break;
	case _ENDIAN_BIG:
	case _ENDIAN_LITTLE:
		if (n < 4) {
			*rstate = (n == 0)
			    ? _STDENC_SDGEN_INITIAL
			    : _STDENC_SDGEN_INCOMPLETE_CHAR;
			return E2BIG;
		}
		bp = bom[psenc->byteorder];
		*nresult = 4;
		break;
	default:
        	return EINVAL;
	}
	c32 = (s[bp->order[0]] & 0xff)       |
	      (s[bp->order[1]] & 0xff) <<  8 |
	      (s[bp->order[2]] & 0xff) << 16 |
	      (s[bp->order[3]] & 0xff) << 24;
	if (is_surrogate(c32))
		return EILSEQ;
	*pwc = c32;
	return 0;
}

static int
/*ARGSUSED*/
_citrus_UTF32_encode(_UTF32EncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _UTF32State * __restrict psenc, size_t * __restrict nresult)
{
	const _UTF32Bom *bp;
	uint32_t c32;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	c32 = (uint32_t)wc;
	if (is_surrogate(c32))
		return EILSEQ;
	switch (psenc->byteorder) {
	case _ENDIAN_UNKNOWN:
		if (n < 8)
			return E2BIG;
		bp = bom[psenc->byteorder];
		psenc->byteorder = bp->byteorder;
		memcpy(s, bp->ch, 4);
		s += 4;
		*nresult = 8;
		break;
	case _ENDIAN_BIG:
	case _ENDIAN_LITTLE:
		if (n < 4)
			return E2BIG;
		bp = bom[psenc->byteorder];
		*nresult = 4;
		break;
	default:
        	return EINVAL;
	}
	s[bp->order[3]] = (c32 >> 24) & 0xff;
	s[bp->order[2]] = (c32 >> 16) & 0xff;
	s[bp->order[1]] = (c32 >> 8) & 0xff;
	s[bp->order[0]] = c32 & 0xff;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF32_stdenc_wctocs(_UTF32EncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx,
    wchar_t wc)
{
	_DIAGASSERT(csid != NULL);
	_DIAGASSERT(idx != NULL);

	*csid = 0;
	*idx = (_index_t)wc;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_UTF32_stdenc_cstowc(_UTF32EncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, _csid_t csid, _index_t idx)
{
	_DIAGASSERT(pwc != NULL);

	if (csid != 0)
		return EILSEQ;
	*pwc = (wchar_t)idx;
	return 0;
}

static int
_citrus_UTF32_encoding_module_init(_UTF32EncodingInfo * __restrict ei,
    const void * __restrict var, size_t lenvar)
{
        const char *p;

	_DIAGASSERT(ei != NULL);
	_DIAGASSERT(var != NULL || lenvar < 1);

	if (lenvar > 0) {
		p = (const char *)var;
		if (!_bcs_strncasecmp(p, "UTF32BE", lenvar)) {
			ei->byteorder = _ENDIAN_BIG;
			ei->mb_cur_max = 4;
			goto done;
		} else if (!_bcs_strncasecmp(p, "UTF32LE", lenvar)) {
			ei->byteorder = _ENDIAN_LITTLE;
			ei->mb_cur_max = 4;
			goto done;
		}
	}
	ei->byteorder = _ENDIAN_UNKNOWN;
	ei->mb_cur_max = 8;
done:
	return 0;
}

static void
/*ARGSUSED*/
_citrus_UTF32_encoding_module_uninit(_UTF32EncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(UTF32);
_CITRUS_STDENC_DEF_OPS(UTF32);

#include "citrus_stdenc_template.h"
