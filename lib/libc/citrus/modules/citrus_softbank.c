/*-
 * Copyright (c)2016, 2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_module.h"
#include "citrus_stdenc.h"

#include "citrus_softbank.h"

/* ----------------------------------------------------------------------
 * private stuffs used by templates
 */

typedef struct _SoftBankState {
	int chlen;
	char ch[5];
	int charset;
} _SoftBankState;

typedef struct {
	int dummy;
} _SoftBankEncodingInfo;

#define _FUNCNAME(m)			_citrus_SoftBank_##m
#define _ENCODING_INFO			_SoftBankEncodingInfo
#define _CTYPE_INFO			_SoftBankCTypeInfo
#define _ENCODING_STATE			_SoftBankState
#define _ENCODING_IS_STATE_DEPENDENT		1
#define _STATE_NEEDS_EXPLICIT_INIT(_ps_)	((_ps_)->charset != 0)

#define SI	(0x0F)
#define ESC	(0x1B)

static __inline int
islead(int c)
{

	return (c >= 0x81 && c <= 0x9F) || (c >= 0xE0 && c <= 0xFC);
}

static __inline int
istrail(int c)
{
	return (c >= 0x40 && c <= 0x7E) || (c >= 0x80 && c <= 0xFC);
}

static __inline int
is94cs(int c)
{
	return c >= 0x21 && c <= 0x7E;
}

static __inline int
seqmatch(int c)
{
	return memchr("EFGOPQ", c, 6) != NULL;
}

static __inline void
_citrus_SoftBank_init_state(_SoftBankEncodingInfo * __restrict ei,
    _SoftBankState * __restrict s)
{
	s->chlen = 0;
	s->charset = 0;
}

static __inline void
_citrus_SoftBank_pack_state(_SoftBankEncodingInfo * __restrict ei,
    void * __restrict pspriv, const _SoftBankState * __restrict s)
{
	memcpy(pspriv, (const void *)s, sizeof(*s));
}

static __inline void
_citrus_SoftBank_unpack_state(_SoftBankEncodingInfo * __restrict ei,
    _SoftBankState * __restrict s, const void * __restrict pspriv)
{
	memcpy((void *)s, pspriv, sizeof(*s));
}

static int
_citrus_SoftBank_decode(_SoftBankEncodingInfo * __restrict ei,
    wchar_t * __restrict pwc, const char * __restrict s, size_t n,
    _SoftBankState * __restrict psenc, size_t * __restrict nresult,
    int * __restrict rstate)
{
	const char *t;
	int c0, c1;
	wchar_t wc;

	t = s;
restart:
	if (n < 1) {
		*rstate = _STDENC_SDGEN_STABLE;
		return E2BIG;
	}
	c0 = (unsigned char)s[0];
	if (psenc->charset == 0) {
		if (c0 == ESC) {
			if (n < 3) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_SHIFT;
				return E2BIG;
			}
			c0 = (unsigned char)s[1];
			c1 = (unsigned char)s[2];
			if (c0 != '$' || !seqmatch(c1))
				return EILSEQ;
			psenc->charset = c1;
			s += 3;
			n -= 3;
			goto restart;
		}
		if (!islead(c0)) {
			wc = (wchar_t)c0;
			++s;
		} else {
			if (n < 2) {
				*rstate = _STDENC_SDGEN_INCOMPLETE_CHAR;
				return E2BIG;
			}
			c1 = (unsigned char)s[1];
			if (!istrail(c1))
				return EILSEQ;
			wc = (wchar_t)((c0 << 8) | c1);
			s += 2;
		}
	} else {
		if (c0 == SI) {
			psenc->charset = 0;
			++s;
			--n;
			goto restart;
		}
		if (!is94cs(c0))
			return EILSEQ;
		wc = (wchar_t)((psenc->charset << 24) | c0);
		++s;
	}
	*pwc = wc;
	*nresult = (size_t)(s - t);
	return 0;
}

static int
/*ARGSUSED*/
_citrus_SoftBank_encode(_SoftBankEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _SoftBankState * __restrict psenc, size_t * __restrict nresult)
{
	int c0, c1;

	if (psenc->charset != 0 && !seqmatch(psenc->charset))
		return EINVAL;
	if (wc & ~0xffff) {
		c0 = (wc >> 24) & 0xff;
		if (!seqmatch(c0))
			return EINVAL;
		c1 = wc & 0xff;
		if (!is94cs(c1))
			return EILSEQ;
		if (psenc->charset == c0) {
			if (n < 1)
				return E2BIG;
			s[0] = c1;
			*nresult = 1;
		} else {
			if (psenc->charset == 0) {
				if (n < 4)
					return E2BIG;
				s[0] = ESC;
				s[1] = '$';
				s[2] = c0;
				s[3] = c1;
				*nresult = 4;
			} else {
				if (n < 5)
					return E2BIG;
				s[0] = SI;
				s[1] = ESC;
				s[2] = '$';
				s[3] = c0;
				s[4] = c1;
				*nresult = 5;
			}
			psenc->charset = c0;
		}
	} else if (wc & ~0xff) {
		c0 = (wc >> 8) & 0xff;
		c1 = wc & 0xff;
		if (!islead(c0) || !istrail(c1))
			return EILSEQ;
		if (psenc->charset == 0) {
			if (n < 2)
				return E2BIG;
			s[0] = c0;
			s[1] = c1;
			*nresult = 2;
		} else {
			if (n < 3)
				return E2BIG;
			s[0] = SI;
			s[1] = c0;
			s[2] = c1;
			*nresult = 3;
			psenc->charset = 0;
		}
	} else {
		c0 = wc & 0xff;
		if (islead(c0))
			return EILSEQ;
		if (psenc->charset == 0) {
			if (n < 1)
				return E2BIG;
			s[0] = c0;
			*nresult = 1;
		} else {
			if (n < 2)
				return E2BIG;
			s[0] = SI;
			s[1] = c0;
			*nresult = 2;
			psenc->charset = 0;
		}
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_SoftBank_put_state_reset(_SoftBankEncodingInfo * __restrict ei,
    char * __restrict s, size_t n, _SoftBankState * __restrict psenc,
    size_t * __restrict nresult)
{
	char *t;

	_DIAGASSERT(s != NULL || n < 1);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	t = s;
	if (psenc->charset != 0) {
		if (n-- < 1) {
			*nresult = (size_t)-1;
			return E2BIG;
		}
		*t++ = SI;
		psenc->charset = 0;
	}
	*nresult = t - s;
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_SoftBank_stdenc_wctocs(_SoftBankEncodingInfo * __restrict ei,
    _csid_t * __restrict csid, _index_t * __restrict idx, wchar_t wc)
{
	int charset, row, col;

	if (wc <= 0x7F) {
		*csid = 0;
		*idx = wc;
	} else if (wc <= 0xFF) {
		*csid = 1;
		*idx = wc & 0x7F;
	} else if (wc <= 0xFFFF) {
		*csid = 2;
	 	row = (wc >> 8) & 0xFF;
		if (!islead(row))
			return EILSEQ;
		col = wc & 0xFF;
		if (!istrail(col))
			return EILSEQ;
		row -= 0x81;
		if (row >= 0x5F)
			row -= 0x40;
		row = row * 2 + 0x21;
		col -= 0x1F;
		if (col >= 0x61)
			col -= 1;
		if (col > 0x7E) {
			row += 1;
			col -= 0x5E;
		}
		*idx = (row << 8) | col;
	} else {
		charset = (wc >> 24) & 0xFF;
		if (!seqmatch(charset))
			return EINVAL;
		*csid = charset << 24;
		*idx = wc & 0xFF;
	}
	return 0;
}

static __inline int
/*ARGSUSED*/
_citrus_SoftBank_stdenc_cstowc(_SoftBankEncodingInfo * __restrict ei,
    wchar_t * __restrict wc, _csid_t csid, _index_t idx)
{
	int row, col, offset, charset;

	if (csid == 0) {
		if (idx > 0x7F)
			return EILSEQ;
		*wc = idx;
	} else if (csid == 1) {
		if (idx > 0x7F)
			return EILSEQ;
		*wc = idx | 0x80;
	} else if (csid == 2) {
		/* kanji */
		row = (idx >> 8);
		if (row < 0x21)
			return EILSEQ;
		if (row > 0x97)
			return EILSEQ;
		offset = (row < 0x5F) ? 0x81 : 0xC1;
		col = idx & 0xFF;
		if (col < 0x21 || col > 0x7E)
			return EILSEQ;
		row -= 0x21;
		col -= 0x21;
		if ((row & 1) == 0) {
			col += 0x40;
			if (col >= 0x7F)
				col += 1;
		} else {
			col += 0x9F;
		}
		row = row / 2 + offset;
		*wc = (row << 8) | col;
	} else {
		charset = (csid >> 24) & 0xFF;
		if (!seqmatch(charset))
			return EILSEQ;
		*wc = charset << 24 | idx;
	}
	return 0;
}

static int
/*ARGSUSED*/
_citrus_SoftBank_encoding_module_init(_SoftBankEncodingInfo *  __restrict ei,
    const void * __restrict var, size_t lenvar)
{
	return 0;
}

static void
/*ARGSUSED*/
_citrus_SoftBank_encoding_module_uninit(_SoftBankEncodingInfo *ei)
{
}

#include "citrus_mbwc_template.h"

/* ----------------------------------------------------------------------
 * public interface for stdenc
 */

_CITRUS_STDENC_DECLS(SoftBank);
_CITRUS_STDENC_DEF_OPS(SoftBank);

#include "citrus_stdenc_template.h"
