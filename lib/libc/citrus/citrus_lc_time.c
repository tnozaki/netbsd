/* $NetBSD: citrus_lc_time.c,v 1.5 2010/06/13 04:14:57 tnozaki Exp $ */

/*-
 * Copyright (c)2008 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: citrus_lc_time.c,v 1.5 2010/06/13 04:14:57 tnozaki Exp $");
#endif /* LIBC_SCCS and not lint */

#include "namespace.h"
#include "reentrant.h"
#include <sys/types.h>
#include <sys/queue.h>
#include <assert.h>
#include <errno.h>
#include <langinfo.h>
#include <limits.h>
#define __SETLOCALE_SOURCE__
#include <locale.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "setlocale_local.h"

#include "citrus_namespace.h"
#include "citrus_types.h"
#include "citrus_bcs.h"
#include "citrus_region.h"
#include "citrus_lookup.h"
#include "citrus_module.h"
#include "citrus_mmap.h"
#include "citrus_hash.h"
#include "citrus_db.h"
#include "citrus_db_hash.h"
#include "citrus_memstream.h"

#include "runetype_local.h"
#include "localedef.h"

/*
 * macro required by all template headers
 */
#define _PREFIX(name)		__CONCAT(_citrus_LC_TIME_, name)

/*
 * macro required by nb_lc_template(_decl).h
 */
#define _CATEGORY_TYPE		_TimeLocale

#define ABDAY_IDX(idx)	((size_t)idx - (size_t)ABDAY_1)
#define DAY_IDX(idx)	((size_t)idx - (size_t)DAY_1)
#define ABMON_IDX(idx)	((size_t)idx - (size_t)ABMON_1)
#define MON_IDX(idx)	((size_t)idx - (size_t)MON_1)
#define AM_PM_IDX(idx)	((size_t)idx - (size_t)AM_STR)

static __inline void
/*ARGSUSED*/
_citrus_LC_TIME_build_cache(struct _locale_cache_t * __restrict cache,
    _TimeLocale * __restrict data)
{
	size_t i;

	_DIAGASSERT(cache != NULL);
	_DIAGASSERT(cache->items != NULL);
	_DIAGASSERT(data != NULL);

        for (i = (size_t)ABDAY_1; i <= ABDAY_7;  ++i)
		cache->items[i] = data->abday[ABDAY_IDX(i)];
        for (i = (size_t)DAY_1;   i <= DAY_7;    ++i)
		cache->items[i] = data->day[DAY_IDX(i)];
        for (i = (size_t)ABMON_1; i <= ABMON_12; ++i)
		cache->items[i] = data->abmon[ABMON_IDX(i)];
        for (i = (size_t)MON_1;   i <= MON_12;   ++i)
		cache->items[i] = data->mon[MON_IDX(i)];
        for (i = (size_t)AM_STR;  i <= PM_STR;   ++i)
		cache->items[i] = data->am_pm[AM_PM_IDX(i)];
	cache->items[(size_t)D_T_FMT    ] = data->d_t_fmt;
	cache->items[(size_t)D_FMT      ] = data->d_fmt;
	cache->items[(size_t)T_FMT      ] = data->t_fmt;
	cache->items[(size_t)T_FMT_AMPM ] = data->t_fmt_ampm;
	cache->items[(size_t)ERA        ] = data->era;
	cache->items[(size_t)ERA_D_FMT  ] = data->era_d_fmt;
	cache->items[(size_t)ERA_D_T_FMT] = data->era_d_t_fmt;
	cache->items[(size_t)ERA_T_FMT  ] = data->era_t_fmt;
	cache->items[(size_t)ALT_DIGITS ] = data->alt_digits;

	cache->items[(size_t)_DATE_FMT  ] = data->_date_fmt;
}

static __inline void
/*ARGSUSED*/
_citrus_LC_TIME_fixup(_TimeLocale *data)
{
}

/*
 * macro required by nb_lc_template.h
 */
#define _CATEGORY_ID		LC_TIME
#define _CATEGORY_NAME		"LC_TIME"
#define _CATEGORY_DEFAULT	_DefaultTimeLocale

#include "citrus_lc_template_decl.h"

static __inline void
_citrus_LC_TIME_uninit(_TimeLocale *data)
{
	size_t i, j;

	_DIAGASSERT(data != NULL);

	for (i = ABDAY_IDX(ABDAY_1), j = ABDAY_IDX(ABDAY_7);  i <= j; ++i)
		free(__UNCONST(data->abday[i]));
	for (i = DAY_IDX(DAY_1),     j = DAY_IDX(DAY_7);      i <= j; ++i)
		free(__UNCONST(data->day[i]));
	for (i = ABMON_IDX(ABMON_1), j = ABMON_IDX(ABMON_12); i <= j; ++i)
		free(__UNCONST(data->abmon[i]));
	for (i = MON_IDX(MON_1),     j = MON_IDX(MON_12);     i <= j; ++i)
		free(__UNCONST(data->mon[i]));
	for (i = AM_PM_IDX(AM_STR),  j = AM_PM_IDX(PM_STR);   i <= j; ++i)
		free(__UNCONST(data->am_pm[i]));
	free(__UNCONST(data->d_t_fmt));
	free(__UNCONST(data->d_fmt));
	free(__UNCONST(data->t_fmt));
	free(__UNCONST(data->t_fmt_ampm));
	free(__UNCONST(data->_date_fmt));
}

#include "citrus_lc_time.h"

struct _citrus_LC_TIME_key {
	const char *name;
	size_t offset;
};

#define OFFSET(field) offsetof(_TimeLocale, field)
static const struct _citrus_LC_TIME_key keys_version1[] = {
  { _CITRUS_LC_TIME_SYM_ABDAY_1,       OFFSET(abday[ABDAY_IDX(ABDAY_1)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_2,       OFFSET(abday[ABDAY_IDX(ABDAY_2)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_3,       OFFSET(abday[ABDAY_IDX(ABDAY_3)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_4,       OFFSET(abday[ABDAY_IDX(ABDAY_4)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_5,       OFFSET(abday[ABDAY_IDX(ABDAY_5)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_6,       OFFSET(abday[ABDAY_IDX(ABDAY_6)] ) },
  { _CITRUS_LC_TIME_SYM_ABDAY_7,       OFFSET(abday[ABDAY_IDX(ABDAY_7)] ) },
  { _CITRUS_LC_TIME_SYM_DAY_1,         OFFSET(day[DAY_IDX(DAY_1)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_2,         OFFSET(day[DAY_IDX(DAY_2)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_3,         OFFSET(day[DAY_IDX(DAY_3)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_4,         OFFSET(day[DAY_IDX(DAY_4)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_5,         OFFSET(day[DAY_IDX(DAY_5)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_6,         OFFSET(day[DAY_IDX(DAY_6)]       ) },
  { _CITRUS_LC_TIME_SYM_DAY_7,         OFFSET(day[DAY_IDX(DAY_7)]       ) },
  { _CITRUS_LC_TIME_SYM_ABMON_1,       OFFSET(abmon[ABMON_IDX(ABMON_1)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_2,       OFFSET(abmon[ABMON_IDX(ABMON_2)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_3,       OFFSET(abmon[ABMON_IDX(ABMON_3)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_4,       OFFSET(abmon[ABMON_IDX(ABMON_4)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_5,       OFFSET(abmon[ABMON_IDX(ABMON_5)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_6,       OFFSET(abmon[ABMON_IDX(ABMON_6)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_7,       OFFSET(abmon[ABMON_IDX(ABMON_7)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_8,       OFFSET(abmon[ABMON_IDX(ABMON_8)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_9,       OFFSET(abmon[ABMON_IDX(ABMON_9)] ) },
  { _CITRUS_LC_TIME_SYM_ABMON_10,      OFFSET(abmon[ABMON_IDX(ABMON_10)]) },
  { _CITRUS_LC_TIME_SYM_ABMON_11,      OFFSET(abmon[ABMON_IDX(ABMON_11)]) },
  { _CITRUS_LC_TIME_SYM_ABMON_12,      OFFSET(abmon[ABMON_IDX(ABMON_12)]) },
  { _CITRUS_LC_TIME_SYM_MON_1,         OFFSET(mon[MON_IDX(MON_1)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_2,         OFFSET(mon[MON_IDX(MON_2)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_3,         OFFSET(mon[MON_IDX(MON_3)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_4,         OFFSET(mon[MON_IDX(MON_4)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_5,         OFFSET(mon[MON_IDX(MON_5)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_6,         OFFSET(mon[MON_IDX(MON_6)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_7,         OFFSET(mon[MON_IDX(MON_7)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_8,         OFFSET(mon[MON_IDX(MON_8)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_9,         OFFSET(mon[MON_IDX(MON_9)]       ) },
  { _CITRUS_LC_TIME_SYM_MON_10,        OFFSET(mon[MON_IDX(MON_10)]      ) },
  { _CITRUS_LC_TIME_SYM_MON_11,        OFFSET(mon[MON_IDX(MON_11)]      ) },
  { _CITRUS_LC_TIME_SYM_MON_12,        OFFSET(mon[MON_IDX(MON_12)]      ) },
  { _CITRUS_LC_TIME_SYM_AM_STR,        OFFSET(am_pm[AM_PM_IDX(AM_STR)]  ) },
  { _CITRUS_LC_TIME_SYM_PM_STR,        OFFSET(am_pm[AM_PM_IDX(PM_STR)]  ) },
  { _CITRUS_LC_TIME_SYM_D_T_FMT,       OFFSET(d_t_fmt                   ) },
  { _CITRUS_LC_TIME_SYM_D_FMT,         OFFSET(d_fmt                     ) },
  { _CITRUS_LC_TIME_SYM_T_FMT,         OFFSET(t_fmt                     ) },
  { _CITRUS_LC_TIME_SYM_T_FMT_AMPM,    OFFSET(t_fmt_ampm                ) },
  { NULL, 0 }
};

static const struct _citrus_LC_TIME_key keys_version2[] = {
  { _CITRUS_LC_TIME_SYM_ERA,           OFFSET(era                       ) },
  { _CITRUS_LC_TIME_SYM_ERA_D_FMT,     OFFSET(era_d_fmt                 ) },
  { _CITRUS_LC_TIME_SYM_ERA_D_T_FMT,   OFFSET(era_d_t_fmt               ) },
  { _CITRUS_LC_TIME_SYM_ERA_T_FMT,     OFFSET(era_t_fmt                 ) },
  { _CITRUS_LC_TIME_SYM_ALT_DIGITS,    OFFSET(alt_digits                ) },
  { _CITRUS_LC_TIME_SYM__DATE_FMT,     OFFSET(_date_fmt                 ) },
#if 0
  { _CITRUS_LC_TIME_SYM_ERA_YEAR,      OFFSET(era_year                  ) },
  { _CITRUS_LC_TIME_SYM_WEEK,          OFFSET(week                      ) },
  { _CITRUS_LC_TIME_SYM_FIRST_WEEKDAY, OFFSET(first_weekday             ) },
  { _CITRUS_LC_TIME_SYM_FIRST_WORKDAY, OFFSET(first_workday             ) },
  { _CITRUS_LC_TIME_SYM_CAL_DIRECTION, OFFSET(cal_direction             ) },
  { _CITRUS_LC_TIME_SYM_TIMEZONE,      OFFSET(timezone                  ) },
#endif
  { NULL, 0 }
};

static const struct _citrus_LC_TIME_key *keys[_CITRUS_LC_TIME_VERSION] = {
  &keys_version1[0],
  &keys_version2[0],
};

static __inline int
_citrus_LC_TIME_init_normal(_TimeLocale * __restrict data,
    struct _citrus_db * __restrict db)
{
        const struct _citrus_LC_TIME_key *key;
	char **p;
	const char *s;
	uint32_t version, i;


	_DIAGASSERT(data != NULL);
	_DIAGASSERT(db != NULL);

	memset(data, 0, sizeof(*data));
	if (_db_lookup32_by_s(db, _CITRUS_LC_TIME_SYM_VERSION, &version, NULL))
		goto fatal;
	for (i = (uint32_t)0; i < version; ++i) {
		for (key = &keys[i][0]; key->name != NULL; ++key) {
			if (_db_lookupstr_by_s(db, key->name, &s, NULL))
				goto fatal;
			p = (char **)(void *)
			    (((char *)(void *)data) + key->offset);
			*p = strdup(s);
			if (*p == NULL)
				goto fatal;
		}
	}
	return 0;

fatal:
	_citrus_LC_TIME_uninit(data);
	return EFTYPE;
}

static __inline int
_citrus_LC_TIME_init_fallback(_TimeLocale * __restrict data,
    struct _memstream * __restrict ms)
{
        const struct _citrus_LC_TIME_key *key;
	char **p;
	const char *s;
	size_t n;
	uint32_t i;


	memset(data, 0, sizeof(*data));
	for (i = (uint32_t)0; i < _CITRUS_LC_TIME_VERSION; ++i) {
		for (key = &keys[i][0]; key->name != NULL; ++key) {
			if ((s = _memstream_getln(ms, &n)) == NULL)
				goto fatal;
			p = (char **)(void *)
			    (((char *)(void *)data) + key->offset);
			*p = strndup(s, n - 1);
			if (*p == NULL)
				goto fatal;
		}
	}
	return 0;

fatal:
	_citrus_LC_TIME_uninit(data);
	return EFTYPE;
}

/*
 * macro required by citrus_lc_template.h
 */
#define _CATEGORY_DB		"LC_TIME"
#define _CATEGORY_MAGIC		_CITRUS_LC_TIME_MAGIC_1

#include "citrus_lc_template.h"
