/*-
 * Copyright (c)2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

static int
/*ARGSUSED*/
_FUNCNAME(mbrtowc_priv)(_ENCODING_INFO * __restrict ei,
    wchar_t * __restrict pwc, const char ** __restrict s, size_t n,
    _ENCODING_STATE * __restrict psenc, size_t * __restrict nresult)
{
	int ret, r;
	wchar_t wc;
	size_t copy, len;

	_DIAGASSERT(s != NULL);
	_DIAGASSERT(psenc != NULL);
	_DIAGASSERT(nresult != NULL);

	if (psenc->chlen == 0) {
		ret = _FUNCNAME(decode)(ei, &wc, *s, n, psenc, nresult, &r);
		switch (ret) {
		case 0:
			if (pwc != NULL)
				*pwc = wc;
			*s += *nresult;
			if (wc == 0)
				*nresult = (size_t)0;
			break;
		case E2BIG:
			if (n < sizeof(psenc->ch)) {
				ret = 0;
				memcpy(&psenc->ch[0], *s, n);
				*s += n;
				psenc->chlen = n;
				*nresult = (size_t)-2;
				break;
			}
			ret = EILSEQ;
		/*FALLTHROUGH*/
		default:
			*nresult = (size_t)-1;
		}
	} else if (psenc->chlen < sizeof(psenc->ch)) {
		copy = sizeof(psenc->ch) - psenc->chlen;
		if (copy > n)
			copy = n;
		n = psenc->chlen + copy;
		memcpy(&psenc->ch[psenc->chlen], *s, copy);
		ret = _FUNCNAME(decode)(ei, &wc, (const char *)
		    &psenc->ch[0], n, psenc, &len, &r);
		switch (ret) {
		case 0:
			len -= psenc->chlen;
			if (pwc != NULL)
				*pwc = wc;
			*s += len;
			*nresult = wc ? len : 0;
			psenc->chlen = 0;
			break;
		case E2BIG:
			if (n < sizeof(psenc->ch)) {
				ret = 0;
				*s += copy;
				psenc->chlen += copy;
				*nresult = (size_t)-2;
				break;
			}
			ret = EILSEQ;
		/*FALLTHROUGH*/
		default:
			*nresult = (size_t)-1;
		}
	} else {
		ret = EILSEQ;
		*nresult = (size_t)-1;
	}
	return ret;
}

static int
/*ARGSUSED*/
_FUNCNAME(wcrtomb_priv)(_ENCODING_INFO * __restrict ei,
    char * __restrict s, size_t n, wchar_t wc,
    _ENCODING_STATE * __restrict psenc, size_t * __restrict nresult)
{
	int ret;

	_DIAGASSERT(nresult != NULL);

	ret = _FUNCNAME(encode)(ei, s, n, wc, psenc, nresult);
	if (ret)
		*nresult = (size_t)-1;
	return ret;
}

static __inline int
/*ARGSUSED*/
_FUNCNAME(stdenc_get_state_desc_generic)(_ENCODING_INFO * __restrict ei,
    _ENCODING_STATE * __restrict psenc, int * __restrict rstate)
{
	wchar_t wc;
	size_t nr;

	_DIAGASSERT(psenc != NULL);

	if (_FUNCNAME(decode)(ei, &wc, (const char *)&psenc->ch[0],
	    psenc->chlen, psenc, &nr, rstate) == E2BIG)
		return 0;
	return EINVAL;
}
