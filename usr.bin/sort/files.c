/*	$NetBSD: files.c,v 1.41 2009/11/06 18:34:22 joerg Exp $	*/

/*-
 * Copyright (c) 2000-2003 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Ben Harris and Jaromir Dolecek.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*-
 * Copyright (c) 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Peter McIlroy.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "sort.h"
#include "fsort.h"

__RCSID("$NetBSD: files.c,v 1.41 2009/11/06 18:34:22 joerg Exp $");

#include <string.h>

/* Align records in temporary files to avoid misaligned copies */
#define REC_ROUNDUP(n) (((n) + sizeof (long) - 1) & ~(sizeof (long) - 1))

/*
 * this is called when there is no special key. It's only called
 * in the first fsort pass.
 */

static u_char *opos;
static size_t osz;

void
makeline_copydown(RECHEADER *recbuf)
{
	memmove(recbuf->data, opos, osz);
}

int
makeline(FILE *fp, RECHEADER *recbuf, u_char *bufend, struct field *dummy2)
{
	u_char *pos;
	int c;

	pos = recbuf->data;
	if (osz != 0) {
		/*
		 * Buffer shortage is solved by either of two ways:
		 * o flush previous buffered data and start using the
		 *   buffer from start.
		 *   makeline_copydown() above must be called.
		 * o realloc buffer
		 * 
		 * This code has relied on realloc changing 'bufend',
		 * but that isn't necessarily true.
		 */
		pos += osz;
		osz = 0;
	}

	while (pos < bufend) {
		c = getc(fp);
		if (c == EOF) {
			if (pos == recbuf->data) {
				FCLOSE(fp);
				return EOF;
			}
			/* Add terminator to partial line */
			c = REC_D;
		}
		*pos++ = c;
		if (c == REC_D) {
			recbuf->offset = 0;
			recbuf->length = pos - recbuf->data;
			recbuf->keylen = recbuf->length - 1;
			return (0);
		}
	}

	/* Ran out of buffer space... */
	if (recbuf->data < bufend) {
		/* Remember where the partial record is */
		osz = pos - recbuf->data;
		opos = recbuf->data;
	}
	return (BUFFEND);
}

/*
 * This generates keys. It's only called in the first fsort pass
 */
int
makekey(FILE *fp, RECHEADER *recbuf, u_char *bufend, struct field *ftbl)
{
	static char *line_data = NULL;
	static ssize_t line_size;
	static size_t line_alloc;
	static int overflow = 0;

	/* We get re-entered after returning BUFFEND - save old data */
	if (overflow) {
		overflow = enterkey(recbuf, bufend, (u_char *)line_data, line_size, ftbl);
		return overflow ? BUFFEND : 0;
	}

	line_size = getdelim(&line_data, &line_alloc, REC_D, fp);
	if (line_size == -1) {
		FCLOSE(fp);
		return EOF;
	}

	if (line_size > bufend - recbuf->data) {
		overflow = 1;
	} else {
		overflow = enterkey(recbuf, bufend, (u_char *)line_data, line_size, ftbl);
	}
	return overflow ? BUFFEND : 0;
}

/*
 * write a key/line pair to a temporary file
 */
void
putrec(const RECHEADER *rec, FILE *fp)
{
	EWRITE(rec, 1, REC_ROUNDUP(offsetof(RECHEADER, data) + rec->length), fp);
}

/*
 * write a line to output
 */
void
putline(const RECHEADER *rec, FILE *fp)
{
	EWRITE(rec->data+rec->offset, 1, rec->length - rec->offset, fp);
}

/*
 * write dump of key to output (for -Dk)
 */
void
putkeydump(const RECHEADER *rec, FILE *fp)
{
	EWRITE(rec, 1, REC_ROUNDUP(offsetof(RECHEADER, data) + rec->offset), fp);
}

/*
 * get a record from a temporary file. (Used by merge sort.)
 */
int
geteasy(FILE *fp, RECHEADER *rec, u_char *end, struct field *dummy2)
{
	length_t file_len;
	int i;

	(void)sizeof (char[offsetof(RECHEADER, length) == 0 ? 1 : -1]);

	if ((u_char *)(rec + 1) > end)
		return (BUFFEND);
	if (!fread(&rec->length, 1, sizeof rec->length, fp)) {
		fclose(fp);
		return (EOF);
	}
	file_len = REC_ROUNDUP(offsetof(RECHEADER, data) + rec->length);
	if (end - rec->data < (ptrdiff_t)file_len) {
		for (i = sizeof rec->length - 1; i >= 0;  i--)
			ungetc(*((char *) rec + i), fp);
		return (BUFFEND);
	}

	fread(&rec->length + 1, file_len - sizeof rec->length, 1, fp);
	return (0);
}
