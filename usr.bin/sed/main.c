/*	$NetBSD: main.c,v 1.21 2010/02/19 16:35:27 tnn Exp $	*/

/*-
 * Copyright (c) 1992 Diomidis Spinellis.
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Diomidis Spinellis of Imperial College, University of London.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if HAVE_NBTOOL_CONFIG_H
#include "nbtool_config.h"
#endif

#include <sys/cdefs.h>
#ifndef lint
__COPYRIGHT("@(#) Copyright (c) 1992, 1993\
 The Regents of the University of California.  All rights reserved.");
#endif /* not lint */

#ifndef HAVE_NBTOOL_CONFIG_H
#include <sys/ioctl.h>
#endif

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <libgen.h>
#include <regex.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <util.h>
#undef EMPTY /* XXX */

#include "defs.h"
#include "extern.h"

/*
 * Linked list of units (strings and files) to be compiled
 */
struct s_compunit {
	struct s_compunit *next;
	enum e_cut {CU_FILE, CU_STRING} type;
	char *s;			/* Pointer to string or fname */
};

/*
 * Linked list pointer to compilation units and pointer to current
 * next pointer.
 */
static struct s_compunit *script, **cu_nextp = &script;

/*
 * Linked list of files to be processed
 */
struct s_flist {
	char *fname;
	struct s_flist *next;
};

/*
 * Linked list pointer to files and pointer to current
 * next pointer.
 */
static struct s_flist *files, **fl_nextp = &files;

static FILE *infile;		/* Current input file */
FILE *outfile;			/* Current output file */

bool aflag, eflag, nflag;
int ere;
static int rval;		/* Exit status */

static int ispan;		/* Whether inplace editing spans across files */

/*
 * Current file and line number; line numbers restart across compilation
 * units, but span across input files. The latter is optional if editing
 * in place.
 */
const char *fname;		/* File name. */
const char *outfname;		/* Output file name */
static char oldfname[PATH_MAX];	/* Old file name (for in-place editing) */
static char tmpfname[PATH_MAX];	/* Temporary file name (for in-place editing) */
static const char *inplace;	/* Inplace edit file extension */
size_t linenum;

static void add_compunit(enum e_cut, char *);
static void add_file(char *);
static const char *string_ident(const char *);
static void usage(void) __dead;

int termwidth;

int
main(int argc, char *argv[])
{
#ifndef HAVE_NBTOOL_CONFIG_H
	struct winsize win;
#endif
	int c;
	bool fflag, fflagstdin;
	char *p;
	long l;

	setprogname(*argv);
	fflag = fflagstdin = false;
	inplace = NULL;

	while ((c = getopt(argc, argv, "EI::ae:f:i::lnru")) != -1)
		switch (c) {
		case 'r':
		case 'E':
			ere = REG_EXTENDED;
			break;
		case 'I':
			inplace = optarg ? optarg : __UNCONST("");
			ispan = 1;      /* span across input files */
			break;
		case 'a':
			aflag = true;
			break;
		case 'e':
			eflag = true;
			add_compunit(CU_STRING, optarg);
			break;
		case 'f':
			fflag = true;
			if (!strcmp(optarg, "-"))
				fflagstdin = true;
			add_compunit(CU_FILE, optarg);
			break;
		case 'i':
			inplace = optarg ? optarg : __UNCONST("");
			ispan = 0;      /* don't span across input files */
			break;
		case 'l':
			if (setvbuf(stdout, NULL, _IOLBF, 0) != 0)
				warn("setting line buffered output failed");
			break;
		case 'n':
			nflag = true;
			break;
		case 'u':
			if (setvbuf(stdout, NULL, _IOLBF, 0) != 0)
				warn("setting unbuffered output failed");
			break;
		default:
		case '?':
			usage();
		}
	argc -= optind;
	argv += optind;

	if ((p = getenv("COLUMNS"))) {
		l = strtol(p, NULL, 10);
		if (l >= 0 && l <= INT_MAX)
			termwidth = l;
	}
#ifndef HAVE_NBTOOL_CONFIG_H
	if (termwidth == 0 &&
	    ioctl(STDOUT_FILENO, TIOCGWINSZ, &win) == 0 &&
	    win.ws_col > 0)
		termwidth = win.ws_col;
#endif
	if (termwidth == 0)
		termwidth = 60;

	/* First usage case; script is the first arg */
	if (!eflag && !fflag && *argv) {
		add_compunit(CU_STRING, *argv);
		argv++;
	}

	compile();

	/* Continue with first and start second usage */
	if (*argv)
		for (; *argv; argv++)
			add_file(*argv);
	else if (fflagstdin)
		exit(rval);
	else
		add_file(NULL);
	process();
	cfclose(prog, NULL);
	if (fclose(stdout))
		err(EXIT_FAILURE, "stdout");
	exit (rval);
}

static void
usage(void)
{
	(void)fprintf(stderr,
	    "Usage:  %s [-aElnru] command [file ...]\n"
	    "\t%s [-aElnru] [-e command] [-f command_file] [-I[extension]]\n"
	    "\t    [-i[extension]] [file ...]\n", getprogname(), getprogname());
	exit(EXIT_FAILURE);
}

/*
 * Like fgets, but go through the chain of compilation units chaining them
 * together.  Empty strings and files are ignored.
 */
char *
cu_fgets(char **outbuf, size_t *outsize)
{
	static FILE *f;		/* Current open file */
	ssize_t len;
	char *p;

	if (f == NULL) {
again:
		if (script == NULL)
			return (NULL);
		linenum = (size_t)0;
		switch (script->type) {
		case CU_FILE:
			if (!strcmp(script->s, "-")) {
				f = stdin;
				fname = "stdin";
			} else {
				f = efopen(script->s, "r");
				fname = script->s;
			}
			break;
		case CU_STRING:
			fname = string_ident(script->s);
			f = fmemopen(script->s, strlen(script->s) + 1, "r");
		}
		if (f == NULL)
			err(EXIT_FAILURE, "%s", fname);
		if ((len = getline(outbuf, outsize, f)) == -1)
			goto reacheof;
		p = *outbuf;
		if (len >= 2 && p[0] == '#' && p[1] == 'n')
			nflag = true;
	} else if ((len = getline(outbuf, outsize, f)) == -1) {
reacheof:
		if (ferror(f))
			ioerror(fname);
		script = script->next;
		fclose(f);
		f = NULL;
		goto again;
	}
	linenum++;
	return (*outbuf);
}

void
finish_file()
{
	if (infile != NULL) {
		fclose(infile);
		if (*oldfname != '\0') {
			if (rename(fname, oldfname) != 0) {
				warn("rename()");
				unlink(tmpfname);
				exit(EXIT_FAILURE);
			}
			*oldfname = '\0';
		}
		if (*tmpfname != '\0') {
			if (outfile != NULL && outfile != stdout)
				fclose(outfile);
			outfile = NULL;
			rename(tmpfname, fname);
			*tmpfname = '\0';
		}
		outfname = NULL;
	}
}

/*
 * Like fgets, but go through the list of files chaining them together.
 * Set len to the length of the line.
 */
int
mf_fgets(SPACE *sp, enum e_spflag spflag)
{
	struct stat sb;
	ssize_t len;
	size_t siz;
	char *p;
	int c, fd;
	static int firstfile;

	if (infile == NULL) {
		/* stdin? */
		if (files->fname == NULL) {
			if (inplace != NULL)
				errx(EXIT_FAILURE,
				    "-I or -i may not be used with stdin");
			infile = stdin;
			fname = "stdin";
			outfile = stdout;
			outfname = "stdout";
		}
		firstfile = 1;
	}
	for (;;) {
		if (infile != NULL && (c = getc(infile)) != EOF) {
			(void)ungetc(c, infile);
			break;
		}
		/* If we are here then either eof or no files are open yet */
		if (infile == stdin) {
			sp->len = 0;
			return (0);
		}
		finish_file();
		if (firstfile == 0)
			files = files->next;
		else
			firstfile = 0;
		if (files == NULL) {
			sp->len = 0;
			return (0);
		}
		fname = files->fname;
		if (inplace != NULL) {
			if (lstat(fname, &sb) != 0)
				err(EXIT_FAILURE, "%s", fname);
			if (!S_ISREG(sb.st_mode))
				errx(EXIT_FAILURE, "%s: in-place editing "
				    "only works for regular files", fname);
			if (*inplace != '\0') {
				estrlcpy(oldfname, fname, sizeof(oldfname));
				estrlcat(oldfname, inplace, sizeof(oldfname));
			}
			char d_name[PATH_MAX];
			estrlcpy(d_name, fname, sizeof(d_name));
			siz = snprintf(tmpfname, sizeof(tmpfname),
			    "%s/sedXXXXXXXXXX", dirname(d_name));
			if (siz >= sizeof(tmpfname))
				errx(EXIT_FAILURE, "%s: name too long", fname);
			if (outfile != NULL && outfile != stdout)
				fclose(outfile);
			fd = mkstemp(tmpfname);
			if (fd == -1)
				errx(EXIT_FAILURE, "%s", fname);
			fchown(fd, sb.st_uid, sb.st_gid);
			fchmod(fd, sb.st_mode & ALLPERMS);
			outfile = fdopen(fd, "w");
			if (outfile == NULL) {
				unlink(tmpfname);
				errx(EXIT_FAILURE, "%s", fname);
			}
			outfname = tmpfname;
			if (!ispan) {
				linenum = 0;
				resetstate();
			}
		} else {
			outfile = stdout;
			outfname = "stdout";
		}
		if ((infile = fopen(fname, "r")) == NULL) {
			warn("%s", fname);
			rval = 1;
			continue;
		}
	}

	/*
	 * We are here only when infile is open and we still have something
	 * to read from it.
	 *
	 * Use getline so that we can handle essentially infinite input data.
	 * Can't use the pointer into the stdio buffer as the process space
	 * because the ungetc() can cause it to move.
	 */
	p = NULL;
	len = getline(&p, &siz, infile);
	if (len == -1) {
		if (ferror(infile))
			ioerror(fname);
		len = 0;
	}
	if (len != 0 && p[len - 1] == '\n')
		len--;
	cspace(sp, p, (size_t)len, spflag);
	free(p);

	linenum++;

	return (1);
}

/*
 * Add a compilation unit to the linked list
 */
static void
add_compunit(enum e_cut type, char *s)
{
	struct s_compunit *cu;

	cu = emalloc(sizeof(struct s_compunit));
	cu->type = type;
	cu->s = s;
	cu->next = NULL;
	*cu_nextp = cu;
	cu_nextp = &cu->next;
}

/*
 * Add a file to the linked list
 */
static void
add_file(char *s)
{
	struct s_flist *fp;

	fp = emalloc(sizeof(struct s_flist));
	fp->next = NULL;
	*fl_nextp = fp;
	fp->fname = s;
	fl_nextp = &fp->next;
}

static const char *
string_ident(const char *s)
{
	static char buf[30];
	static const char trail[4] = " ...";
	char *p;
	const char *t;

	p = &buf[0];
	*p++ = '"';
	while (p < &buf[sizeof(buf) - sizeof(trail) - 2]) {
		if (*s == '\0')
			goto terminate;
		*p++ = *s++;
	}
	for (t = s; t < &s[sizeof(trail)]; ++t) {
		if (*t == '\0') {
			while (s < t)
				*p++ = *s++;
			goto terminate;
		}
	}
	t = &trail[0];
	while (t < &trail[sizeof(trail)])
		*p++ = *t++;
terminate:
	*p++ = '"';
	*p = '\0';
	return buf;
}

int
lastline(void)
{
	int ch;

	if (files->next != NULL && (inplace == NULL || ispan))
		return (0);
	if ((ch = getc(infile)) == EOF)
		return (1);
	ungetc(ch, infile);
	return (0);
}
