/*	$NetBSD: extern.h,v 1.11 2010/02/19 16:35:27 tnn Exp $	*/

/*-
 * Copyright (c) 1992 Diomidis Spinellis.
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Diomidis Spinellis of Imperial College, University of London.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	from: @(#)extern.h	8.1 (Berkeley) 6/6/93
 *	$NetBSD: extern.h,v 1.11 2010/02/19 16:35:27 tnn Exp $
 */

extern struct s_command *prog;
extern struct s_appends *appends;
extern regmatch_t *match;
extern size_t maxnsub;
extern size_t linenum;
extern size_t appendnum;
extern bool aflag, eflag, nflag;
extern int ere;
extern const char *fname, *outfname;
extern FILE *outfile;

void	 cfclose(struct s_command *, struct s_command *);
void	 compile(void);
void	 cspace(SPACE *, const char *, size_t, enum e_spflag);
char	*cu_fgets(char **, size_t *);
int	 lastline(void);
void	 finish_file(void);
int	 mf_fgets(SPACE *, enum e_spflag);
void	 process(void);
void	 resetstate(void);
char	*strregerror(int, regex_t *);

/*
 * Just print the warning
 */
#define warning(fmt, ...) \
    warnx("%zu: %s: " fmt, linenum, fname, ##__VA_ARGS__)
/*
 * Print error, count and finish script
 */
#define error(fmt, ...) \
    errx(EXIT_FAILURE, "%zu: %s: " fmt, linenum, fname, ##__VA_ARGS__)

/*
 * Print IO error
 */
#define ioerror(file) \
    errx(EXIT_FAILURE, "%s: %s", file, strerror(errno ? errno : EIO))

static __inline size_t
efwrite(const void *p, size_t size, size_t nmemb, FILE *fp)
{
	if (nmemb > 0 && fwrite(p, size, nmemb, fp) != nmemb)
		err(EXIT_FAILURE, NULL);
	return nmemb;
}

static __inline int
efputc(int c, FILE *fp)
{
	if (fputc(c, fp) == EOF)
		err(EXIT_FAILURE, NULL);
	return c;
}
