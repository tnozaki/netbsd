/*	$NetBSD: compile.c,v 1.37.8.1 2012/12/25 21:10:35 snj Exp $	*/

/*-
 * Copyright (c) 1992 Diomidis Spinellis.
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Diomidis Spinellis of Imperial College, University of London.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if HAVE_NBTOOL_CONFIG_H
#include "nbtool_config.h"
#endif

#include <sys/cdefs.h>

#include <ctype.h>
#include <err.h>
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>
#include <regex.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util.h>
#undef EMPTY /* XXX */

#include "defs.h"
#include "extern.h"

#define LHMASK	UINT32_C(0x7f)
static struct labhash {
	struct	labhash *lh_next;
	uint32_t lh_hash;
	struct	s_command *lh_cmd;
	int	lh_ref;
} *labels[LHMASK+1];

static char	 *compile_addr(char *, struct s_addr *);
static char	 *compile_ccl(char *, char **);
static char	 *compile_delimited(char *, char *, size_t *, int);
static char	 *compile_flags(char *, struct s_subst *);
static char	 *compile_re(char *, regex_t **);
static char	 *compile_subst(char *, struct s_subst *);
static char	 *compile_text(size_t *);
static char	 *compile_tr(char *, char **);
static struct s_command
		**compile_stream(struct s_command **);
static char	 *duptoeol(char *, const char *, char **, size_t *);
static void	  enterlabel(struct s_command *);
static struct s_command
		 *findlabel(char *);
static void	  fixuplabel(struct s_command *, struct s_command *);
static void	  uselabel(void);
static uint32_t	  labelhash(const char *);

/*
 * Command specification.  This is used to drive the command parser.
 */
struct s_format {
	int code;				/* Command code */
	int naddr;				/* Number of address args */
	enum e_args args;			/* Argument type */
};

static struct s_format cmd_fmts[] = {
	{'{', 2, GROUP},
	{'}', 0, ENDGROUP},
	{'a', 1, TEXT},
	{'b', 2, BRANCH},
	{'c', 2, TEXT},
	{'d', 2, EMPTY},
	{'D', 2, EMPTY},
	{'g', 2, EMPTY},
	{'G', 2, EMPTY},
	{'h', 2, EMPTY},
	{'H', 2, EMPTY},
	{'i', 1, TEXT},
	{'l', 2, EMPTY},
	{'n', 2, EMPTY},
	{'N', 2, EMPTY},
	{'p', 2, EMPTY},
	{'P', 2, EMPTY},
	{'q', 1, EMPTY},
	{'r', 1, RFILE},
	{'s', 2, SUBST},
	{'t', 2, BRANCH},
	{'w', 2, WFILE},
	{'x', 2, EMPTY},
	{'y', 2, TR},
	{'!', 2, NONSEL},
	{':', 0, LABEL},
	{'#', 0, COMMENT},
	{'=', 1, EMPTY},
	{'\0', 0, COMMENT},
};

/* The compiled program. */
struct s_command *prog;

/*
 * Compile the program into prog.
 * Initialise appends.
 */
void
compile(void)
{
	*compile_stream(&prog) = NULL;
	fixuplabel(prog, NULL);
	uselabel();
	if (appendnum > 0)
		appends = emalloc(sizeof(*appends) * appendnum);
	match = emalloc((maxnsub + 1) * sizeof(*match));
}

#define EATSPACE() do {						\
	while (*p && isspace((unsigned char)*p))		\
		p++;						\
} while (/*CONSTCOND*/0)

static struct s_command **
compile_stream(struct s_command **link)
{
	char *p;
	static char *lbuf;	/* To avoid excessive malloc calls */
	static size_t bufsize;
	struct s_command *cmd, *cmd2, *stack;
	struct s_format *fp;
	int naddr;				/* Number of addresses */

	stack = NULL;
	while ((p = cu_fgets(&lbuf, &bufsize)) != NULL) {

semicolon:	EATSPACE();
		if (*p == '#' || *p == '\0')
			continue;
		if (*p == ';') {
			p++;
			goto semicolon;
		}
		*link = cmd = emalloc(sizeof(*cmd));
		link = &cmd->next;
		cmd->nonsel = cmd->inrange = false;
		/* First parse the addresses */
		naddr = 0;

/* Valid characters to start an address */
		switch (*p) {
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case '/': case '\\': case '$':
			naddr++;
			cmd->a1 = emalloc(sizeof(*cmd->a1));
			p = compile_addr(p, cmd->a1);
			EATSPACE();				/* EXTENSION */
			if (*p == ',') {
				p++;
				EATSPACE();			/* EXTENSION */
				naddr++;
				cmd->a2 = emalloc(sizeof(*cmd->a2));
				p = compile_addr(p, cmd->a2);
				EATSPACE();
			} else {
				cmd->a2 = NULL;
			}
			break;
		default:
			cmd->a1 = cmd->a2 = NULL;
		}

nonsel:		/* Now parse the command */
		if (!*p)
			error("command expected");
		cmd->code = (unsigned char)*p;
		for (fp = cmd_fmts; fp->code; fp++)
			if (fp->code == (unsigned char)*p)
				break;
		if (!fp->code)
			error("invalid command code %c", (unsigned char)*p);
		if (naddr > fp->naddr)
			error("command %c expects up to %d address(es), found %d",
			    *p, fp->naddr, naddr);
		switch (fp->args) {
		case NONSEL:			/* ! */
			p++;
			EATSPACE();
			cmd->nonsel = true;
			goto nonsel;
		case GROUP:			/* { */
			p++;
			EATSPACE();
			cmd->next = stack;
			stack = cmd;
			link = &cmd->u.c;
			if (*p)
				goto semicolon;
			break;
		case ENDGROUP:
			/*
			 * Short-circuit command processing, since end of
			 * group is really just a noop.
			 */
			cmd->nonsel = true;
			if (stack == NULL)
				error("unexpected }");
			cmd2 = stack;
			stack = cmd2->next;
			cmd2->next = cmd;
			/*FALLTHROUGH*/
		case EMPTY:		/* d D g G h H l n N p P q x = \0 */
			p++;
			EATSPACE();
			if (*p == ';') {
				p++;
				link = &cmd->next;
				goto semicolon;
			}
			if (*p)
				error("extra characters at the end of %c "
				    "command", cmd->code);
			break;
		case TEXT:			/* a c i */
			p++;
			EATSPACE();
			if (*p != '\\')
				error("command %c expects \\ followed by text",
				    cmd->code);
			p++;
			EATSPACE();
			if (*p)
				error("extra characters after \\ at the end "
				    "of %c command", cmd->code);
			cmd->t = compile_text(&cmd->n);
			break;
		case COMMENT:			/* \0 # */
			break;
		case WFILE:			/* w */
			p++;
			EATSPACE();
			cmd->t = duptoeol(p, "w command", NULL, &cmd->n);
			if (cmd->t == NULL)
				error("filename expected");
			if (aflag)
				cmd->u.fd = -1;
			else if ((cmd->u.fd = open(p,
			    O_WRONLY|O_APPEND|O_CREAT|O_TRUNC,
			    DEFFILEMODE)) == -1)
				err(EXIT_FAILURE, "%s", p);
			break;
		case RFILE:			/* r */
			p++;
			EATSPACE();
			cmd->t = duptoeol(p, "read command", NULL, &cmd->n);
			if (cmd->t == NULL)
				error("filename expected");
			break;
		case BRANCH:			/* b t */
			p++;
			EATSPACE();
			cmd->t = duptoeol(p, "branch", &p, &cmd->n);
			if (*p == ';') {
				p++;
				goto semicolon;
			}
			break;
		case LABEL:			/* : */
			p++;
			EATSPACE();
			cmd->t = duptoeol(p, "label", &p, &cmd->n);
			if (cmd->t ==  NULL)
				error("empty label");
			enterlabel(cmd);
			if (*p == ';') {
				p++;
				goto semicolon;
			}
			break;
		case SUBST:			/* s */
			p++;
			if (*p == '\0' || *p == '\\')
				error("substitute pattern can not be "
				    "delimited by newline or backslash");
			cmd->u.s = emalloc(sizeof(*cmd->u.s));
			p = compile_re(p, &cmd->u.s->re);
			if (p == NULL)
				error("unterminated substitute pattern");
			--p;
			p = compile_subst(p, cmd->u.s);
			p = compile_flags(p, cmd->u.s);
			EATSPACE();
			if (*p == ';') {
				p++;
				link = &cmd->next;
				goto semicolon;
			}
			break;
		case TR:			/* y */
			p++;
			p = compile_tr(p, &cmd->u.y);
			EATSPACE();
			if (*p == ';') {
				p++;
				link = &cmd->next;
				goto semicolon;
			}
			if (*p)
				error("extra text at the end of a transform "
				    "command");
			break;
		}
	}
	if (stack != NULL)
		error("unexpected EOF (pending }'s)");
	return (link);
}

/*
 * Get a delimited string.  P points to the delimiter of the string; d points
 * to a buffer area.  Newline and delimiter escapes are processed; other
 * escapes are ignored.
 *
 * Returns a pointer to the first character after the final delimiter or NULL
 * in the case of a non-terminated string.  The character array d is filled
 * with the processed string.
 */
static char *
compile_delimited(char *p, char *d, size_t *plen, int is_tr)
{
	int delimiter, c;
	char *pd;

	delimiter = (unsigned char)*p++;
	switch (delimiter) {
	case '\0':
		return NULL;
	case '\\':
		error("\\ can not be used as a string delimiter");
	case '\n':
		error("newline can not be used as a string delimiter");
	}
	pd = d;
	while ((c = (unsigned char)*p++) != delimiter) {
		if (c == '\0')
			return NULL;
		if (c == '[') {
			*d++ = c;
			if ((p = compile_ccl(p, &d)) == NULL)
				error("unbalanced brackets ([])");
			continue;
		}
		if (c == '\\' && (c = (unsigned char)*p++) != delimiter) {
			if (c == '\0')
				return NULL;
			if (c == 'n')
				c = '\n';
			else if (c != '\\' || !is_tr)
				*d++ = '\\';
		}
		*d++ = c;
	}
	*d = '\0';
	if (plen != NULL)
		*plen = (size_t)(d - pd);
	return p;
}

/* compile_ccl: expand a POSIX character class */
static char *
compile_ccl(char *s, char **tp)
{
	int c, d;
	char *t = *tp;

	if (*s == '^')
		*t++ = *s++;
	if (*s == ']')
		*t++ = *s++;
	for (; *s && (*t = *s) != ']'; s++, t++)
		if (*s == '[' && ((d = *(s+1)) == '.' || d == ':' || d == '=')) {
			*++t = *++s, t++, s++;
			for (c = *s; (*t = *s) != ']' || c != d; s++, t++)
				if ((c = *s) == '\0')
					return NULL;
		} else if (*s == '\\' && s[1] == 'n') {
			*t = '\n';
			s++;
		}
	if (*s == ']') {
		*tp = ++t;
		return (++s);
	} else {
		return (NULL);
	}
}

/*
 * Get a regular expression.  P points to the delimiter of the regular
 * expression; repp points to the address of a regexp pointer.  Newline
 * and delimiter escapes are processed; other escapes are ignored.
 * Returns a pointer to the first character after the final delimiter
 * or NULL in the case of a non terminated regular expression.  The regexp
 * pointer is set to the compiled regular expression.
 * Cflags are passed to regcomp.
 */
static char *
compile_re(char *p, regex_t **repp)
{
	int eval;
	char *re;
	size_t len;

	re = emalloc(strlen(p) + 1); /* strlen(re) <= strlen(p) */
	p = compile_delimited(p, re, &len, 0);
	if (p && len == 0) {
		*repp = NULL;
		free(re);
		return (p);
	}
	*repp = emalloc(sizeof(**repp));
	if (p && (eval = regcomp(*repp, re, ere)) != 0)
		error("RE error: %s", strregerror(eval, *repp));
	if (maxnsub < (*repp)->re_nsub)
		maxnsub = (*repp)->re_nsub;
	free(re);
	return (p);
}

/*
 * Compile the substitution string of a regular expression and set res to
 * point to a saved copy of it.  Nsub is the number of parenthesized regular
 * expressions.
 */
static char *
compile_subst(char *p, struct s_subst *s)
{
	static char *lbuf;
	static size_t bufsize;
	int term;
	char *text, *t;
	size_t len, bref;
	FILE *fp;
	regex_t *re;
	bool sawesc = false;

	term = (unsigned char)*p++;		/* Terminator character */
	if (term == '\0')
		return (NULL);

	text = NULL;
	len = 0;
	fp = open_memstream(&text, &len);
	if (fp == NULL)
		err(EXIT_FAILURE, NULL);

	s->maxbref = 0;
	s->linenum = linenum;
	do {
		for (t = p; *t != '\0'; ++t) {
			if (sawesc) {
				/*
				 * If this is a continuation from the last
				 * buffer, we won't have a character to
				 * skip over.
				 */
				sawesc = false;
			} else if (*t == '\\') {
				efwrite(p, sizeof(*p), t - p, fp);
				if (t[1] == '\0') {
					/*
					 * This escaped character is continued
					 * in the next part of the line.  Note
					 * this fact, then cause the loop to
					 * exit w/ normal EOL case and reenter
					 * above with the new buffer.
					 */
					sawesc = true;
					break;
				}
				p = ++t;
			} else if (*t == term) {
				efwrite(p, sizeof(*p), t - p, fp);
				fclose(fp);
				s->new = text;
				return (t + 1);
			} else if (*t == '\n') {
				error("unescaped newline inside substitute "
				    "pattern");
			} else {
				continue;
			}
			switch (*t) {
			case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				bref = *t - '0';
				re = s->re;
				if (re != NULL && re->re_nsub < bref)
					error("\\%c not defined in the RE", *t);
				if (s->maxbref < bref)
					s->maxbref = bref;
			/*FALLTHROUGH*/
			case '&': case '\\':
				efputc('\\', fp);
				break;
			}
		}
		efwrite(p, sizeof(*p), t - p, fp);
	} while ((p = cu_fgets(&lbuf, &bufsize)) != NULL);
	error("unterminated substitute in regular expression");
	/*NOTREACHED*/
}

/*
 * Compile the flags of the s command
 */
static char *
compile_flags(char *p, struct s_subst *s)
{
	int gn;			/* True if we have seen g or n */
	long l;
	char *q;

	s->n = 1;				/* Default */
	s->p = false;
	s->wfile = NULL;
	s->wfd = -1;
	for (gn = 0;;) {
		EATSPACE();			/* EXTENSION */
		switch (*p) {
		case 'g':
			if (gn)
				error("more than one number or 'g' in "
				    "substitute flags");
			gn = 1;
			s->n = 0;
			break;
		case '\0':
		case '\n':
		case ';':
			return (p);
		case 'p':
			s->p = true;
			break;
		case '1': case '2': case '3':
		case '4': case '5': case '6':
		case '7': case '8': case '9':
			if (gn)
				error("more than one number or 'g' in "
				    "substitute flags");
			gn = 1;
			l = strtol(p, &p, 10);
			if (l <= 0 || l >= INT_MAX)
				error("number in substitute flags out of "
				    "range");
			s->n = (int)l;
			continue;
		case 'w':
			p++;
			EATSPACE();
			for (q = p; *p != '\0' && *p != '\n'; ++p)
				;
			if (p == q)
				error("no wfile specified");
			s->wfile = estrndup(q, (size_t)(p - q));
			if (!aflag && (s->wfd = open(s->wfile,
			    O_WRONLY|O_APPEND|O_CREAT|O_TRUNC,
			    DEFFILEMODE)) == -1)
				err(EXIT_FAILURE, "%s", s->wfile);
			return (p);
		default:
			error("bad flag in substitute command: '%c'", *p);
			break;
		}
		p++;
	}
}

/*
 * Compile a translation set of strings into a lookup table.
 */
static char *
compile_tr(char *p, char **transtab)
{
	int i;
	char *lt, *op, *np;
	char *old = NULL, *new = NULL;
	size_t olen, nlen;

	if (*p == '\0' || *p == '\\')
		error("transform pattern can not be delimited by newline or "
		    "backslash");
	old = emalloc(strlen(p) + 1);
	p = compile_delimited(p, old, &olen, 1);
	if (p == NULL)
		error("unterminated transform source string");
	new = emalloc(strlen(p) + 1);
	p = compile_delimited(p - 1, new, &nlen, 1);
	if (p == NULL)
		error("unterminated transform target string");
	EATSPACE();
	if (nlen != olen)
		error("transform strings are not the same length");
	/* We assume characters are 8 bits */
	lt = emalloc(UCHAR_MAX+1);
	for (i = 0; i <= UCHAR_MAX; i++)
		lt[i] = (unsigned char)i;
	for (op = old, np = new; *op; op++, np++)
		lt[(unsigned char)*op] = *np;
	*transtab = lt;
	free(old);
	free(new);
	return (p);
}

/*
 * Compile the text following an a, c, or i command.
 */
static char *
compile_text(size_t *plen)
{
	static char *lbuf;
	static size_t bufsize;
	char *text, *s, *t;
	size_t len;
	FILE *fp;
	bool esc_nl;

	text = NULL;
	len = 0;
	fp = open_memstream(&text, &len);
	if (fp == NULL)
		err(EXIT_FAILURE, NULL);

	while ((s = cu_fgets(&lbuf, &bufsize))) {
		esc_nl = false;
		for (t = s; *t != '\0'; ++t) {
			if (*t == '\\') {
				if (t[1] == '\0') {
					++t;
					break;
				}
				if (t[1] == '\n')
					esc_nl = true;
				efwrite(s, sizeof(*s), t - s, fp);
				s = ++t;
			}
		}
		efwrite(s, sizeof(*s), t - s, fp);
		if (!esc_nl)
			break;
	}
	fclose(fp);
	if (plen != NULL)
		*plen = len;
	return (text);
}

/*
 * Get an address and return a pointer to the first character after
 * it.  Fill the structure pointed to according to the address.
 */
static char *
compile_addr(char *p, struct s_addr *a)
{
	char *end;
	uintmax_t l;

	switch (*p) {
	case '\\':				/* Context address */
		++p;
		/* FALLTHROUGH */
	case '/':				/* Context address */
		p = compile_re(p, &a->u.r);
		if (p == NULL)
			error("unterminated regular expression");
		a->type = AT_RE;
		return (p);

	case '$':				/* Last line */
		a->type = AT_LAST;
		return (p + 1);
						/* Line number */
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
		a->type = AT_LINE;
		l = strtoumax(p, &end, 10);
		if (l <= SIZE_MAX) {
			a->u.l = (size_t)l;
			return (end);
		}
	default:
		error("expected context address");
		return (NULL);
	}
}

/*
 * duptoeol --
 *	Return a copy of all the characters up to \n or \0.
 */
static char *
duptoeol(char *s, const char *ctype, char **semi, size_t *plen)
{
	char *p;
	size_t len;

	p = s;
	if (semi) {
		while (*p != '\0' && *p != '\n' && *p != ';')
			++p;
		*semi = p;
	} else {
		while (*p != '\0' && *p != '\n')
			++p;
		*p = '\0';
	}
	len = (size_t)(p - s);
	if (len < 1)
		return NULL;
	if (isspace((unsigned char)*(p - 1)))
		warning("whitespace after %s", ctype);
	p = estrndup(s, len);
	if (plen != NULL)
		*plen = len;
	return p;
}

/*
 * Convert goto label names to addresses, and count a and r commands, in
 * the given subset of the script.  Free the memory used by labels in b
 * and t commands (but not by :).
 *
 * TODO: Remove } nodes
 */
static void
fixuplabel(struct s_command *cp, struct s_command *end)
{

	for (; cp != end; cp = cp->next)
		switch (cp->code) {
		case 'a':
		case 'r':
			appendnum++;
			break;
		case 'b':
		case 't':
			/* Resolve branch target. */
			if (cp->t == NULL) {
				cp->u.c = NULL;
				break;
			}
			if ((cp->u.c = findlabel(cp->t)) == NULL)
				error("undefined label '%s'", cp->t);
			free(cp->t);
			break;
		case '{':
			/* Do interior commands. */
			fixuplabel(cp->u.c, cp->next);
			break;
		}
}

/*
 * Associate the given command label for later lookup.
 */
static void
enterlabel(struct s_command *cp)
{
	struct labhash **lhp, *lh;
	uint32_t h;

	h = labelhash((const char *)cp->t);
	lhp = &labels[h & LHMASK];
	for (lh = *lhp; lh != NULL; lh = lh->lh_next)
		if (lh->lh_hash == h && strcmp(cp->t, lh->lh_cmd->t) == 0)
			error("duplicate label '%s'", cp->t);
	lh = emalloc(sizeof(*lh));
	lh->lh_next = *lhp;
	lh->lh_hash = h;
	lh->lh_cmd = cp;
	lh->lh_ref = 0;
	*lhp = lh;
}

/*
 * Find the label contained in the command l in the command linked
 * list cp.  L is excluded from the search.  Return NULL if not found.
 */
static struct s_command *
findlabel(char *name)
{
	struct labhash *lh;
	uint32_t h;

	h = labelhash((const char *)name);
	for (lh = labels[h & LHMASK]; lh != NULL; lh = lh->lh_next) {
		if (lh->lh_hash == h && strcmp(name, lh->lh_cmd->t) == 0) {
			lh->lh_ref = 1;
			return (lh->lh_cmd);
		}
	}
	return (NULL);
}

/*
 * Warn about any unused labels.  As a side effect, release the label hash
 * table space.
 */
static void
uselabel(void)
{
	struct labhash *lh, *next;
	size_t i;

	for (i = 0; i < __arraycount(labels); i++) {
		for (lh = labels[i]; lh != NULL; lh = next) {
			next = lh->lh_next;
			if (!lh->lh_ref)
				warning("unused label '%s'", lh->lh_cmd->t);
			free(lh);
		}
	}
}

static uint32_t
labelhash(const char *s)
{
	uint32_t h;
	const char *p;
	int c;

	h = (uint32_t)0;
	for (p = s; (c = (unsigned char)*p) != '\0'; ++p)
		h = (h << 5) + h + (uint32_t)c;
	return h;
}
