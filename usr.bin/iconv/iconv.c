/*-
 * Copyright (c)2007, 2013, 2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*-
 * Copyright (c)2003 Citrus Project,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if !defined(lint) && !defined(SHELL)
__COPYRIGHT("@(#) Copyright (c) 2007, 2013, 2014\
 Takehiko NOZAKI, All rights reserved.");
__COPYRIGHT("@(#) Copyright (c) 2003\
 Citrus Project, All rights reserved.");
#endif /* not lint */

#include <err.h>
#include <errno.h>
#include <iconv.h>
#include <langinfo.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <util.h>

typedef struct {
	iconv_t cd;
	size_t inbufsize, outbufsize;
	char *inbuf, *outbuf;
	int silent;
	uint32_t flags;
} converter_t;

static void usage(void) __dead;
static int scmp(const void *, const void *);
static void show_codesets(void);
static void do_conv(FILE *, FILE *, converter_t *);

static __inline void
converter_init(converter_t *p, const char *to, const char *from,
    int silent, uint32_t flags)
{
	p->cd = iconv_open(to, from);
	if (p->cd == (iconv_t)-1)
		err(EXIT_FAILURE, "iconv_open(%s, %s)", to, from);

	p->inbufsize = BUFSIZ;
	p->outbufsize = BUFSIZ;

	p->inbuf = emalloc(p->inbufsize);
	p->outbuf = emalloc(p->outbufsize);

	p->silent = silent;
	p->flags = flags;
}

static __inline void
converter_uninit(converter_t *p)
{
	iconv_close(p->cd);
	free(p->inbuf);
	free(p->outbuf);
}

static void
usage(void)
{
	(void)fprintf(stderr,
	    "Usage:\t%1$s [-cs] -f <from_code> -t <to_code> [file ...]\n"
	    "\t%1$s -f <from_code> [-cs] [-t <to_code>] [file ...]\n"
	    "\t%1$s -t <to_code> [-cs] [-f <from_code>] [file ...]\n"
	    "\t%1$s -l\n", getprogname());
	exit(EXIT_FAILURE);
}

/*
 * qsort() helper function
 */
static int
scmp(const void *v1, const void *v2)
{
	const char * const *s1 = v1;
	const char * const *s2 = v2;

	return strcasecmp(*s1, *s2);
}

static void
show_codesets(void)
{
	char **list;
	size_t sz, i;

	if (__iconv_get_list(&list, &sz))
		err(EXIT_FAILURE, "__iconv_get_list()");

	qsort(list, sz, sizeof(char *), scmp);

	for (i = 0; i < sz; i++)
		puts(list[i]);

	__iconv_free_list(list, sz);
}

static void
grow(char **buf, size_t *bufsize)
{
	if (SIZE_MAX / 2 < *bufsize)
		err(EXIT_FAILURE, "iconv()");
	*bufsize *= 2;
	*buf = erealloc(*buf, *bufsize);
}

static __inline int
doprint(const char *s, size_t n, FILE *writer)
{
	if (n > 0) {
		if (fwrite(s, 1, n, writer) != n)
			return EINVAL;
	}
	return 0;
}

static void
do_conv(FILE *reader, FILE *writer, converter_t *p)
{
	const char *in;
	char *out;
	size_t inbytes, outbytes, ret, invalids, inval, nread;

	invalids = 0;
	while ((nread = fread(p->inbuf, 1, p->inbufsize, reader)) > 0) {
		in = p->inbuf;
		inbytes = nread;
		while (inbytes > 0) {
			out = p->outbuf;
			outbytes = p->outbufsize;
			/*
			 * XXX: iconv(3) is bad interface.
			 *   the results of encountering invalid characters
			 *   in the input stream is lost when an error occured.
			 *   instead, we just provide __iconv function.
			 */
			ret = __iconv(p->cd, &in, &inbytes, &out, &outbytes,
			    p->flags, &inval);
			invalids += inval;
			if (ret == (size_t)-1) {
				switch (errno) {
				case EINVAL:
					if (in == p->inbuf) {
						grow(&p->inbuf, &p->inbufsize);
					} else {
						/* incomplete input character */
						(void)memmove(p->inbuf, in,
						    inbytes);
					}
					nread = fread(p->inbuf + inbytes, 1,
					    p->inbufsize - inbytes, reader);
					if (nread == 0) {
						if (doprint(p->outbuf,
						    p->outbufsize - outbytes,
						    writer))
							goto werror;
						goto bailout;
					}
					in = (const char *)p->inbuf;
					inbytes += nread;
					break;
				case E2BIG:
					if (out == p->outbuf)
						grow(&p->outbuf,
						    &p->outbufsize);
					break;
				default:
					goto fatal;
				}
			}
			if (doprint(p->outbuf, p->outbufsize - outbytes,
			    writer))
				goto werror;
		}
	}
bailout:
	/* reset the shift state of the output buffer */
	do {
		out = p->outbuf;
		outbytes = p->outbufsize;
		ret = __iconv(p->cd, NULL, NULL, &out, &outbytes,
		    p->flags, &inval);
		invalids += inval;
		if (ret == (size_t)-1) {
			switch (errno) {
			case EINVAL:
				goto rerror;
			case E2BIG:
				if (out == p->outbuf)
					grow(&p->outbuf, &p->outbufsize);
				break;
			default:
				goto fatal;
			}
		}
		if (doprint(p->outbuf, p->outbufsize - outbytes, writer))
			goto werror;
	} while (ret == (size_t)-1);
	/* full reset state */
	iconv(p->cd, NULL, NULL, NULL, NULL);

	if (invalids > 0 && !p->silent)
		warnx("warning: invalid characters: %zu", invalids);

	return;

rerror:
	fflush(writer);
	if (feof(reader))
		errx(EXIT_FAILURE, "unexpected end of file; "
		    "the last character is incomplete.");
	err(EXIT_FAILURE, "fread()");

werror:
	errx(EXIT_FAILURE, "fwrite()");

fatal:
	err(EXIT_FAILURE, "iconv()");
}

int
main(int argc, char **argv)
{
	int ch, i;
	int list = 0, silent = 0;
	const char *from = NULL, *to = NULL;
	uint32_t flags = 0;
	FILE *reader = stdin, *writer = stdout;
	converter_t converter;

	setlocale(LC_ALL, "");
	setprogname(argv[0]);

	while ((ch = getopt(argc, argv, "cslf:t:")) != EOF) {
		switch (ch) {
		case 'c':
			flags |= __ICONV_F_HIDE_INVALID;
			break;
		case 's':
			silent = 1;
			break;
		case 'l':
			list = 1;
			break;
		case 'f':
			from = estrdup(optarg);
			break;
		case 't':
			to = estrdup(optarg);
			break;
		default:
			usage();
		}
	}
	argc -= optind;
	argv += optind;
	if (list) {
		if (argc > 0 || silent != 0 || flags != 0 ||
		    from != NULL || to != NULL) {
			warnx("-l is not allowed with other flags.");
			usage();
		}
		show_codesets();
	} else {
		if (from == NULL) {
			if (to == NULL)
				usage();
			from = nl_langinfo(CODESET);
		} else if (to == NULL) {
			to = nl_langinfo(CODESET);
		}
		converter_init(&converter, to, from, silent, flags);
		if (argc < 1)
			do_conv(reader, writer, &converter);
		for (i = 0; i < argc; ++i) {
			reader = fopen(argv[i], "r");
			if (reader == NULL)
				err(EXIT_FAILURE, "Cannot open `%s'", argv[i]);
			do_conv(reader, writer, &converter);
			(void)fclose(reader);
		}
		converter_uninit(&converter);
	}
	return EXIT_SUCCESS;
}
