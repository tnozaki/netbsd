/*	$NetBSD: defs.h,v 1.35.8.1 2012/06/12 19:23:33 riz Exp $	*/

/*
 * Copyright (c) 1992, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This software was developed by the Computer Systems Engineering group
 * at Lawrence Berkeley Laboratory under DARPA contract BG 91-66 and
 * contributed to Berkeley.
 *
 * All advertising materials mentioning features or use of this software
 * must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Lawrence Berkeley Laboratories.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	from: @(#)config.h	8.1 (Berkeley) 6/6/93
 */

/*
 * defs.h:  Global definitions for "config"
 */

#if HAVE_NBTOOL_CONFIG_H
#include "nbtool_config.h"
#endif

#include <sys/types.h>
#include <sys/param.h>
#include <sys/queue.h>

#if !defined(MAKE_BOOTSTRAP) && defined(BSD)
#include <sys/cdefs.h>
#include <paths.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef EXTERN
#define EXTERN extern
#endif

/* These are really for MAKE_BOOTSTRAP but harmless. */
#ifndef __dead
#define __dead
#endif
#ifndef _PATH_DEVNULL
#define _PATH_DEVNULL "/dev/null"
#endif

#ifdef	MAKE_BOOTSTRAP
#undef	dev_t
#undef	devmajor_t
#undef	devminor_t
#undef	NODEV
#undef	NODEVMAJOR
#undef	major
#undef	minor
#undef	makedev
#define	dev_t		unsigned int	/* XXX: assumes int is 32 bits */
#define	NODEV		((dev_t)-1)
#define devmajor_t	int
#define devminor_t	int
#define NODEVMAJOR	(-1)
#define major(x)        ((devmajor_t)((((x) & 0x000fff00) >>  8)))
#define minor(x)        ((devminor_t)((((x) & 0xfff00000) >> 12) | \
			       (((x) & 0x000000ff) >>  0)))
#define makedev(x,y)    ((dev_t)((((x) <<  8) & 0x000fff00) | \
                                 (((y) << 12) & 0xfff00000) | \
                                 (((y) <<  0) & 0x000000ff)))
#define __attribute__(x)
#endif	/* MAKE_BOOTSTRAP */

#undef setprogname
#undef getprogname
extern const char *progname;
#define	setprogname(s)	((void)(progname = (s)))
#define	getprogname()	(progname)

#define ARRCHR '#'

/*
 * The next two lines define the current version of the config(1) binary,
 * and the minimum version of the configuration files it supports.
 */
#define CONFIG_VERSION		20100430
#define CONFIG_MINVERSION	0

/*
 * Name/value lists.  Values can be strings or pointers and/or can carry
 * integers.  The names can be NULL, resulting in simple value lists.
 */
struct nvlist {
	struct nvlist	*nv_next;
	const char	*nv_name;
	const char	*nv_str;
	void		*nv_ptr;
	long long	nv_num;
	int		nv_ifunit;		/* XXX XXX XXX */
	int		nv_flags;
#define	NV_DEPENDED	1
#define	NV_OBSOLETE	2
};

/*
 * Kernel configurations.
 */
struct config {
	TAILQ_ENTRY(config) cf_next;
	const char *cf_name;		/* "netbsd" */
	int	cf_lineno;		/* source line */
	const char *cf_fstype;		/* file system type */
	struct	nvlist *cf_root;	/* "root on ra0a" */
	struct	nvlist *cf_swap;	/* "swap on ra0b and ra1b" */
	struct	nvlist *cf_dump;	/* "dumps on ra0b" */
};

/*
 * Attributes.  These come in three flavors: "plain", "device class,"
 * and "interface".  Plain attributes (e.g., "ether") simply serve
 * to pull in files.  Device class attributes are like plain
 * attributes, but additionally specify a device class (e.g., the
 * "disk" device class attribute specifies that devices with the
 * attribute belong to the "DV_DISK" class) and are mutually exclusive.
 * Interface attributes (e.g., "scsi") carry three lists: locators,
 * child devices, and references.  The locators are those things
 * that must be specified in order to configure a device instance
 * using this attribute (e.g., "tg0 at scsi0").  The a_devs field
 * lists child devices that can connect here (e.g., "tg"s), while
 * the a_refs are parents that carry the attribute (e.g., actual
 * SCSI host adapter drivers such as the SPARC "esp").
 */
struct attr {
	const char *a_name;		/* name of this attribute */
	int	a_iattr;		/* true => allows children */
	const char *a_devclass;		/* device class described */
	struct	nvlist *a_locs;		/* locators required */
	int	a_loclen;		/* length of above list */
	struct	nvlist *a_devs;		/* children */
	struct	nvlist *a_refs;		/* parents */
	struct	nvlist *a_deps;		/* we depend on these other attrs */
	int	a_expanding;		/* to detect cycles in attr graph */
};

/*
 * Parent specification.  Multiple device instances may share a
 * given parent spec.  Parent specs are emitted only if there are
 * device instances which actually reference it.
 */
struct pspec {
	TAILQ_ENTRY(pspec) p_list;	/* link on parent spec list */
	struct	attr *p_iattr;		/* interface attribute of parent */
	struct	devbase *p_atdev;	/* optional parent device base */
	int	p_atunit;		/* optional parent device unit */
	struct	nvlist *p_devs;		/* children using it */
	int	p_inst;			/* parent spec instance */
	int	p_active;		/* parent spec is actively used */
};

/*
 * The "base" part (struct devbase) of a device ("uba", "sd"; but not
 * "uba2" or "sd0").  It may be found "at" one or more attributes,
 * including "at root" (this is represented by a NULL attribute), as
 * specified by the device attachments (struct deva).
 *
 * Each device may also export attributes.  If any provide an output
 * interface (e.g., "esp" provides "scsi"), other devices (e.g.,
 * "tg"s) can be found at instances of this one (e.g., "esp"s).
 * Such a connection must provide locators as specified by that
 * interface attribute (e.g., "target").  The base device can
 * export both output (aka `interface') attributes, as well as
 * import input (`plain') attributes.  Device attachments may
 * only import input attributes; it makes no sense to have a
 * specific attachment export a new interface to other devices.
 *
 * Each base carries a list of instances (via d_ihead).  Note that this
 * list "skips over" aliases; those must be found through the instances
 * themselves.  Each base also carries a list of possible attachments,
 * each of which specify a set of devices that the device can attach
 * to, as well as the device instances that are actually using that
 * attachment.
 */
struct devbase {
	const char *d_name;		/* e.g., "sd" */
	TAILQ_ENTRY(devbase) d_next;
	int	d_isdef;		/* set once properly defined */
	int	d_ispseudo;		/* is a pseudo-device */
	devmajor_t d_major;		/* used for "root on sd0", e.g. */
	struct	nvlist *d_attrs;	/* attributes, if any */
	int	d_umax;			/* highest unit number + 1 */
	struct	devi *d_ihead;		/* first instance, if any */
	struct	devi **d_ipp;		/* used for tacking on more instances */
	struct	deva *d_ahead;		/* first attachment, if any */
	struct	deva **d_app;		/* used for tacking on attachments */
	struct	attr *d_classattr;	/* device class attribute (if any) */
};

struct deva {
	const char *d_name;		/* name of attachment, e.g. "com_isa" */
	TAILQ_ENTRY(deva) d_next;	/* list of all instances */
	struct	deva *d_bsame;		/* list on same base */
	int	d_isdef;		/* set once properly defined */
	struct	devbase *d_devbase;	/* the base device */
	struct	nvlist *d_atlist;	/* e.g., "at tg" (attr list) */
	struct	nvlist *d_attrs;	/* attributes, if any */
	struct	devi *d_ihead;		/* first instance, if any */
	struct	devi **d_ipp;		/* used for tacking on more instances */
};

/*
 * An "instance" of a device.  The same instance may be listed more
 * than once, e.g., "xx0 at isa? port FOO" + "xx0 at isa? port BAR".
 *
 * After everything has been read in and verified, the devi's are
 * "packed" to collect all the information needed to generate ioconf.c.
 * In particular, we try to collapse multiple aliases into a single entry.
 * We then assign each "primary" (non-collapsed) instance a cfdata index.
 * Note that there may still be aliases among these.
 */
struct devi {
	/* created while parsing config file */
	const char *i_name;	/* e.g., "sd0" */
	int	i_unit;		/* unit from name, e.g., 0 */
	struct	devbase *i_base;/* e.g., pointer to "sd" base */
	TAILQ_ENTRY(devi) i_next; /* list of all instances */
	struct	devi *i_bsame;	/* list on same base */
	struct	devi *i_asame;	/* list on same base attachment */
	struct	devi *i_alias;	/* other aliases of this instance */
	const char *i_at;	/* where this is "at" (NULL if at root) */
	struct	pspec *i_pspec;	/* parent spec (NULL if at root) */
	struct	deva *i_atdeva;
	const char **i_locs;	/* locators (as given by pspec's iattr) */
	int	i_cfflags;	/* flags from config line */
	int	i_lineno;	/* line # in config, for later errors */
	const char *i_srcfile;	/* file it appears in */
	int	i_level;	/* position between negated instances */
	int	i_active;
#define	DEVI_ORPHAN	0	/* instance has no active parent */
#define	DEVI_ACTIVE	1	/* instance has an active parent */
#define	DEVI_IGNORED	2	/* instance's parent has been removed */
#define DEVI_BROKEN	3	/* instance is broken (syntax error) */
	int	i_pseudoroot;	/* instance is pseudoroot */

	/* created during packing or ioconf.c generation */
	short	i_collapsed;	/* set => this alias no longer needed */
	short	i_cfindex;	/* our index in cfdata */
	short	i_locoff;	/* offset in locators.vec */

};
/* special units */
#define	STAR	(-1)		/* unit number for, e.g., "sd*" */
#define	WILD	(-2)		/* unit number for, e.g., "sd?" */

/*
 * Files or objects.  This structure defines the common fields
 * between the two.
 */
struct filetype
{
	const char *fit_srcfile;	/* the name of the "files" file that got us */
	u_short	fit_srcline;	/* and the line number */
	u_char	fit_flags;	/* as below */
	char	fit_lastc;	/* last char from path */
	const char *fit_path;	/* full file path */
	const char *fit_prefix;	/* any file prefix */
};
/* Anything less than 0x10 is sub-type specific */
#define FIT_NOPROLOGUE  0x10    /* Don't prepend $S/ */
#define FIT_FORCESELECT 0x20    /* Always include this file */

/*
 * Files.  Each file is either standard (always included) or optional,
 * depending on whether it has names on which to *be* optional.  The
 * options field (fi_optx) is actually an expression tree, with nodes
 * for OR, AND, and NOT, as well as atoms (words) representing some   
 * particular option.  The node type is stored in the nv_num field.
 * Subexpressions appear in the `next' field; for the binary operators
 * AND and OR, the left subexpression is first stored in the nv_ptr field.
 * 
 * For any file marked as needs-count or needs-flag, fixfiles() will
 * build fi_optf, a `flat list' of the options with nv_num fields that
 * contain counts or `need' flags; this is used in mkheaders().
 */
struct files {
	struct filetype fi_fit;
	TAILQ_ENTRY(files) fi_next;
	const  char *fi_tail;	/* name, i.e., strrchr(fi_path, '/') + 1 */
	const  char *fi_base;	/* tail minus ".c" (or whatever) */
	struct nvlist *fi_optx; /* options expression */
	struct nvlist *fi_optf; /* flattened version of above, if needed */
	const  char *fi_mkrule;	/* special make rule, if any */
};
#define fi_srcfile fi_fit.fit_srcfile
#define fi_srcline fi_fit.fit_srcline
#define fi_flags   fi_fit.fit_flags
#define fi_lastc   fi_fit.fit_lastc
#define fi_path    fi_fit.fit_path
#define fi_prefix  fi_fit.fit_prefix

/* flags */
#define	FI_SEL		0x01	/* selected */
#define	FI_NEEDSCOUNT	0x02	/* needs-count */
#define	FI_NEEDSFLAG	0x04	/* needs-flag */
#define	FI_HIDDEN	0x08	/* obscured by other(s), base names overlap */

/*
 * Objects and libraries.  This allows precompiled object and library
 * files (e.g. binary-only device drivers) to be linked in.
 */
struct objects {
	struct  filetype oi_fit;
	TAILQ_ENTRY(objects) oi_next;
	struct  nvlist *oi_optx;/* options expression */
	struct  nvlist *oi_optf;/* flattened version of above, if needed */
};

#define oi_srcfile oi_fit.fit_srcfile
#define oi_srcline oi_fit.fit_srcline
#define oi_flags   oi_fit.fit_flags
#define oi_lastc   oi_fit.fit_lastc
#define oi_path    oi_fit.fit_path
#define oi_prefix  oi_fit.fit_prefix

/* flags */
#define	OI_SEL		0x01	/* selected */
#define	OI_NEEDSFLAG	0x02	/* needs-flag */

#define	FX_ATOM		0	/* atom (in nv_name) */
#define	FX_NOT		1	/* NOT expr (subexpression in nv_next) */
#define	FX_AND		2	/* AND expr (lhs in nv_ptr, rhs in nv_next) */
#define	FX_OR		3	/* OR expr (lhs in nv_ptr, rhs in nv_next) */

/*
 * File/object prefixes.  These are arranged in a stack, and affect
 * the behavior of the source path.
 */
struct prefix {
	SLIST_ENTRY(prefix)	pf_next;	/* next prefix in stack */
	const char		*pf_prefix;	/* the actual prefix */
};

/*
 * Device major informations.
 */
struct devm {
	TAILQ_ENTRY(devm) dm_next;
	const char	*dm_srcfile;	/* the name of the "majors" file */
	u_short		dm_srcline;	/* the line number */
	const char	*dm_name;	/* [bc]devsw name */
	devmajor_t	dm_cmajor;	/* character major */
	devmajor_t	dm_bmajor;	/* block major */
	struct nvlist	*dm_opts;	/* options */
	struct nvlist	*dm_devnodes;	/* information on /dev nodes */
};

/*
 * Hash tables look up name=value pairs.  The pointer value of the name
 * is assumed to be constant forever; this can be arranged by interning
 * the name.  (This is fairly convenient since our lexer does this for
 * all identifier-like strings---it has to save them anyway, lest yacc's
 * look-ahead wipe out the current one.)
 */
struct hashtab;

EXTERN int lkmmode;
EXTERN const char *conffile;		/* source file, e.g., "GENERIC.sparc" */
EXTERN const char *machine;		/* machine type, e.g., "sparc" or "sun3" */
EXTERN const char *machinearch;		/* machine arch, e.g., "sparc" or "m68k" */
EXTERN struct	nvlist *machinesubarches;
					/* machine subarches, e.g., "sun68k" or "hpc" */
EXTERN const char *ioconfname;		/* ioconf name, mutually exclusive to machine */
EXTERN const char *srcdir;		/* path to source directory (rel. to build) */
EXTERN const char *builddir;		/* path to build directory */
EXTERN const char *defbuilddir;		/* default build directory */
EXTERN const char *ident;		/* kernel "ident"ification string */
EXTERN int	errors;			/* counts calls to error() */
EXTERN int	minmaxusers;		/* minimum "maxusers" parameter */
EXTERN int	defmaxusers;		/* default "maxusers" parameter */
EXTERN int	maxmaxusers;		/* default "maxusers" parameter */
EXTERN int	maxusers;		/* configuration's "maxusers" parameter */
EXTERN int	maxpartitions;		/* configuration's "maxpartitions" parameter */
EXTERN int	version;		/* version of the configuration file */
EXTERN struct	nvlist *options;	/* options */
EXTERN struct	nvlist *fsoptions;	/* filesystems */
EXTERN struct	nvlist *mkoptions;	/* makeoptions */
EXTERN struct	nvlist *appmkoptions;	/* appending mkoptions */
EXTERN struct	nvlist *condmkoptions;	/* conditional makeoption table */
EXTERN struct	hashtab *devbasetab;	/* devbase lookup */
EXTERN struct	hashtab *devroottab;	/* attach at root lookup */
EXTERN struct	hashtab *devatab;	/* devbase attachment lookup */
extern struct	hashtab *devitab;	/* device instance lookup */
EXTERN struct	hashtab *deaddevitab;	/* removed instances lookup */
EXTERN struct	hashtab *selecttab;	/* selects things that are "optional foo" */
EXTERN struct	hashtab *needcnttab;	/* retains names marked "needs-count" */
EXTERN struct	hashtab *opttab;	/* table of configured options */
EXTERN struct	hashtab *fsopttab;	/* table of configured file systems */
EXTERN struct	hashtab *defopttab;	/* options that have been "defopt"'d */
EXTERN struct	hashtab *defflagtab;	/* options that have been "defflag"'d */
EXTERN struct	hashtab *defparamtab;	/* options that have been "defparam"'d */
EXTERN struct	hashtab *defoptlint;	/* lint values for options */
EXTERN struct	hashtab *deffstab;	/* defined file systems */
EXTERN struct	hashtab *optfiletab;	/* "defopt"'d option .h files */
EXTERN struct	hashtab *attrtab;	/* attributes (locators, etc.) */
EXTERN struct	hashtab *bdevmtab;	/* block devm lookup */
EXTERN struct	hashtab *cdevmtab;	/* character devm lookup */

TAILQ_HEAD(devbasetq, devbase);
TAILQ_HEAD(devatq, deva);
TAILQ_HEAD(conftq, config);
TAILQ_HEAD(devitq, devi);
TAILQ_HEAD(devmtq, devm);
TAILQ_HEAD(pspectq, pspec);

EXTERN struct devbasetq allbases;	/* list of all devbase structures */
EXTERN struct devatq alldevas;		/* list of all devbase attachments */
EXTERN struct conftq allcf;		/* list of configured kernels */
EXTERN struct devitq alldevi,		/* list of all instances */
		     allpseudo;		/* list of all pseudo-devices */
EXTERN struct devmtq alldevms;		/* list of all device-majors */
EXTERN struct pspectq allpspecs;	/* list of all parent specs */
EXTERN int	ndevi;			/* number of devi's (before packing) */
EXTERN int	npspecs;		/* number of parent specs */
EXTERN devmajor_t maxbdevm;		/* max number of block major */
EXTERN devmajor_t maxcdevm;		/* max number of character major */
EXTERN int	do_devsw;		/* 0 if pre-devsw config */
EXTERN int	oktopackage;		/* 0 before setmachine() */
EXTERN int	devilevel;		/* used for devi->i_level */

TAILQ_HEAD(filesq, files);
TAILQ_HEAD(objectsq, objects);

EXTERN struct filesq allfiles;		/* list of all kernel source files */
EXTERN struct objectsq allobjects;	/* list of all kernel object and
					   library files */

SLIST_HEAD(prefixlist, prefix);

EXTERN struct prefixlist prefixes,	/* prefix stack */
			 allprefixes;	/* all prefixes used (after popped) */

EXTERN struct devi **packed;		/* arrayified table for packed devi's */
EXTERN size_t npacked;			/* size of packed table, <= ndevi */

EXTERN struct locators {		/* loc[] table for config */
	const char **vec;
	int	used;
} locators;

struct numconst {
	int64_t	val;
	int fmt;
};

/* files.c */
void	initfiles(void);
void	checkfiles(void);
int	fixfiles(void);		/* finalize */
int	fixobjects(void);
int	fixdevsw(void);
void	addfile(const char *, struct nvlist *, int, const char *);
void	addobject(const char *, struct nvlist *, int);
int	expr_eval(struct nvlist *, int (*)(const char *, void *), void *);
void	expr_free(struct nvlist *);

/* hash.c */
struct	hashtab *ht_new(void);
void	ht_free(struct hashtab *);
int	ht_insrep(struct hashtab *, const char *, void *, int);
#define	ht_insert(ht, nam, val) ht_insrep(ht, nam, val, 0)
#define	ht_replace(ht, nam, val) ht_insrep(ht, nam, val, 1)
int	ht_remove(struct hashtab *, const char *);
void	*ht_lookup(struct hashtab *, const char *);
void	initintern(void);
const char *intern(const char *);
typedef int (*ht_callback)(const char *, void *, void *);
int	ht_enumerate(struct hashtab *, ht_callback, void *);

/* lint.c */
void	emit_instances(void);
void	emit_options(void);
void	emit_params(void);

/* main.c */
void	addoption(const char *, const char *);
void	addfsoption(const char *);
void	addmkoption(const char *, const char *);
void	appendmkoption(const char *, const char *);
void	appendcondmkoption(struct nvlist *, const char *, const char *);
void	deffilesystem(struct nvlist *, struct nvlist *);
void	defoption(const char *, struct nvlist *, struct nvlist *);
void	defflag(const char *, struct nvlist *, struct nvlist *, int);
void	defparam(const char *, struct nvlist *, struct nvlist *, int);
void	deloption(const char *);
void	delfsoption(const char *);
void	delmkoption(const char *);
int	devbase_has_instances(struct devbase *, int);
struct nvlist * find_declared_option(const char *);
int	deva_has_instances(struct deva *, int);
void	setupdirs(void);
const char *strtolower(const char *);

/* tests on option types */
#define OPT_FSOPT(n)	(ht_lookup(deffstab, (n)) != NULL)
#define OPT_DEFOPT(n)	(ht_lookup(defopttab, (n)) != NULL)
#define OPT_DEFFLAG(n)	(ht_lookup(defflagtab, (n)) != NULL)
#define OPT_DEFPARAM(n)	(ht_lookup(defparamtab, (n)) != NULL)
#define OPT_OBSOLETE(n)	(ht_lookup(obsopttab, (n)) != NULL)
#define DEFINED_OPTION(n) (find_declared_option((n)) != NULL)

/* main.c */
void	logconfig_include(FILE *, const char *);

/* mkdevsw.c */
int	mkdevsw(void);

/* mkheaders.c */
int	mkheaders(void);
int	moveifchanged(const char *, const char *);
int	emitlocs(void);
int	emitioconfh(void);

/* mkioconf.c */
int	mkioconf(void);

/* mkmakefile.c */
int	mkmakefile(void);

/* mkswap.c */
int	mkswap(void);

/* pack.c */
void	pack(void);

/* scan.l */
int	currentline(void);
int	firstfile(const char *);
void	package(const char *);
int	include(const char *, int, int, int);

/* sem.c, other than for yacc actions */
void	initsem(void);
int	onlist(struct nvlist *, void *);

/* util.c */
void	prefix_push(const char *);
void	prefix_pop(void);
char	*sourcepath(const char *);
void	cfgwarn(const char *, ...)			/* immediate warns */
     __attribute__((__format__(__printf__, 1, 2)));	
void	cfgxwarn(const char *, int, const char *, ...)	/* delayed warns */
     __attribute__((__format__(__printf__, 3, 4)));
void	cfgerror(const char *, ...)			/* immediate errs */
     __attribute__((__format__(__printf__, 1, 2)));
void	cfgxerror(const char *, int, const char *, ...)	/* delayed errs */
     __attribute__((__format__(__printf__, 3, 4)));
__dead void panic(const char *, ...)
     __attribute__((__format__(__printf__, 1, 2)));
struct nvlist *newnv(const char *, const char *, void *, long long, struct nvlist *);
void	nvfree(struct nvlist *);
void	nvfreel(struct nvlist *);
struct nvlist *nvcat(struct nvlist *, struct nvlist *);
void	autogen_comment(FILE *, const char *);

/* liby */
void	yyerror(const char *);
int	yylex(void);

#undef EXTERN
