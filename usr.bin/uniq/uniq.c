/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if !defined(lint) && !defined(SHELL)
__COPYRIGHT("@(#) Copyright (c) 2015\
 Takehiko NOZAKI, All rights reserved.");
#endif /* not lint */

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static const char *optstr = "cdiuzf:s:w:D";
static const struct option options[] = {
	{ "count",           no_argument,       NULL, 'c'         },
	{ "repeated",        no_argument,       NULL, 'd'         },
	{ "ignore-case",     no_argument,       NULL, 'i'         },
	{ "unique",          no_argument,       NULL, 'u'         },
	{ "zero-terminated", no_argument,       NULL, 'z'         },
	{ "skip-fields",     required_argument, NULL, 'f'         },
	{ "skip-chars",      required_argument, NULL, 's'         },
	{ "check-chars",     required_argument, NULL, 'w'         },
	{ "all-repeated",    optional_argument, NULL, UCHAR_MAX+1 },
	{ "group",           optional_argument, NULL, UCHAR_MAX+2 },
	{ NULL,              0,                 NULL,  0          },
};

typedef int uqmode_t;

#define UQM_COUNT		0x1
#define UQM_REPEATED		0x2
#define UQM_UNIQUE		0x4
#define UQM_POSIX \
    (UQM_COUNT|UQM_REPEATED|UQM_UNIQUE)

#define UQM_ALREP_NONE		0x10
#define UQM_ALREP_PREPEND	0x20
#define UQM_ALREP_SEPARATE	0x40
#define UQM_ALREP \
    (UQM_ALREP_NONE|UQM_ALREP_PREPEND|UQM_ALREP_SEPARATE)

#define UQM_GROUP_SEPARATE	0x100
#define UQM_GROUP_PREPEND	0x200
#define UQM_GROUP_APPEND	0x400
#define UQM_GROUP_BOTH \
    (UQM_GROUP_PREPEND|UQM_GROUP_APPEND)
#define UQM_GROUP \
    (UQM_GROUP_SEPARATE|UQM_GROUP_BOTH)

struct subopt {
	const char *name;
	uqmode_t mode;
};

static const struct subopt alrep[] = {
	{ "none",     UQM_ALREP_NONE     },
	{ "prepend",  UQM_ALREP_PREPEND  },
	{ "separate", UQM_ALREP_SEPARATE },
	{ NULL,       0                  }
};

static const struct subopt group[] = {
	{ "separate", UQM_GROUP_SEPARATE },
	{ "prepend",  UQM_GROUP_PREPEND  },
	{ "append",   UQM_GROUP_APPEND   },
	{ "both",     UQM_GROUP_BOTH     },
	{ NULL,       0                  }
};

struct line {
	char *str, *buf;
	size_t bufsiz;
	ssize_t buflen;
};

struct config {
	FILE *reader, *writer;
	char delim;
	uqmode_t mode;
	ssize_t fields, chars, check;
	int (*readfn)(struct config *, struct line *);
	int (*cmpfn)(struct config *, struct line *, struct line *);
};

static inline void
line_init(struct line *l)
{
	l->buf = NULL;
}

static inline void
line_uninit(struct line *l)
{
	free(l->buf);
}

static inline int
line_read(struct config *cf, struct line *l)
{
	l->buflen = getdelim(&l->buf, &l->bufsiz,
	    (int)cf->delim, cf->reader);
	if (l->buflen == -1) {
		if (ferror(cf->reader))
			err(EXIT_FAILURE, "getline");
		return 1;
	}
	l->str = l->buf;
	return 0;
}

static int
line_skip(struct config *cf, struct line *l)
{
	ssize_t fields, chars;

	if (line_read(cf, l))
		return 1;
	for (fields = cf->fields; fields > 0 && *l->str; --fields) {
		while (isblank((unsigned char)*l->str))
			++l->str;
		while (*l->str && !isblank((unsigned char)*l->str))
			++l->str;
	}
	for (chars = cf->chars; chars > 0 && *l->str; --chars)
		++l->str;
	return 0;
}

static int
line_strcmp(struct config *cf, struct line *a, struct line *b)
{
	return strcmp(a->str, b->str);
}

static int
line_strncmp(struct config *cf, struct line *a, struct line *b)
{
	return strncmp(a->str, b->str, (size_t)cf->check);
}

static int
line_strcasecmp(struct config *cf, struct line *a, struct line *b)
{
	return strcasecmp(a->str, b->str);
}

static int
line_strncasecmp(struct config *cf, struct line *a, struct line *b)
{
	return strncasecmp(a->str, b->str, (size_t)cf->check);
}

static inline void
line_write(struct config *cf, struct line *l)
{
	if (fwrite(l->buf, 1, (size_t)l->buflen, cf->writer) != (size_t)
	    l->buflen)
		err(EXIT_FAILURE, "fwrite");
}

static inline void
line_delim(struct config *cf)
{
	if (fwrite(&cf->delim, 1, 1, cf->writer) != 1)
		err(EXIT_FAILURE, "fwrite");
}

static inline void
line_show(struct config *cf, struct line *l, size_t cnt)
{
	if (cnt == 0) {
		if (cf->mode & UQM_REPEATED)
			return;
	} else {
		if (cf->mode & UQM_UNIQUE)
			return;
	}
	if (cf->mode & UQM_COUNT)
		if (fprintf(cf->writer, "%4zd ", cnt + 1) == -1)
			err(EXIT_FAILURE, "fprintf");
	line_write(cf, l);
}

static inline void
line_swap(struct line *a, struct line *b)
{
	struct line c;

	c = *a;
	*a = *b;
	*b = c;
}

static inline int
parse_number(const char *s, long *value)
{
	char *t;
	long l;

	errno = 0;
	l = strtol(s, &t, 10);
	if (l < 0 || (l == LONG_MAX && errno == ERANGE) ||
	    s == t || *t != '\0')
		return 1;
	*value = l;
	return 0;
}

static inline int
parse_length(const char *option, const char *s, ssize_t *value)
{
	long l;

	if (parse_number(s, &l) == 0) {
		*value = (ssize_t)l;
		return 0;
	}
	warnx("invalid argument for '--%s': %s",
	    option, s);
	return 1;
}

static inline int
parse_subopt(const struct subopt *head,
    const char *option, const char *s, uqmode_t *mode)
{
	size_t n;
	const struct subopt *p;

	if (s == NULL) {
		*mode |= head->mode;
		return 0;
	}
	n = strlen(s);
	for (p = head; p->name != NULL; ++p) {
		if (!strncasecmp(s, p->name, n)) {
			*mode |= p->mode;
			return 0;
		}
	}
	warnx("invalid argument for '--%s': %s",
	    option, s);
	return 1;
}

static FILE *
file_open(const char *name, const char *mode)
{
	FILE *fp;

	fp = fopen(name, mode);
	if (fp == NULL)
		err(EXIT_FAILURE, "fopen: %s", name);
	return fp;
}

static void
file_close(FILE *fp)
{
	if (fclose(fp) == EOF)
		errx(EXIT_FAILURE, "fclose");
}

static inline char *
fix_argument(const char *arg)
{
	long value;
	char *s;

	switch (arg[0]) {
	case '-':
		if (parse_number(&arg[1], &value) == 0) {
			if (asprintf(&s, "-f %lu", value) == -1)
				err(EXIT_FAILURE, "asprintf");
			return s;
		}
		break;
	case '+':
		if (parse_number(&arg[1], &value) == 0) {
			if (asprintf(&s, "-s %lu", value) == -1)
				err(EXIT_FAILURE, "asprintf");
			return s;
		}
		break;
	}
	s = strdup(arg);
	if (s == NULL)
		err(EXIT_FAILURE, "strdup");
	return s;
}

static inline int
validate_mode(struct config *cf)
{
	int cnt;

	cnt = 0;
	if (cf->mode & UQM_POSIX)
		++cnt;
	if (cf->mode & UQM_ALREP)
		++cnt;
	if (cf->mode & UQM_GROUP)
		++cnt;
	if (cnt < 2)
		return 0;
	warnx("-cdu, -D and -G options are incompatible.");
	return 1;
}

static inline int
config_init(struct config *cf, int argc, char **argv)
{
	int ignorecase, ch, idx;

	cf->reader = stdin;
	cf->writer = stdout;
	cf->delim = '\n';
	cf->mode = 0;
	cf->fields = cf->chars = 0;
	cf->check = -1;

	ignorecase = 0;

	while ((ch = getopt_long(argc, argv, optstr, options, &idx)) != -1) {
		switch (ch) {
		case 'c':
			cf->mode |= UQM_COUNT;
			break;
		case 'd':
			cf->mode |= UQM_REPEATED;
			break;
		case 'i':
			ignorecase = 1;
			break;
		case 'u':
			cf->mode |= UQM_UNIQUE;
			break;
		case 'z':
			cf->delim = '\0';
			break;
		case 'f':
			if (parse_length(options[idx].name, optarg,
			    &cf->fields))
				return 1;
			break;
		case 's':
			if (parse_length(options[idx].name, optarg,
			    &cf->chars))
				return 1;
			break;
		case 'w':
			if (parse_length(options[idx].name, optarg,
			    &cf->check))
				return 1;
			break;
		case 'D':
			cf->mode |= UQM_ALREP_NONE;
			break;
		case UCHAR_MAX+1:
			if (parse_subopt(&alrep[0],
			    options[idx].name, optarg, &cf->mode))
				return 1;
			break;
		case UCHAR_MAX+2:
			if (parse_subopt(&group[0],
			    options[idx].name, optarg, &cf->mode))
				return 1;
			break;
		case '?':
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	if (validate_mode(cf))
		return 1;

	switch (argc) {
	case 2:
		cf->writer = file_open(argv[1], "w");
	/*FALLTHROUGH*/
	case 1:
		if (strcmp(argv[0], "-"))
			cf->reader = file_open(argv[0], "r");
	/*FALLTHROUGH*/
	case 0:
		break;
	default:
		return 1;
	}

	cf->readfn = (cf->fields || cf->chars)
	    ? line_skip : line_read;
	cf->cmpfn = (cf->check != -1)
	    ? (ignorecase) ? line_strncasecmp : line_strncmp
	    : (ignorecase) ? line_strcasecmp : line_strcmp;

	return 0;
}

static inline void
config_uninit(struct config *cf)
{
	if (cf->reader != stdin)
		file_close(cf->reader);
	if (cf->writer != stdout)
		file_close(cf->writer);
}

static inline void
uniq_fast(struct config *cf, struct line *prev, struct line *cur)
{
	while ((*cf->readfn)(cf, cur) == 0) {
		if ((*cf->cmpfn)(cf, cur, prev)) {
			line_write(cf, prev);
			line_swap(prev, cur);
		}
	}
	line_write(cf, prev);
}

static inline void
uniq_posix(struct config *cf, struct line *prev, struct line *cur)
{
	ssize_t cnt;

	cnt = 0;
	while ((*cf->readfn)(cf, cur) == 0) {
		if ((*cf->cmpfn)(cf, cur, prev)) {
			line_show(cf, prev, cnt);
			line_swap(prev, cur);
			cnt = 0;
		} else {
			++cnt;
		}
	}
	line_show(cf, prev, cnt);
}

static inline void
uniq_alrep(struct config *cf, struct line *prev, struct line *cur)
{
	ssize_t cnt;
	int delim;

	cnt = -1;
	while ((*cf->readfn)(cf, cur) == 0) {
		if ((*cf->cmpfn)(cf, cur, prev)) {
			if (cnt > 0) {
				line_write(cf, prev);
				cnt = 0;
			}
		} else {
			delim = 0;
			switch (cnt) {
			case -1:
				if (cf->mode & UQM_ALREP_PREPEND)
					delim = 1;
				cnt = 0;
				break;
			case 0:
				if ((cf->mode & UQM_ALREP_NONE) == 0)
					delim = 1;
			}
			if (delim)
				line_delim(cf);
			line_write(cf, prev);
			++cnt;
		}
		line_swap(prev, cur);
	}
	if (cnt > 0)
		line_write(cf, prev);
	line_delim(cf);
}

static inline void
uniq_group(struct config *cf, struct line *prev, struct line *cur)
{
	ssize_t cnt;
	int delim;

	cnt = -1;
	while ((*cf->readfn)(cf, cur) == 0) {
		delim = 0;
		switch (cnt) {
		case -1:
			if (cf->mode & UQM_GROUP_PREPEND)
				delim = 1;
			cnt = 0;
			break;
		case 0:
			delim = 1;
		}
		if ((*cf->cmpfn)(cf, cur, prev)) {
			if (cnt > 0)
				cnt = 0;
		} else {
			++cnt;
		}
		if (delim)
			line_delim(cf);
		line_write(cf, prev);
		line_swap(prev, cur);
	}
	if (cnt > 0)
		line_write(cf, prev);
	if (cf->mode & UQM_GROUP_APPEND)
		line_delim(cf);
	line_delim(cf);
}

static inline void
uniq(struct config *cf)
{
	struct line prev, cur;

	line_init(&prev);
	if ((*cf->readfn)(cf, &prev) == 0) {
		line_init(&cur);
		if (cf->mode & UQM_GROUP)
			uniq_group(cf, &prev, &cur);
		else if (cf->mode & UQM_ALREP)
			uniq_alrep(cf, &prev, &cur);
		else if (cf->mode & UQM_POSIX)
			uniq_posix(cf, &prev, &cur);
		else
			uniq_fast(cf, &prev, &cur);
		line_uninit(&cur);
	}
	line_uninit(&prev);
}

static inline void
usage(void)
{
	fprintf(stderr,
"Usage: %s [-cdu | --all-repeated[=method] | --group[=method]] [-iz] \\\n"
"  [-f fields] [-s chars] [-w check] [input_file [output_file]]\n",
	getprogname());
	exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	char **copy_argv;
	int i;
	struct config cf;

	setprogname((const char *)argv[0]);

	setlocale(LC_ALL, "");

	copy_argv = calloc(argc, sizeof(*argv));
	if (copy_argv == NULL)
		err(EXIT_FAILURE, "calloc");
	for (i = 0; i < argc; ++i)
		copy_argv[i] = fix_argument(argv[i]);
	if (config_init(&cf, argc, copy_argv))
		usage();
	uniq(&cf);
	config_uninit(&cf);

	exit(EXIT_SUCCESS);
}
