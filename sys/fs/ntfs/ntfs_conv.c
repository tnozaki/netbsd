/*	$NetBSD: ntfs_conv.c,v 1.9 2008/04/28 20:24:02 martin Exp $	*/

/*-
 * Copyright (c) 2001 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * File name recode stuff.
 */

#include <sys/cdefs.h>
__KERNEL_RCSID(0, "$NetBSD: ntfs_conv.c,v 1.9 2008/04/28 20:24:02 martin Exp $");

#include <sys/param.h>
#include <sys/systm.h>
#include <sys/namei.h>
#include <sys/proc.h>
#include <sys/kernel.h>
#include <sys/vnode.h>
#include <sys/mount.h>
#include <sys/buf.h>
#include <sys/file.h>
#include <sys/malloc.h>
#include <sys/lock.h>

#include <miscfs/specfs/specdev.h>

#include <fs/ntfs/ntfs.h>
#include <fs/ntfs/ntfsmount.h>
#include <fs/ntfs/ntfs_inode.h>
#include <fs/ntfs/ntfs_vfsops.h>
#include <fs/ntfs/ntfs_subr.h>
#include <fs/ntfs/ntfs_compr.h>
#include <fs/ntfs/ntfs_ihash.h>

static __inline int
ntfs_utf8_size(codepoint x)
{
	if (x <= 0x80)
		return 1;
	else if (x >= 0x80 && x <= 0x7ff)
		return 2;
	else if ((x >= 0x800 && x < 0xd800) || (x > 0xdfff && x <= 0xffff))
		return 3;
	else if (x >= 0x10000 && x <= 0x1fffff)
		return 4;
	return 0;
}

/*
 * Read one wide character off the string, shift the string pointer
 * and return the character.
 */
int
/*ARGSUSED*/
ntfs_utf8_wget(void *ctx, const char **psrc, size_t *psrc_len,
    wchar *dst, size_t dst_len, size_t *nresult)
{
	const char *src;
	size_t src_len, mblen, wclen, i;
	codepoint x;

	src = *psrc;
	src_len = *psrc_len;

	if (src_len < 1)
		return EINVAL;
	if ((src[0] & 0x80) == 0) {
		mblen = 1;
		dst[0] = src[0] & 0xff;
		wclen = 1;
	} else {
		if ((src[0] & 0xe0) == 0xc0)
			mblen = 2;
		else if ((src[0] & 0xf0) == 0xe0)
			mblen = 3;
		else if ((src[0] & 0xf8) == 0xf0)
			mblen = 4;
		else
			return EILSEQ;
		if (src_len < mblen)
			return EINVAL;
		x = src[0] & (0x7f >> mblen);
		for (i = 1; i < mblen; ++i) {
			if ((src[i] & 0xc0) != 0x80)
				return EILSEQ;
			x <<= 6;
			x |= (src[i] & 0x3f);
		}
		if (ntfs_utf8_size(x) != mblen)
			return EILSEQ;
		if (mblen == 4) {
			x -= 0x10000;
			dst[0] = (x >> 10) + 0xd800;
			dst[1] = (x & 0x3ff) + 0xdc00;
			wclen = 2;
		} else {
			dst[0] = x;
			wclen = 1;
		}
	}

	*psrc = src + mblen;
	*psrc_len = src_len - mblen;
	*nresult = wclen;

	return 0;

}

/*
 * Encode wide character and write it to the string. 'n' specifies
 * how much space there is in the string. Returns number of bytes written
 * to the target string.
 */
int
/*ARGSUSED*/
ntfs_utf8_wput(void *ctx, const wchar **src, size_t *src_len,
    char *dst, size_t dst_len, size_t *nresult)
{
	size_t mblen, i;
	int error;
	codepoint x;

	error = ntfs_wchar_to_codepoint(src, src_len, &x);
	if (error)
		return error;

	mblen = ntfs_utf8_size(x);
	if (mblen == 0)
		return EILSEQ;
	if (dst_len < mblen)
		return E2BIG;

	for (i = mblen - 1; i > 0; --i) {
		dst[i] = (x & 0x3f) | 0x80;
		x >>= 6;
	}
	dst[0] = x;
	if (mblen == 1) {
		dst[0] &= 0x7f;
	} else {
		dst[0] &= (0x7f >> mblen);
		dst[0] |= ((0xff00 >> mblen) & 0xff);
	}
	*nresult = mblen;

	return 0;
}

/*
 * Compare two wide characters, returning 1, 0, -1 if the first is
 * bigger, equal or lower than the second.
 */
int
ntfs_utf8_wcmp(codepoint x1, codepoint x2)
{
	/* no conversions needed for utf8 */

	if (x1 == x2)
		return 0;
	else
		return (int32_t)x1 - (int32_t)x2;
}

int
ntfs_wchar_to_codepoint(const wchar **psrc, size_t *psrc_len, codepoint *px)
{
	const wchar *src;
	size_t src_len;
	codepoint x;
	size_t n;

	src = *psrc;
	src_len = *psrc_len;

	if (src_len < 1)
		return EINVAL;
	if (src[0] >= 0xd800 && src[0] <= 0xdbff) {
		if (src_len < 2)
			return EINVAL;
		if (src[1] < 0xdc00 || src[1] > 0xdfff)
			return EILSEQ;
		x = ((src[0] - 0xd800) << 10 | (src[1] - 0xdc00)) + 0x10000;
		n = 2;
	} else {
		if (src[0] >= 0xdc00 && src[0] <= 0xdfff)
			return EILSEQ;
		x = src[0];
		n = 1;
	}

	*psrc = src + n;
	*psrc_len = src_len - n;
	*px = x;

	return 0;
}
