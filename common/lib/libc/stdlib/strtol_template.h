/*-
 * Copyright (c) 2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if defined(_KERNEL)
#include <sys/param.h>
#include <lib/libkern/libkern.h>
#elif defined(_STANDALONE)
#include <sys/param.h>
#include <lib/libkern/libkern.h>
#include <lib/libsa/stand.h>
#else
#include <assert.h>
#include <errno.h>
#endif

struct t_limit {
	NUMTYPE	cutoff;
	int cutlim;
};

struct t_overflow {
	NUMTYPE maxval;
	struct t_limit limits[36 + 1];
};

static const struct t_overflow overflows[] = {
  {
    .maxval = NUMTYPE_MAX,
    .limits = {
#define LIMIT(base) [base] = {			\
	.cutoff = NUMTYPE_MAX / base,		\
	.cutlim = NUMTYPE_MAX % base		\
}
      LIMIT(2),  LIMIT(3),  LIMIT(4),  LIMIT(5),  LIMIT(6),
      LIMIT(7),  LIMIT(8),  LIMIT(9),  LIMIT(10), LIMIT(11),
      LIMIT(12), LIMIT(13), LIMIT(14), LIMIT(15), LIMIT(16),
      LIMIT(17), LIMIT(18), LIMIT(19), LIMIT(20), LIMIT(21),
      LIMIT(22), LIMIT(23), LIMIT(24), LIMIT(25), LIMIT(26),
      LIMIT(27), LIMIT(28), LIMIT(29), LIMIT(30), LIMIT(31),
      LIMIT(32), LIMIT(33), LIMIT(34), LIMIT(35), LIMIT(36)
#undef LIMIT
    }
#if defined(NUMTYPE_MIN)
  },
  {
    .maxval = NUMTYPE_MIN,
    .limits = {
#define LIMIT(base) [base] = {			\
	.cutoff = -(NUMTYPE_MIN / base),	\
	.cutlim = -(NUMTYPE_MIN % base)		\
}
      LIMIT(2),  LIMIT(3),  LIMIT(4),  LIMIT(5),  LIMIT(6),
      LIMIT(7),  LIMIT(8),  LIMIT(9),  LIMIT(10), LIMIT(11),
      LIMIT(12), LIMIT(13), LIMIT(14), LIMIT(15), LIMIT(16),
      LIMIT(17), LIMIT(18), LIMIT(19), LIMIT(20), LIMIT(21),
      LIMIT(22), LIMIT(23), LIMIT(24), LIMIT(25), LIMIT(26),
      LIMIT(27), LIMIT(28), LIMIT(29), LIMIT(30), LIMIT(31),
      LIMIT(32), LIMIT(33), LIMIT(34), LIMIT(35), LIMIT(36)
#undef LIMIT
    }
#endif
  }
};

#ifdef NUMTYPE_MIN
#define OVERFLOWS(neg)	&overflows[neg]
#else
#define OVERFLOWS(neg)	&overflows[0]
#endif

static __inline int
chartoint(INTTYPE c)
{
	switch (c) {
	case CHAR('0'): return 0;
	case CHAR('1'): return 1;
	case CHAR('2'): return 2;
	case CHAR('3'): return 3;
	case CHAR('4'): return 4;
	case CHAR('5'): return 5;
	case CHAR('6'): return 6;
	case CHAR('7'): return 7;
	case CHAR('8'): return 8;
	case CHAR('9'): return 9;
	case CHAR('A'): case CHAR('a'): return 10;
	case CHAR('B'): case CHAR('b'): return 11;
	case CHAR('C'): case CHAR('c'): return 12;
	case CHAR('D'): case CHAR('d'): return 13;
	case CHAR('E'): case CHAR('e'): return 14;
	case CHAR('F'): case CHAR('f'): return 15;
	case CHAR('G'): case CHAR('g'): return 16;
	case CHAR('H'): case CHAR('h'): return 17;
	case CHAR('I'): case CHAR('i'): return 18;
	case CHAR('J'): case CHAR('j'): return 19;
	case CHAR('K'): case CHAR('k'): return 20;
	case CHAR('L'): case CHAR('l'): return 21;
	case CHAR('M'): case CHAR('m'): return 22;
	case CHAR('N'): case CHAR('n'): return 23;
	case CHAR('O'): case CHAR('o'): return 24;
	case CHAR('P'): case CHAR('p'): return 25;
	case CHAR('Q'): case CHAR('q'): return 26;
	case CHAR('R'): case CHAR('r'): return 27;
	case CHAR('S'): case CHAR('s'): return 28;
	case CHAR('T'): case CHAR('t'): return 29;
	case CHAR('U'): case CHAR('u'): return 30;
	case CHAR('V'): case CHAR('v'): return 31;
	case CHAR('W'): case CHAR('w'): return 32;
	case CHAR('X'): case CHAR('x'): return 33;
	case CHAR('Y'): case CHAR('y'): return 34;
	case CHAR('Z'): case CHAR('z'): return 35;
	default: return 36; /* error */
	}
}

NUMTYPE
FUNCNAME(const CHARTYPE * __restrict str, CHARTYPE ** __restrict ptail,
    int base)
{
	NUMTYPE retval;
	const CHARTYPE *s, *t;
	int negative, autodetect, i;
	const struct t_overflow *overflow;
	const struct t_limit *limit;
#if defined(_LIBC) && !defined(NO_LOCALE)
	struct _locale_impl_t *impl;
#endif

	_DIAGASSERT(str != NULL);
	/* ptail may be null */

	retval = NUMTYPE_C(0);
	t = str;

	if (base < 0 || base == 1 || base > 36) {
#if defined(_KERNEL) || defined(_STANDALONE)
		panic("%s: invalid base %d", __func__, base);
#else
		errno = EINVAL;
		goto done;
#endif
	}

	s = str;
#if defined(_LIBC) && !defined(NO_LOCALE)
	impl = *_current_locale();
	while (ISSPACE_L(INTPROM(*s), impl))
#else
	while (ISSPACE(INTPROM(*s)))
#endif
		++s;

	negative = 0;
	switch (*s) {
	case CHAR('-'):
		negative = 1;
	/*FALLTHROUGH*/
	case CHAR('+'):
		++s;
	}

	autodetect = 0;
	if (base == 0 || base == 16) {
		do {
			if (*s != CHAR('0')) {
				autodetect = 10;
				break;
			}
			t = ++s;
			if (*s != CHAR('x') && *s != CHAR('X')) {
				autodetect = 8;
				break;
			}
			++s;
			autodetect = 16;
		} while (/*CONSTCOND*/0);
		if (base == 0)
			base = autodetect;
	}

	i = chartoint(INTPROM(*s));
	if (i >= base) {
#if !defined(_KERNEL) && !defined(_STANDALONE)
		if (autodetect == 10)
			errno = EINVAL;
#endif
		goto done;
	}
	++s;

	overflow = OVERFLOWS(negative);
	limit = &overflow->limits[base];

	for (;;) {
		if (retval > limit->cutoff ||
		    (retval == limit->cutoff && i > limit->cutlim)) {
#if defined(_KERNEL) || defined(_STANDALONE)
			t = str;
#else
			retval = overflow->maxval;
			for (;;) {
				i = chartoint(INTPROM(*s));
				if (i >= base)
					break;
				++s;
			}
			t = s;
			errno = ERANGE;
#endif
			goto done;
		}
		retval *= base;
		retval += i;
		i = chartoint(INTPROM(*s));
		if (i >= base)
			break;
		++s;
	}
	if (negative)
		retval = -retval;
	t = s;
done:
	if (ptail != NULL)
		*ptail = __UNCONST(t);
	return retval;
}
