/*	$NetBSD: zopen.c,v 1.2 2005/06/27 01:00:06 christos Exp $	*/

/*
 * Public domain stdio wrapper for libz, written by Johan Danielsson.
 */

#include <sys/cdefs.h>
#ifndef lint
__RCSID("$NetBSD: zopen.c,v 1.2 2005/06/27 01:00:06 christos Exp $");
#endif

#include <stdio.h>
#include <zlib.h>

FILE *zopen(const char *fname, const char *mode);

/* convert arguments */
static int
xgzread(void *cookie, char *data, int size)
{
    return gzread((gzFile)cookie, data, size);
}

static int
xgzwrite(void *cookie, const char *data, int size)
{
    return gzwrite((gzFile)cookie, __UNCONST(data), size);
}

static int
xgzclose(void *cookie)
{
    return gzclose((gzFile)cookie);
}

FILE *
zopen(const char *fname, const char *mode)
{
    gzFile gz = gzopen(fname, mode);
    if(gz == NULL)
	return NULL;

    if(*mode == 'r')
	return (funopen(gz, xgzread, NULL, NULL, xgzclose));
    else
	return (funopen(gz, NULL, xgzwrite, NULL, xgzclose));
}
