/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_multibyte.h"
COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");
#include <limits.h>

static void
dotest1(struct test *t, wchar_t *wcs, void *closure)
{
	size_t i, result, m, n;
	mbstate_t st;
	char mb[MB_LEN_MAX];

	memset(&st, 0, sizeof(st));
	for (i = 1; i <= t->norm_wc_size; ++i) {
		m = (i == t->norm_wc_size)
		    ? t->norm_mb_size : t->norm_mb_result[i];
		n = t->norm_mb_result[i - 1];
		errno = 0;
		result = wcrtomb(&mb[0], *wcs, &st);
		ATF_CHECK(result == m - n);
		ATF_CHECK(errno == 0);
		/* XXX: mb<->wc conversion is intrinsically irreversible. */
		ATF_CHECK(!memcmp(&mb[0], t->norm + n, result));
		++wcs;
	}
	ATF_CHECK(mbsinit(&st) != 0);
}

ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcrtomb test1");
}
ATF_TC_BODY(test1, tc)
{
	wctest_run(&dotest1, NULL);
}

static void
/*ARGSUSED*/
dotest2(struct test *t, wchar_t *wcs, void *closure)
{
	size_t result;
	char mb[MB_LEN_MAX];
	mbstate_t st;

	memset(&st, 0, sizeof(st));
	errno = 0;
	result = wcrtomb(&mb[0], t->abnorm_wc_ilseq, &st);
	ATF_CHECK(result == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcrtomb test2");
}
ATF_TC_BODY(test2, tc)
{
	wctest_run(&dotest2, NULL);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);

	return atf_no_error();
}
