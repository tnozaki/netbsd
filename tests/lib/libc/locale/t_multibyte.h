/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if defined(__NetBSD__)
#include <sys/cdefs.h>
#define COPYRIGHT(s)		__COPYRIGHT(s)
#include <atf-c.h>
#define arraycount(array)	__arraycount(array)
#else
#define COPYRIGHT(s)		/**/
#define arraycount(p)		(sizeof(p)/sizeof(p[0]))
#include <assert.h>
#include <stdio.h>
#define ATF_TC(arg0)		static void arg0##_head(void)
#define ATF_TC_HEAD(arg0, arg1)	static void arg0##_head()
#define atf_tc_set_md_var(arg0, arg1, ...) do {	\
	printf(__VA_ARGS__);			\
	puts("");				\
} while (/*CONSTCOND*/0)
#define ATF_TC_BODY(arg0, arg1)	static void arg0##_body()
#define ATF_CHECK(arg0)		assert(arg0)
#define ATF_TP_ADD_TCS(arg0)	int main(void)
#define ATF_TP_ADD_TC(arg0, arg1) do {	\
	arg1##_head();			\
	arg1##_body();			\
} while (/*CONSTCOND*/0)
#define atf_no_error()		0
#endif

#include <errno.h>
#include <locale.h>
#include <stddef.h>
#include <stdlib.h>
#include <wchar.h>
#include <string.h>

#define TEST_STRING_MAX	128

struct test {
	const char *locale;
	int stateful;
	/* normal case */
	const char *norm;
	size_t norm_mb_size;
	size_t norm_wc_size;
	size_t norm_mb_result[TEST_STRING_MAX];
	size_t norm_wc_result[TEST_STRING_MAX];
	/* abnormal case */
	const char *abnorm;
	size_t abnorm_mb_size;
	size_t abnorm_mb_result;
	wint_t abnorm_wc_ilseq;
} testcase[] = {

/* TODO: add libISO2022 test */

/* test for citrus_none */
	{
		"C",
		0, /* stateless */
		"ABCDE",
		6, 6,
		{
		    0,
		    1, 2, 3, 4, 5,
		    /* nul terminate */
		    5
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    /* nul terminate */
		    5
		},
		/* skip test, C locale never throw EILSEQ */
		NULL, 0, 0,
#if defined (__NetBSD__) || defined(__FreeBSD__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's C locale's wchar_t mapping is:
		 *
		 * GL:  ISO646-US       00000000 00000000 00000000 0xxxxxxx
		 *
		 * so 0xffffffff(11111111 11111111 11111111 11111111) should be
		 * EISEQ.
		 */
		(wint_t)0xffffffff
#elif defined(__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libEUC */
	{
		"ja_JP.eucJP",
		0, /* stateless */
		/* GL 1byte - ISO646-US */
		"ABCDE"
		/* GR 2byte - JIS X 0208 */
		"\xa4\xa2\xa4\xa3\xa4\xa4\xa4\xa5\xa4\xa6"
		/* SS2 2byte - JIS X 0201 KANA */
		"\x8e\xb1\x8e\xb2\x8e\xb3\x8e\xb4\x8e\xb5"
		/* SS3 3byte - JIS X 0212 */
		"\x8f\xaa\xa1\x8f\xaa\xa2\x8f\xaa\xa3\x8f\xaa\xa4\x8f\xaa\xa5",
		41, 21,
		{
		    0,
		    1, 2, 3, 4, 5,
		    7, 9, 11, 13, 15,
		    17, 19, 21, 23, 25,
		    28, 31, 34, 37, 40,
		    /* nul terminate */
		    40
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    5, 6, 6, 7, 7, 8, 8, 9, 9, 10,
		    10, 11, 11, 12, 12, 13, 13, 14, 14, 15,
		    15, 15, 16, 16, 16, 17, 17, 17, 18, 18, 18, 19, 19, 19, 20,
		    /* nul terminate */
		    20
		},
		/*
		 * XXX: FreeBSD may fail this, their euc.c doesn't check second byte.
		 */
		"ABCDE\xff\x7f", 8, 5,
#if defined (__NetBSD__) || defined(__FreeBSD__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's ja_JP.eucJP locale's wchar_t mapping is:
		 *
		 * GL:  ISO646-US       00000000 00000000 00000000 0xxxxxxx
		 * GR:  JIS X 0208      00000000 00000000 1xxxxxxx 1xxxxxxx
		 * SS2: JIS X 0201 KANA 00000000 00000000 00000000 1xxxxxxx
		 * SS3: JIS X 0212      00000000 00000000 1xxxxxxx 0xxxxxxx
		 *
		 * so 0xffffffff(11111111 11111111 11111111 11111111) should be
		 * EISEQ.
		 */
		(wint_t)0xffffffff
#elif defined(__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libMSKanji */
	{
		"ja_JP.SJIS",
		0, /* stateless */
		/* 1byte - JIS X 0201 ROMAN */
		"ABCDE"
		/* 1byte - JIS X 0201 KANA */
		"\xb1\xb2\xb3\xb4\xb5"
		/* 2byte - JIS X 0208 */
		"\x82\xa0\x82\xa2\x82\xa4\x82\xa6\x82\xa8",
		21, 16,
		{
		    0,
		    1, 2, 3, 4, 5,
		    6, 7, 8, 9, 10,
		    12, 14, 16, 18, 20,
		    /* nul terminate */
		    20
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    6, 7, 8, 9, 10,
		    10, 11, 11, 12, 12, 13, 13, 14, 14, 15,
		    /* nul terminate */
		    15
		},
		"ABCDE\x81\xff", 8, 5,
#if defined(__NetBSD__) || defined(__FreeBSD__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's ja_JP.SJIS locale's wchar_t mapping is 0x0000 - 0xfefe,
		 * so 0xffffffff should be EILSEQ.
		 */
		(wint_t)0xffffffff
#elif defined(__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libBIG5 */
	{
		"zh_TW.Big5",
		0, /* stateless */
		/* 1byte - IS646-US */
		"ABCDE"
		/* 2byte - Big5 */
		"\xa5\x40\xa5\x41\xa5\x42\xa5\x43\xa5\x44",
		16, 11,
		{
		    0,
		    1, 2, 3, 4, 5,
		    7, 9, 11, 13, 15,
		    /* nul terminate */
		    15
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    5, 6, 6, 7, 7, 8, 8, 9, 9, 10,
		    /* nul terminate */
		    10
		},
		"ABCDE\xa1\xff", 8, 5,
#if defined(__NetBSD__) || defined(__FreeBSD__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's zh_TW.Big5 locale's wchar_t mapping is 0x0000 - 0xfefe,
		 * so 0xffffffff should be EILSEQ.
		 */
		(wint_t)0xffffffff
#elif defined(__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libEUCTW */
	{
		"zh_TW.eucTW",
		0, /* stateless */
		/* GL 1byte - ISO646-US */
		"ABCDE"
		/* GR 2byte - CNS11643-1 */
		"\xa3\xa4\xa3\xa5\xa3\xa6\xa3\xa7\xa3\xa8"
		/* SS2 + 'G' 4byte - CNS11643-2 */
		"\x8e\xa2\xa1\xa1\x8e\xa2\xa1\xa1\x8e\xa2\xa1\xa1"
		"\x8e\xa2\xa1\xa1\x8e\xa2\xa1\xa1",
		36, 16,
		{
		    0,
		    1, 2, 3, 4, 5,
		    7, 9, 11, 13, 15,
		    19, 23, 27, 31, 35,
		    /* nul terminate */
		    35
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    5, 6, 6, 7, 7, 8, 8, 9, 9, 10,
		    10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13,
		    13, 13, 13, 14, 14, 14, 14, 15,
		    /* nul terminate */
		    15
		},
		"ABCDE\xff\x7f", 8, 5,
#if defined(__NetBSD__) || defined(__FreeBSD__)
		/* XXX: wchar_t is opaque object and implementation defined.
		 * citrus's zh_TW.eucTW locale's wchar_t mapping is:
		 *
		 * GL:  ISO646-US       00000000 00000000 00000000 0xxxxxxx
		 * GR:  CNS11643-1      0mmmmmmm 00000000 1xxxxxxx 1xxxxxxx
		 * SS2: CNS11643-2~7    0mmmmmmm 00000000 1xxxxxxx 1xxxxxxx
		 *
		 * so 0xffffffff(11111111 11111111 11111111 11111111) should be
		 * EISEQ.
		 */
		(wint_t)0xffffffff
#elif defined(__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libUTF8 */
	{
		"en_US.UTF-8",
		0, /* stateless */
	/* U+0000 - U+007F 1byte */
		"ABCDE"
	/* U+0080 - U+07FF 2byte */
		"\xc2\x80\xc2\x81\xc2\x82\xc2\x83\xc2\x84"
	/* U+0800 - U+FFFF 3byte  */
		"\xe3\x81\x82\xe3\x81\x84\xe3\x81\x86\xe3\x81\x88\xe3\x81\x8a"
	/* U+10000 - U+1FFFFF 4byte */
		"\xf0\x90\x80\x80\xf0\x90\x80\x81\xf0\x90\x80\x82"
		"\xf0\x90\x80\x83\xf0\x90\x80\x84",
		51, 21,
		{
		    0,
		    1, 2, 3, 4, 5,
		    7, 9, 11, 13, 15,
		    18, 21, 24, 27, 30,
		    34, 38, 42, 46, 50,
		    /* nul terminate */
		    50
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    5, 6, 6, 7, 7, 8, 8, 9, 9, 10,
		    10, 10, 11, 11, 11, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15,
		    15, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 18,
		    18, 18, 18, 19, 19, 19, 19, 20,
		    /* nul terminate */
		    20
		},
		"ABCDE\xff\x7f", 8, 5,
#if defined(__linux__)
		/* XXX glibc2 en_US.UTF-8 locale doesn't check surrogate */
		(wint_t)0xffffffff
#elif defined(__NetBSD__) || defined(__FreeBSD__) || defined (__STDC_ISO_10646__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's en_US.UTF-8 locale's wchar_t mapping is UCS4.
		 * so surrogate character (0xd800 - 0xdffff) should be EILSEQ.
		 */
		(wint_t)0xd800
#else
#error
#endif
	},
/* test for libGBK2K */
	{
		"zh_CN.GB18030",
		0, /* stateless */
	/* 1byte ISO646-US */
		"ABCDE"
	/* 2byte GB2312 compatible */
		"\xa4\xa1\xa4\xa2\xa4\xa3\xa4\xa4\xa4\xa5"
	/* 4byte GB18030 */
		"\x81\x30\x81\x30\x81\x30\x81\x31\x81\x30\x81\x32"
		"\x81\x30\x81\x33\x81\x30\x81\x34",
		36, 16,
		{
		    0,
		    1, 2, 3, 4, 5,
		    7, 9, 11, 13, 15,
		    19, 23, 27, 31, 35,
		    /* nul terminate */
		    35
		},
		{
		    0,
		    1, 2, 3, 4, 5,
		    5, 6, 6, 7, 7, 8, 8, 9, 9, 10,
		    10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13,
		    13, 13, 13, 14, 14, 14, 14, 15,
		    /* nul terminate */
		    15
		},
		"ABCDE\xa1\xff", 8, 5,
#if defined(__NetBSD__) || defined(__FreeBSD__)
		/*
		 * XXX: wchar_t is opaque object and implementation defined.
		 * citrus's zh_TW.Big5 locale's wchar_t mapping is 0x0000 - 0xfe39fe39,
		 * so 0xffffffff should be EILSEQ.
		 */
		(wint_t)0xffffffff
#elif defined (__STDC_ISO_10646__)
		/* XXX surrogate character (0xd800 - 0xdffff) should be EILSEQ. */
		(wint_t)0xd800
#else
#error
#endif
	}
};

static __inline void
mbtest_run(void (*dotest)(struct test *, void *), void *closure)
{
	size_t i;
	struct test *t;
	const char *locale;

	for (i = 0; i < arraycount(testcase); ++i) {
		t = &testcase[i];
		locale = setlocale(LC_CTYPE, t->locale);
		if (locale == NULL)
			continue; /* locale not supported */
		(*dotest)(t, closure);
	}
}

static __inline void
wctest_run(void (*dotest)(struct test *, wchar_t *, void *), void *closure)
{
	size_t i, assumed, converted;
	struct test *t;
	const char *locale;
	wchar_t wcs[TEST_STRING_MAX];

	for (i = 0; i < arraycount(testcase); ++i) {
		t = &testcase[i];
		locale = setlocale(LC_CTYPE, t->locale);
		if (locale == NULL)
			continue; /* locale not supported */
                assumed = mbstowcs(NULL, t->norm, 0);
                ATF_CHECK(assumed == t->norm_wc_size - 1);
                converted = mbstowcs(&wcs[0], t->norm, t->norm_wc_size);
                ATF_CHECK(assumed == converted);
		(*dotest)(t, &wcs[0], closure);
	}
}
