/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_multibyte.h"
COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

static void
dotest1(struct test *t, void *closure)
{
	size_t i, len, result;
	const char *str;
	wchar_t *pwc;
	mbstate_t st;

	pwc = (wchar_t *)closure;
	memset(&st, 0, sizeof(st));
	str = t->norm;
	len = t->norm_mb_size;
	for (i = 1; i <= t->norm_wc_size; ++i) {
		errno = 0;
		result = mbrtowc(pwc, str, len, &st);
		ATF_CHECK(result != (size_t)-1);
		ATF_CHECK(result != (size_t)-2);
		ATF_CHECK(errno == 0);
		if (i == t->norm_wc_size) {
			ATF_CHECK(result == 0);
			if (pwc)
				ATF_CHECK(*pwc == L'\0');
			ATF_CHECK(mbsinit(&st) != 0);
		} else {
			str += result;
			len -= result;
			result = (size_t)(str - t->norm);
			ATF_CHECK(result == t->norm_mb_result[i]);
		}
	}
}

ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbrtowc test1");
}
ATF_TC_BODY(test1, tc)
{
	wchar_t wc;

	mbtest_run(&dotest1, (void *)&wc);
	mbtest_run(&dotest1, NULL);
}

static void
dotest2(struct test *t, void *closure)
{
	size_t i, len, result, m, n;
	const char *str;
	wchar_t *pwc;
	mbstate_t st;

	pwc = (wchar_t *)closure;
	memset(&st, 0, sizeof(st));
	str = t->norm;
	len = t->norm_mb_size;
	m = 0;
	for (i = 1; i <= t->norm_mb_size; ++i) {
		n = t->norm_wc_result[i];
		errno = 0;
		result = mbrtowc(pwc, str, 1, &st);
		if (i == t->norm_mb_size) {
			ATF_CHECK(result == (size_t)0);
			ATF_CHECK(errno == 0);
			if (pwc)
				ATF_CHECK(*pwc == L'\0');
			ATF_CHECK(mbsinit(&st) != 0);
		} else if (m == n) {
			ATF_CHECK(result == (size_t)-2);
			ATF_CHECK(errno == 0);
			++str, --len;
		} else {
			ATF_CHECK(result == (size_t)1);
			ATF_CHECK(errno == 0);
			str += result;
			len -= result;
		}
		m = n;
	}
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbrtowc test2");
}
ATF_TC_BODY(test2, tc)
{
	wchar_t wc;

	mbtest_run(&dotest2, (void *)&wc);
	mbtest_run(&dotest2, NULL);
}

static void
/*ARGSUSED*/
dotest3(struct test *t, void *closure)
{
	size_t i, len, result;
	const char *str;
	wchar_t *pwc;
	mbstate_t st;

	pwc = (wchar_t *)closure;
	memset(&st, 0, sizeof(st));
	str = t->norm;
	len = t->norm_mb_size;
	for (i = 1; i <= t->norm_mb_size; ++i) {
		errno = 0;
		result = mbrtowc(pwc, str, 1, &st);
		if (result == (size_t)-2) {
			result = mbrtowc(pwc, NULL, 0, &st);
			ATF_CHECK(result == (size_t)-1);
			ATF_CHECK(errno == EILSEQ);
			break;
		}
		++str, --len;
	}
}
ATF_TC(test3);
ATF_TC_HEAD(test3, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbrtowc test3");
}
ATF_TC_BODY(test3, tc)
{
	wchar_t wc;

	mbtest_run(&dotest3, (void *)&wc);
	mbtest_run(&dotest3, NULL);
}

static void
/*ARGSUSED*/
dotest4(struct test *t, void *closure)
{
	size_t result;
	wchar_t *pwc;
	mbstate_t st;

	pwc = (wchar_t *)closure;
	memset(&st, 0, sizeof(st));
	errno = 0;
	result = mbrtowc(pwc, NULL, 0, &st);
	ATF_CHECK(result == (size_t)0);
	ATF_CHECK(errno == 0);
	if (pwc != NULL)
		ATF_CHECK(*pwc == L'\0');
	ATF_CHECK(mbsinit(&st) != 0);
}
ATF_TC(test4);
ATF_TC_HEAD(test4, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbrtowc test4");
}
ATF_TC_BODY(test4, tc)
{
	wchar_t wc;

	mbtest_run(&dotest4, (void *)&wc);
	mbtest_run(&dotest4, NULL);
}

static void
/*ARGSUSED*/
dotest5(struct test *t, void *closure)
{
	size_t result;
	wchar_t *pwc;
	mbstate_t st;

	pwc = (wchar_t *)closure;
	memset(&st, 0, sizeof(st));
	if (t->abnorm == NULL)
		return;
	errno = 0;
	result = mbrtowc(pwc, &t->abnorm[t->abnorm_mb_result],
	    t->abnorm_mb_size - t->abnorm_mb_result, &st);
	ATF_CHECK(result == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
}
ATF_TC(test5);
ATF_TC_HEAD(test5, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbrtowc test5");
}
ATF_TC_BODY(test5, tc)
{
	wchar_t wc;

	mbtest_run(&dotest5, (void *)&wc);
	mbtest_run(&dotest5, NULL);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);
	ATF_TP_ADD_TC(tp, test3);
	ATF_TP_ADD_TC(tp, test4);
	ATF_TP_ADD_TC(tp, test5);

	return atf_no_error();
}
