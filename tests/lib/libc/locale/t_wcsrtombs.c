/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_multibyte.h"
COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

static void
/*ARGSUSED*/
dotest1(struct test *t, wchar_t *wcs, void *closure)
{
	size_t assumed, converted;
	const wchar_t *pwcs;
	mbstate_t st;

	assumed = t->norm_mb_result[t->norm_wc_size];
	pwcs = wcs;
	memset(&st, 0, sizeof(st));
	errno = 0;
	converted = wcsrtombs(NULL, &pwcs, 0, &st);
	ATF_CHECK(assumed == converted);
	ATF_CHECK(errno == 0);
	ATF_CHECK(pwcs == wcs);
}
ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcsrtombs test1");
}
ATF_TC_BODY(test1, tc)
{
	wctest_run(&dotest1, NULL);
}

static void
/*ARGSUSED*/
dotest2(struct test *t, wchar_t *wcs, void *closure)
{
	size_t i, assumed, converted, pos;
	const wchar_t *pwcs;
	mbstate_t st;
	char str[TEST_STRING_MAX];

	for (i = 1; i <= t->norm_mb_size; ++i) {
		pwcs = wcs;
		memset(&st, 0, sizeof(st));
		errno = 0;
		converted = wcsrtombs(&str[0], &pwcs, i, &st);
		pos = t->norm_wc_result[i];
		assumed = t->norm_mb_result[pos];
		ATF_CHECK(assumed == converted);
		ATF_CHECK(errno == 0);
		if (i == t->norm_mb_size) {
			ATF_CHECK(!strcmp(t->norm, str));
			ATF_CHECK(pwcs == NULL);
			ATF_CHECK(mbsinit(&st) != 0);
		} else {
			ATF_CHECK(!strncmp(t->norm, str, converted));
			ATF_CHECK(pwcs == wcs + pos);
		}
	}
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcsrtombs test2");
}
ATF_TC_BODY(test2, tc)
{
	wctest_run(&dotest2, NULL);
}

static void
/*ARGSUSED*/
dotest3(struct test *t, wchar_t *wcs, void *closure)
{
	size_t converted, pos;
	const wchar_t *pwcs;
	mbstate_t st;

	pos = t->norm_wc_size - 2;
	wcs[pos] = t->abnorm_wc_ilseq;

	pwcs = wcs;
	memset(&st, 0, sizeof(st));
	errno = 0;
	converted = wcsrtombs(NULL, &pwcs, 0, &st);
	ATF_CHECK(converted == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
	ATF_CHECK(pwcs == wcs);
}
ATF_TC(test3);
ATF_TC_HEAD(test3, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcsrtombs test3");
}
ATF_TC_BODY(test3, tc)
{
	wctest_run(&dotest3, NULL);
}

static void
/*ARGSUSED*/
dotest4(struct test *t, wchar_t *wcs, void *closure)
{
	size_t assumed, converted, pos;
	const wchar_t *pwcs;
	char str[TEST_STRING_MAX];
	mbstate_t st;

	pos = t->norm_wc_size - 2;
	wcs[pos] = 0;

	pwcs = wcs;
	memset(&st, 0, sizeof(st));
	errno = 0;
	assumed = wcsrtombs(NULL, &pwcs, 0, &st);
	ATF_CHECK(assumed != (size_t)-1);
	ATF_CHECK(errno == 0);
	ATF_CHECK(pwcs == wcs);

	wcs[pos] = t->abnorm_wc_ilseq;

	pwcs = wcs;
	memset(&st, 0, sizeof(st));
	errno = 0;
	converted = wcsrtombs(&str[0], &pwcs, sizeof(str), &st);
	ATF_CHECK(converted == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
	ATF_CHECK(pwcs == wcs + pos);
	ATF_CHECK(!strncmp(t->norm, str, assumed));
}
ATF_TC(test4);
ATF_TC_HEAD(test4, tc)
{
	atf_tc_set_md_var(tc, "descr", "wcsrtombs test4");
}
ATF_TC_BODY(test4, tc)
{
	wctest_run(&dotest4, NULL);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);
	ATF_TP_ADD_TC(tp, test3);
	ATF_TP_ADD_TC(tp, test4);

	return atf_no_error();
}
