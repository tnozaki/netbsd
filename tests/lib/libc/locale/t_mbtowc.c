/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_multibyte.h"
COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

static void
/*ARGSUSED*/
dotest1(struct test *t, void *closure)
{
	int result;
	wchar_t *pwc;

	pwc = (wchar_t *)closure;
	result = mbtowc(pwc, NULL, 0);
	ATF_CHECK(!!result == t->stateful);
}
ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbtowc test1");
}
ATF_TC_BODY(test1, tc)
{
	wchar_t wc;

	mbtest_run(&dotest1, &wc);
	mbtest_run(&dotest1, NULL);
}

static void
/*ARGSUSED*/
dotest2(struct test *t, void *closure)
{
	int result;
	size_t i, head, tail;
	wchar_t *pwc;

	pwc = (wchar_t *)closure;
	result = mbtowc(pwc, NULL, 0);
	ATF_CHECK(!!result == t->stateful);

	for (i = 1; i <= t->norm_wc_size; ++i) {
		head = t->norm_mb_result[i - 1];
		tail = t->norm_mb_result[i];
		errno = 0;
		result = mbtowc(pwc, t->norm + head,
		    t->norm_mb_size - head);
		ATF_CHECK((size_t)result == tail - head);
		ATF_CHECK(errno == 0);
	}
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbtowc test2");
}
ATF_TC_BODY(test2, tc)
{
	wchar_t wc;

	mbtest_run(&dotest2, &wc);
	mbtest_run(&dotest2, NULL);
}

static void
/*ARGSUSED*/
dotest3(struct test *t, void *closure)
{
	int result;
	size_t i, j, head, tail, len;
	wchar_t *pwc;

	pwc = (wchar_t *)closure;
	result = mbtowc(pwc, NULL, 0);
	ATF_CHECK(!!result == t->stateful);

	for (i = 1; i <= t->norm_wc_size; ++i) {
		head = t->norm_mb_result[i - 1];
		tail = (i == t->norm_wc_size)
		    ? t->norm_mb_size : t->norm_mb_result[i];
		len = t->norm_mb_result[i] - t->norm_mb_result[i - 1];
		for (j = tail - 1; j > head; --j) {
			errno = 0;
			result = mbtowc(pwc, t->norm + head, tail - j);
			ATF_CHECK(result == -1);
			ATF_CHECK(errno == 0);
		}
		errno = 0;
		result = mbtowc(pwc, t->norm + head, tail - head);
		ATF_CHECK((size_t)result == len);
		ATF_CHECK(errno == 0);
	}
}
ATF_TC(test3);
ATF_TC_HEAD(test3, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbtowc test3");
}
ATF_TC_BODY(test3, tc)
{
	wchar_t wc;

	mbtest_run(&dotest3, &wc);
	mbtest_run(&dotest3, NULL);
}

static void
/*ARGSUSED*/
dotest4(struct test *t, void *closure)
{
	const char *str;
	size_t len;
	int result;
	wchar_t *pwc;

	if (t->abnorm == NULL)
		return;

	pwc = (wchar_t *)closure;
	result = mbtowc(pwc, NULL, 0);
	ATF_CHECK(!!result == t->stateful);

	str = t->abnorm + t->abnorm_mb_result;
	len = t->abnorm_mb_size - t->abnorm_mb_result;
	errno = 0;
	result = mbtowc(pwc, str, len);
	ATF_CHECK(result == -1);
	ATF_CHECK(errno == EILSEQ);
}
ATF_TC(test4);
ATF_TC_HEAD(test4, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbtowc test4");
}
ATF_TC_BODY(test4, tc)
{
	wchar_t wc;

	mbtest_run(&dotest4, &wc);
	mbtest_run(&dotest4, NULL);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);
	ATF_TP_ADD_TC(tp, test3);
	ATF_TP_ADD_TC(tp, test4);
	return atf_no_error();
}
