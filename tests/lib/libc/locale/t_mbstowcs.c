/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_multibyte.h"
COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

static void
/*ARGSUSED*/
dotest1(struct test *t, void *closure)
{
	size_t result;

	errno = 0;
	result = mbstowcs(NULL, t->norm, 0);
	ATF_CHECK(result == t->norm_wc_result[t->norm_mb_size]);
	ATF_CHECK(errno == 0);
}
ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbstowcs test1");
}
ATF_TC_BODY(test1, tc)
{
	mbtest_run(&dotest1, NULL);
}

static void
/*ARGSUSED*/
dotest2(struct test *t, void *closure)
{
	size_t i, result;
	wchar_t wcs[TEST_STRING_MAX];

	for (i = 0; i <= t->norm_wc_size; ++i) {
		errno = 0;
		result = mbstowcs(&wcs[0], t->norm, i);
		ATF_CHECK(errno == 0);
		if (i == t->norm_wc_size) {
			ATF_CHECK(result == i - 1);
		} else {
			ATF_CHECK(result == i);
		}
	}
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbstowcs test2");
}
ATF_TC_BODY(test2, tc)
{
	mbtest_run(&dotest2, NULL);
}

static void
/*ARGSUSED*/
dotest3(struct test *t, void *closure)
{
	size_t result;

	if (t->abnorm == NULL)
		return; /* skip test */

	errno = 0;
	result = mbstowcs(NULL, t->abnorm, 0);
	ATF_CHECK(result == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
}
ATF_TC(test3);
ATF_TC_HEAD(test3, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbstowcs test3");
}
ATF_TC_BODY(test3, tc)
{
	mbtest_run(&dotest3, NULL);
}

static void
/*ARGSUSED*/
dotest4(struct test *t, void *closure)
{
	size_t result;
	wchar_t wcs[TEST_STRING_MAX];
	mbstate_t st;

	if (t->abnorm == NULL)
		return; /* skip test */

	memset(&st, 0, sizeof(st));
	errno = 0;
	result = mbstowcs(&wcs[0], t->abnorm, arraycount(wcs));
	ATF_CHECK(result == (size_t)-1);
	ATF_CHECK(errno == EILSEQ);
}
ATF_TC(test4);
ATF_TC_HEAD(test4, tc)
{
	atf_tc_set_md_var(tc, "descr", "mbstowcs test4");
}
ATF_TC_BODY(test4, tc)
{
	mbtest_run(&dotest4, NULL);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);
	ATF_TP_ADD_TC(tp, test3);
	ATF_TP_ADD_TC(tp, test4);

	return atf_no_error();
}
