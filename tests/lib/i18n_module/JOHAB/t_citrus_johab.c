/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

/*
 *
 */
struct charmap johab_charmap[] = {
	/* ISO646-KR */
	{ "",                 1, 0x00000000, 0x00000000, 0x00000000 },
	{ "\x7f",             1, 0x0000007f, 0x00000000, 0x0000007f },
	/*
	 * Hangul
	 *     1st byte : 0x84-0xd3
	 *     2nd byte : 0x41-0x7e, 0x81-0xfe
	 */
	{ "\x84\x41",         2, 0x00008441, 0x00000001, 0x00008441 },
	{ "\x84\x7e",         2, 0x0000847e, 0x00000001, 0x0000847e },
	{ "\x84\x81",         2, 0x00008481, 0x00000001, 0x00008481 },
	{ "\x84\xfe",         2, 0x000084fe, 0x00000001, 0x000084fe },
	{ "\xd3\x41",         2, 0x0000d341, 0x00000001, 0x0000d341 },
	{ "\xd3\x7e",         2, 0x0000d37e, 0x00000001, 0x0000d37e },
	{ "\xd3\x81",         2, 0x0000d381, 0x00000001, 0x0000d381 },
	{ "\xd3\xfe",         2, 0x0000d3fe, 0x00000001, 0x0000d3fe },
	/*
	 * User-defined area
	 *     1st byte : 0xd8
	 *     2nd byte : 0x31-0x7e, 0x91-0xfe
	 */
	{ "\xd8\x31",         2, 0x0000d831, 0x00000001, 0x0000d831 },
	{ "\xd8\x7e",         2, 0x0000d87e, 0x00000001, 0x0000d87e },
	{ "\xd8\x91",         2, 0x0000d891, 0x00000001, 0x0000d891 },
	{ "\xd8\xfe",         2, 0x0000d8fe, 0x00000001, 0x0000d8fe },
	/*
	 * Hanja & Symbol
	 * (can be arithmetically translated from KS X 1001 position)
	 *     1st byte : 0xd9-0xde, 0xe0-0xf9
	 *     2nd byte : 0x31-0x7e, 0x91-0xfe
	 */
	{ "\xd9\x31",         2, 0x0000d931, 0x00000002, 0x00002121 },
	{ "\xd9\x7e",         2, 0x0000d97e, 0x00000002, 0x0000216e },
	{ "\xd9\x91",         2, 0x0000d991, 0x00000002, 0x0000216f },
	{ "\xd9\xfe",         2, 0x0000d9fe, 0x00000002, 0x0000227e },
	{ "\xde\x31",         2, 0x0000de31, 0x00000002, 0x00002b21 },
	{ "\xde\x7e",         2, 0x0000de7e, 0x00000002, 0x00002b6e },
	{ "\xde\x91",         2, 0x0000de91, 0x00000002, 0x00002b6f },
	{ "\xde\xfe",         2, 0x0000defe, 0x00000002, 0x00002c7e },
	{ "\xe0\x31",         2, 0x0000e031, 0x00000002, 0x00004a21 },
	{ "\xe0\x7e",         2, 0x0000e07e, 0x00000002, 0x00004a6e },
	{ "\xe0\x91",         2, 0x0000e091, 0x00000002, 0x00004a6f },
	{ "\xe0\xfe",         2, 0x0000e0fe, 0x00000002, 0x00004b7e },
	{ "\xf9\x31",         2, 0x0000f931, 0x00000002, 0x00007c21 },
	{ "\xf9\x7e",         2, 0x0000f97e, 0x00000002, 0x00007c6e },
	{ "\xf9\x91",         2, 0x0000f991, 0x00000002, 0x00007c6f },
	{ "\xf9\xfe",         2, 0x0000f9fe, 0x00000002, 0x00007d7e },
};
struct encoding encodings[] = {
	{
		VARIABLE(""),
		CHARMAP(johab_charmap)
	},
};

#define T_CITRUS_STDENC_MODNAME	"JOHAB"
#include "t_citrus_encmod_template.h"
