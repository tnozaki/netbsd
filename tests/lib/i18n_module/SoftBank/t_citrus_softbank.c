/*-
 * Copyright (c)2016 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2016\
  Takehiko NOZAKI, All rights reserved.");

struct charmap sotbank_charmap[] = {
  { "",         1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",     1, 0x0000007f, 0x00000000, 0x0000007f },
/* JIS X 0201 */
  { "\xa1",     1, 0x000000a1, 0x00000001, 0x00000021 },
  { "\xdf",     1, 0x000000df, 0x00000001, 0x0000005f },
/* JIS X 0208 */
  { "\x81\x40", 2, 0x00008140, 0x00000002, 0x00002121 },
  { "\x81\x7e", 2, 0x0000817e, 0x00000002, 0x0000215f },
  { "\x81\x80", 2, 0x00008180, 0x00000002, 0x00002160 },
  { "\x81\x9e", 2, 0x0000819e, 0x00000002, 0x0000217e },
  { "\x81\x9f", 2, 0x0000819f, 0x00000002, 0x00002221 },
  { "\x81\xfc", 2, 0x000081fc, 0x00000002, 0x0000227e },
  { "\x82\x40", 2, 0x00008240, 0x00000002, 0x00002321 },
  { "\x82\x7e", 2, 0x0000827e, 0x00000002, 0x0000235f },
  { "\x9f\x9f", 2, 0x00009f9f, 0x00000002, 0x00005e21 },
  { "\x9f\xfc", 2, 0x00009ffc, 0x00000002, 0x00005e7e },
  { "\xe0\x40", 2, 0x0000e040, 0x00000002, 0x00005f21 },
  { "\xe0\x7e", 2, 0x0000e07e, 0x00000002, 0x00005f5f },
  { "\xef\x9f", 2, 0x0000ef9f, 0x00000002, 0x00007e21 },
  { "\xef\xfc", 2, 0x0000effc, 0x00000002, 0x00007e7e },
/* CP932SoftBank */
  { "\xf0\x40", 2, 0x0000f040, 0x00000002, 0x00007f21 },
  { "\xf9\xfc", 2, 0x0000f9fc, 0x00000002, 0x0000927e },
/* Emoji */
  { "\x1b$G!",  4, 0x47000021, 0x47000000, 0x00000021 },
  { "\x1b$G~",  4, 0x4700007e, 0x47000000, 0x0000007e },
  { "\x1b$E!",  4, 0x45000021, 0x45000000, 0x00000021 },
  { "\x1b$E~",  4, 0x4500007e, 0x45000000, 0x0000007e },
  { "\x1b$F!",  4, 0x46000021, 0x46000000, 0x00000021 },
  { "\x1b$F~",  4, 0x4600007e, 0x46000000, 0x0000007e },
  { "\x1b$O!",  4, 0x4f000021, 0x4f000000, 0x00000021 },
  { "\x1b$O~",  4, 0x4f00007e, 0x4f000000, 0x0000007e },
  { "\x1b$P!",  4, 0x50000021, 0x50000000, 0x00000021 },
  { "\x1b$P~",  4, 0x5000007e, 0x50000000, 0x0000007e },
  { "\x1b$Q!",  4, 0x51000021, 0x51000000, 0x00000021 },
  { "\x1b$Q~",  4, 0x5100007e, 0x51000000, 0x0000007e },
};
struct encoding encodings[] = {
  {
    VARIABLE("Shift_JIS-SoftBank"),
    CHARMAP(sotbank_charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"SoftBank"
#include "t_citrus_encmod_template.h"
