/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

struct charmap utf8charmap[] = {
  { "",                         1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",                     1, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xc2\x80",                 2, 0x00000080, 0x00000000, 0x00000080 },
  { "\xdf\xbf",                 2, 0x000007ff, 0x00000000, 0x000007ff },
  { "\xe0\xa0\x80",             3, 0x00000800, 0x00000000, 0x00000800 },
  { "\xef\xbf\xbf",             3, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xf0\x90\x80\x80",         4, 0x00010000, 0x00000000, 0x00010000 },
  { "\xf7\xbf\xbf\xbf",         4, 0x001fffff, 0x00000000, 0x001fffff },
  { "\xf8\x88\x80\x80\x80",     5, 0x00200000, 0x00000000, 0x00200000 },
  { "\xfb\xbf\xbf\xbf\xbf",     5, 0x03ffffff, 0x00000000, 0x03ffffff },
  { "\xfc\x84\x80\x80\x80\x80", 6, 0x04000000, 0x00000000, 0x04000000 },
  { "\xfd\xbf\xbf\xbf\xbf\xbf", 6, 0x7fffffff, 0x00000000, 0x7fffffff },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(utf8charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"UTF8"
#include "t_citrus_encmod_template.h"
