/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define MULTIPLE_REPRESENTATION 1
#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

struct charmap utf7charmap[] = {
  { "",        1, 0x00000000, 0x00000000, 0x00000000,
    "",        1                                     },
  { "+AAA",    4, 0x00000000, 0x00000000, 0x00000000,
    "",        1                                     },
  { "+AAE",    4, 0x00000001, 0x00000000, 0x00000001,
    "+AA",     3                                     },
  { "+AAI",    4, 0x00000002, 0x00000000, 0x00000002,
    "+AA",     3                                     },
  { "+AAM",    4, 0x00000003, 0x00000000, 0x00000003,
    "+AA",     3                                     },
  { "+AAQ",    4, 0x00000004, 0x00000000, 0x00000004,
    "+AA",     3                                     },
  { "+AAU",    4, 0x00000005, 0x00000000, 0x00000005,
    "+AA",     3                                     },
  { "+AAY",    4, 0x00000006, 0x00000000, 0x00000006,
    "+AA",     3                                     },
  { "+AAc",    4, 0x00000007, 0x00000000, 0x00000007,
    "+AA",     3                                     },
  { "+AAg",    4, 0x00000008, 0x00000000, 0x00000008,
    "+AA",     3                                     },
  { "\t",      1, 0x00000009, 0x00000000, 0x00000009,
    "\t",      1                                     },
  { "+AAk",    4, 0x00000009, 0x00000000, 0x00000009,
    "\t",      1                                     },
  { "\n",      1, 0x0000000a, 0x00000000, 0x0000000a,
    "\n",      1                                     },
  { "+AAo",    4, 0x0000000a, 0x00000000, 0x0000000a,
    "\n",      1                                     },
  { "+AAs",    4, 0x0000000b, 0x00000000, 0x0000000b,
    "+AA",     3                                     },
  { "+AAw",    4, 0x0000000c, 0x00000000, 0x0000000c,
    "+AA",     3                                     },
  { "\r",      1, 0x0000000d, 0x00000000, 0x0000000d,
    "\r",      1                                     },
  { "+AA0",    4, 0x0000000d, 0x00000000, 0x0000000d,
    "\r",      1                                     },
  { "+AA4",    4, 0x0000000e, 0x00000000, 0x0000000e,
    "+AA",     3                                     },
  { "+AA8",    4, 0x0000000f, 0x00000000, 0x0000000f,
    "+AA",     3                                     },
  { "+ABA",    4, 0x00000010, 0x00000000, 0x00000010,
    "+AB",     3                                     },
  { "+ABE",    4, 0x00000011, 0x00000000, 0x00000011,
    "+AB",     3                                     },
  { "+ABI",    4, 0x00000012, 0x00000000, 0x00000012,
    "+AB",     3                                     },
  { "+ABM",    4, 0x00000013, 0x00000000, 0x00000013,
    "+AB",     3                                     },
  { "+ABQ",    4, 0x00000014, 0x00000000, 0x00000014,
    "+AB",     3                                     },
  { "+ABU",    4, 0x00000015, 0x00000000, 0x00000015,
    "+AB",     3                                     },
  { "+ABY",    4, 0x00000016, 0x00000000, 0x00000016,
    "+AB",     3                                     },
  { "+ABc",    4, 0x00000017, 0x00000000, 0x00000017,
    "+AB",     3                                     },
  { "+ABg",    4, 0x00000018, 0x00000000, 0x00000018,
    "+AB",     3                                     },
  { "+ABk",    4, 0x00000019, 0x00000000, 0x00000019,
    "+AB",     3                                     },
  { "+ABo",    4, 0x0000001a, 0x00000000, 0x0000001a,
    "+AB",     3                                     },
  { "+ABs",    4, 0x0000001b, 0x00000000, 0x0000001b,
    "+AB",     3                                     },
  { "+ABw",    4, 0x0000001c, 0x00000000, 0x0000001c,
    "+AB",     3                                     },
  { "+AB0",    4, 0x0000001d, 0x00000000, 0x0000001d,
    "+AB",     3                                     },
  { "+AB4",    4, 0x0000001e, 0x00000000, 0x0000001e,
    "+AB",     3                                     },
  { "+AB8",    4, 0x0000001f, 0x00000000, 0x0000001f,
    "+AB",     3                                     },
  { " ",       1, 0x00000020, 0x00000000, 0x00000020,
    " ",       1                                     },
  { "+ACA",    4, 0x00000020, 0x00000000, 0x00000020,
    " ",       1                                     },
  { "!",       1, 0x00000021, 0x00000000, 0x00000021,
    "+AC",     3                                     },
  { "+ACE",    4, 0x00000021, 0x00000000, 0x00000021,
    "+AC",     3                                     },
  { "\"",      1, 0x00000022, 0x00000000, 0x00000022,
    "+AC",     3                                     },
  { "+ACI",    4, 0x00000022, 0x00000000, 0x00000022,
    "+AC",     3                                     },
  { "#",       1, 0x00000023, 0x00000000, 0x00000023,
    "+AC",     3                                     },
  { "+ACM",    4, 0x00000023, 0x00000000, 0x00000023,
    "+AC",     3                                     },
  { "$",       1, 0x00000024, 0x00000000, 0x00000024,
    "+AC",     3                                     },
  { "+ACQ",    4, 0x00000024, 0x00000000, 0x00000024,
    "+AC",     3                                     },
  { "%",       1, 0x00000025, 0x00000000, 0x00000025,
    "+AC",     3                                     },
  { "+ACU",    4, 0x00000025, 0x00000000, 0x00000025,
    "+AC",     3                                     },
  { "&",       1, 0x00000026, 0x00000000, 0x00000026,
    "+AC",     3                                     },
  { "+ACY",    4, 0x00000026, 0x00000000, 0x00000026,
    "+AC",     3                                     },
  { "'",       1, 0x00000027, 0x00000000, 0x00000027,
    "'",       1                                     },
  { "+ACc",    4, 0x00000027, 0x00000000, 0x00000027,
    "'",       1                                     },
  { "(",       1, 0x00000028, 0x00000000, 0x00000028,
    "(",       1                                     },
  { "+ACg",    4, 0x00000028, 0x00000000, 0x00000028,
    "(",       1                                     },
  { ")",       1, 0x00000029, 0x00000000, 0x00000029,
    ")",       1                                     },
  { "+ACk",    4, 0x00000029, 0x00000000, 0x00000029,
    ")",       1                                     },
  { "*",       1, 0x0000002a, 0x00000000, 0x0000002a,
    "+AC",     3                                     },
  { "+ACo",    4, 0x0000002a, 0x00000000, 0x0000002a,
    "+AC",     3                                     },
  { "+-",      2, 0x0000002b, 0x00000000, 0x0000002b,
    "+-",      2                                     },
  { "+ACs",    4, 0x0000002b, 0x00000000, 0x0000002b,
    "+-",      2                                     },
  { ",",       1, 0x0000002c, 0x00000000, 0x0000002c,
    ",",       1                                     },
  { "+ACw",    4, 0x0000002c, 0x00000000, 0x0000002c,
    ",",       1                                     },
  { "-",       1, 0x0000002d, 0x00000000, 0x0000002d,
    "-",       1                                     },
  { "+AC0",    4, 0x0000002d, 0x00000000, 0x0000002d,
    "-",       1                                     },
  { ".",       1, 0x0000002e, 0x00000000, 0x0000002e,
    ".",       1                                     },
  { "+AC4",    4, 0x0000002e, 0x00000000, 0x0000002e,
    ".",       1                                     },
  { "/",       1, 0x0000002f, 0x00000000, 0x0000002f,
    "/",       1                                     },
  { "+AC8",    4, 0x0000002f, 0x00000000, 0x0000002f,
    "/",       1                                     },
  { "0",       1, 0x00000030, 0x00000000, 0x00000030,
    "0",       1                                     },
  { "+ADA",    4, 0x00000030, 0x00000000, 0x00000030,
    "0",       1                                     },
  { "1",       1, 0x00000031, 0x00000000, 0x00000031,
    "1",       1                                     },
  { "+ADE",    4, 0x00000031, 0x00000000, 0x00000031,
    "1",       1                                     },
  { "2",       1, 0x00000032, 0x00000000, 0x00000032,
    "2",       1                                     },
  { "+ADI",    4, 0x00000032, 0x00000000, 0x00000032,
    "2",       1                                     },
  { "3",       1, 0x00000033, 0x00000000, 0x00000033,
    "3",       1                                     },
  { "+ADM",    4, 0x00000033, 0x00000000, 0x00000033,
    "3",       1                                     },
  { "4",       1, 0x00000034, 0x00000000, 0x00000034,
    "4",       1                                     },
  { "+ADQ",    4, 0x00000034, 0x00000000, 0x00000034,
    "4",       1                                     },
  { "5",       1, 0x00000035, 0x00000000, 0x00000035,
    "5",       1                                     },
  { "+ADU",    4, 0x00000035, 0x00000000, 0x00000035,
    "5",       1                                     },
  { "6",       1, 0x00000036, 0x00000000, 0x00000036,
    "6",       1                                     },
  { "+ADY",    4, 0x00000036, 0x00000000, 0x00000036,
    "6",       1                                     },
  { "7",       1, 0x00000037, 0x00000000, 0x00000037,
    "7",       1                                     },
  { "+ADc",    4, 0x00000037, 0x00000000, 0x00000037,
    "7",       1                                     },
  { "8",       1, 0x00000038, 0x00000000, 0x00000038,
    "8",       1                                     },
  { "+ADg",    4, 0x00000038, 0x00000000, 0x00000038,
    "8",       1                                     },
  { "9",       1, 0x00000039, 0x00000000, 0x00000039,
    "9",       1                                     },
  { "+ADk",    4, 0x00000039, 0x00000000, 0x00000039,
    "9",       1                                     },
  { ":",       1, 0x0000003a, 0x00000000, 0x0000003a,
    ":",       1                                     },
  { "+ADo",    4, 0x0000003a, 0x00000000, 0x0000003a,
    ":",       1                                     },
  { ";",       1, 0x0000003b, 0x00000000, 0x0000003b,
    "+AD",     3                                     },
  { "+ADs",    4, 0x0000003b, 0x00000000, 0x0000003b,
    "+AD",     3                                     },
  { "<",       1, 0x0000003c, 0x00000000, 0x0000003c,
    "+AD",     3                                     },
  { "+ADw",    4, 0x0000003c, 0x00000000, 0x0000003c,
    "+AD",     3                                     },
  { "=",       1, 0x0000003d, 0x00000000, 0x0000003d,
    "+AD",     3                                     },
  { "+AD0",    4, 0x0000003d, 0x00000000, 0x0000003d,
    "+AD",     3                                     },
  { ">",       1, 0x0000003e, 0x00000000, 0x0000003e,
    "+AD",     3                                     },
  { "+AD4",    4, 0x0000003e, 0x00000000, 0x0000003e,
    "+AD",     3                                     },
  { "?",       1, 0x0000003f, 0x00000000, 0x0000003f,
    "?",       1                                     },
  { "+AD8",    4, 0x0000003f, 0x00000000, 0x0000003f,
    "?",       1                                     },
  { "@",       1, 0x00000040, 0x00000000, 0x00000040,
    "+AE",     3                                     },
  { "+AEA",    4, 0x00000040, 0x00000000, 0x00000040,
    "+AE",     3                                     },
  { "A",       1, 0x00000041, 0x00000000, 0x00000041,
    "A",       1                                     },
  { "+AEE",    4, 0x00000041, 0x00000000, 0x00000041,
    "A",       1                                     },
  { "B",       1, 0x00000042, 0x00000000, 0x00000042,
    "B",       1                                     },
  { "+AEI",    4, 0x00000042, 0x00000000, 0x00000042,
    "B",       1                                     },
  { "C",       1, 0x00000043, 0x00000000, 0x00000043,
    "C",       1                                     },
  { "+AEM",    4, 0x00000043, 0x00000000, 0x00000043,
    "C",       1                                     },
  { "D",       1, 0x00000044, 0x00000000, 0x00000044,
    "D",       1                                     },
  { "+AEQ",    4, 0x00000044, 0x00000000, 0x00000044,
    "D",       1                                     },
  { "E",       1, 0x00000045, 0x00000000, 0x00000045,
    "E",       1                                     },
  { "+AEU",    4, 0x00000045, 0x00000000, 0x00000045,
    "E",       1                                     },
  { "F",       1, 0x00000046, 0x00000000, 0x00000046,
    "F",       1                                     },
  { "+AEY",    4, 0x00000046, 0x00000000, 0x00000046,
    "F",       1                                     },
  { "G",       1, 0x00000047, 0x00000000, 0x00000047,
    "G",       1                                     },
  { "+AEc",    4, 0x00000047, 0x00000000, 0x00000047,
    "G",       1                                     },
  { "H",       1, 0x00000048, 0x00000000, 0x00000048,
    "H",       1                                     },
  { "+AEg",    4, 0x00000048, 0x00000000, 0x00000048,
    "H",       1                                     },
  { "I",       1, 0x00000049, 0x00000000, 0x00000049,
    "I",       1                                     },
  { "+AEk",    4, 0x00000049, 0x00000000, 0x00000049,
    "I",       1                                     },
  { "J",       1, 0x0000004a, 0x00000000, 0x0000004a,
    "J",       1                                     },
  { "+AEo",    4, 0x0000004a, 0x00000000, 0x0000004a,
    "J",       1                                     },
  { "K",       1, 0x0000004b, 0x00000000, 0x0000004b,
    "K",       1                                     },
  { "+AEs",    4, 0x0000004b, 0x00000000, 0x0000004b,
    "K",       1                                     },
  { "L",       1, 0x0000004c, 0x00000000, 0x0000004c,
    "L",       1                                     },
  { "+AEw",    4, 0x0000004c, 0x00000000, 0x0000004c,
    "L",       1                                     },
  { "M",       1, 0x0000004d, 0x00000000, 0x0000004d,
    "M",       1                                     },
  { "+AE0",    4, 0x0000004d, 0x00000000, 0x0000004d,
    "M",       1                                     },
  { "N",       1, 0x0000004e, 0x00000000, 0x0000004e,
    "N",       1                                     },
  { "+AE4",    4, 0x0000004e, 0x00000000, 0x0000004e,
    "N",       1                                     },
  { "O",       1, 0x0000004f, 0x00000000, 0x0000004f,
    "O",       1                                     },
  { "+AE8",    4, 0x0000004f, 0x00000000, 0x0000004f,
    "O",       1                                     },
  { "P",       1, 0x00000050, 0x00000000, 0x00000050,
    "P",       1                                     },
  { "+AFA",    4, 0x00000050, 0x00000000, 0x00000050,
    "P",       1                                     },
  { "Q",       1, 0x00000051, 0x00000000, 0x00000051,
    "Q",       1                                     },
  { "+AFE",    4, 0x00000051, 0x00000000, 0x00000051,
    "Q",       1                                     },
  { "R",       1, 0x00000052, 0x00000000, 0x00000052,
    "R",       1                                     },
  { "+AFI",    4, 0x00000052, 0x00000000, 0x00000052,
    "R",       1                                     },
  { "S",       1, 0x00000053, 0x00000000, 0x00000053,
    "S",       1                                     },
  { "+AFM",    4, 0x00000053, 0x00000000, 0x00000053,
    "S",       1                                     },
  { "T",       1, 0x00000054, 0x00000000, 0x00000054,
    "T",       1                                     },
  { "+AFQ",    4, 0x00000054, 0x00000000, 0x00000054,
    "T",       1                                     },
  { "U",       1, 0x00000055, 0x00000000, 0x00000055,
    "U",       1                                     },
  { "+AFU",    4, 0x00000055, 0x00000000, 0x00000055,
    "U",       1                                     },
  { "V",       1, 0x00000056, 0x00000000, 0x00000056,
    "V",       1                                     },
  { "+AFY",    4, 0x00000056, 0x00000000, 0x00000056,
    "V",       1                                     },
  { "W",       1, 0x00000057, 0x00000000, 0x00000057,
    "W",       1                                     },
  { "+AFc",    4, 0x00000057, 0x00000000, 0x00000057,
    "W",       1                                     },
  { "X",       1, 0x00000058, 0x00000000, 0x00000058,
    "X",       1                                     },
  { "+AFg",    4, 0x00000058, 0x00000000, 0x00000058,
    "X",       1                                     },
  { "Y",       1, 0x00000059, 0x00000000, 0x00000059,
    "Y",       1                                     },
  { "+AFk",    4, 0x00000059, 0x00000000, 0x00000059,
    "Y",       1                                     },
  { "Z",       1, 0x0000005a, 0x00000000, 0x0000005a,
    "Z",       1                                     },
  { "+AFo",    4, 0x0000005a, 0x00000000, 0x0000005a,
    "Z",       1                                     },
  { "[",       1, 0x0000005b, 0x00000000, 0x0000005b,
    "+AF",     3                                     },
  { "+AFs",    4, 0x0000005b, 0x00000000, 0x0000005b,
    "+AF",     3                                     },
  { "+AFw",    4, 0x0000005c, 0x00000000, 0x0000005c,
    "+AF",     3                                     },
  { "]",       1, 0x0000005d, 0x00000000, 0x0000005d,
    "+AF",     3                                     },
  { "+AF0",    4, 0x0000005d, 0x00000000, 0x0000005d,
    "+AF",     3                                     },
  { "^",       1, 0x0000005e, 0x00000000, 0x0000005e,
    "+AF",     3                                     },
  { "+AF4",    4, 0x0000005e, 0x00000000, 0x0000005e,
    "+AF",     3                                     },
  { "_",       1, 0x0000005f, 0x00000000, 0x0000005f,
    "+AF",     3                                     },
  { "+AF8",    4, 0x0000005f, 0x00000000, 0x0000005f,
    "+AF",     3                                     },
  { "`",       1, 0x00000060, 0x00000000, 0x00000060,
    "+AG",     3                                     },
  { "+AGA",    4, 0x00000060, 0x00000000, 0x00000060,
    "+AG",     3                                     },
  { "a",       1, 0x00000061, 0x00000000, 0x00000061,
    "a",       1                                     },
  { "+AGE",    4, 0x00000061, 0x00000000, 0x00000061,
    "a",       1                                     },
  { "b",       1, 0x00000062, 0x00000000, 0x00000062,
    "b",       1                                     },
  { "+AGI",    4, 0x00000062, 0x00000000, 0x00000062,
    "b",       1                                     },
  { "c",       1, 0x00000063, 0x00000000, 0x00000063,
    "c",       1                                     },
  { "+AGM",    4, 0x00000063, 0x00000000, 0x00000063,
    "c",       1                                     },
  { "d",       1, 0x00000064, 0x00000000, 0x00000064,
    "d",       1                                     },
  { "+AGQ",    4, 0x00000064, 0x00000000, 0x00000064,
    "d",       1                                     },
  { "e",       1, 0x00000065, 0x00000000, 0x00000065,
    "e",       1                                     },
  { "+AGU",    4, 0x00000065, 0x00000000, 0x00000065,
    "e",       1                                     },
  { "f",       1, 0x00000066, 0x00000000, 0x00000066,
    "f",       1                                     },
  { "+AGY",    4, 0x00000066, 0x00000000, 0x00000066,
    "f",       1                                     },
  { "g",       1, 0x00000067, 0x00000000, 0x00000067,
    "g",       1                                     },
  { "+AGc",    4, 0x00000067, 0x00000000, 0x00000067,
    "g",       1                                     },
  { "h",       1, 0x00000068, 0x00000000, 0x00000068,
    "h",       1                                     },
  { "+AGg",    4, 0x00000068, 0x00000000, 0x00000068,
    "h",       1                                     },
  { "i",       1, 0x00000069, 0x00000000, 0x00000069,
    "i",       1                                     },
  { "+AGk",    4, 0x00000069, 0x00000000, 0x00000069,
    "i",       1                                     },
  { "j",       1, 0x0000006a, 0x00000000, 0x0000006a,
    "j",       1                                     },
  { "+AGo",    4, 0x0000006a, 0x00000000, 0x0000006a,
    "j",       1                                     },
  { "k",       1, 0x0000006b, 0x00000000, 0x0000006b,
    "k",       1                                     },
  { "+AGs",    4, 0x0000006b, 0x00000000, 0x0000006b,
    "k",       1                                     },
  { "l",       1, 0x0000006c, 0x00000000, 0x0000006c,
    "l",       1                                     },
  { "+AGw",    4, 0x0000006c, 0x00000000, 0x0000006c,
    "l",       1                                     },
  { "m",       1, 0x0000006d, 0x00000000, 0x0000006d,
    "m",       1                                     },
  { "+AG0",    4, 0x0000006d, 0x00000000, 0x0000006d,
    "m",       1                                     },
  { "n",       1, 0x0000006e, 0x00000000, 0x0000006e,
    "n",       1                                     },
  { "+AG4",    4, 0x0000006e, 0x00000000, 0x0000006e,
    "n",       1                                     },
  { "o",       1, 0x0000006f, 0x00000000, 0x0000006f,
    "o",       1                                     },
  { "+AG8",    4, 0x0000006f, 0x00000000, 0x0000006f,
    "o",       1                                     },
  { "p",       1, 0x00000070, 0x00000000, 0x00000070,
    "p",       1                                     },
  { "+AHA",    4, 0x00000070, 0x00000000, 0x00000070,
    "p",       1                                     },
  { "q",       1, 0x00000071, 0x00000000, 0x00000071,
    "q",       1                                     },
  { "+AHE",    4, 0x00000071, 0x00000000, 0x00000071,
    "q",       1                                     },
  { "r",       1, 0x00000072, 0x00000000, 0x00000072,
    "r",       1                                     },
  { "+AHI",    4, 0x00000072, 0x00000000, 0x00000072,
    "r",       1                                     },
  { "s",       1, 0x00000073, 0x00000000, 0x00000073,
    "s",       1                                     },
  { "+AHM",    4, 0x00000073, 0x00000000, 0x00000073,
    "s",       1                                     },
  { "t",       1, 0x00000074, 0x00000000, 0x00000074,
    "t",       1                                     },
  { "+AHQ",    4, 0x00000074, 0x00000000, 0x00000074,
    "t",       1                                     },
  { "u",       1, 0x00000075, 0x00000000, 0x00000075,
    "u",       1                                     },
  { "+AHU",    4, 0x00000075, 0x00000000, 0x00000075,
    "u",       1                                     },
  { "v",       1, 0x00000076, 0x00000000, 0x00000076,
    "v",       1                                     },
  { "+AHY",    4, 0x00000076, 0x00000000, 0x00000076,
    "v",       1                                     },
  { "w",       1, 0x00000077, 0x00000000, 0x00000077,
    "w",       1                                     },
  { "+AHc",    4, 0x00000077, 0x00000000, 0x00000077,
    "w",       1                                     },
  { "x",       1, 0x00000078, 0x00000000, 0x00000078,
    "x",       1                                     },
  { "+AHg",    4, 0x00000078, 0x00000000, 0x00000078,
    "x",       1                                     },
  { "y",       1, 0x00000079, 0x00000000, 0x00000079,
    "y",       1                                     },
  { "+AHk",    4, 0x00000079, 0x00000000, 0x00000079,
    "y",       1                                     },
  { "z",       1, 0x0000007a, 0x00000000, 0x0000007a,
    "z",       1                                     },
  { "+AHo",    4, 0x0000007a, 0x00000000, 0x0000007a,
    "z",       1                                     },
  { "{",       1, 0x0000007b, 0x00000000, 0x0000007b,
    "+AH",     3                                     },
  { "+AHs",    4, 0x0000007b, 0x00000000, 0x0000007b,
    "+AH",     3                                     },
  { "|",       1, 0x0000007c, 0x00000000, 0x0000007c,
    "+AH",     3                                     },
  { "+AHw",    4, 0x0000007c, 0x00000000, 0x0000007c,
    "+AH",     3                                     },
  { "}",       1, 0x0000007d, 0x00000000, 0x0000007d,
    "+AH",     3                                     },
  { "+AH0",    4, 0x0000007d, 0x00000000, 0x0000007d,
    "+AH",     3                                     },
  { "+AH4",    4, 0x0000007e, 0x00000000, 0x0000007e,
    "+AH",     3                                     },
  { "+AH8",    4, 0x0000007f, 0x00000000, 0x0000007f,
    "+AH",     3                                     },
  { "+AIA",    4, 0x00000080, 0x00000000, 0x00000080,
    "+AI",     3                                     },
  { "+AJ8",    4, 0x0000009f, 0x00000000, 0x0000009f,
    "+AJ",     3                                     },
  { "+AKA",    4, 0x000000a0, 0x00000000, 0x000000a0,
    "+AK",     3                                     },
  { "+AP8",    4, 0x000000ff, 0x00000000, 0x000000ff,
    "+AP",     3                                     },
  { "+AQA",    4, 0x00000100, 0x00000000, 0x00000100,
    "+AQ",     3                                     },
  { "+Af8",    4, 0x000001ff, 0x00000000, 0x000001ff,
    "+Af",     3                                     },
  { "+/wA",    4, 0x0000ff00, 0x00000000, 0x0000ff00,
    "+/w",     3                                     },
  { "+//8",    4, 0x0000ffff, 0x00000000, 0x0000ffff,
    "+//",     3                                     },
  { "+2ADcAA", 7, 0x00010000, 0x00000000, 0x00010000,
    "+2ADcA",  6                                     },
  { "+2D/f/w", 7, 0x0001ffff, 0x00000000, 0x0001ffff,
    "+2D/f/",  6                                     },
  { "+28DcAA", 7, 0x00100000, 0x00000000, 0x00100000,
    "+28DcA",  6                                     },
  { "+2//f/w", 7, 0x0010ffff, 0x00000000, 0x0010ffff,
    "+2//f/",  6                                     },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(utf7charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"UTF7"
#include "t_citrus_encmod_template.h"
