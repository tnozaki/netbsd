/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

/*
 * http://tools.ietf.org/html/rfc1922
 */
struct charmap iso2022cn_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* GB2312-1980 */
  { "\x1b$)A\x0e!!",   7, 0x41002121, 0x41007f00, 0x00002121 },
  { "\x1b$)A\x0e~~",   7, 0x41007e7e, 0x41007f00, 0x00007e7e },
  /* CNS11643-1 */
  { "\x1b$)G\x0e!!",   7, 0x47002121, 0x47007f00, 0x00002121 },
  { "\x1b$)G\x0e~~",   7, 0x47007e7e, 0x47007f00, 0x00007e7e },
  /* CNS11643-2 */
  { "\x1b$*H\x1bN!!",  8, 0x48002121, 0x48007f00, 0x00002121 },
  { "\x1b$*H\x1bN~~",  8, 0x48007e7e, 0x48007f00, 0x00007e7e },
};
/*
 * http://tools.ietf.org/html/rfc1922
 */
struct charmap iso2022cnext_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* GB2312-1980 */
  { "\x1b$)A\x0e!!",   7, 0x41002121, 0x41007f00, 0x00002121 },
  { "\x1b$)A\x0e~~",   7, 0x41007e7e, 0x41007f00, 0x00007e7e },
  /* CNS11643-1 */
  { "\x1b$)G\x0e!!",   7, 0x47002121, 0x47007f00, 0x00002121 },
  { "\x1b$)G\x0e~~",   7, 0x47007e7e, 0x47007f00, 0x00007e7e },
  /* CNS11643-2 */
  { "\x1b$*H\x1bN!!",  8, 0x48002121, 0x48007f00, 0x00002121 },
  { "\x1b$*H\x1bN~~",  8, 0x48007e7e, 0x48007f00, 0x00007e7e },
  /* CNS11643-3 */
  { "\x1b$+I\x1bO!!",  8, 0x49002121, 0x49007f00, 0x00002121 },
  { "\x1b$+I\x1bO~~",  8, 0x49007e7e, 0x49007f00, 0x00007e7e },
  /* CNS11643-4 */
  { "\x1b$+J\x1bO!!",  8, 0x4a002121, 0x4a007f00, 0x00002121 },
  { "\x1b$+J\x1bO~~",  8, 0x4a007e7e, 0x4a007f00, 0x00007e7e },
  /* CNS11643-5 */
  { "\x1b$+K\x1bO!!",  8, 0x4b002121, 0x4b007f00, 0x00002121 },
  { "\x1b$+K\x1bO~~",  8, 0x4b007e7e, 0x4b007f00, 0x00007e7e },
  /* CNS11643-6 */
  { "\x1b$+L\x1bO!!",  8, 0x4c002121, 0x4c007f00, 0x00002121 },
  { "\x1b$+L\x1bO~~",  8, 0x4c007e7e, 0x4c007f00, 0x00007e7e },
  /* CNS11643-7 */
  { "\x1b$+M\x1bO!!",  8, 0x4d002121, 0x4d007f00, 0x00002121 },
  { "\x1b$+M\x1bO~~",  8, 0x4d007e7e, 0x4d007f00, 0x00007e7e },
};
/*
 * http://tools.ietf.org/html/rfc1468
 */
struct charmap iso2022jp_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* JIS X 0201-1976 ("Roman" set) */
  { "\x1b(J!",         4, 0x4A000021, 0x4A000000, 0x00000021 },
  { "\x1b(J~",         4, 0x4A00007e, 0x4A000000, 0x0000007e },
  /* JIS X 0208-1978 */
  { "\x1b$@!!",        5, 0x40002121, 0x40007f00, 0x00002121 },
  { "\x1b$@~~",        5, 0x40007e7e, 0x40007f00, 0x00007e7e },
  /* JIS X 0208-1983 */
  { "\x1b$B!!",        5, 0x42002121, 0x42007f00, 0x00002121 },
  { "\x1b$B~~",        5, 0x42007e7e, 0x42007f00, 0x00007e7e },
};
/*
 * http://tools.ietf.org/html/rfc2237
 */
struct charmap iso2022jp1_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* JIS X 0208-1978 */
  { "\x1b$@!!",        5, 0x40002121, 0x40007f00, 0x00002121 },
  { "\x1b$@~~",        5, 0x40007e7e, 0x40007f00, 0x00007e7e },
  /* JIS X 0208-1983 */
  { "\x1b$B!!",        5, 0x42002121, 0x42007f00, 0x00002121 },
  { "\x1b$B~~",        5, 0x42007e7e, 0x42007f00, 0x00007e7e },
  /* JIS X 0201-Roman */
  { "\x1b(J!",         4, 0x4A000021, 0x4A000000, 0x00000021 },
  { "\x1b(J~",         4, 0x4A00007e, 0x4A000000, 0x0000007e },
  /* JIS X 0212-1990 */
  { "\x1b$(D!!",       6, 0x44002121, 0x44007f00, 0x00002121 },
  { "\x1b$(D~~",       6, 0x44007e7e, 0x44007f00, 0x00007e7e },
};
/*
 * http://tools.ietf.org/html/rfc1554
 */
struct charmap iso2022jp2_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* JIS X 0208-1978 */
  { "\x1b$@!!",        5, 0x40002121, 0x40007f00, 0x00002121 },
  { "\x1b$@~~",        5, 0x40007e7e, 0x40007f00, 0x00007e7e },
  /* JIS X 0208-1983 */
  { "\x1b$B!!",        5, 0x42002121, 0x42007f00, 0x00002121 },
  { "\x1b$B~~",        5, 0x42007e7e, 0x42007f00, 0x00007e7e },
  /* JIS X 0201-Roman */
  { "\x1b(J!",         4, 0x4A000021, 0x4A000000, 0x00000021 },
  { "\x1b(J~",         4, 0x4A00007e, 0x4A000000, 0x0000007e },
  /* GB2312-1980 */
  { "\x1b$A!!",        5, 0x41002121, 0x41007f00, 0x00002121 },
  { "\x1b$A~~",        5, 0x41007e7e, 0x41007f00, 0x00007e7e },
  /* KSC5601-1987 */
  { "\x1b$(C!!",       6, 0x43002121, 0x43007f00, 0x00002121 },
  { "\x1b$(C~~",       6, 0x43007e7e, 0x43007f00, 0x00007e7e },
  /* JIS X 0212-1990 */
  { "\x1b$(D!!",       6, 0x44002121, 0x44007f00, 0x00002121 },
  { "\x1b$(D~~",       6, 0x44007e7e, 0x44007f00, 0x00007e7e },
  /* ISO8859-1 */
  { "\x1b.A\x1bN\x20", 6, 0x000000a0, 0x00000080, 0x00000020 },
  { "\x1b.A\x1bN\x21", 6, 0x000000a1, 0x00000080, 0x00000021 },
  { "\x1b.A\x1bN\x7f", 6, 0x000000ff, 0x00000080, 0x0000007f },
  { "\x1b.A\x1bN\x7e", 6, 0x000000fe, 0x00000080, 0x0000007e },
  /* ISO8859-7(Greek) */
  { "\x1b.F\x1bN\x20", 6, 0x460000a0, 0x46000080, 0x00000020 },
  { "\x1b.F\x1bN\x21", 6, 0x460000a1, 0x46000080, 0x00000021 },
  { "\x1b.F\x1bN\x7e", 6, 0x460000fe, 0x46000080, 0x0000007e },
  { "\x1b.F\x1bN\x7f", 6, 0x460000ff, 0x46000080, 0x0000007f },
};
/*
 * http://www.jisc.go.jp/app/pager?%23jps.JPSH0090D:JPSO0020:/JPS/JPSO0090.jsp=&RKKNP_vJISJISNO=X0213
 */
struct charmap iso2022jp2004_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* JIS X 0201-Roman */
  { "\x1b(J!",         4, 0x4A000021, 0x4A000000, 0x00000021 },
  { "\x1b(J~",         4, 0x4A00007e, 0x4A000000, 0x0000007e },
  /* JIS X 0208-1990 */
  { "\x1b$B!!",        5, 0x42002121, 0x42007f00, 0x00002121 },
  { "\x1b$B~~",        5, 0x42007e7e, 0x42007f00, 0x00007e7e },
  /* JIS X 0208-1990 */
  { "\x1b$B!!",        5, 0x42002121, 0x42007f00, 0x00002121 },
  { "\x1b$B~~",        5, 0x42007e7e, 0x42007f00, 0x00007e7e },
  /* JISX0213-1 */
  { "\x1b$(O!!",       6, 0x4f002121, 0x4f007f00, 0x00002121 },
  { "\x1b$(O~~",       6, 0x4f007e7e, 0x4f007f00, 0x00007e7e },
  /* JISX0213-2 */
  { "\x1b$(P!!",       6, 0x50002121, 0x50007f00, 0x00002121 },
  { "\x1b$(P~~",       6, 0x50007e7e, 0x50007f00, 0x00007e7e },
};
/*
 * https://tools.ietf.org/rfc/rfc1557.txt
 */
struct charmap iso2022kr_charmap[] = {
  /* ASCII */
  { "",                1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f",            1, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x21",            1, 0x00000021, 0x00000000, 0x00000021 },
  { "\x7e",            1, 0x0000007e, 0x00000000, 0x0000007e },
  /* KSC5601 */
  { "\x1b$)C\x0e!!",   7, 0x43002121, 0x43007f00, 0x00002121 },
  { "\x1b$)C\x0e~~",   7, 0x43007e7e, 0x43007f00, 0x00007e7e },
};
struct encoding encodings[] = {
  {
    VARIABLE("INIT0=94B 1=94$A 1=94$G 2=94$H SI SO SS2"),
    CHARMAP(iso2022cn_charmap),
  },
  {
    VARIABLE("INIT0=94B 1=94$A 1=94$E 1=94$G 2=94$H 3=94$I 3=94$J 3=94$K 3=94$L 3=94$M SI SO SS2 SS3"),
    CHARMAP(iso2022cnext_charmap),
  },
  {
    VARIABLE("INIT0=94B 0=94B 0=94J 0=94$@ 0=94$B"),
    CHARMAP(iso2022jp_charmap),
  },
  {
    VARIABLE("INIT0=94B 0=94B 0=94$@ 0=94$B 0=94J 0=94$D"),
    CHARMAP(iso2022jp1_charmap),
  },
  {
    VARIABLE("MAX2 INIT0=94B 0=94 0=94$ 2=96 2=96$ SS2"),
    CHARMAP(iso2022jp2_charmap),
  },
  {
    VARIABLE("INIT0=94B 0=94 0=94$"),
    CHARMAP(iso2022jp2004_charmap),
  },
  {
    VARIABLE("INIT0=94B 1=94$C SI SO"),
    CHARMAP(iso2022kr_charmap),
  },
};

#define T_CITRUS_STDENC_MODNAME	"ISO2022"
#include "t_citrus_encmod_template.h"
