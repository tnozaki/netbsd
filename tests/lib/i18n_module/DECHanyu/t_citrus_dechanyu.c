/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2015\
  Takehiko NOZAKI, All rights reserved.");

/*
 * http://h50146.www5.hp.com/products/software/oe/tru64unix/manual/v51a_ref/HTML/MAN/MAN5/0384____.HTM
 */
struct charmap dechanyu_charmap[] = {
	/* ISO646 */
	{ "",                 1, 0x00000000, 0x00000000, 0x00000000 },
	{ "\x7f",             1, 0x0000007f, 0x00000000, 0x0000007f },
	/* CNS11643-1 */
	{ "\xa1\xa1",         2, 0x0000a1a1, 0x00008080, 0x00002121 },
	{ "\xfe\xfe",         2, 0x0000fefe, 0x00008080, 0x00007e7e },
	/* CNS11643-2 */
	{ "\xa1\x21",         2, 0x0000a121, 0x00008000, 0x00002121 },
	{ "\xfe\x7e",         2, 0x0000fe7e, 0x00008000, 0x00007e7e },
	/* CNS11643-3 */
	{ "\xc2\xcb\xa1\xa1", 4, 0xc2cba1a1, 0xc2cb8080, 0x00002121 },
	{ "\xc2\xcb\xfe\xfe", 4, 0xc2cbfefe, 0xc2cb8080, 0x00007e7e },
	/* DECUDA */
	{ "\xc2\xcb\xa1\x21", 4, 0xc2cba121, 0xc2cb8000, 0x00002121 },
	{ "\xc2\xcb\xfe\x7e", 4, 0xc2cbfe7e, 0xc2cb8000, 0x00007e7e },
};
struct encoding encodings[] = {
	{
		VARIABLE(""),
		CHARMAP(dechanyu_charmap)
	},
};

#define T_CITRUS_STDENC_MODNAME	"DECHanyu"
#include "t_citrus_encmod_template.h"
