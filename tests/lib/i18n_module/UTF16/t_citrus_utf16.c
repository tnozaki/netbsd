/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/endian.h>
#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2015\
  Takehiko NOZAKI, All rights reserved.");

struct charmap utf16charmap[] = {
#if _BYTE_ORDER == _BIG_ENDIAN
  { "\xfe\xff\0\0",             4, 0x00000000, 0x00000000, 0x00000000 },
  { "\xfe\xff\0\0",             4, 0x00000000, 0x00000000, 0x00000000 },
  { "\xfe\xff\0\x1f",           4, 0x0000001f, 0x00000000, 0x0000001f },
  { "\xfe\xff\0\x20",           4, 0x00000020, 0x00000000, 0x00000020 },
  { "\xfe\xff\0\x7f",           4, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xfe\xff\0\x80",           4, 0x00000080, 0x00000000, 0x00000080 },
  { "\xfe\xff\0\x9f",           4, 0x0000009f, 0x00000000, 0x0000009f },
  { "\xfe\xff\0\xa0",           4, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\xfe\xff\0\xff",           4, 0x000000ff, 0x00000000, 0x000000ff },
  { "\xfe\xff\x1\0",            4, 0x00000100, 0x00000000, 0x00000100 },
  { "\xfe\xff\x1\xff",          4, 0x000001ff, 0x00000000, 0x000001ff },
  { "\xfe\xff\xff\0",           4, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xfe\xff\xff\xff",         4, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xfe\xff\xd8\0\xdc\0",     6, 0x00010000, 0x00000000, 0x00010000 },
  { "\xfe\xff\xd8\x3f\xdf\xff", 6, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\xfe\xff\xdb\xc0\xdc\0",   6, 0x00100000, 0x00000000, 0x00100000 },
  { "\xfe\xff\xdb\xff\xdf\xff", 6, 0x0010ffff, 0x00000000, 0x0010ffff },
#elif _BYTE_ORDER == _LITTLE_ENDIAN
  { "\xff\xfe\0\0",             4, 0x00000000, 0x00000000, 0x00000000 },
  { "\xff\xfe\0\0",             4, 0x00000000, 0x00000000, 0x00000000 },
  { "\xff\xfe\x1f\0",           4, 0x0000001f, 0x00000000, 0x0000001f },
  { "\xff\xfe\x20\0",           4, 0x00000020, 0x00000000, 0x00000020 },
  { "\xff\xfe\x7f\0",           4, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xff\xfe\x80\0",           4, 0x00000080, 0x00000000, 0x00000080 },
  { "\xff\xfe\x9f\0",           4, 0x0000009f, 0x00000000, 0x0000009f },
  { "\xff\xfe\xa0\0",           4, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\xff\xfe\xff\0",           4, 0x000000ff, 0x00000000, 0x000000ff },
  { "\xff\xfe\0\x1",            4, 0x00000100, 0x00000000, 0x00000100 },
  { "\xff\xfe\xff\x1",          4, 0x000001ff, 0x00000000, 0x000001ff },
  { "\xff\xfe\0\xff",           4, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xff\xfe\xff\xff",         4, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xff\xfe\0\xd8\0\xdc",     6, 0x00010000, 0x00000000, 0x00010000 },
  { "\xff\xfe\x3f\xd8\xff\xdf", 6, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\xff\xfe\xc0\xdb\0\xdc",   6, 0x00100000, 0x00000000, 0x00100000 },
  { "\xff\xfe\xff\xdb\xff\xdf", 6, 0x0010ffff, 0x00000000, 0x0010ffff },
#else
#error
#endif
};
struct charmap utf16becharmap[] = {
  { "\0\0",             2, 0x00000000, 0x00000000, 0x00000000 },
  { "\0\x1f",           2, 0x0000001f, 0x00000000, 0x0000001f },
  { "\0\x20",           2, 0x00000020, 0x00000000, 0x00000020 },
  { "\0\x7f",           2, 0x0000007f, 0x00000000, 0x0000007f },
  { "\0\x80",           2, 0x00000080, 0x00000000, 0x00000080 },
  { "\0\x9f",           2, 0x0000009f, 0x00000000, 0x0000009f },
  { "\0\xa0",           2, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\0\xff",           2, 0x000000ff, 0x00000000, 0x000000ff },
  { "\x1\0",            2, 0x00000100, 0x00000000, 0x00000100 },
  { "\x1\xff",          2, 0x000001ff, 0x00000000, 0x000001ff },
  { "\xff\0",           2, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xff\xff",         2, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xd8\0\xdc\0",     4, 0x00010000, 0x00000000, 0x00010000 },
  { "\xd8\x3f\xdf\xff", 4, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\xdb\xc0\xdc\0",   4, 0x00100000, 0x00000000, 0x00100000 },
  { "\xdb\xff\xdf\xff", 4, 0x0010ffff, 0x00000000, 0x0010ffff },
};
struct charmap utf16lecharmap[] = {
  { "\0\0",             2, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f\0",           2, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x20\0",           2, 0x00000020, 0x00000000, 0x00000020 },
  { "\x7f\0",           2, 0x0000007f, 0x00000000, 0x0000007f },
  { "\x80\0",           2, 0x00000080, 0x00000000, 0x00000080 },
  { "\x9f\0",           2, 0x0000009f, 0x00000000, 0x0000009f },
  { "\xa0\0",           2, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\xff\0",           2, 0x000000ff, 0x00000000, 0x000000ff },
  { "\0\x1",            2, 0x00000100, 0x00000000, 0x00000100 },
  { "\xff\x1",          2, 0x000001ff, 0x00000000, 0x000001ff },
  { "\0\xff",           2, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xff\xff",         2, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\0\xd8\0\xdc",     4, 0x00010000, 0x00000000, 0x00010000 },
  { "\x3f\xd8\xff\xdf", 4, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\xc0\xdb\0\xdc",   4, 0x00100000, 0x00000000, 0x00100000 },
  { "\xff\xdb\xff\xdf", 4, 0x0010ffff, 0x00000000, 0x0010ffff },
};
struct encoding encodings[] = {
  {
    VARIABLE("UTF16"),
    CHARMAP(utf16charmap),
  },
  {
    VARIABLE("UTF16BE"),
    CHARMAP(utf16becharmap),
  },
  {
    VARIABLE("UTF16LE"),
    CHARMAP(utf16lecharmap),
  },
};

#define T_CITRUS_STDENC_MODNAME	"UTF16"
#include "t_citrus_encmod_template.h"
