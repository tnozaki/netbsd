/*-
 * Copyright (c)2020 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define MULTIPLE_REPRESENTATION 1
#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2020\
  Takehiko NOZAKI, All rights reserved.");

struct charmap hz_charmap[] = {
  /* ASCII */
  { "",                   1, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "\x1f",               1, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "\x20",               1, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "\x21",               1, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~~",                 2, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "\x7f",               1, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  /* GB2312-1980 */
  { "~{\x21\x21",         4, 0x00002121, 0x00000001, 0x00002121,
    "~{\x21\x21",         4                                     },
  { "~{\x7d\x7e",         4, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\x7d\x7e",         4                                     },
  /* tilde + space */
  { "~\n",                3, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~\n\x1f",            3, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~\n\x20",            3, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~\n\x21",            3, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~\n~~",              4, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~\n\x7f",            3, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~\n~{\x21\x21",      6, 0x00002121, 0x00000001, 0x00002121,
    "~{\x21\x21",         4                                     },
  { "~\n~{\x7d\x7e",      6, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\x7d\x7e",         4                                     },
  /* redundant escape sequence */
  { "~{~}",               5, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~{~}\x1f",           5, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~{~}\x20",           5, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~{~}\x21",           5, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~{~}~~",             6, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~{~}\x7f",           5, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~{~}~{\x21\x21",     8, 0x00002121, 0x00000001, 0x00002121,
    "~{\x21\x21",         4                                     },
  { "~{~}~{\x7d\x7e",     8, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\x7d\x7e",         4                                     },
  { "~\n~{~}",            7, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~\n~{~}\x1f",        7, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~\n~{~}\x20",        7, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~\n~{~}\x21",        7, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~\n~{~}~~",          8, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~\n~{~}\x7f",        7, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~\n~{~}~{\x21\x21", 10, 0x00002121, 0x00000001, 0x00002121,
    "~{\x21\x21",         4                                     },
  { "~\n~{~}~{\x7d\x7e", 10, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\x7d\x7e",         4                                     },
};
struct charmap hz8_charmap[] = {
  /* ASCII */
  { "",                   1, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "\x1f",               1, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "\x20",               1, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "\x21",               1, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~~",                 2, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "\x7f",               1, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  /* GB2312-1980 */
  { "~{\xa1\xa1",         4, 0x00002121, 0x00000001, 0x00002121,
    "~{\xa1\xa1",         4                                     },
  { "~{\xfd\xfe",         4, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\xfd\xfe",         4                                     },
  /* tilde + space */
  { "~\n",                3, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~\n\x1f",            3, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~\n\x20",            3, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~\n\x21",            3, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~\n~~",              4, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~\n\x7f",            3, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~\n~{\xa1\xa1",      6, 0x00002121, 0x00000001, 0x00002121,
    "~{\xa1\xa1",         4                                     },
  { "~\n~{\xfd\xfe",      6, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\xfd\xfe",         4                                     },
  /* redundant escape sequence */
  { "~{~}",               5, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~{~}\x1f",           5, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~{~}\x20",           5, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~{~}\x21",           5, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~{~}~~",             6, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~{~}\x7f",           5, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~{~}~{\xa1\xa1",     8, 0x00002121, 0x00000001, 0x00002121,
    "~{\xa1\xa1",         4                                     },
  { "~{~}~{\xfd\xfe",     8, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\xfd\xfe",         4                                     },
  { "~\n~{~}",            7, 0x00000000, 0x00000000, 0x00000000,
    "",                   1                                     },
  { "~\n~{~}\x1f",        7, 0x0000001f, 0x00000000, 0x0000001f,
    "\x1f",               1                                     },
  { "~\n~{~}\x20",        7, 0x00000020, 0x00000000, 0x00000020,
    "\x20",               1                                     },
  { "~\n~{~}\x21",        7, 0x00000021, 0x00000000, 0x00000021,
    "\x21",               1                                     },
  { "~\n~{~}~~",          8, 0x0000007e, 0x00000000, 0x0000007e,
    "~~",                 2                                     },
  { "~\n~{~}\x7f",        7, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",               1                                     },
  { "~\n~{~}~{\xa1\xa1", 10, 0x00002121, 0x00000001, 0x00002121,
    "~{\xa1\xa1",         4                                     },
  { "~\n~{~}~{\xfd\xfe", 10, 0x00007d7e, 0x00000001, 0x00007d7e,
    "~{\xfd\xfe",         4                                     },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(hz_charmap),
  },
  {
    VARIABLE("8BIT"),
    CHARMAP(hz8_charmap),
  },
};

#define T_CITRUS_STDENC_MODNAME	"HZ"
#include "t_citrus_encmod_template.h"
