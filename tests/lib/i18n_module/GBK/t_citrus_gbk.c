/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2015\
  Takehiko NOZAKI, All rights reserved.");

struct charmap gbk_charmap[] = {
  /* ISO646 */
  { "",                 1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",             1, 0x0000007f, 0x00000000, 0x0000007f },
  /* GBK euro sign */
  { "\x80",             1, 0x00000080, 0x00000000, 0x00000080 },
  /* GB2312 compatible */
  { "\xa1\xa1",         2, 0x0000a1a1, 0x00000001, 0x00002121 },
  { "\xa1\xfe",         2, 0x0000a1fe, 0x00000001, 0x0000217e },
  { "\xfe\xa1",         2, 0x0000fea1, 0x00000001, 0x00007e21 },
  { "\xfe\xfe",         2, 0x0000fefe, 0x00000001, 0x00007e7e },
  /* GBK 2byte */
  { "\x81\x40",         2, 0x00008140, 0x00000002, 0x00008140 },
  { "\x81\x7e",         2, 0x0000817e, 0x00000002, 0x0000817e },
  { "\xa0\x40",         2, 0x0000a040, 0x00000002, 0x0000a040 },
  { "\xa0\x7e",         2, 0x0000a07e, 0x00000002, 0x0000a07e },
  { "\xa1\x40",         2, 0x0000a140, 0x00000002, 0x0000a140 },
  { "\xa1\x7e",         2, 0x0000a17e, 0x00000002, 0x0000a17e },
  { "\xa1\x80",         2, 0x0000a180, 0x00000002, 0x0000a180 },
  { "\xa1\xa0",         2, 0x0000a1a0, 0x00000002, 0x0000a1a0 },
  { "\xfe\x40",         2, 0x0000fe40, 0x00000002, 0x0000fe40 },
  { "\xfe\x7e",         2, 0x0000fe7e, 0x00000002, 0x0000fe7e },
  { "\xfe\x80",         2, 0x0000fe80, 0x00000002, 0x0000fe80 },
  { "\xfe\xa0",         2, 0x0000fea0, 0x00000002, 0x0000fea0 },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(gbk_charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"GBK"
#include "t_citrus_encmod_template.h"
