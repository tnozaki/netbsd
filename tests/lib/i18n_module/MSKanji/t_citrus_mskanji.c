/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2014\
  Takehiko NOZAKI, All rights reserved.");

struct charmap sjis_charmap[] = {
  { "",         1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",     1, 0x0000007f, 0x00000000, 0x0000007f },
/* JIS X 0201 */
  { "\xa1",     1, 0x000000a1, 0x00000001, 0x00000021 },
  { "\xdf",     1, 0x000000df, 0x00000001, 0x0000005f },
/* JIS X 0208 */
  { "\x81\x40", 2, 0x00008140, 0x00000002, 0x00002121 },
  { "\x81\x7e", 2, 0x0000817e, 0x00000002, 0x0000215f },
  { "\x81\x80", 2, 0x00008180, 0x00000002, 0x00002160 },
  { "\x81\x9e", 2, 0x0000819e, 0x00000002, 0x0000217e },
  { "\x81\x9f", 2, 0x0000819f, 0x00000002, 0x00002221 },
  { "\x81\xfc", 2, 0x000081fc, 0x00000002, 0x0000227e },
  { "\x82\x40", 2, 0x00008240, 0x00000002, 0x00002321 },
  { "\x82\x7e", 2, 0x0000827e, 0x00000002, 0x0000235f },
  { "\x9f\x9f", 2, 0x00009f9f, 0x00000002, 0x00005e21 },
  { "\x9f\xfc", 2, 0x00009ffc, 0x00000002, 0x00005e7e },
  { "\xe0\x40", 2, 0x0000e040, 0x00000002, 0x00005f21 },
  { "\xe0\x7e", 2, 0x0000e07e, 0x00000002, 0x00005f5f },
  { "\xef\x9f", 2, 0x0000ef9f, 0x00000002, 0x00007e21 },
  { "\xef\xfc", 2, 0x0000effc, 0x00000002, 0x00007e7e },
/* CP932UDA */
  { "\xf0\x40", 2, 0x0000f040, 0x00000002, 0x00007f21 }, 
  { "\xf9\xfc", 2, 0x0000f9fc, 0x00000002, 0x0000927e },
/* reserved */
  { "\xfa\xfc", 2, 0x0000fafc, 0x00000002, 0x0000947e },
  { "\xfb\xfc", 2, 0x0000fbfc, 0x00000002, 0x0000967e },
  { "\xfc\xfc", 2, 0x0000fcfc, 0x00000002, 0x0000987e },
};
struct charmap jis2004_charmap[] = {
  { "",         1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",     1, 0x0000007f, 0x00000000, 0x0000007f },
/* JIS X 0201 */
  { "\xa1",     1, 0x000000a1, 0x00000001, 0x00000021 },
  { "\xdf",     1, 0x000000df, 0x00000001, 0x0000005f },
/* JIS X 0208 */
  { "\x81\x40", 2, 0x00008140, 0x00000002, 0x00002121 },
  { "\x81\x7e", 2, 0x0000817e, 0x00000002, 0x0000215f },
  { "\x81\x80", 2, 0x00008180, 0x00000002, 0x00002160 },
  { "\x81\x9e", 2, 0x0000819e, 0x00000002, 0x0000217e },
  { "\x81\x9f", 2, 0x0000819f, 0x00000002, 0x00002221 },
  { "\x81\xfc", 2, 0x000081fc, 0x00000002, 0x0000227e },
  { "\x82\x40", 2, 0x00008240, 0x00000002, 0x00002321 },
  { "\x82\x7e", 2, 0x0000827e, 0x00000002, 0x0000235f },
  { "\x9f\x9f", 2, 0x00009f9f, 0x00000002, 0x00005e21 },
  { "\x9f\xfc", 2, 0x00009ffc, 0x00000002, 0x00005e7e },
  { "\xe0\x40", 2, 0x0000e040, 0x00000002, 0x00005f21 },
  { "\xe0\x7e", 2, 0x0000e07e, 0x00000002, 0x00005f5f },
  { "\xef\x9f", 2, 0x0000ef9f, 0x00000002, 0x00007e21 },
  { "\xef\xfc", 2, 0x0000effc, 0x00000002, 0x00007e7e },
/* JIS X 0213 Plane2 */
  /* 1 */
  { "\xf0\x40", 2, 0x0000f040, 0x00000003, 0x00002121 },
  { "\xf0\x9e", 2, 0x0000f09e, 0x00000003, 0x0000217e },
  /* 3 */
  { "\xf1\x40", 2, 0x0000f140, 0x00000003, 0x00002321 },
  { "\xf1\x9e", 2, 0x0000f19e, 0x00000003, 0x0000237e },
  /* 4 */
  { "\xf1\x9f", 2, 0x0000f19f, 0x00000003, 0x00002421 },
  { "\xf1\xfc", 2, 0x0000f1fc, 0x00000003, 0x0000247e },
  /* 5 */
  { "\xf2\x40", 2, 0x0000f240, 0x00000003, 0x00002521 },
  { "\xf2\x9e", 2, 0x0000f29e, 0x00000003, 0x0000257e },
  /* 8 */
  { "\xf0\x9f", 2, 0x0000f09f, 0x00000003, 0x00002821 },
  { "\xf0\xfc", 2, 0x0000f0fc, 0x00000003, 0x0000287e },
  /* 12 */
  { "\xf2\x9f", 2, 0x0000f29f, 0x00000003, 0x00002c21 },
  { "\xf2\xfc", 2, 0x0000f2fc, 0x00000003, 0x00002c7e },
  /* 13 */
  { "\xf3\x40", 2, 0x0000f340, 0x00000003, 0x00002d21 },
  { "\xf3\x9e", 2, 0x0000f39e, 0x00000003, 0x00002d7e },
  /* 14 */
  { "\xf3\x9f", 2, 0x0000f39f, 0x00000003, 0x00002e21 },
  { "\xf3\xfc", 2, 0x0000f3fc, 0x00000003, 0x00002e7e },
  /* 15 */
  { "\xf4\x40", 2, 0x0000f440, 0x00000003, 0x00002f21 },
  { "\xf4\x9e", 2, 0x0000f49e, 0x00000003, 0x00002f7e },
  /* 78 */
  { "\xf4\x9f", 2, 0x0000f49f, 0x00000003, 0x00006e21 },
  { "\xf4\xfc", 2, 0x0000f4fc, 0x00000003, 0x00006e7e },
  /* 79 */
  { "\xf5\x40", 2, 0x0000f540, 0x00000003, 0x00006f21 },
  { "\xf5\x9e", 2, 0x0000f59e, 0x00000003, 0x00006f7e },
  /* 80 */
  { "\xf5\x9f", 2, 0x0000f59f, 0x00000003, 0x00007021 },
  { "\xf5\xfc", 2, 0x0000f5fc, 0x00000003, 0x0000707e },
  /* 81 */
  { "\xf6\x40", 2, 0x0000f640, 0x00000003, 0x00007121 },
  { "\xf6\x9e", 2, 0x0000f69e, 0x00000003, 0x0000717e },
  /* 82 */
  { "\xf6\x9f", 2, 0x0000f69f, 0x00000003, 0x00007221 },
  { "\xf6\xfc", 2, 0x0000f6fc, 0x00000003, 0x0000727e },
  /* 83 */
  { "\xf7\x40", 2, 0x0000f740, 0x00000003, 0x00007321 },
  { "\xf7\x9e", 2, 0x0000f79e, 0x00000003, 0x0000737e },
  /* 84 */
  { "\xf7\x9f", 2, 0x0000f79f, 0x00000003, 0x00007421 },
  { "\xf7\xfc", 2, 0x0000f7fc, 0x00000003, 0x0000747e },
  /* 85 */
  { "\xf8\x40", 2, 0x0000f840, 0x00000003, 0x00007521 },
  { "\xf8\x9e", 2, 0x0000f89e, 0x00000003, 0x0000757e },
  /* 86 */
  { "\xf8\x9f", 2, 0x0000f89f, 0x00000003, 0x00007621 },
  { "\xf8\xfc", 2, 0x0000f8fc, 0x00000003, 0x0000767e },
  /* 87 */
  { "\xf9\x40", 2, 0x0000f940, 0x00000003, 0x00007721 },
  { "\xf9\x9e", 2, 0x0000f99e, 0x00000003, 0x0000777e },
  /* 88 */
  { "\xf9\x9f", 2, 0x0000f99f, 0x00000003, 0x00007821 },
  { "\xf9\xfc", 2, 0x0000f9fc, 0x00000003, 0x0000787e },
  /* 89 */
  { "\xfa\x40", 2, 0x0000fa40, 0x00000003, 0x00007921 },
  { "\xfa\x9e", 2, 0x0000fa9e, 0x00000003, 0x0000797e },
  /* 90 */
  { "\xfa\x9f", 2, 0x0000fa9f, 0x00000003, 0x00007a21 },
  { "\xfa\xfc", 2, 0x0000fafc, 0x00000003, 0x00007a7e },
  /* 91 */
  { "\xfb\x40", 2, 0x0000fb40, 0x00000003, 0x00007b21 },
  { "\xfb\x9e", 2, 0x0000fb9e, 0x00000003, 0x00007b7e },
  /* 92 */
  { "\xfb\x9f", 2, 0x0000fb9f, 0x00000003, 0x00007c21 },
  { "\xfb\xfc", 2, 0x0000fbfc, 0x00000003, 0x00007c7e },
  /* 93 */
  { "\xfc\x40", 2, 0x0000fc40, 0x00000003, 0x00007d21 },
  { "\xfc\x9e", 2, 0x0000fc9e, 0x00000003, 0x00007d7e },
  /* 94 */
  { "\xfc\x9f", 2, 0x0000fc9f, 0x00000003, 0x00007e21 },
  { "\xfc\xfc", 2, 0x0000fcfc, 0x00000003, 0x00007e7e },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(sjis_charmap),
  },
  {
    VARIABLE("JIS2004"),
    CHARMAP(jis2004_charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"MSKanji"
#include "t_citrus_encmod_template.h"
