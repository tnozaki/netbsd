/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/endian.h>
#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2015\
  Takehiko NOZAKI, All rights reserved.");

struct charmap utf32charmap[] = {
#if _BYTE_ORDER == _BIG_ENDIAN
  { "\0\0\xfe\xff\0\0\0\0",         8, 0x00000000, 0x00000000, 0x00000000 },
  { "\0\0\xfe\xff\0\0\0\x1f",       8, 0x0000001f, 0x00000000, 0x0000001f },
  { "\0\0\xfe\xff\0\0\0\x20",       8, 0x00000020, 0x00000000, 0x00000020 },
  { "\0\0\xfe\xff\0\0\0\x7f",       8, 0x0000007f, 0x00000000, 0x0000007f },
  { "\0\0\xfe\xff\0\0\0\x80",       8, 0x00000080, 0x00000000, 0x00000080 },
  { "\0\0\xfe\xff\0\0\0\x9f",       8, 0x0000009f, 0x00000000, 0x0000009f },
  { "\0\0\xfe\xff\0\0\0\xa0",       8, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\0\0\xfe\xff\0\0\0\xff",       8, 0x000000ff, 0x00000000, 0x000000ff },
  { "\0\0\xfe\xff\0\0\x1\0",        8, 0x00000100, 0x00000000, 0x00000100 },
  { "\0\0\xfe\xff\0\0\x1\xff",      8, 0x000001ff, 0x00000000, 0x000001ff },
  { "\0\0\xfe\xff\0\0\xff\0",       8, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\0\0\xfe\xff\0\0\xff\xff",     8, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\0\0\xfe\xff\0\x1\0\0",        8, 0x00010000, 0x00000000, 0x00010000 },
  { "\0\0\xfe\xff\0\x1\xff\xff",    8, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\0\0\xfe\xff\0\x10\0\0",       8, 0x00100000, 0x00000000, 0x00100000 },
  { "\0\0\xfe\xff\0\x10\xff\xff",   8, 0x0010ffff, 0x00000000, 0x0010ffff },
  { "\0\0\xfe\xff\0\x11\0\0",       8, 0x00110000, 0x00000000, 0x00110000 },
  { "\0\0\xfe\xff\x7f\xff\xff\xff", 8, 0x7fffffff, 0x00000000, 0x7fffffff },
#elif _BYTE_ORDER == _LITTLE_ENDIAN
  { "\xff\xfe\0\0\0\0\0\0",         8, 0x00000000, 0x00000000, 0x00000000 },
  { "\xff\xfe\0\0\x1f\0\0\0",       8, 0x0000001f, 0x00000000, 0x0000001f },
  { "\xff\xfe\0\0\x20\0\0\0",       8, 0x00000020, 0x00000000, 0x00000020 },
  { "\xff\xfe\0\0\x7f\0\0\0",       8, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xff\xfe\0\0\x80\0\0\0",       8, 0x00000080, 0x00000000, 0x00000080 },
  { "\xff\xfe\0\0\x9f\0\0\0",       8, 0x0000009f, 0x00000000, 0x0000009f },
  { "\xff\xfe\0\0\xa0\0\0\0",       8, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\xff\xfe\0\0\xff\0\0\0",       8, 0x000000ff, 0x00000000, 0x000000ff },
  { "\xff\xfe\0\0\0\x1\0\0",        8, 0x00000100, 0x00000000, 0x00000100 },
  { "\xff\xfe\0\0\xff\x1\0\0",      8, 0x000001ff, 0x00000000, 0x000001ff },
  { "\xff\xfe\0\0\0\xff\0\0",       8, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xff\xfe\0\0\xff\xff\0\0",     8, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xff\xfe\0\0\0\0\x1\0",        8, 0x00010000, 0x00000000, 0x00010000 },
  { "\xff\xfe\0\0\xff\xff\x1\0",    8, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\xff\xfe\0\0\0\0\x10\0",       8, 0x00100000, 0x00000000, 0x00100000 },
  { "\xff\xfe\0\0\xff\xff\x10\0",   8, 0x0010ffff, 0x00000000, 0x0010ffff },
  { "\xff\xfe\0\0\0\0\x11\0",       8, 0x00110000, 0x00000000, 0x00110000 },
  { "\xff\xfe\0\0\xff\xff\xff\x7f", 8, 0x7fffffff, 0x00000000, 0x7fffffff },
#else
#error
#endif
};
struct charmap utf32becharmap[] = {
  { "\0\0\0\0",         4, 0x00000000, 0x00000000, 0x00000000 },
  { "\0\0\0\x1f",       4, 0x0000001f, 0x00000000, 0x0000001f },
  { "\0\0\0\x20",       4, 0x00000020, 0x00000000, 0x00000020 },
  { "\0\0\0\x7f",       4, 0x0000007f, 0x00000000, 0x0000007f },
  { "\0\0\0\x80",       4, 0x00000080, 0x00000000, 0x00000080 },
  { "\0\0\0\x9f",       4, 0x0000009f, 0x00000000, 0x0000009f },
  { "\0\0\0\xa0",       4, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\0\0\0\xff",       4, 0x000000ff, 0x00000000, 0x000000ff },
  { "\0\0\x1\0",        4, 0x00000100, 0x00000000, 0x00000100 },
  { "\0\0\x1\xff",      4, 0x000001ff, 0x00000000, 0x000001ff },
  { "\0\0\xff\0",       4, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\0\0\xff\xff",     4, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\0\x1\0\0",        4, 0x00010000, 0x00000000, 0x00010000 },
  { "\0\x1\xff\xff",    4, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\0\x10\0\0",       4, 0x00100000, 0x00000000, 0x00100000 },
  { "\0\x10\xff\xff",   4, 0x0010ffff, 0x00000000, 0x0010ffff },
  { "\0\x11\0\0",       4, 0x00110000, 0x00000000, 0x00110000 },
  { "\x7f\xff\xff\xff", 4, 0x7fffffff, 0x00000000, 0x7fffffff },
};
struct charmap utf32lecharmap[] = {
  { "\0\0\0\0",         4, 0x00000000, 0x00000000, 0x00000000 },
  { "\x1f\0\0\0",       4, 0x0000001f, 0x00000000, 0x0000001f },
  { "\x20\0\0\0",       4, 0x00000020, 0x00000000, 0x00000020 },
  { "\x7f\0\0\0",       4, 0x0000007f, 0x00000000, 0x0000007f },
  { "\x80\0\0\0",       4, 0x00000080, 0x00000000, 0x00000080 },
  { "\x9f\0\0\0",       4, 0x0000009f, 0x00000000, 0x0000009f },
  { "\xa0\0\0\0",       4, 0x000000a0, 0x00000000, 0x000000a0 },
  { "\xff\0\0\0",       4, 0x000000ff, 0x00000000, 0x000000ff },
  { "\0\x1\0\0",        4, 0x00000100, 0x00000000, 0x00000100 },
  { "\xff\x1\0\0",      4, 0x000001ff, 0x00000000, 0x000001ff },
  { "\0\xff\0\0",       4, 0x0000ff00, 0x00000000, 0x0000ff00 },
  { "\xff\xff\0\0",     4, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\0\0\x1\0",        4, 0x00010000, 0x00000000, 0x00010000 },
  { "\xff\xff\x1\0",    4, 0x0001ffff, 0x00000000, 0x0001ffff },
  { "\0\0\x10\0",       4, 0x00100000, 0x00000000, 0x00100000 },
  { "\xff\xff\x10\0",   4, 0x0010ffff, 0x00000000, 0x0010ffff },
  { "\0\0\x11\0",       4, 0x00110000, 0x00000000, 0x00110000 },
  { "\xff\xff\xff\x7f", 4, 0x7fffffff, 0x00000000, 0x7fffffff },
};
struct encoding encodings[] = {
  {
    VARIABLE("UTF32"),
    CHARMAP(utf32charmap),
  },
  {
    VARIABLE("UTF32BE"),
    CHARMAP(utf32becharmap),
  },
  {
    VARIABLE("UTF32LE"),
    CHARMAP(utf32lecharmap),
  },
};

#define T_CITRUS_STDENC_MODNAME	"UTF32"
#include "t_citrus_encmod_template.h"
