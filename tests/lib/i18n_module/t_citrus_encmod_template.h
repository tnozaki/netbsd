/*-
 * Copyright (c)2014 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

static void
test_run(void (*test)(struct _citrus_stdenc *, struct charmap *))
{
	int ret;
	size_t i, j;
	struct encoding *enc;
	struct _citrus_stdenc *ce;
	struct charmap *cm;

	for (i = 0; i < __arraycount(encodings); ++i) {
		enc = &encodings[i];

		ret = _citrus_stdenc_open(&ce,
		    T_CITRUS_STDENC_MODNAME, enc->var, enc->lenvar);

		ATF_CHECK(ret == 0);

		for (j = 0; j < enc->lencm; ++j) {
			cm = enc->cm + j;
			(*test)(ce, cm);
		}

		_citrus_stdenc_close(ce);
	}
}

static void
do_test1(struct _citrus_stdenc *ce, struct charmap *cm)
{
	size_t n, m;
	int ret;
	char st[sizeof(mbstate_t)];
	const char *s;
	_citrus_wc_t wc;

	/* mb -> wc */
	(*ce->ce_ops->eo_init_state)(ce, &st);
	s = cm->s;
	ret = (*ce->ce_ops->eo_mbtowc)(ce,
	    &wc, &s, cm->n, (void *)&st, &n);
	ATF_CHECK(ret == 0);
	m = (wc == L'\0') ? 0 : cm->n;
	ATF_CHECK(n == m);
	ATF_CHECK(wc == cm->wc);
}
ATF_TC(test1);
ATF_TC_HEAD(test1, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    T_CITRUS_STDENC_MODNAME " test1");
}
ATF_TC_BODY(test1, tc)
{
	test_run(&do_test1);
}

static void
do_test2(struct _citrus_stdenc *ce, struct charmap *cm)
{
	size_t n;
	int ret;
	char st[sizeof(mbstate_t)];
	char mb[MB_LEN_MAX];

	/* wc -> mb */
	(*ce->ce_ops->eo_init_state)(ce, &st);
	ret = (*ce->ce_ops->eo_wctomb)(ce,
	    &mb[0], sizeof(mb), cm->wc, (void *)&st, &n);
	ATF_CHECK(ret == 0);
#ifdef MULTIPLE_REPRESENTATION
	ATF_CHECK(n == cm->altn);
	ATF_CHECK(!memcmp(&mb[0], cm->alts, cm->altn));
#else
	ATF_CHECK(n == cm->n);
	ATF_CHECK(!memcmp(&mb[0], cm->s, cm->n));
#endif
}
ATF_TC(test2);
ATF_TC_HEAD(test2, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    T_CITRUS_STDENC_MODNAME " test2");
}
ATF_TC_BODY(test2, tc)
{
	test_run(&do_test2);
}

static void
do_test3(struct _citrus_stdenc *ce, struct charmap *cm)
{
	size_t n, m;
	int ret;
	char st[sizeof(mbstate_t)];
	const char *s;
	_citrus_csid_t csid;
	_citrus_index_t idx;

	/* mb -> cs */
	(*ce->ce_ops->eo_init_state)(ce, &st);
	s = cm->s;
	ret = (*ce->ce_ops->eo_mbtocs)(ce,
	    &csid, &idx, &s, cm->n, (void *)&st, &n);
	ATF_CHECK(ret == 0);
	m = (csid == 0 && idx == 0) ? 0 : cm->n;
	ATF_CHECK(n == m);
	ATF_CHECK(csid == cm->csid);
	ATF_CHECK(idx == cm->idx);
}
ATF_TC(test3);
ATF_TC_HEAD(test3, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    T_CITRUS_STDENC_MODNAME " test3");
}
ATF_TC_BODY(test3, tc)
{
	test_run(&do_test3);
}

static void
do_test4(struct _citrus_stdenc *ce, struct charmap *cm)
{
	size_t n;
	int ret;
	char st[sizeof(mbstate_t)];
	char mb[MB_LEN_MAX];

	/* cs -> mb */
	(*ce->ce_ops->eo_init_state)(ce, &st);
	ret = (*ce->ce_ops->eo_cstomb)(ce,
	    &mb[0], sizeof(mb), cm->csid, cm->idx, (void *)&st, &n);
	ATF_CHECK(ret == 0);
#ifdef MULTIPLE_REPRESENTATION
	ATF_CHECK(n == cm->altn);
	ATF_CHECK(!memcmp(&mb[0], cm->alts, cm->altn));
#else
	ATF_CHECK(n == cm->n);
	ATF_CHECK(!memcmp(&mb[0], cm->s, cm->n));
#endif
}
ATF_TC(test4);
ATF_TC_HEAD(test4, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    T_CITRUS_STDENC_MODNAME " test4");
}
ATF_TC_BODY(test4, tc)
{
	test_run(&do_test4);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test1);
	ATF_TP_ADD_TC(tp, test2);
	ATF_TP_ADD_TC(tp, test3);
	ATF_TP_ADD_TC(tp, test4);
	return atf_no_error();
}
