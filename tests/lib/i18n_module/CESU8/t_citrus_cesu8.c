/*-
 * Copyright (c) 2019 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2019\
  Takehiko NOZAKI, All rights reserved.");

struct charmap cesu8charmap[] = {
  { "",                         1, 0x00000000, 0x00000000, 0x00000000 },
  { "\x7f",                     1, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xc2\x80",                 2, 0x00000080, 0x00000000, 0x00000080 },
  { "\xdf\xbf",                 2, 0x000007ff, 0x00000000, 0x000007ff },
  { "\xe0\xa0\x80",             3, 0x00000800, 0x00000000, 0x00000800 },
  { "\xef\xbf\xbf",             3, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xed\xa0\x80\xed\xb0\x80", 6, 0x00010000, 0x00000000, 0x00010000 },
  { "\xed\xaf\xbf\xed\xbf\xbf", 6, 0x0010ffff, 0x00000000, 0x0010ffff },
};
struct charmap mutf8charmap[] = {
  { "\xc0\x80",                 2, 0x00000000, 0x00000000, 0x00000000 },
  { "\x01",                     1, 0x00000001, 0x00000000, 0x00000001 },
  { "\x7f",                     1, 0x0000007f, 0x00000000, 0x0000007f },
  { "\xc2\x80",                 2, 0x00000080, 0x00000000, 0x00000080 },
  { "\xdf\xbf",                 2, 0x000007ff, 0x00000000, 0x000007ff },
  { "\xe0\xa0\x80",             3, 0x00000800, 0x00000000, 0x00000800 },
  { "\xef\xbf\xbf",             3, 0x0000ffff, 0x00000000, 0x0000ffff },
  { "\xed\xa0\x80\xed\xb0\x80", 6, 0x00010000, 0x00000000, 0x00010000 },
  { "\xed\xaf\xbf\xed\xbf\xbf", 6, 0x0010ffff, 0x00000000, 0x0010ffff },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(cesu8charmap),
  },
  {
    VARIABLE("MUTF-8"),
    CHARMAP(mutf8charmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"CESU8"
#include "t_citrus_encmod_template.h"
