/*-
 * Copyright (c) 2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define MULTIPLE_REPRESENTATION	1
#include "t_citrus_encmod_defs.h"

__COPYRIGHT("@(#) Copyright (c) 2015\
  Takehiko NOZAKI, All rights reserved.");

struct charmap javacharmap[] = {
  { "",                         1, 0x00000000, 0x00000000, 0x00000000,
    "",                         1                                     },
  { "\x7f",                     1, 0x0000007f, 0x00000000, 0x0000007f,
    "\x7f",                     1                                     },
  { "\\u0080",                  6, 0x00000080, 0x00000000, 0x00000080,
    "\\u0080",                  6                                     },
  { "\\u009f",                  6, 0x0000009f, 0x00000000, 0x0000009f,
    "\\u009f",                  6                                     },
  { "\\u009F",                  6, 0x0000009F, 0x00000000, 0x0000009F,
    "\\u009f",                  6                                     },
  { "\\u00a0",                  6, 0x000000a0, 0x00000000, 0x000000a0,
    "\\u00a0",                  6                                     },
  { "\\u00A0",                  6, 0x000000a0, 0x00000000, 0x000000a0,
    "\\u00a0",                  6                                     },
  { "\\uffff",                  6, 0x0000ffff, 0x00000000, 0x0000ffff,
    "\\uffff",                  6                                     },
  { "\\uFFFF",                  6, 0x0000ffff, 0x00000000, 0x0000ffff,
    "\\uffff",                  6                                     },
  { "\\ud800\\udc00",          12, 0x00010000, 0x00000000, 0x00010000,
    "\\ud800\\udc00",          12                                     },
  { "\\uD800\\uDC00",          12, 0x00010000, 0x00000000, 0x00010000,
    "\\ud800\\udc00",          12                                     },
  { "\\ud83f\\udfff",          12, 0x0001ffff, 0x00000000, 0x0001ffff,
    "\\ud83f\\udfff",          12                                     },
  { "\\uD83F\\uDFFF",          12, 0x0001ffff, 0x00000000, 0x0001ffff,
    "\\ud83f\\udfff",          12                                     },
  { "\\udbc0\\udc00",          12, 0x00100000, 0x00000000, 0x00100000,
    "\\udbc0\\udc00",          12                                     },
  { "\\uDBC0\\uDC00",          12, 0x00100000, 0x00000000, 0x00100000,
    "\\udbc0\\udc00",          12                                     },
  { "\\udbff\\udfff",          12, 0x0010ffff, 0x00000000, 0x0010ffff,
    "\\udbff\\udfff",          12                                     },
  { "\\uDBFF\\uDFFF",          12, 0x0010ffff, 0x00000000, 0x0010ffff,
    "\\udbff\\udfff",          12                                     },
};
struct encoding encodings[] = {
  {
    VARIABLE(""),
    CHARMAP(javacharmap),
  }
};

#define T_CITRUS_STDENC_MODNAME	"Java"
#include "t_citrus_encmod_template.h"
