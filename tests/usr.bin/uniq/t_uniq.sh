atf_test_case posix_cf1
posix_cf1() {
	atf_set "descr" "test uniq -c -f 1"
}
posix_cf1_body() {
	atf_check -s ignore -o file:$(atf_get_srcdir)/d_posix_cf1.out \
	    -x "uniq -c -f 1 $(atf_get_srcdir)/d_posix.in"
}

atf_test_case posix_df1
posix_df1() {
	atf_set "descr" "test uniq -d -f 1"
}
posix_df1_body() {
	atf_check -s ignore -o file:$(atf_get_srcdir)/d_posix_df1.out \
	    -x "uniq -d -f 1 $(atf_get_srcdir)/d_posix.in"
}

atf_test_case posix_uf1
posix_uf1() {
	atf_set "descr" "test uniq -u -f 1"
}
posix_uf1_body() {
	atf_check -s ignore -o file:$(atf_get_srcdir)/d_posix_uf1.out \
	    -x "uniq -u -f 1 $(atf_get_srcdir)/d_posix.in"
}

atf_test_case posix_ds2
posix_ds2() {
	atf_set "descr" "test uniq -d -s 2"
}
posix_ds2_body() {
	atf_check -s ignore -o file:$(atf_get_srcdir)/d_posix_ds2.out \
	    -x "uniq -d -s 2 $(atf_get_srcdir)/d_posix.in"
}

atf_init_test_cases() {
	atf_add_test_case posix_cf1
	atf_add_test_case posix_df1
	atf_add_test_case posix_uf1
	atf_add_test_case posix_ds2
}
