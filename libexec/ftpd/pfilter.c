/*	$NetBSD: pfilter.c,v 1.4 2020/07/04 05:18:37 lukem Exp $	*/

#include <stdio.h>
#include <unistd.h>
#if USE_BLOCKLIST
#include <blocklist.h>
#endif

#include "pfilter.h"

#if USE_BLOCKLIST
static struct blocklist *blstate;
#endif

void
pfilter_open(void)
{
#if USE_BLOCKLIST
	if (blstate == NULL)
		blstate = blocklist_open();
#endif
}

void
pfilter_notify(int what, const char *msg)
{
#if USE_BLOCKLIST
	pfilter_open();

	if (blstate == NULL)
		return;

	blocklist_r(blstate, what, STDIN_FILENO, msg);
#endif
}
