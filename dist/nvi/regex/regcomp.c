/*-
 * Copyright (c) 1992, 1993, 1994 Henry Spencer.
 * Copyright (c) 1992, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Henry Spencer.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <sys/types.h>
#include <ctype.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

#include "utils.h"
#include "regex2.h"

#include "cclass.h"
#include "cname.h"

/*
 * parse structure, passed up and down to avoid global variables and
 * other clumsinesses
 */
struct parse {
	const RCHAR_T *next;	/* next character in RE */
	const RCHAR_T *end;	/* end of string (-> NUL normally) */
	int error;		/* has an error been seen? */
	sop *strip;		/* malloced strip */
	RCHAR_T *stripdata;	/* malloced stripdata */
	sopno ssize;		/* malloced strip size (allocated) */
	sopno slen;		/* malloced strip length (used) */
	int ncsalloc;		/* number of csets allocated */
	struct re_guts *g;
#	define	NPAREN	10	/* we need to remember () 1-9 for back refs */
	sopno pbegin[NPAREN];	/* -> ( ([0] unused) */
	sopno pend[NPAREN];	/* -> ) ([0] unused) */
};

static void p_ere(struct parse *, int, size_t);
static void p_ere_exp(struct parse *, size_t);
static void p_str(struct parse *);
static void p_bre(struct parse *, int, int, size_t);
static int p_simp_re(struct parse *, int, size_t);
static int p_count(struct parse *);
static void p_bracket(struct parse *);
static void p_b_term(struct parse *, cset *);
static void p_b_cclass(struct parse *, cset *);
static void p_b_eclass(struct parse *, cset *);
static char p_b_symbol(struct parse *);
static char p_b_coll_elem(struct parse *, int);
static int othercase(int);
static void bothcases(struct parse *, int);
static void ordinary(struct parse *, int);
static void backslash(struct parse *, int);
static void nonnewline(struct parse *);
static void repeat(struct parse *, sopno, int, int, size_t);
static void seterr(struct parse *, int);
static cset *allocset(struct parse *);
static void freeset(struct parse *, cset *);
static sopno freezeset(struct parse *, cset *);
static int firstch(struct parse *, cset *);
static int nch(struct parse *, cset *);
static sopno dupl(struct parse *, sopno, sopno);
static void doemit(struct parse *, sop, sopno);
static void doinsert(struct parse *, sop, sopno, sopno);
static void dofwd(struct parse *, sopno, sop);
static int enlarge(struct parse *, sopno);
static void stripsnug(struct parse *, struct re_guts *);
static void findmust(struct parse *, struct re_guts *);
static int altoffset(sop *, RCHAR_T *, int);
static void computejumps(struct parse *, struct re_guts *);
static void computematchjumps(struct parse *, struct re_guts *);
static sopno pluscount(struct parse *, struct re_guts *);

static RCHAR_T nuls[10];	/* place to point scanner in event of error */

/*
 * macros for use with parse structure
 * BEWARE:  these know that the parse structure is named `p' !!!
 */
#define	PEEK()	(*p->next)
#define	PEEK2()	(*(p->next+1))
#define	MORE()	(p->end - p->next > 0)
#define	MORE2()	(p->end - p->next > 1)
#define	SEE(c)	(MORE() && PEEK() == (c))
#define	SEETWO(a, b)	(MORE2() && PEEK() == (a) && PEEK2() == (b))
#define	EAT(c)	((SEE(c)) ? (NEXT(), 1) : 0)
#define	EATTWO(a, b)	((SEETWO(a, b)) ? (NEXT2(), 1) : 0)
#define	NEXT()	(p->next++)
#define	NEXT2()	(p->next += 2)
#define	NEXTn(n)	(p->next += (n))
#define	GETNEXT()	(*p->next++)
#define	SETERROR(e)	seterr(p, (e))
#define	REQUIRE(co, e)	do { if (!(co)) SETERROR(e); } while (0)
#define	EMIT(op, sopnd)	doemit(p, (sop)(op), sopnd)
#define	INSERT(op, pos)	doinsert(p, (sop)(op), HERE()-(pos)+1, pos)
#define	AHEAD(pos)		dofwd(p, pos, HERE()-(pos))
#define	ASTERN(sop, pos)	EMIT(sop, HERE()-pos)
#define	HERE()		(p->slen)
#define	THERE()		(p->slen - 1)
#define	THERETHERE()	(p->slen - 2)
#define	DROP(n)	(p->slen -= (n))

#ifndef NDEBUG
static int never = 0;		/* for use in asserts; shuts lint up */
#else
#define	never	0		/* some <assert.h>s have bugs too */
#endif

/* Macro used by computejump()/computematchjump() */
#define MIN(a,b)	((a)<(b)?(a):(b))

#define	RECLIMIT	256

/*
 - regcomp - interface for parser and compilation
 */
int				/* 0 success, otherwise REG_something */
regcomp(regex_t *preg, const RCHAR_T *pattern, int cflags)
{
	struct parse pa;
	struct re_guts *g;
	struct parse *p = &pa;
	int i;
	size_t len;
#ifdef REDEBUG
#	define	GOODFLAGS(f)	(f)
#else
#	define	GOODFLAGS(f)	((f)&~REG_DUMP)
#endif

	cflags = GOODFLAGS(cflags);
	if ((cflags&REG_EXTENDED) && (cflags&REG_NOSPEC))
		return(REG_INVARG);

	if (cflags&REG_PEND) {
		if (preg->re_endp < pattern)
			return(REG_INVARG);
		len = preg->re_endp - pattern;
	} else
		len = STRLEN(pattern);

	/* do the mallocs early so failure handling is easy */
	g = malloc(sizeof(*g));
	if (g == NULL)
		return(REG_ESPACE);

	p->ssize = len / 2 * 3 + 1;	/* ugh */
	if (p->ssize > MEMLIMIT(*p->strip) ||
	    (p->strip = malloc(p->ssize * sizeof(*p->strip))) == NULL) {
		free(g);
		return(REG_ESPACE);
	}
	if (p->ssize > MEMLIMIT(*p->stripdata) ||
	    (p->stripdata = malloc(p->ssize * sizeof(*p->stripdata))) == NULL) {
		free(p->strip);
		free(g);
		return(REG_ESPACE);
	}
	p->slen = 0;

	/* set things up */
	p->g = g;
	p->next = pattern;
	p->end = p->next + len;
	p->error = 0;
	p->ncsalloc = 0;
	for (i = 0; i < NPAREN; i++) {
		p->pbegin[i] = 0;
		p->pend[i] = 0;
	}
	g->csetsize = NC;
	g->sets = NULL;
	g->setbits = NULL;
	g->ncsets = 0;
	g->cflags = cflags;
	g->iflags = 0;
	g->nbol = 0;
	g->neol = 0;
	g->must = NULL;
	g->moffset = -1;
	g->charjump = NULL;
	g->matchjump = NULL;
	g->mlen = 0;
	g->nsub = 0;
	g->backrefs = 0;

	/* do it */
	EMIT(OEND, 0);
	g->firststate = THERE();
	if (cflags&REG_EXTENDED)
		p_ere(p, OUT, 0);
	else if (cflags&REG_NOSPEC)
		p_str(p);
	else
		p_bre(p, OUT, OUT, 0);
	EMIT(OEND, 0);
	g->laststate = THERE();

	/* tidy up loose ends and fill things in */
	stripsnug(p, g);
	findmust(p, g);
	/* only use Boyer-Moore algorithm if the pattern is bigger
	 * than three characters
	 */
	if (g->mlen > 3) {
		computejumps(p, g);
		computematchjumps(p, g);
		if (g->matchjump == NULL) {
			free(g->charjump);
			g->charjump = NULL;
		}
	}
	g->nplus = pluscount(p, g);
	g->magic = MAGIC2;
	preg->re_nsub = g->nsub;
	preg->re_g = g;
	preg->re_magic = MAGIC1;
#ifndef REDEBUG
	/* not debugging, so can't rely on the assert() in regexec() */
	if (g->iflags&BAD)
		SETERROR(REG_ASSERT);
#endif

	/* win or lose, we're done */
	if (p->error != 0)	/* lose */
		regfree(preg);
	return(p->error);
}

/*
 - p_ere - ERE parser top level, concatenation and alternation
 */
static void
p_ere(struct parse *p,
    int stop,			/* character this ERE should end at */
    size_t reclimit)
{
	char c;
	sopno prevback = 0;	/* pacify gcc */
	sopno prevfwd = 0; 	/* pacify gcc */
	sopno conc;
	int first = 1;		/* is this the first alternative? */

	if (reclimit++ > RECLIMIT || p->error == REG_ESPACE) {
		p->error = REG_ESPACE;
		return;
	}

	for (;;) {
		/* do a bunch of concatenated expressions */
		conc = HERE();
		while (MORE() && (c = PEEK()) != '|' && c != stop)
			p_ere_exp(p, reclimit);
		REQUIRE(HERE() != conc, REG_EMPTY);	/* require nonempty */

		if (!EAT('|'))
			break;		/* NOTE BREAK OUT */

		if (first) {
			INSERT(OCH_, conc);	/* offset is wrong */
			prevfwd = conc;
			prevback = conc;
			first = 0;
		}
		ASTERN(OOR1, prevback);
		prevback = THERE();
		AHEAD(prevfwd);			/* fix previous offset */
		prevfwd = HERE();
		EMIT(OOR2, 0);			/* offset is very wrong */
	}

	if (!first) {		/* tail-end fixups */
		AHEAD(prevfwd);
		ASTERN(O_CH, prevback);
	}

	assert(!MORE() || SEE(stop));
}

/*
 - p_ere_exp - parse one subERE, an atom possibly followed by a repetition op
 */
static void
p_ere_exp(struct parse *p, size_t reclimit)
{
	char c;
	sopno pos;
	int count;
	int count2;
	sopno subno;
	int wascaret = 0;

	assert(MORE());		/* caller should have ensured this */
	c = GETNEXT();

	pos = HERE();
	switch (c) {
	case '(':
		REQUIRE(MORE(), REG_EPAREN);
		p->g->nsub++;
		subno = p->g->nsub;
		if (subno < NPAREN)
			p->pbegin[subno] = HERE();
		EMIT(OLPAREN, subno);
		if (!SEE(')'))
			p_ere(p, ')', reclimit);
		if (subno < NPAREN) {
			p->pend[subno] = HERE();
			assert(p->pend[subno] != 0);
		}
		EMIT(ORPAREN, subno);
		REQUIRE(MORE() && GETNEXT() == ')', REG_EPAREN);
		break;
#ifndef POSIX_MISTAKE
	case ')':		/* happens only if no current unmatched ( */
		/*
		 * You may ask, why the ifndef?  Because I didn't notice
		 * this until slightly too late for 1003.2, and none of the
		 * other 1003.2 regular-expression reviewers noticed it at
		 * all.  So an unmatched ) is legal POSIX, at least until
		 * we can get it fixed.
		 */
		SETERROR(REG_EPAREN);
		break;
#endif
	case '^':
		EMIT(OBOL, 0);
		p->g->iflags |= USEBOL;
		p->g->nbol++;
		wascaret = 1;
		break;
	case '$':
		EMIT(OEOL, 0);
		p->g->iflags |= USEEOL;
		p->g->neol++;
		break;
	case '|':
		SETERROR(REG_EMPTY);
		break;
	case '*':
	case '+':
	case '?':
		SETERROR(REG_BADRPT);
		break;
	case '.':
		if (p->g->cflags&REG_NEWLINE)
			nonnewline(p);
		else
			EMIT(OANY, 0);
		break;
	case '[':
		p_bracket(p);
		break;
	case '\\':
		REQUIRE(MORE(), REG_EESCAPE);
		c = GETNEXT();
		backslash(p, c);
		break;
	case '{':		/* okay as ordinary except if digit follows */
		REQUIRE(!MORE() || !ISDIGIT((UCHAR_T)PEEK()), REG_BADRPT);
		/* FALLTHROUGH */
	default:
		if (p->error != 0)
			return;
		ordinary(p, c);
		break;
	}

	if (!MORE())
		return;
	c = PEEK();
	/* we call { a repetition if followed by a digit */
	if (!( c == '*' || c == '+' || c == '?' ||
	    (c == '{' && MORE2() && ISDIGIT((UCHAR_T)PEEK2())) ))
		return;		/* no repetition, we're done */
	NEXT();

	REQUIRE(!wascaret, REG_BADRPT);
	switch (c) {
	case '*':	/* implemented as +? */
		/* this case does not require the (y|) trick, noKLUDGE */
		INSERT(OPLUS_, pos);
		ASTERN(O_PLUS, pos);
		INSERT(OQUEST_, pos);
		ASTERN(O_QUEST, pos);
		break;
	case '+':
		INSERT(OPLUS_, pos);
		ASTERN(O_PLUS, pos);
		break;
	case '?':
		/* KLUDGE: emit y? as (y|) until subtle bug gets fixed */
		INSERT(OCH_, pos);		/* offset slightly wrong */
		ASTERN(OOR1, pos);		/* this one's right */
		AHEAD(pos);			/* fix the OCH_ */
		EMIT(OOR2, 0);			/* offset very wrong... */
		AHEAD(THERE());			/* ...so fix it */
		ASTERN(O_CH, THERETHERE());
		break;
	case '{':
		count = p_count(p);
		if (EAT(',')) {
			if (ISDIGIT((UCHAR_T)PEEK())) {
				count2 = p_count(p);
				REQUIRE(count <= count2, REG_BADBR);
			} else		/* single number with comma */
				count2 = INFINITY;
		} else		/* just a single number */
			count2 = count;
		repeat(p, pos, count, count2, 0);
		if (!EAT('}')) {	/* error heuristics */
			while (MORE() && PEEK() != '}')
				NEXT();
			REQUIRE(MORE(), REG_EBRACE);
			SETERROR(REG_BADBR);
		}
		break;
	}

	if (!MORE())
		return;
	c = PEEK();
	if (!( c == '*' || c == '+' || c == '?' ||
	    (c == '{' && MORE2() && ISDIGIT((UCHAR_T)PEEK2())) ) )
		return;
	SETERROR(REG_BADRPT);
}

/*
 - p_str - string (no metacharacters) "parser"
 */
static void
p_str(struct parse *p)
{
	REQUIRE(MORE(), REG_EMPTY);
	while (MORE())
		ordinary(p, GETNEXT());
}

/*
 - p_bre - BRE parser top level, anchoring and concatenation
 * Giving end1 as OUT essentially eliminates the end1/end2 check.
 *
 * This implementation is a bit of a kludge, in that a trailing $ is first
 * taken as an ordinary character and then revised to be an anchor.
 * The amount of lookahead needed to avoid this kludge is excessive.
 */
static void
p_bre(struct parse *p,
    int end1,		/* first terminating character */
    int end2,		/* second terminating character */
    size_t reclimit)
{
	sopno start;
	int first = 1;			/* first subexpression? */
	int wasdollar = 0;

	if (reclimit++ > RECLIMIT || p->error == REG_ESPACE) {
		p->error = REG_ESPACE;
		return;
	}

	start = HERE();

	if (EAT('^')) {
		EMIT(OBOL, 0);
		p->g->iflags |= USEBOL;
		p->g->nbol++;
	}
	while (MORE() && !SEETWO(end1, end2)) {
		wasdollar = p_simp_re(p, first, reclimit);
		first = 0;
	}
	if (wasdollar) {	/* oops, that was a trailing anchor */
		DROP(1);
		EMIT(OEOL, 0);
		p->g->iflags |= USEEOL;
		p->g->neol++;
	}

	REQUIRE(HERE() != start, REG_EMPTY);	/* require nonempty */
}

/*
 - p_simp_re - parse a simple RE, an atom possibly followed by a repetition
 */
static int			/* was the simple RE an unbackslashed $? */
p_simp_re(struct parse *p,
    int starordinary,		/* is a leading * an ordinary character? */
    size_t reclimit)
{
	int c;
	int count;
	int count2;
	sopno pos;
	int i;
	sopno subno;
	int backsl;

	pos = HERE();		/* repetion op, if any, covers from here */

	assert(MORE());		/* caller should have ensured this */
	c = GETNEXT();
	backsl = c == '\\';
	if (backsl) {
		REQUIRE(MORE(), REG_EESCAPE);
		c = (unsigned char)GETNEXT();
		switch (c) {
		case '<':
			EMIT(OBOW, 0);
			break;
		case '>':
			EMIT(OEOW, 0);
			break;
		case '{':
			SETERROR(REG_BADRPT);
			break;
		case '(':
			p->g->nsub++;
			subno = p->g->nsub;
			if (subno < NPAREN)
				p->pbegin[subno] = HERE();
			EMIT(OLPAREN, subno);
			/* the MORE here is an error heuristic */
			if (MORE() && !SEETWO('\\', ')'))
				p_bre(p, '\\', ')', reclimit);
			if (subno < NPAREN) {
				p->pend[subno] = HERE();
				assert(p->pend[subno] != 0);
			}
			EMIT(ORPAREN, subno);
			REQUIRE(EATTWO('\\', ')'), REG_EPAREN);
			break;
		case ')':	/* should not get here -- must be user */
		case '}':
			SETERROR(REG_EPAREN);
			break;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			i = c - '0';
			assert(i < NPAREN);
			if (p->pend[i] != 0) {
				assert((size_t)i <= p->g->nsub);
				EMIT(OBACK_, i);
				assert(p->pbegin[i] != 0);
				assert(p->strip[p->pbegin[i]] == OLPAREN);
				assert(p->strip[p->pend[i]] == ORPAREN);
				dupl(p, p->pbegin[i]+1, p->pend[i]);
				EMIT(O_BACK, i);
			} else
				SETERROR(REG_ESUBREG);
			p->g->backrefs = 1;
			break;
		default:
			if (p->error != 0)
				return 0;
			ordinary(p, c);
			break;
		}
	} else {
		switch (c) {
		case '.':
			if (p->g->cflags&REG_NEWLINE)
				nonnewline(p);
			else
				EMIT(OANY, 0);
			break;
		case '[':
			p_bracket(p);
			break;
		case '*':
			REQUIRE(starordinary, REG_BADRPT);
			/* FALLTHROUGH */
		default:
			if (p->error != 0)
				return 0;
			ordinary(p, c);
			break;
		}
	}

	if (EAT('*')) {		/* implemented as +? */
		/* this case does not require the (y|) trick, noKLUDGE */
		INSERT(OPLUS_, pos);
		ASTERN(O_PLUS, pos);
		INSERT(OQUEST_, pos);
		ASTERN(O_QUEST, pos);
	} else if (EATTWO('\\', '{')) {
		count = p_count(p);
		if (EAT(',')) {
			if (MORE() && ISDIGIT((UCHAR_T)PEEK())) {
				count2 = p_count(p);
				REQUIRE(count <= count2, REG_BADBR);
			} else		/* single number with comma */
				count2 = INFINITY;
		} else		/* just a single number */
			count2 = count;
		repeat(p, pos, count, count2, 0);
		if (!EATTWO('\\', '}')) {	/* error heuristics */
			while (MORE() && !SEETWO('\\', '}'))
				NEXT();
			REQUIRE(MORE(), REG_EBRACE);
			SETERROR(REG_BADBR);
		}
	} else if (!backsl && c == (unsigned char)'$')	/* $ (but not \$) ends it */
		return(1);

	return(0);
}

/*
 - p_count - parse a repetition count
 */
static int			/* the value */
p_count(struct parse *p)
{
	int count = 0;
	int ndigits = 0;

	while (MORE() && ISDIGIT((UCHAR_T)PEEK()) && count <= DUPMAX) {
		count = count*10 + (GETNEXT() - '0');
		ndigits++;
	}

	REQUIRE(ndigits > 0 && count <= DUPMAX, REG_BADBR);
	return(count);
}

/*
 - p_bracket - parse a bracketed character list
 *
 * Note a significant property of this code:  if the allocset() did SETERROR,
 * no set operations are done.
 */
static void
p_bracket(struct parse *p)
{
	cset *cs;
	int invert = 0;

	cs = allocset(p);
	if (cs == NULL)
		return;

	/* Dept of Truly Sickening Special-Case Kludges */
	if (p->end - p->next > 5) {
		if (MEMCMP(p->next, L("[:<:]]"), 6) == 0) {
			EMIT(OBOW, 0);
			NEXTn(6);
			return;
		}
		if (MEMCMP(p->next, L("[:>:]]"), 6) == 0) {
			EMIT(OEOW, 0);
			NEXTn(6);
			return;
		}
	}

	if (EAT('^'))
		invert++;	/* make note to invert set at end */
	if (EAT(']'))
		CHadd(cs, ']');
	else if (EAT('-'))
		CHadd(cs, '-');
	while (MORE() && PEEK() != ']' && !SEETWO('-', ']'))
		p_b_term(p, cs);
	if (EAT('-'))
		CHadd(cs, '-');
	REQUIRE(MORE() && GETNEXT() == ']', REG_EBRACK);

	if (p->error != 0) {	/* don't mess things up further */
		freeset(p, cs);
		return;
	}

	if (p->g->cflags&REG_ICASE) {
		int i;
		int ci;

		for (i = p->g->csetsize - 1; i >= 0; i--)
			if (CHIN(cs, i) && isalpha(i)) {
				ci = othercase(i);
				if (ci != i)
					CHadd(cs, ci);
			}
	}
	if (invert) {
		int i;

		for (i = p->g->csetsize - 1; i >= 0; i--)
			if (CHIN(cs, i))
				CHsub(cs, i);
			else
				CHadd(cs, i);
		if (p->g->cflags&REG_NEWLINE)
			CHsub(cs, '\n');
	}

	if (nch(p, cs) == 1) {		/* optimize singleton sets */
		ordinary(p, firstch(p, cs));
		freeset(p, cs);
	} else
		EMIT(OANYOF, freezeset(p, cs));
}

/*
 - p_b_term - parse one term of a bracketed character list
 */
static void
p_b_term(struct parse *p, cset *cs)
{
	char c;
	char start, finish;
	int i;

	/* classify what we've got */
	switch ((MORE()) ? PEEK() : '\0') {
	case '[':
		c = (MORE2()) ? PEEK2() : '\0';
		break;

	case '-':
		SETERROR(REG_ERANGE);
		return;			/* NOTE RETURN */

	default:
		c = '\0';
		break;
	}

	switch (c) {
	case ':':		/* character class */
		NEXT2();
		REQUIRE(MORE(), REG_EBRACK);
		c = PEEK();
		REQUIRE(c != '-' && c != ']', REG_ECTYPE);
		p_b_cclass(p, cs);
		REQUIRE(MORE(), REG_EBRACK);
		REQUIRE(EATTWO(':', ']'), REG_ECTYPE);
		break;
	case '=':		/* equivalence class */
		NEXT2();
		REQUIRE(MORE(), REG_EBRACK);
		c = PEEK();
		REQUIRE(c != '-' && c != ']', REG_ECOLLATE);
		p_b_eclass(p, cs);
		REQUIRE(MORE(), REG_EBRACK);
		REQUIRE(EATTWO('=', ']'), REG_ECOLLATE);
		break;
	default:		/* symbol, ordinary character, or range */
		start = p_b_symbol(p);
		if (SEE('-') && MORE2() && PEEK2() != ']') {
			/* range */
			NEXT();
			if (EAT('-'))
				finish = '-';
			else
				finish = p_b_symbol(p);
		} else
			finish = start;
/* xxx what about signed chars here... */
		REQUIRE(start <= finish, REG_ERANGE);
		for (i = start; i <= finish; i++)
			CHadd(cs, i);
		break;
	}
}

/*
 - p_b_cclass - parse a character-class name and deal with it
 */
static void
p_b_cclass(struct parse *p, cset *cs)
{
	const RCHAR_T *sp;
	const struct cclass *cp;
	size_t len;
	int c;

	sp = p->next;

	while (MORE() && isalpha(PEEK()))
		NEXT();
	len = p->next - sp;
	for (cp = cclasses; cp->name != NULL; cp++)
		if (STRLEN(cp->name) == len && MEMCMP(cp->name, sp, len) == 0)
			break;
	if (cp->name == NULL) {
		/* oops, didn't find it */
		SETERROR(REG_ECTYPE);
		return;
	}

	switch (cp->fidx) {
	case CALNUM:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isalnum((unsigned char)c))
				CHadd(cs, c);
		break;
	case CALPHA:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isalpha((unsigned char)c))
				CHadd(cs, c);
		break;
	case CBLANK:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isblank((unsigned char)c))
				CHadd(cs, c);
		break;
	case CCNTRL:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (iscntrl((unsigned char)c))
				CHadd(cs, c);
		break;
	case CDIGIT:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isdigit((unsigned char)c))
				CHadd(cs, c);
		break;
	case CGRAPH:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isgraph((unsigned char)c))
				CHadd(cs, c);
		break;
	case CLOWER:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (islower((unsigned char)c))
				CHadd(cs, c);
		break;
	case CPRINT:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isprint((unsigned char)c))
				CHadd(cs, c);
		break;
	case CPUNCT:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (ispunct((unsigned char)c))
				CHadd(cs, c);
		break;
	case CSPACE:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isspace((unsigned char)c))
				CHadd(cs, c);
		break;
	case CUPPER:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isupper((unsigned char)c))
				CHadd(cs, c);
		break;
	case CXDIGIT:
		for (c = CHAR_MIN; c <= CHAR_MAX; c++)
			if (isxdigit((unsigned char)c))
				CHadd(cs, c);
		break;
	}
}

/*
 - p_b_eclass - parse an equivalence-class name and deal with it
 *
 * This implementation is incomplete. xxx
 */
static void
p_b_eclass(struct parse *p, cset *cs)
{
	char c;

	c = p_b_coll_elem(p, '=');
	CHadd(cs, c);
}

/*
 - p_b_symbol - parse a character or [..]ed multicharacter collating symbol
 */
static char			/* value of symbol */
p_b_symbol(struct parse *p)
{
	char value;

	REQUIRE(MORE(), REG_EBRACK);
	if (!EATTWO('[', '.'))
		return(GETNEXT());

	/* collating symbol */
	value = p_b_coll_elem(p, '.');
	REQUIRE(EATTWO('.', ']'), REG_ECOLLATE);
	return(value);
}

/*
 - p_b_coll_elem - parse a collating-element name and look it up
 */
static char			/* value of collating element */
p_b_coll_elem(struct parse *p,
    int endc)			/* name ended by endc,']' */
{
	const RCHAR_T *sp;
	const struct cname *cp;
	size_t len;

	sp = p->next;

	while (MORE() && !SEETWO(endc, ']'))
		NEXT();
	if (!MORE()) {
		SETERROR(REG_EBRACK);
		return(0);
	}
	len = p->next - sp;
	for (cp = cnames; cp->name != NULL; cp++)
		if (STRLEN(cp->name) == len && MEMCMP(cp->name, sp, len) == 0)
			return(cp->code);	/* known name */
	if (len == 1)
		return(*sp);	/* single character */
	SETERROR(REG_ECOLLATE);			/* neither */
	return(0);
}

/*
 - othercase - return the case counterpart of an alphabetic
 */
static int			/* if no counterpart, return ch */
othercase(int ch)
{
	assert(isalpha(ch));
	if (isupper(ch))
		return(tolower(ch));
	else if (islower(ch))
		return(toupper(ch));
	else			/* peculiar, but could happen */
		return(ch);
}

/*
 - bothcases - emit a dualcase version of a two-case character
 *
 * Boy, is this implementation ever a kludge...
 */
static void
bothcases(struct parse *p, int ch)
{
	const RCHAR_T *oldnext;
	const RCHAR_T *oldend;
	RCHAR_T bracket[3];

	oldnext = p->next;
	oldend = p->end;

	assert(othercase(ch) != ch);	/* p_bracket() would recurse */
	p->next = bracket;
	p->end = bracket+2;
	bracket[0] = ch;
	bracket[1] = ']';
	bracket[2] = '\0';
	p_bracket(p);
	assert(p->next == bracket+2);
	p->next = oldnext;
	p->end = oldend;
}

/*
 - ordinary - emit an ordinary character
 */
static void
ordinary(struct parse *p, int ch)
{
	if ((p->g->cflags&REG_ICASE) && isalpha(ch) && othercase(ch) != ch)
		bothcases(p, ch);
	else
		EMIT(OCHAR, (UCHAR_T)ch);
}

/*
 * do something magic with this character, but only if it's extra magic
 */
static void
backslash(struct parse *p, int ch)
{
	switch (ch) {
	case '<':
		EMIT(OBOW, 0);
		break;
	case '>':
		EMIT(OEOW, 0);
		break;
	default:
		ordinary(p, ch);
		break;
	}
}

/*
 - nonnewline - emit REG_NEWLINE version of OANY
 *
 * Boy, is this implementation ever a kludge...
 */
static void
nonnewline(struct parse *p)
{
	const RCHAR_T *oldnext;
	const RCHAR_T *oldend;
	static const RCHAR_T bracket[4] = L("^\n]");

	oldnext = p->next;
	oldend = p->end;

	p->next = bracket;
	p->end = bracket+3;
	p_bracket(p);
	assert(p->next == bracket+3);
	p->next = oldnext;
	p->end = oldend;
}

/*
 - repeat - generate code for a bounded repetition, recursively if needed
 */
static void
repeat(struct parse *p,
    sopno start,		/* operand from here to end of strip */
    int from,			/* repeated from this number */
    int to,			/* to this number of times (maybe INFINITY) */
    size_t reclimit)
{
	sopno finish;
#	define	N	2
#	define	INF	3
#	define	REP(f, t)	((f)*8 + (t))
#	define	MAP(n)	(((n) <= 1) ? (n) : ((n) == INFINITY) ? INF : N)
	sopno copy;

	if (reclimit++ > RECLIMIT) 
		p->error = REG_ESPACE;
	if (p->error)
		return;

	finish = HERE();

	assert(from <= to);

	switch (REP(MAP(from), MAP(to))) {
	case REP(0, 0):			/* must be user doing this */
		DROP(finish-start);	/* drop the operand */
		break;
	case REP(0, 1):			/* as x{1,1}? */
	case REP(0, N):			/* as x{1,n}? */
	case REP(0, INF):		/* as x{1,}? */
		/* KLUDGE: emit y? as (y|) until subtle bug gets fixed */
		INSERT(OCH_, start);		/* offset is wrong... */
		repeat(p, start+1, 1, to, reclimit);
		ASTERN(OOR1, start);
		AHEAD(start);			/* ... fix it */
		EMIT(OOR2, 0);
		AHEAD(THERE());
		ASTERN(O_CH, THERETHERE());
		break;
	case REP(1, 1):			/* trivial case */
		/* done */
		break;
	case REP(1, N):			/* as x?x{1,n-1} */
		/* KLUDGE: emit y? as (y|) until subtle bug gets fixed */
		INSERT(OCH_, start);
		ASTERN(OOR1, start);
		AHEAD(start);
		EMIT(OOR2, 0);			/* offset very wrong... */
		AHEAD(THERE());			/* ...so fix it */
		ASTERN(O_CH, THERETHERE());
		copy = dupl(p, start+1, finish+1);
		assert(copy == finish+4);
		repeat(p, copy, 1, to-1, reclimit);
		break;
	case REP(1, INF):		/* as x+ */
		INSERT(OPLUS_, start);
		ASTERN(O_PLUS, start);
		break;
	case REP(N, N):			/* as xx{m-1,n-1} */
		copy = dupl(p, start, finish);
		repeat(p, copy, from-1, to-1, reclimit);
		break;
	case REP(N, INF):		/* as xx{n-1,INF} */
		copy = dupl(p, start, finish);
		repeat(p, copy, from-1, to, reclimit);
		break;
	default:			/* "can't happen" */
		SETERROR(REG_ASSERT);	/* just in case */
		break;
	}
}

/*
 - seterr - set an error condition
 */
static void			/* useless but makes type checking happy */
seterr(struct parse *p, int e)
{
	if (p->error == 0)	/* keep earliest error condition */
		p->error = e;
	p->next = nuls;		/* try to bring things to a halt */
	p->end = nuls;
}

/*
 - allocset - allocate a set of characters for []
 */
static cset *
allocset(struct parse *p)
{
	int no;
	size_t nc;
	size_t nbytes;
	cset *cs;
	size_t css;
	int i;
	void *ptr;

	no = p->g->ncsets++;
	css = (size_t)p->g->csetsize;
	if (no >= p->ncsalloc) {	/* need another column of space */
		p->ncsalloc += CHAR_BIT;
		nc = p->ncsalloc;
		assert(nc % CHAR_BIT == 0);
		if (nc > MEMLIMIT(*p->g->sets) || (ptr = realloc(p->g->sets,
		    nc * sizeof(*p->g->sets))) == NULL)
			goto nomem;
		p->g->sets = ptr;
		nbytes = nc / CHAR_BIT;
		if (nbytes > SIZE_MAX / css)
			goto nomem;
		nbytes *= css;
		if ((ptr = realloc(p->g->setbits, nbytes)) == NULL)
			goto nomem;
		p->g->setbits = ptr;
		for (i = 0; i < no; i++)
			p->g->sets[i].ptr = p->g->setbits + css*(i/CHAR_BIT);
		memset(p->g->setbits + (nbytes - css), 0, css);
	}
	/* XXX should not happen */
	if (p->g->sets == NULL || p->g->setbits == NULL)
		goto nomem;

	cs = &p->g->sets[no];
	cs->ptr = p->g->setbits + css*((no)/CHAR_BIT);
	cs->mask = 1 << ((no) % CHAR_BIT);
	cs->hash = 0;

	return(cs);
nomem:
	free(p->g->sets);
	p->g->sets = NULL;
	free(p->g->setbits);
	p->g->setbits = NULL;

	SETERROR(REG_ESPACE);
	/* caller's responsibility not to do set ops */
	return NULL;
}

/*
 - freeset - free a now-unused set
 */
static void
freeset(struct parse *p, cset *cs)
{
	size_t i;
	cset *top;
	size_t css;

	top = &p->g->sets[p->g->ncsets];
	css = (size_t)p->g->csetsize;

	for (i = 0; i < css; i++)
		CHsub(cs, i);
	if (cs == top-1)	/* recover only the easy case */
		p->g->ncsets--;
}

/*
 - freezeset - final processing on a set of characters
 *
 * The main task here is merging identical sets.  This is usually a waste
 * of time (although the hash code minimizes the overhead), but can win
 * big if REG_ICASE is being used.  REG_ICASE, by the way, is why the hash
 * is done using addition rather than xor -- all ASCII [aA] sets xor to
 * the same value!
 */
static sopno			/* set number */
freezeset(struct parse *p, cset *cs)
{
	uch h;
	size_t i;
	cset *top;
	cset *cs2;
	size_t css;

	h = cs->hash;
	top = &p->g->sets[p->g->ncsets];
	css = (size_t)p->g->csetsize;

	/* look for an earlier one which is the same */
	for (cs2 = &p->g->sets[0]; cs2 < top; cs2++)
		if (cs2->hash == h && cs2 != cs) {
			/* maybe */
			for (i = 0; i < css; i++)
				if (CHIN(cs2, i) != CHIN(cs, i))
					break;		/* no */
			if (i == css)
				break;			/* yes */
		}

	if (cs2 < top) {	/* found one */
		freeset(p, cs);
		cs = cs2;
	}

	return (sopno)(cs - p->g->sets);
}

/*
 - firstch - return first character in a set (which must have at least one)
 */
static int			/* character; there is no "none" value */
firstch(struct parse *p, cset *cs)
{
	size_t i;
	size_t css;

	css = (size_t)p->g->csetsize;

	for (i = 0; i < css; i++)
		if (CHIN(cs, i))
			return((char)i);
	assert(never);
	return(0);		/* arbitrary */
}

/*
 - nch - number of characters in a set
 */
static int
nch(struct parse *p, cset *cs)
{
	size_t i;
	size_t css = (size_t)p->g->csetsize;
	int n = 0;

	for (i = 0; i < css; i++)
		if (CHIN(cs, i))
			n++;
	return(n);
}

/*
 - dupl - emit a duplicate of a bunch of sops
 */
static sopno			/* start of duplicate */
dupl(struct parse *p,
    sopno start,		/* from here */
    sopno finish)		/* to this less one */
{
	sopno ret;
	sopno len = finish - start;

	ret = HERE();

	assert(finish >= start);
	if (len == 0)
		return ret;
	if (!enlarge(p, p->ssize + len))	/* this many unexpected additions */
		return ret;
	memcpy(p->strip + p->slen, p->strip + start,
	    len * sizeof(*p->strip));
	memcpy(p->stripdata + p->slen, p->stripdata + start,
	    len * sizeof(*p->stripdata));
	p->slen += len;
	return ret;
}

/*
 - doemit - emit a strip operator
 *
 * It might seem better to implement this as a macro with a function as
 * hard-case backup, but it's just too big and messy unless there are
 * some changes to the data structures.  Maybe later.
 */
static void
doemit(struct parse *p, sop op, sopno opnd)
{
	/* avoid making error situations worse */
	if (p->error != 0)
		return;

	/* deal with undersized strip */
	if (p->slen >= p->ssize)
		if (!enlarge(p, (p->ssize+1) / 2 * 3))	/* +50% */
			return;

	/* finally, it's all reduced to the easy case */
	p->strip[p->slen] = op;
	p->stripdata[p->slen] = opnd;
	p->slen++;
}

/*
 - doinsert - insert a sop into the strip
 */
static void
doinsert(struct parse *p, sop op, sopno opnd, sopno pos)
{
	sopno sn;
	sop s;
	RCHAR_T d;
	int i;

	/* avoid making error situations worse */
	if (p->error != 0)
		return;

	sn = HERE();
	EMIT(op, opnd);		/* do checks, ensure space */
	assert(HERE() == sn+1);
	s = p->strip[sn];
	d = p->stripdata[sn];

	/* adjust paren pointers */
	assert(pos > 0);
	for (i = 1; i < NPAREN; i++) {
		if (p->pbegin[i] >= pos) {
			p->pbegin[i]++;
		}
		if (p->pend[i] >= pos) {
			p->pend[i]++;
		}
	}

	memmove(&p->strip[pos+1], &p->strip[pos],
	    (HERE()-pos-1) * sizeof(*p->strip));
	memmove(&p->stripdata[pos+1], &p->stripdata[pos],
	    (HERE()-pos-1) * sizeof(*p->stripdata));
	p->strip[pos] = s;
	p->stripdata[pos] = d;
}

/*
 - dofwd - complete a forward reference
 */
static void
dofwd(struct parse *p, sopno pos, sop value)
{
	/* avoid making error situations worse */
	if (p->error != 0)
		return;

	assert(value < 1);
	p->stripdata[pos] = value;
}

/*
 - enlarge - enlarge the strip
 */
static int
enlarge(struct parse *p, sopno size)
{
	sop *sp;
	RCHAR_T *dp;
	sopno osize;

	if (p->ssize >= size)
		return 1;

	osize = p->ssize;
	p->ssize = size;
	if (p->ssize > MEMLIMIT(*p->strip) || (sp = realloc(p->strip,
	    p->ssize * sizeof(*p->strip))) == NULL)
		goto oomem;
	p->strip = sp;
	if (p->ssize > MEMLIMIT(*p->stripdata) || (dp = realloc(p->stripdata,
	    p->ssize * sizeof(*p->stripdata))) == NULL)
		goto oomem;
	p->stripdata = dp;
	return 1;
oomem:
	p->ssize = osize;
	SETERROR(REG_ESPACE);
	return 0;
}

/*
 - stripsnug - compact the strip
 */
static void
stripsnug(struct parse *p, struct re_guts *g)
{
	g->nstates = p->slen;
	if (p->slen > MEMLIMIT(*p->strip) || (g->strip = realloc(p->strip,
	    p->slen * sizeof(*p->strip))) == NULL)
		goto nomem;
	if (p->slen > MEMLIMIT(*p->stripdata) ||
	    (g->stripdata = realloc(p->stripdata,
	    p->slen * sizeof(*p->stripdata))) == NULL)
		goto nomem;
	return;
nomem:
	SETERROR(REG_ESPACE);
	g->strip = p->strip;
	g->stripdata = p->stripdata;
}

/*
 - findmust - fill in must and mlen with longest mandatory literal string
 *
 * This algorithm could do fancy things like analyzing the operands of |
 * for common subsequences.  Someday.  This code is simple and finds most
 * of the interesting cases.
 *
 * Note that must and mlen got initialized during setup.
 */
static void
findmust(struct parse *p, struct re_guts *g)
{
	sop *scans;
	RCHAR_T *scand;
	sop *starts = NULL;
	RCHAR_T *startd = NULL;
	sop *newstarts = NULL;
	RCHAR_T *newstartd = NULL;
	sopno newlen;
	sop s;
	RCHAR_T d;
	RCHAR_T *cp;
	sopno i;
	int offset;

	/* avoid making error situations worse */
	if (p->error != 0)
		return;

	/* find the longest OCHAR sequence in strip */
	newlen = 0;
	offset = 0;
	g->moffset = 0;
	scans = g->strip + 1;
	scand = g->stripdata + 1;
	do {
		s = *scans++;
		d = *scand++;
		switch (s) {
		case OCHAR:		/* sequence member */
			if (newlen == 0) {		/* new sequence */
				newstarts = scans - 1;
				newstartd = scand - 1;
			}
			newlen++;
			break;
		case OPLUS_:		/* things that don't break one */
		case OLPAREN:
		case ORPAREN:
			break;
		case OQUEST_:		/* things that must be skipped */
		case OCH_:
			offset = altoffset(scans, scand, offset);
			scans--;
			scand--;
			do {
				scans += d;
				scand += d;
				s = *scans;
				d = *scand;
				/* assert() interferes w debug printouts */
				if (s != O_QUEST && s != O_CH && s != OOR2) {
					g->iflags |= BAD;
					return;
				}
			} while (s != O_QUEST && s != O_CH);
			/* fallthrough */
		case OBOW:		/* things that break a sequence */
		case OEOW:
		case OBOL:
		case OEOL:
		case O_QUEST:
		case O_CH:
		case OEND:
			if (newlen > g->mlen) {		/* ends one */
				starts = newstarts;
				startd = newstartd;
				g->mlen = newlen;
				if (offset > -1) {
					g->moffset += offset;
					offset = newlen;
				} else
					g->moffset = offset;
			} else {
				if (offset > -1)
					offset += newlen;
			}
			newlen = 0;
			break;
		case OANY:
			if (newlen > g->mlen) {		/* ends one */
				starts = newstarts;
				startd = newstartd;
				g->mlen = newlen;
				if (offset > -1) {
					g->moffset += offset;
					offset = newlen;
				} else
					g->moffset = offset;
			} else {
				if (offset > -1)
					offset += newlen;
			}
			if (offset > -1)
				offset++;
			newlen = 0;
			break;
		case OANYOF:		/* may or may not invalidate offset */
			/* First, everything as OANY */
			if (newlen > g->mlen) {		/* ends one */
				starts = newstarts;
				startd = newstartd;
				g->mlen = newlen;
				if (offset > -1) {
					g->moffset += offset;
					offset = newlen;
				} else
					g->moffset = offset;
			} else {
				if (offset > -1)
					offset += newlen;
			}
			if (offset > -1)
				offset++;
			newlen = 0;
			break;
		default:
			/* Anything here makes it impossible or too hard
			 * to calculate the offset -- so we give up;
			 * save the last known good offset, in case the
			 * must sequence doesn't occur later.
			 */
			if (newlen > g->mlen) {		/* ends one */
				starts = newstarts;
				startd = newstartd;
				g->mlen = newlen;
				if (offset > -1)
					g->moffset += offset;
				else
					g->moffset = offset;
			}
			offset = -1;
			newlen = 0;
			break;
		}
	} while (s != OEND);

	if (g->mlen == 0) {		/* there isn't one */
		g->moffset = -1;
		return;
	}

	/* turn it into a character string */
	if (g->mlen >= MEMLIMIT(*g->must) ||
	    (g->must = malloc((g->mlen + 1) * sizeof(*g->must))) == NULL) {
		/* argh; just forget it */
		g->mlen = 0;
		g->moffset = -1;
		return;
	}
	cp = g->must;
	scans = starts;
	scand = startd;
	for (i = g->mlen; i > 0; i--) {
		for (;;) {
			s = *scans++;
			d = *scand++;
			if (s == OCHAR)
				break;
		}
		assert(cp < g->must + g->mlen);
		*cp++ = d;
	}
	assert(cp == g->must + g->mlen);
	*cp = '\0';		/* just on general principles */
}

/*
 - altoffset - choose biggest offset among multiple choices
 *
 * Compute, recursively if necessary, the largest offset among multiple
 * re paths.
 */
static int
altoffset(sop *scans, RCHAR_T *scand, int offset)
{
	int largest;
	int try;
	sop s;
	RCHAR_T d;

	/* If we gave up already on offsets, return */
	if (offset == -1)
		return -1;

	largest = 0;
	try = 0;
	s = *scans++;
	d = *scand++;
	while (s != O_QUEST && s != O_CH) {
		switch (s) {
		case OOR1:
			if (try > largest)
				largest = try;
			try = 0;
			break;
		case OQUEST_:
		case OCH_:
			try = altoffset(scans, scand, try);
			if (try == -1)
				return -1;
			scans--;
			scand--;
			do {
				scans += d;
				scand += d;
				s = *scans;
				d = *scand;
				if (s != O_QUEST && s != O_CH &&
							s != OOR2)
					return -1;
			} while (s != O_QUEST && s != O_CH);
			/* We must skip to the next position, or we'll
			 * leave altoffset() too early.
			 */
			scans++;
			scand++;
			break;
		case OANYOF:
		case OCHAR:
		case OANY:
			try++;
		case OBOW:
		case OEOW:
		case OLPAREN:
		case ORPAREN:
		case OOR2:
			break;
		default:
			try = -1;
			break;
		}
		if (try == -1)
			return -1;
		s = *scans++;
		d = *scand++;
	}

	if (try > largest)
		largest = try;

	return largest+offset;
}

/*
 - computejumps - compute char jumps for BM scan
 *
 * This algorithm assumes g->must exists and is has size greater than
 * zero. It's based on the algorithm found on Computer Algorithms by
 * Sara Baase.
 *
 * A char jump is the number of characters one needs to jump based on
 * the value of the character from the text that was mismatched.
 */
static void
computejumps(struct parse *p, struct re_guts *g)
{
	int ch;
	size_t mindex;

	/* Avoid making errors worse */
	if (p->error != 0)
		return;

	assert(NC < MEMLIMIT(*g->charjump));
	g->charjump = malloc((NC + 1) * sizeof(*g->charjump));
	if (g->charjump == NULL)	/* Not a fatal error */
		return;
	/* Adjust for signed chars, if necessary */
	g->charjump = &g->charjump[-(CHAR_MIN)];

	/* If the character does not exist in the pattern, the jump
	 * is equal to the number of characters in the pattern.
	 */
	for (ch = CHAR_MIN; ch < (CHAR_MAX + 1); ch++)
		g->charjump[ch] = g->mlen;

	/* If the character does exist, compute the jump that would
	 * take us to the last character in the pattern equal to it
	 * (notice that we match right to left, so that last character
	 * is the first one that would be matched).
	 */
	for (mindex = 0; mindex < g->mlen; mindex++)
		g->charjump[(int)g->must[mindex]] = g->mlen - mindex - 1;
}

/*
 - computematchjumps - compute match jumps for BM scan
 *
 * This algorithm assumes g->must exists and is has size greater than
 * zero. It's based on the algorithm found on Computer Algorithms by
 * Sara Baase.
 *
 * A match jump is the number of characters one needs to advance based
 * on the already-matched suffix.
 * Notice that all values here are minus (g->mlen-1), because of the way
 * the search algorithm works.
 */
static void
computematchjumps(struct parse *p, struct re_guts *g)
{
	size_t mindex;		/* General "must" iterator */
	size_t suffix;		/* Keeps track of matching suffix */
	size_t ssuffix;		/* Keeps track of suffixes' suffix */
	size_t *pmatches;	/* pmatches[k] points to the next i
				 * such that i+1...mlen is a substring
				 * of k+1...k+mlen-i-1
				 */

	/* Avoid making errors worse */
	if (p->error != 0)
		return;

	if (g->mlen > MEMLIMIT(*pmatches) ||
	    (pmatches = malloc(g->mlen * sizeof(*pmatches))) == NULL) {
		g->matchjump = NULL;
		return;
	}

	if (g->mlen > MEMLIMIT(*g->matchjump) ||
	    (g->matchjump = malloc(g->mlen * sizeof(*g->matchjump))) == NULL) {
		/* Not a fatal error */
		free(pmatches);
		return;
	}

	/* Set maximum possible jump for each character in the pattern */
	for (mindex = 0; mindex < g->mlen; mindex++)
		g->matchjump[mindex] = 2 * g->mlen - mindex - 1;

	/* Compute pmatches[] */
	for (suffix = mindex = g->mlen; mindex-- > 0; suffix--) {
		pmatches[mindex] = suffix;

		/* If a mismatch is found, interrupting the substring,
		 * compute the matchjump for that position. If no
		 * mismatch is found, then a text substring mismatched
		 * against the suffix will also mismatch against the
		 * substring.
		 */
		while (suffix < g->mlen
		    && g->must[mindex] != g->must[suffix]) {
			g->matchjump[suffix] = MIN(g->matchjump[suffix],
			    g->mlen - mindex - 1);
			suffix = pmatches[suffix];
		}
	}

	/* Compute the matchjump up to the last substring found to jump
	 * to the beginning of the largest must pattern prefix matching
	 * it's own suffix.
	 */
	for (mindex = 0; mindex <= suffix; mindex++)
		g->matchjump[mindex] = MIN(g->matchjump[mindex],
		    g->mlen + suffix - mindex);

	ssuffix = pmatches[suffix];
	while (suffix < g->mlen) {
		while (suffix <= ssuffix && suffix < g->mlen) {
			g->matchjump[suffix] = MIN(g->matchjump[suffix],
			    g->mlen + ssuffix - suffix);
			suffix++;
		}
		if (suffix < g->mlen)
			ssuffix = pmatches[ssuffix];
	}

	free(pmatches);
}

/*
 - pluscount - count + nesting
 */
static sopno			/* nesting depth */
pluscount(struct parse *p, struct re_guts *g)
{
	sop *scan;
	sop s;
	sopno plusnest = 0;
	sopno maxnest = 0;

	if (p->error != 0)
		return(0);	/* there may not be an OEND */

	scan = g->strip + 1;
	do {
		s = *scan++;
		switch (s) {
		case OPLUS_:
			plusnest++;
			break;
		case O_PLUS:
			if (plusnest > maxnest)
				maxnest = plusnest;
			plusnest--;
			break;
		}
	} while (s != OEND);
	if (plusnest != 0)
		g->iflags |= BAD;
	return(maxnest);
}
