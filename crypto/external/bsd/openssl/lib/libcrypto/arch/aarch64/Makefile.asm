.include "bsd.own.mk"

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

regen:
	for i in ${OPENSSLSRC}/crypto/aes/asm/aesv8-armx.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghashv8-armx.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-armv8.pl; do \
		perl $$i >$$(basename $$i .pl).S; \
	done
