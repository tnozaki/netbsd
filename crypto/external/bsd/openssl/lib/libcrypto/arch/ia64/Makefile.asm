.include <bsd.own.mk>

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

regen:
	for i in ${OPENSSLSRC}/crypto/bn/asm/ia64-mont.pl \
		 ${OPENSSLSRC}/crypto/rc4/asm/rc4-ia64.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghash-ia64.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-ia64.pl; do \
		perl $$i > $$(basename $$i .pl).S; \
	done
	for i in sha256-ia64 sha512-ia64; do \
		perl ${OPENSSLSRC}/crypto/sha/asm/sha512-ia64.pl $$i.s; \
		mv $$i.s $$i.S; \
	done
