.ident "rc4-ia64.s, version 3.0"
.ident "Copyright (c) 2005 Hewlett-Packard Development Company, L.P."

#define LCSave		r8
#define PRSave		r9

/* Inputs become invalid once rotation begins!  */

#define StateTable	in0
#define DataLen		in1
#define InputBuffer	in2
#define OutputBuffer	in3

#define KTable		r14
#define J		r15
#define InPtr		r16
#define OutPtr		r17
#define InPrefetch	r18
#define OutPrefetch	r19
#define One		r20
#define LoopCount	r21
#define Remainder	r22
#define IFinal		r23
#define EndPtr		r24

#define tmp0		r25
#define tmp1		r26

#define pBypass		p6
#define pDone		p7
#define pSmall		p8
#define pAligned	p9
#define pUnaligned	p10

#define pComputeI	pPhase[0]
#define pComputeJ	pPhase[1]
#define pComputeT	pPhase[2]
#define pOutput		pPhase[3]

#define RetVal		r8
#define L_OK		p7
#define L_NOK		p8

#define	_NINPUTS	4
#define	_NOUTPUT	0

#define	_NROTATE	24
#define	_NLOCALS	(_NROTATE - _NINPUTS - _NOUTPUT)

#ifndef SZ
# define SZ	4	// this must be set to sizeof(RC4_INT)
#endif

#if SZ == 1
# define LKEY			ld1
# define SKEY			st1
# define KEYADDR(dst, i)	add dst = i, KTable
#elif SZ == 2
# define LKEY			ld2
# define SKEY			st2
# define KEYADDR(dst, i)	shladd dst = i, 1, KTable
#elif SZ == 4
# define LKEY			ld4
# define SKEY			st4
# define KEYADDR(dst, i)	shladd dst = i, 2, KTable
#else
# define LKEY			ld8
# define SKEY			st8
# define KEYADDR(dst, i)	shladd dst = i, 3, KTable
#endif

#if defined(_HPUX_SOURCE) && !defined(_LP64)
# define ADDP	addp4
#else
# define ADDP	add
#endif

/* Define a macro for the bit number of the n-th byte: */

#if defined(_HPUX_SOURCE) || defined(B_ENDIAN)
# define HOST_IS_BIG_ENDIAN
# define BYTE_POS(n)	(56 - (8 * (n)))
#else
# define BYTE_POS(n)	(8 * (n))
#endif

/*
   We must perform the first phase of the pipeline explicitly since
   we will always load from the stable the first time. The br.cexit
   will never be taken since regardless of the number of bytes because
   the epilogue count is 4.
*/
/* MODSCHED_RC4 macro was split to _PROLOGUE and _LOOP, because HP-UX
   assembler failed on original macro with syntax error. <appro> */
#define MODSCHED_RC4_PROLOGUE						   \
	{								   \
				ld1		Data[0] = [InPtr], 1;	   \
				add		IFinal = 1, I[1];	   \
				KEYADDR(IPr[0], I[1]);			   \
	} ;;								   \
	{								   \
				LKEY		SI[0] = [IPr[0]];	   \
				mov		pr.rot = 0x10000;	   \
				mov		ar.ec = 4;		   \
	} ;;								   \
	{								   \
				add		J = J, SI[0];		   \
				zxt1		I[0] = IFinal;		   \
				br.cexit.spnt.few .+16; /* never taken */  \
	} ;;
#define MODSCHED_RC4_LOOP(label)					   \
label:									   \
	{	.mmi;							   \
		(pComputeI)	ld1		Data[0] = [InPtr], 1;	   \
		(pComputeI)	add		IFinal = 1, I[1];	   \
		(pComputeJ)	zxt1		J = J;			   \
	}{	.mmi;							   \
		(pOutput)	LKEY		T[1] = [T[1]];		   \
		(pComputeT)	add		T[0] = SI[2], SJ[1];	   \
		(pComputeI)	KEYADDR(IPr[0], I[1]);			   \
	} ;;								   \
	{	.mmi;							   \
		(pComputeT)	SKEY		[IPr[2]] = SJ[1];	   \
		(pComputeT)	SKEY		[JP[1]] = SI[2];	   \
		(pComputeT)	zxt1		T[0] = T[0];		   \
	}{	.mmi;							   \
		(pComputeI)	LKEY		SI[0] = [IPr[0]];	   \
		(pComputeJ)	KEYADDR(JP[0], J);			   \
		(pComputeI)	cmp.eq.unc	pBypass, p0 = I[1], J;	   \
	} ;;								   \
	{	.mmi;							   \
		(pComputeJ)	LKEY		SJ[0] = [JP[0]];	   \
		(pOutput)	xor		Data[3] = Data[3], T[1];   \
				nop		0x0;			   \
	}{	.mmi;							   \
		(pComputeT)	KEYADDR(T[0], T[0]);			   \
		(pBypass)	mov		SI[0] = SI[1];		   \
		(pComputeI)	zxt1		I[0] = IFinal;		   \
	} ;;								   \
	{	.mmb;							   \
		(pOutput)	st1		[OutPtr] = Data[3], 1;	   \
		(pComputeI)	add		J = J, SI[0];		   \
				br.ctop.sptk.few label;			   \
	} ;;

	.text

	.align	32

	.type	RC4, @function
	.global	RC4

	.proc	RC4
	.prologue

RC4:
	{
	  	.mmi
		alloc	r2 = ar.pfs, _NINPUTS, _NLOCALS, _NOUTPUT, _NROTATE

		.rotr Data[4], I[2], IPr[3], SI[3], JP[2], SJ[2], T[2], \
		      OutWord[2]
		.rotp pPhase[4]

		ADDP		InPrefetch = 0, InputBuffer
		ADDP		KTable = 0, StateTable
	}
	{
		.mmi
		ADDP		InPtr = 0, InputBuffer
		ADDP		OutPtr = 0, OutputBuffer
		mov		RetVal = r0
	}
	;;
	{
		.mmi
		lfetch.nt1	[InPrefetch], 0x80
		ADDP		OutPrefetch = 0, OutputBuffer
	}
	{               // Return 0 if the input length is nonsensical
        	.mib
		ADDP		StateTable = 0, StateTable
        	cmp.ge.unc  	L_NOK, L_OK = r0, DataLen
	(L_NOK) br.ret.sptk.few rp
	}
	;;
	{
        	.mib
        	cmp.eq.or  	L_NOK, L_OK = r0, InPtr
        	cmp.eq.or  	L_NOK, L_OK = r0, OutPtr
		nop		0x0
	}
	{
		.mib
        	cmp.eq.or  	L_NOK, L_OK = r0, StateTable
		nop		0x0
	(L_NOK) br.ret.sptk.few rp
	}
	;;
		LKEY		I[1] = [KTable], SZ
/* Prefetch the state-table. It contains 256 elements of size SZ */

#if SZ == 1
		ADDP		tmp0 = 1*128, StateTable
#elif SZ == 2
		ADDP		tmp0 = 3*128, StateTable
		ADDP		tmp1 = 2*128, StateTable
#elif SZ == 4
		ADDP		tmp0 = 7*128, StateTable
		ADDP		tmp1 = 6*128, StateTable
#elif SZ == 8
		ADDP		tmp0 = 15*128, StateTable
		ADDP		tmp1 = 14*128, StateTable
#endif
		;;
#if SZ >= 8
		lfetch.fault.nt1		[tmp0], -256	// 15
		lfetch.fault.nt1		[tmp1], -256;;
		lfetch.fault.nt1		[tmp0], -256	// 13
		lfetch.fault.nt1		[tmp1], -256;;
		lfetch.fault.nt1		[tmp0], -256	// 11
		lfetch.fault.nt1		[tmp1], -256;;
		lfetch.fault.nt1		[tmp0], -256	//  9
		lfetch.fault.nt1		[tmp1], -256;;
#endif
#if SZ >= 4
		lfetch.fault.nt1		[tmp0], -256	//  7
		lfetch.fault.nt1		[tmp1], -256;;
		lfetch.fault.nt1		[tmp0], -256	//  5
		lfetch.fault.nt1		[tmp1], -256;;
#endif
#if SZ >= 2
		lfetch.fault.nt1		[tmp0], -256	//  3
		lfetch.fault.nt1		[tmp1], -256;;
#endif
	{
		.mii
		lfetch.fault.nt1		[tmp0]		//  1
		add		I[1]=1,I[1];;
		zxt1		I[1]=I[1]
	}
	{
		.mmi
		lfetch.nt1	[InPrefetch], 0x80
		lfetch.excl.nt1	[OutPrefetch], 0x80
		.save		pr, PRSave
		mov		PRSave = pr
	} ;;
	{
		.mmi
		lfetch.excl.nt1	[OutPrefetch], 0x80
		LKEY		J = [KTable], SZ
		ADDP		EndPtr = DataLen, InPtr
	}  ;;
	{
		.mmi
		ADDP		EndPtr = -1, EndPtr	// Make it point to
							// last data byte.
		mov		One = 1
		.save		ar.lc, LCSave
		mov		LCSave = ar.lc
		.body
	} ;;
	{
		.mmb
		sub		Remainder = 0, OutPtr
		cmp.gtu		pSmall, p0 = 91, DataLen
(pSmall)	br.cond.dpnt	.rc4Remainder		// Data too small for
							// big loop.
	} ;;
	{
		.mmi
		and		Remainder = 0x7, Remainder
		;;
		cmp.eq		pAligned, pUnaligned = Remainder, r0
		nop		0x0
	} ;;
	{
		.mmb
.pred.rel	"mutex",pUnaligned,pAligned
(pUnaligned)	add		Remainder = -1, Remainder
(pAligned)	sub		Remainder = EndPtr, InPtr
(pAligned)	br.cond.dptk.many .rc4Aligned
	} ;;
	{
		.mmi
		nop		0x0
		nop		0x0
		mov.i		ar.lc = Remainder
	}

/* Do the initial few bytes via the compact, modulo-scheduled loop
   until the output pointer is 8-byte-aligned.  */

		MODSCHED_RC4_PROLOGUE
		MODSCHED_RC4_LOOP(.RC4AlignLoop)

	{
		.mib
		sub		Remainder = EndPtr, InPtr
		zxt1		IFinal = IFinal
		clrrrb				// Clear CFM.rrb.pr so
		;;				// next "mov pr.rot = N"
						// does the right thing.
	}
	{
		.mmi
		mov		I[1] = IFinal
		nop		0x0
		nop		0x0
	} ;;


.rc4Aligned:

/*
   Unrolled loop count = (Remainder - (6+1)*4)/(6*4)
 */

	{
		.mlx
		add	LoopCount = 1 - (6 + 1)*4, Remainder
		movl		Remainder = 0xaaaaaaaaaaaaaaab
	} ;;
	{
		.mmi
		setf.sig	f6 = LoopCount		// M2, M3	6 cyc
		setf.sig	f7 = Remainder		// M2, M3	6 cyc
		nop		0x0
	} ;;
	{
		.mfb
		nop		0x0
		xmpy.hu		f6 = f6, f7
		nop		0x0
	} ;;
	{
		.mmi
		getf.sig	LoopCount = f6;;	// M2		5 cyc
		nop		0x0
		shr.u		LoopCount = LoopCount, 4
	} ;;
	{
		.mmi
		nop		0x0
		nop		0x0
		mov.i		ar.lc = LoopCount
	} ;;

/* Now comes the unrolled loop: */

.rc4Prologue:
//////////////////////////////////////////////////
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		KEYADDR(IPr[0], I[1])
		;;
		LKEY   SI[0] = [IPr[0]]
		;;
		add    J = J, SI[0]
		;;
//////////////////////////////////////////////////
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		KEYADDR(IPr[1], I[0])
		;;
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[1], J)
		;;
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		add    J = J, SI[1]
(pBypass)	br.cond.spnt.many .rc4Bypass2
		;;
.rc4Resume2:
//////////////////////////////////////////////////
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		add    T[0] = SI[0], SJ[1]
		KEYADDR(IPr[2], I[1])
		;;
		SKEY   [IPr[0]] = SJ[1]
		SKEY   [JP[1]] = SI[0]
		zxt1   T[0] = T[0]
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[0], J)
		;;
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		add    J = J, SI[2]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass3
		;;
.rc4Resume3:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[1], SJ[0]
		KEYADDR(IPr[0], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[0]
		SKEY   [JP[0]] = SI[1]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[0], OutWord[1], BYTE_POS(0), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass4
		}
		;;
.rc4Resume4:
.rc4Loop:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[2], SJ[1]
		KEYADDR(IPr[1], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[1]
		SKEY   [JP[1]] = SI[2]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[1], OutWord[0], BYTE_POS(1), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass5
		}
		;;
.rc4Resume5:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[0], SJ[0]
		KEYADDR(IPr[2], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[0]
		SKEY   [JP[0]] = SI[0]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[2], OutWord[0], BYTE_POS(2), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass6
		}
		;;
.rc4Resume6:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[1], SJ[1]
		KEYADDR(IPr[0], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[1]
		SKEY   [JP[1]] = SI[1]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[3], OutWord[0], BYTE_POS(3), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass7
		}
		;;
.rc4Resume7:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[2], SJ[0]
		KEYADDR(IPr[1], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[0]
		SKEY   [JP[0]] = SI[2]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[0], OutWord[0], BYTE_POS(4), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass8
		}
		;;
.rc4Resume8:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[0], SJ[1]
		KEYADDR(IPr[2], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[1]
		SKEY   [JP[1]] = SI[0]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[1], OutWord[0], BYTE_POS(5), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass9
		}
		;;
.rc4Resume9:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[1], SJ[0]
		KEYADDR(IPr[0], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[0]
		SKEY   [JP[0]] = SI[1]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[2], OutWord[0], BYTE_POS(6), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass10
		}
		;;
.rc4Resume10:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[2], SJ[1]
		KEYADDR(IPr[1], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[1]
		SKEY   [JP[1]] = SI[2]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[3], OutWord[0], BYTE_POS(7), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass11
		}
		;;
.rc4Resume11:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[0], SJ[0]
		KEYADDR(IPr[2], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[0]
		SKEY   [JP[0]] = SI[0]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[1] = Data[0], OutWord[0], BYTE_POS(0), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass12
		}
		;;
.rc4Resume12:
		st8 [OutPtr] = OutWord[0], 8
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[1], SJ[1]
		KEYADDR(IPr[0], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[1]
		SKEY   [JP[1]] = SI[1]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[1] = Data[1], OutWord[1], BYTE_POS(1), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass13
		}
		;;
.rc4Resume13:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[2], SJ[0]
		KEYADDR(IPr[1], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[0]
		SKEY   [JP[0]] = SI[2]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[1] = Data[2], OutWord[1], BYTE_POS(2), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass14
		}
		;;
.rc4Resume14:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[0], SJ[1]
		KEYADDR(IPr[2], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[1]
		SKEY   [JP[1]] = SI[0]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[1] = Data[3], OutWord[1], BYTE_POS(3), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass15
		}
		;;
.rc4Resume15:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[1], SJ[0]
		KEYADDR(IPr[0], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[0]
		SKEY   [JP[0]] = SI[1]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[1] = Data[0], OutWord[1], BYTE_POS(4), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass16
		}
		;;
.rc4Resume16:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[2], SJ[1]
		KEYADDR(IPr[1], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[1]
		SKEY   [JP[1]] = SI[2]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[1] = Data[1], OutWord[1], BYTE_POS(5), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass17
		}
		;;
.rc4Resume17:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[0], SJ[0]
		KEYADDR(IPr[2], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[0]
		SKEY   [JP[0]] = SI[0]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[1] = Data[2], OutWord[1], BYTE_POS(6), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass18
		}
		;;
.rc4Resume18:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[1], SJ[1]
		KEYADDR(IPr[0], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[1]
		SKEY   [JP[1]] = SI[1]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[1] = Data[3], OutWord[1], BYTE_POS(7), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass19
		}
		;;
.rc4Resume19:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[2], SJ[0]
		KEYADDR(IPr[1], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[0]
		SKEY   [JP[0]] = SI[2]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[0], OutWord[1], BYTE_POS(0), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass20
		}
		;;
.rc4Resume20:
		st8 [OutPtr] = OutWord[1], 8
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[0], SJ[1]
		KEYADDR(IPr[2], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[1]
		SKEY   [JP[1]] = SI[0]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[1], OutWord[0], BYTE_POS(1), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass21
		}
		;;
.rc4Resume21:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[1], SJ[0]
		KEYADDR(IPr[0], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[0]
		SKEY   [JP[0]] = SI[1]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[2], OutWord[0], BYTE_POS(2), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass22
		}
		;;
.rc4Resume22:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[2], SJ[1]
		KEYADDR(IPr[1], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[1]
		SKEY   [JP[1]] = SI[2]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[3], OutWord[0], BYTE_POS(3), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass23
		}
		;;
.rc4Resume23:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[0], SJ[0]
		KEYADDR(IPr[2], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[0]
		SKEY   [JP[0]] = SI[0]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[0], OutWord[0], BYTE_POS(4), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass24
		}
		;;
.rc4Resume24:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[0] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[1], SJ[1]
		KEYADDR(IPr[0], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[1]
		SKEY   [JP[1]] = SI[1]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[1], OutWord[0], BYTE_POS(5), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass25
		}
		;;
.rc4Resume25:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[1] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[2], SJ[0]
		KEYADDR(IPr[1], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[2]] = SJ[0]
		SKEY   [JP[0]] = SI[2]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[1] = [IPr[1]]
		KEYADDR(JP[1], J)
		xor    Data[2] = Data[2], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[0] = Data[2], OutWord[0], BYTE_POS(6), 8
		}
		{ .mmb
		add    J = J, SI[1]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass26
		}
		;;
.rc4Resume26:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[2] = [InPtr], 1
		padd1  I[0] = One, I[1]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[0], SJ[1]
		KEYADDR(IPr[2], I[1])
		}
		;;
		{ .mmi
		SKEY   [IPr[0]] = SJ[1]
		SKEY   [JP[1]] = SI[0]
		zxt1   T[0] = T[0]
		}
		{ .mmi
		LKEY   SI[2] = [IPr[2]]
		KEYADDR(JP[0], J)
		xor    Data[3] = Data[3], T[1]
		}
		;;
		{ .mmi
		LKEY   SJ[0] = [JP[0]]
		cmp.eq pBypass, p0 = I[1], J
		dep OutWord[0] = Data[3], OutWord[0], BYTE_POS(7), 8
		}
		{ .mmb
		add    J = J, SI[2]
		KEYADDR(T[0], T[0])
(pBypass)	br.cond.spnt.many .rc4Bypass27
		}
		;;
.rc4Resume27:
//////////////////////////////////////////////////
		{ .mmi
		ld1    Data[3] = [InPtr], 1
		padd1  I[1] = One, I[0]
		zxt1   J = J
		}
		{ .mmi
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[1], SJ[0]
		KEYADDR(IPr[0], I[0])
		}
		;;
		{ .mmi
		SKEY   [IPr[1]] = SJ[0]
		SKEY   [JP[0]] = SI[1]
		zxt1   T[1] = T[1]
		}
		{ .mmi
		LKEY   SI[0] = [IPr[0]]
		KEYADDR(JP[1], J)
		xor    Data[0] = Data[0], T[0]
		}
		;;
		{ .mmi
		LKEY   SJ[1] = [JP[1]]
		cmp.eq pBypass, p0 = I[0], J
		dep OutWord[1] = Data[0], OutWord[0], BYTE_POS(0), 8
		}
		{ .mmb
		add    J = J, SI[0]
		KEYADDR(T[1], T[1])
(pBypass)	br.cond.spnt.many .rc4Bypass28
		}
		;;
.rc4Resume28:
		st8 [OutPtr] = OutWord[0], 8
		mov OutWord[0] = OutWord[1]
		lfetch.nt1 [InPrefetch], 24
		lfetch.excl.nt1 [OutPrefetch], 24
		br.cloop.sptk.few .rc4Loop
.rc4Epilogue:
//////////////////////////////////////////////////
		zxt1   J = J
		LKEY   T[1] = [T[1]]
		add    T[0] = SI[2], SJ[1]
		;;
		SKEY   [IPr[2]] = SJ[1]
		SKEY   [JP[1]] = SI[2]
		zxt1   T[0] = T[0]
		KEYADDR(JP[0], J)
		xor    Data[1] = Data[1], T[1]
		;;
		LKEY   SJ[0] = [JP[0]]
		dep OutWord[1] = Data[1], OutWord[1], BYTE_POS(1), 8
		KEYADDR(T[0], T[0])
		;;
//////////////////////////////////////////////////
		LKEY   T[0] = [T[0]]
		add    T[1] = SI[0], SJ[0]
		;;
		SKEY   [IPr[0]] = SJ[0]
		SKEY   [JP[0]] = SI[0]
		zxt1   T[1] = T[1]
		xor    Data[2] = Data[2], T[0]
		;;
		dep OutWord[1] = Data[2], OutWord[1], BYTE_POS(2), 8
		KEYADDR(T[1], T[1])
		;;
//////////////////////////////////////////////////
		LKEY   T[1] = [T[1]]
		;;
		xor    Data[3] = Data[3], T[1]
		;;
		dep OutWord[1] = Data[3], OutWord[1], BYTE_POS(3), 8
		;;
//////////////////////////////////////////////////
#ifdef HOST_IS_BIG_ENDIAN
		shr.u	OutWord[1] = OutWord[1], 32;;
#endif
		st4 [OutPtr] = OutWord[1], 4
	{
		.mmi
		lfetch.nt1	[EndPtr]	// fetch line with last byte
		mov		IFinal = I[1]
		nop		0x0
	}

.rc4Remainder:
	{
		.mmi
		sub		Remainder = EndPtr, InPtr	// Calculate
								// # of bytes
								// left - 1
		nop		0x0
		nop		0x0
	} ;;
	{
		.mib
		cmp.eq		pDone, p0 = -1, Remainder // done already?
		mov.i		ar.lc = Remainder
(pDone)		br.cond.dptk.few .rc4Complete
	}

/* Do the remaining bytes via the compact, modulo-scheduled loop */

		MODSCHED_RC4_PROLOGUE
		MODSCHED_RC4_LOOP(.RC4RestLoop)

.rc4Complete:
	{
		.mmi
		add		KTable = -SZ, KTable
		add		IFinal = -1, IFinal
		mov		ar.lc = LCSave
	} ;;
	{
		.mii
		SKEY		[KTable] = J,-SZ
		zxt1		IFinal = IFinal
		mov		pr = PRSave, 0x1FFFF
	} ;;
	{
		.mib
		SKEY		[KTable] = IFinal
		add		RetVal = 1, r0
		br.ret.sptk.few	rp
	} ;;
.rc4Bypass2:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume2

		;;
.rc4Bypass3:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume3

		;;
.rc4Bypass4:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume4

		;;
.rc4Bypass5:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume5

		;;
.rc4Bypass6:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume6

		;;
.rc4Bypass7:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume7

		;;
.rc4Bypass8:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume8

		;;
.rc4Bypass9:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume9

		;;
.rc4Bypass10:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume10

		;;
.rc4Bypass11:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume11

		;;
.rc4Bypass12:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume12

		;;
.rc4Bypass13:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume13

		;;
.rc4Bypass14:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume14

		;;
.rc4Bypass15:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume15

		;;
.rc4Bypass16:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume16

		;;
.rc4Bypass17:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume17

		;;
.rc4Bypass18:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume18

		;;
.rc4Bypass19:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume19

		;;
.rc4Bypass20:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume20

		;;
.rc4Bypass21:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume21

		;;
.rc4Bypass22:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume22

		;;
.rc4Bypass23:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume23

		;;
.rc4Bypass24:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume24

		;;
.rc4Bypass25:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume25

		;;
.rc4Bypass26:
		sub J = J, SI[1]
		nop 0
		nop 0
		;;
		add J = J, SI[0]
		mov SI[1] = SI[0]
		br.sptk.many .rc4Resume26

		;;
.rc4Bypass27:
		sub J = J, SI[2]
		nop 0
		nop 0
		;;
		add J = J, SI[1]
		mov SI[2] = SI[1]
		br.sptk.many .rc4Resume27

		;;
.rc4Bypass28:
		sub J = J, SI[0]
		nop 0
		nop 0
		;;
		add J = J, SI[2]
		mov SI[0] = SI[2]
		br.sptk.many .rc4Resume28

		;;
	.endp RC4
