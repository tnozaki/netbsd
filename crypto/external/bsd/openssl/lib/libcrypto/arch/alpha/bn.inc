.PATH.S: ${.PARSEDIR}
BN_SRCS = bn_asm.c alpha-mont.S
CPPFLAGS.alpha-mont.S = -I${DESTDIR}/usr/include/machine \
    -I${.CURDIR}/arch/${CRYPTO_MACHINE_CPU}
.include "../../bn.inc"
