.PATH.S: ${.PARSEDIR}
MODES_SRCS = ghash-alpha.S
CPPFLAGS.ghash-alpha.S = -I${DESTDIR}/usr/include/machine \
    -I${.CURDIR}/arch/${CRYPTO_MACHINE_CPU}
.include "../../modes.inc"
