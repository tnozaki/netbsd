.PATH.S: ${.PARSEDIR}
SHA_SRCS = sha1-alpha.S
CPPFLAGS.sha1-alpha.S = -I${DESTDIR}/usr/include/machine \
    -I${.CURDIR}/arch/${CRYPTO_MACHINE_CPU}
.include "../../sha.inc"
