.include "bsd.own.mk"

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

regen:
	perl ${OPENSSLSRC}/crypto/alphacpuid.pl > alphacpuid.S
	for i in ${OPENSSLSRC}/crypto/bn/asm/alpha-mont.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-alpha.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghash-alpha.pl; do \
		perl $$i > $$(basename $$i .pl).S; \
	done
