.PATH.S: ${.PARSEDIR}
.PATH.c: ${OPENSSLSRC}/crypto/bn/asm
BN_SRCS = x86_64-gf2m.S x86_64-mont.S x86_64-mont5.S rsaz-x86_64.S rsaz-avx2.S x86_64-gcc.c
.include "../../bn.inc"
