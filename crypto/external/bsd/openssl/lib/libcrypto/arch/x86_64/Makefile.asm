.include "bsd.own.mk"

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

.if make(regen) && ${HAVE_LLVM:U} == "yes"
CC+= -fno-integrated-as
.endif

regen:
	(set +x; echo "#include <machine/asm.h>"; CC=${CC:Q} perl \
	    ${OPENSSLSRC}/crypto/x86_64cpuid.pl elf | sed -e \
	    's/call	OPENSSL_cpuid_setup/call	PIC_PLT(OPENSSL_cpuid_setup)/' \
	) > x86_64cpuid.S
	for i in ${OPENSSLSRC}/crypto/md5/asm/md5-x86_64.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-x86_64.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha512-x86_64.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-mb-x86_64.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha256-mb-x86_64.pl \
		 ${OPENSSLSRC}/crypto/whrlpool/asm/wp-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aes-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/vpaes-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/bsaes-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesni-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesni-sha1-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesni-sha256-x86_64.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesni-mb-x86_64.pl \
		 ${OPENSSLSRC}/crypto/rc4/asm/rc4-x86_64.pl \
		 ${OPENSSLSRC}/crypto/rc4/asm/rc4-md5-x86_64.pl \
		 ${OPENSSLSRC}/crypto/camellia/asm/cmll-x86_64.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghash-x86_64.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/aesni-gcm-x86_64.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/x86_64-mont.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/x86_64-mont5.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/x86_64-gf2m.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/rsaz-x86_64.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/rsaz-avx2.pl \
		 ${OPENSSLSRC}/crypto/ec/asm/ecp_nistz256-x86_64.pl; do \
		(set +x; echo "#include <machine/asm.h>"; CC=${CC:Q} perl $$i elf) > $$(basename $$i .pl).S; \
	done
