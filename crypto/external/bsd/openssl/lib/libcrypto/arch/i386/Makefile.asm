.include "bsd.own.mk"

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

regen:
	perl -I${OPENSSLSRC}/crypto/perlasm -I${OPENSSLSRC}/crypto/bn/asm \
	    ${OPENSSLSRC}/crypto/x86cpuid.pl elf -fPIC -DOPENSSL_IA32_SSE2 | \
	    sed -e 's,^\.file.*$$,#include <machine/asm.h>,' \
	        -e 's/	call	OPENSSL_cpuid_setup/	PIC_PROLOGUE!	call	PIC_PLT(OPENSSL_cpuid_setup)!	PIC_EPILOGUE/' | \
	    tr '!' '\n' \
	> x86cpuid.S;
	for i in ${OPENSSLSRC}/crypto/md5/asm/md5-586.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-586.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha256-586.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha512-586.pl \
		 ${OPENSSLSRC}/crypto/ripemd/asm/rmd-586.pl \
		 ${OPENSSLSRC}/crypto/whrlpool/asm/wp-mmx.pl \
		 ${OPENSSLSRC}/crypto/des/asm/des-586.pl \
		 ${OPENSSLSRC}/crypto/des/asm/crypt586.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aes-586.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/vpaes-x86.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesni-x86.pl \
		 ${OPENSSLSRC}/crypto/rc4/asm/rc4-586.pl \
		 ${OPENSSLSRC}/crypto/rc5/asm/rc5-586.pl \
		 ${OPENSSLSRC}/crypto/bf/asm/bf-586.pl \
		 ${OPENSSLSRC}/crypto/cast/asm/cast-586.pl \
		 ${OPENSSLSRC}/crypto/camellia/asm/cmll-x86.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghash-x86.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/bn-586.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/co-586.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/x86-mont.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/x86-gf2m.pl; do \
		perl -I${OPENSSLSRC}/crypto/perlasm -I${OPENSSLSRC}/crypto/bn/asm \
		    $$i elf -fPIC -DOPENSSL_IA32_SSE2 | \
		    sed -e 's,^\.file.*$$,#include <machine/asm.h>,' \
		> $$(basename $$i .pl).S; \
	done
