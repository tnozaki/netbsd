.include <bsd.own.mk>

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

BITS?=	32
XLATE=	perl ${OPENSSLSRC}/crypto/perlasm/ppc-xlate.pl linux${BITS}
SUBST=	sed -e 's/bc	25,0,/bdnz+	/' \
	    -e 's/bclr	14,2/beqlr/' \
	    -e 's/bclr	6,2/bnelr/' \
	    -e 's/bclr	14,0/bltlr/' \
	    -e 's/bclr	12,0/bltlr/'

regen:
	perl ${OPENSSLSRC}/crypto/bn/asm/ppc.pl ${BITS} | ${XLATE} | \
	    ${SUBST} | cat -s > bn-ppc.S
	for i in ${OPENSSLSRC}/crypto/aes/asm/aes-ppc.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesp8-ppc.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/vpaes-ppc.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/ppc-mont.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/ppc64-mont.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghashp8-ppc.pl \
		 ${OPENSSLSRC}/crypto/ppccpuid.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-ppc.pl; do \
		perl $$i ${BITS} | ${XLATE} | \
		    ${SUBST} > $$(basename $$i .pl).S; \
	done
	for i in sha256-ppc sha512-ppc; do \
		perl ${OPENSSLSRC}/crypto/sha/asm/sha512-ppc.pl ${BITS} $$i.tmp; \
		${SUBST} < $$i.tmp > $$i.S; \
		rm -f $$i.tmp; \
	done
	for i in sha256p8-ppc sha512p8-ppc; do \
		perl ${OPENSSLSRC}/crypto/sha/asm/sha512p8-ppc.pl ${BITS} $$i.tmp; \
		${SUBST} < $$i.tmp > $$i.S; \
		rm -f $$i.tmp; \
	done
