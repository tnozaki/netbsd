.include "bsd.own.mk"

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

regen:
	perl -pe \
		's/^(([[:space:]]*\#[[:space:]]*include[[:space:]]*)\
		(["<])arm_arch\.h([">]).*)$$/$$1\n$$2$$3arm_asm.h$$4/x;\
		s/^([[:space:]]*)bx[[:space:]]+lr/$$1RET/;\
		s/^([[:space:]]*)\.byte([[:space:]]+)\
		0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
		0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
		0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
		(0x[[:alnum:]]{2})([[:space:]]*@.*)?$$\
		/$$1.inst$$2$$6$$5$$4$$3$$7/x' \
	${OPENSSLSRC}/crypto/armv4cpuid.S >armv4cpuid.S
	for i in ${OPENSSLSRC}/crypto/aes/asm/aes-armv4.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aesv8-armx.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/bsaes-armv7.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/armv4-gf2m.pl \
		 ${OPENSSLSRC}/crypto/bn/asm/armv4-mont.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghash-armv4.pl \
		 ${OPENSSLSRC}/crypto/modes/asm/ghashv8-armx.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-armv4-large.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha256-armv4.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha512-armv4.pl; do \
		perl $$i | perl -pe \
			's/^(([[:space:]]*\#[[:space:]]*include[[:space:]]*)\
			(["<])arm_arch\.h([">]).*)$$/$$1\n$$2$$3arm_asm.h$$4/x;\
			s/^([[:space:]]*)bx[[:space:]]+lr/$$1RET/;\
			s/^([[:space:]]*)\.byte([[:space:]]+)\
			0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
			0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
			0x([[:alnum:]]{2})[[:space:]]*,[[:space:]]*\
			(0x[[:alnum:]]{2})([[:space:]]*@.*)?$$\
			/$$1.inst$$2$$6$$5$$4$$3$$7/x' \
		>$$(basename $$i .pl).S; \
	done
