.if !empty(MACHINE_ARCH:Mearmv4*) || ${MACHINE_ARCH} == "armeb"
CPPFLAGS+=	-D__ARM_ARCH__=4
.elif ${MACHINE_ARCH} == "earmeb" || ${MACHINE_ARCH} == "earmhfeb"
CPPFLAGS+=	-D__ARM_ARCH__=5
.elif !empty(MACHINE_ARCH:Mearmv6*eb)
CPPFLAGS+=	-D__ARM_ARCH__=6
.else
CPPFLAGS+=	-D__ARM_ARCH__=8
.endif

CPPFLAGS+=	-DOPENSSL_BN_ASM_MONT \
		-DOPENSSL_BN_ASM_GF2m \
		-DAES_ASM \
		-DBSAES_ASM \
		-DGHASH_ASM
.if ${MACHINE_ARCH:M*armv4*} == ""
CPPFLAGS+=	-DSHA1_ASM \
		-DSHA256_ASM \
		-DSHA512_ASM
.endif
