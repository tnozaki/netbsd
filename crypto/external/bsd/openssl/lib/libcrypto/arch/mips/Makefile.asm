.include <bsd.own.mk>

CRYPTODIST=${NETBSDSRCDIR}/crypto
.include "${NETBSDSRCDIR}/crypto/Makefile.openssl"

PERLASM?=	o32
regen:
	perl ${OPENSSLSRC}/crypto/bn/asm/mips.pl ${PERLASM} > bn-mips.S
	for i in ${OPENSSLSRC}/crypto/bn/asm/mips-mont.pl \
		 ${OPENSSLSRC}/crypto/aes/asm/aes-mips.pl \
		 ${OPENSSLSRC}/crypto/sha/asm/sha1-mips.pl; do \
		perl $$i ${PERLASM} > $$(basename $$i .pl).S; \
	done
	for i in sha256-mips sha512-mips; do \
		perl ${OPENSSLSRC}/crypto/sha/asm/sha512-mips.pl ${PERLASM} $$i.S; \
	done
