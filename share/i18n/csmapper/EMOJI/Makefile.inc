.PATH: ${.CURDIR}/EMOJI

SRCS_mapper.dir+=	mapper.dir.EMOJI
SRCS_charset.pivot+=	charset.pivot.EMOJI
CLEANFILES+=		mapper.dir.EMOJI charset.pivot.EMOJI
MAPS_EMOJI=		CP932UDA@DoCoMo%UCS@BMP.mps \
			CP932UDA@DoCoMo%UCS@SMP.mps \
			UCS@BMP%CP932UDA@DoCoMo.mps \
			UCS@SMP%CP932UDA@DoCoMo.mps \
			CP932UDA@KDDI%UCS@BMP.mps \
			CP932UDA@KDDI%UCS@SMP.mps \
			UCS@BMP%CP932UDA@KDDI.mps \
			UCS@SMP%CP932UDA@KDDI.mps \
			SoftBank-Page1%UCS@BMP.mps \
			SoftBank-Page1%UCS@SMP.mps \
			SoftBank-Page2%UCS@BMP.mps \
			SoftBank-Page2%UCS@SMP.mps \
			SoftBank-Page3%UCS@BMP.mps \
			SoftBank-Page3%UCS@SMP.mps \
			SoftBank-Page4%UCS@BMP.mps \
			SoftBank-Page4%UCS@SMP.mps \
			SoftBank-Page5%UCS@BMP.mps \
			SoftBank-Page5%UCS@SMP.mps \
			SoftBank-Page6%UCS@BMP.mps \
			SoftBank-Page6%UCS@SMP.mps \
			UCS@BMP%SoftBank-Page1.mps \
			UCS@SMP%SoftBank-Page1.mps \
			UCS@BMP%SoftBank-Page2.mps \
			UCS@SMP%SoftBank-Page2.mps \
			UCS@BMP%SoftBank-Page3.mps \
			UCS@SMP%SoftBank-Page3.mps \
			UCS@BMP%SoftBank-Page4.mps \
			UCS@SMP%SoftBank-Page4.mps \
			UCS@BMP%SoftBank-Page5.mps \
			UCS@SMP%SoftBank-Page5.mps \
			UCS@BMP%SoftBank-Page6.mps \
			UCS@SMP%SoftBank-Page6.mps

mapper.dir.EMOJI: ${.CURDIR}/EMOJI/mapper.dir.EMOJI.src
	${_MKTARGET_CREATE}
	(echo "# EMOJI" ; cat ${.ALLSRC} ; echo ) > ${.TARGET}

charset.pivot.EMOJI: ${.CURDIR}/EMOJI/charset.pivot.EMOJI.src
	${_MKTARGET_CREATE}
	(echo "# EMOJI" ; cat ${.ALLSRC} ; echo ) > ${.TARGET}

FILES+= ${MAPS_EMOJI}
CLEANFILES+= ${MAPS_EMOJI}
.for i in ${MAPS_EMOJI}
FILESDIR_$i= ${BINDIR}/EMOJI
.endfor

