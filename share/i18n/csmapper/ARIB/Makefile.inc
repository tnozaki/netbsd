# $NetBSD$

.PATH: ${.CURDIR}/ARIB

SRCS_mapper.dir+=	mapper.dir.ARIB
SRCS_charset.pivot+=	charset.pivot.ARIB
CLEANFILES+=		mapper.dir.ARIB charset.pivot.ARIB
MAPS_ARIB=		ISO646-JP@ARIB-STD-B24%UCS.646 \
			JISX0208VDC@ARIB-STD-B5%UCS@SMP.mps \
			JISX0208VDC@ARIB-STD-B5%UCS@BMP.mps \
			HIRAGANA@ARIB-STD-B24%UCS.mps \
			KATAKANA@ARIB-STD-B24%UCS.mps \
			UCS@BMP%JISX0208VDC@ARIB-STD-B5.mps \
			UCS@SMP%JISX0208VDC@ARIB-STD-B5.mps \
			UCS%HIRAGANA@ARIB-STD-B24.mps \
			UCS%KATAKANA@ARIB-STD-B24.mps

mapper.dir.ARIB: ${.CURDIR}/ARIB/mapper.dir.ARIB.src
	${_MKTARGET_CREATE}
	(echo "# ARIB" ; cat ${.ALLSRC} ; echo ) > ${.TARGET}

charset.pivot.ARIB: ${.CURDIR}/ARIB/charset.pivot.ARIB.src
	${_MKTARGET_CREATE}
	(echo "# ARIB" ; cat ${.ALLSRC} ; echo ) > ${.TARGET}

FILES+= ${MAPS_ARIB}
CLEANFILES+= ${MAPS_ARIB}
.for i in ${MAPS_ARIB}
FILESDIR_$i= ${BINDIR}/ARIB
.endfor

