# $NetBSD: Makefile.inc,v 1.5 2008/10/25 22:27:36 apb Exp $

.PATH: ${.CURDIR}/UTF

CODE:=		UTF
.include "${.CURDIR}/Makefile.part"

UTF-16-mod=	UTF16
UTF-16-var=	UTF16
UTF-16BE-mod=	UTF16
UTF-16BE-var=	UTF16BE
UTF-16LE-mod=	UTF16
UTF-16LE-var=	UTF16LE
UTF-32-mod=	UTF32
UTF-32-var=	UTF32
UTF-32BE-mod=	UTF32
UTF-32BE-var=	UTF32BE
UTF-32LE-mod=	UTF32
UTF-32LE-var=	UTF32LE
UTF-8-mod=	UTF8
UTF-8-var=	utf8
UTF-7-mod=	UTF7
UTF-7-var=	utf7
UTF-5-mod=	UTF5
UTF-5-var=	utf5

.for i in ${UTF_PART}
UTF-$i.src: UTF.src
	${_MKTARGET_CREATE}
	${TOOL_SED} \
		-e 's/UTF-x/UTF-$i/' \
		-e 's/UTF-mod/${UTF-$i-mod}/' \
		-e 's/UTF-var/${UTF-$i-var}/' \
		$> > $@
CLEANFILES+= UTF-$i.src
.endfor
